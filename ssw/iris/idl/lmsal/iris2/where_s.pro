function where_s, cadena, patron

dim=strlen(cadena)
out=intarr(dim)
j=0

while j le dim do begin
    if strmid(cadena,j,strlen(patron)) eq patron then out(j)=1
    j=j+1
end
;for j=0,dim-1 do print, strmid(cadena,j,1)+' '+string(out(j)), j
return, where(out eq 1)

end

