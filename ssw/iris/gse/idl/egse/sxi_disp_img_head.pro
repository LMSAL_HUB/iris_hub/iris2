pro sxi_disp_img_head, sttim, entim, ff_in=ff_in, out=out, hout=h, $
		hc=hc, ff_ref=ff_ref, code=code, win=win, outfil=outfil, $
		files=files, help=help
;+
;NAME:
;	sxi_disp_img_head
;PURPOSE:
;	Dump the FITS image headers
;OPTIONS:
;	0 = General
;	1 = Attitude control and CCD
;	2 = Sequences
;	3 = Image processing & shutter
;OUTPUT:
;	files	- The filenames read
;	out	- The string array results listing
;HISTORY:
;V1.00	Written 24-Oct-00 by M.Morrison
;V1.01	25-Oct-00 (MDM) - Removed temperature
;			- Added LOBT of shutter start
;			- Added sec2isp
;			- Added SEQID 
;			- Created codes 1 & 2
;V1.02	26-Oct-00 (MDM) - Added MIN_CNT, FRMLSTID & MIN_MODE
;V1.03	18-Dec-00 (MDM) - Added SEC2MIN for code 2
;V1.04	 3-Jan-01 (MDM) - Added code 4
;V1.05	 4-Jan-01 (MDM) - Added MIN_CNT to code 4
;V1.06	19-Jan-01 (MDM) - Added FDB to code 4
;V1.07	29-Mar-01 (MDM) - Added code 5
;V1.08	29-Mar-01 (MDM) - Added SHUTMODE to code 3
;			- Added /HELP option
;-
;
if (n_elements(ff_in) ne 0) then ff=ff_in
;
if (n_elements(win) eq 0) then win = 2	;last 2 hrs
if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
if (n_elements(entim) eq 0) then entim = ut_time()
;
if (keyword_set(help)) then begin
     print, 'Code 0 = General / FW
     print, 'Code 1 = Attitude control and CCD
     print, 'Code 2 = Sequence
     print, 'Code 3 = Image processing & shutter
     print, 'Code 4 = Sequence events
     print, 'Code 5 = FW/Shutter/Image stats
     return
end
;
if (n_elements(ff) eq 0) then begin
    print, 'Looking for all images between ' + fmt_tim(sttim) + ' and ' + fmt_tim(entim)
    ff = sxi_sel_images(sttim, entim, nimg=10000)
end
files = ff
;
if (ff(0) eq '') then begin
    print, 'SXI_DISP_IMG_HEAD: No input files found/specified'
    return
end
;
if (keyword_set(ff_ref)) then mreadfits, ff_ref, h_ref
mreadfits, ff, h, strtemplate=h_ref
hout = h
;
out = ['# SXI_DISP_IMG_HEAD  Ver 1.08  Program Run: ' + !stime, $
        '# ', $
        '#']
;
break_file, ff, dsk_log, dir00, filnam, ext
;
if (n_elements(code) eq 0) then code = 0
;
if (code eq 0) then begin	;General
    list = ['dateobs', 'timeobs', 'IMG_SN', 'SHUTMODE', 'EXPOSURE', 'FRMPRIOR', 'F1FILTER', 'F2FILTER', 'FDB']
    tit0 = '#   File         EGSE Date  EGSETime      Frm#  ExpType     Expos   D/L P  Filt1  Filt2     FDB
           ;20001026_023850  26-OCT-00  02:38:50         0  Normal      3.0000  PRIME  Open  12uAl  0x00000200
end
if (code eq 1) then begin	;Attitude control & CCD
    list = ['dateobs', 'timeobs', 'IMG_SN', 'EXPOSURE', 'SHUTMODE', 'AC_TYPE', 'T_CCDS1', 'CAMAMP', 'SERCLK', 'PARCLK']
    tit0 = '#   File         EGSEDate   EGSETime      Frm#     Expos  ExpType   AttCorr    CCD_T  Amp   SerClk ParClk
           ;20001026_040304  26-OCT-00  04:03:04        24      0.5000    Dark  None     26.6768  South  South  West
end
if (code eq 2) then begin	;sequence checkout
    list = ['dateobs', 'timeobs', 'SEXPLOBT', 'IMG_SN', 'MIN_CNT', 'FRMPRIOR', 'SEQID', 'FRMLSTID', 'FDB', 'SEC2ISP','SEC2MIN', 'MIN_MODE']
    tit0 = '#   File         EGSE Date  EGSETime    LOBT Shutter Date/Tim    Frm#     Minute  D/L P    SeqID    Framelist   FDB        Sec2ISP     MinMode
           ;20001027_045715  27-OCT-00  04:57:15  27-OCT-00  04:57:13.875        37        11  PRIME         0         2  0x00041a11     22.5625       ISP
end
if (code eq 3) then begin	;
    list = ['SEXPLOBT', 'IMG_SN', 'NAXIS1', 'NAXIS2', 'BINNING', 'EXT_COL', 'EXT_LIN', 'EXPOSURE', 'SHUTMDUR', 'SHUTMODE']
    format=str2arr('a,i6,i4,i4,a3,i6,i6,f9.3,f9.3,a')
    tit0 = '#   File         LOBT Shutter Date/Tim    Frm#    #Col  #Lin  Bin   ExtC     ExtL    Exposure  ShutMDur  ShtMod
           ;20001031_220156  31-OCT-00  22:01:22.023       7   528   588  1x1       0   65528     30.000      0.000
end
if (code eq 4) then begin	;
    list = ['SEXPLOBT', 'MIN_CNT', 'IMG_SN', 'SEC2MIN', 'SEQ_EV', 'SEQ_EVST', 'FRMPRIOR', 'FDB', 'CIS_FILE', 'CIS_IMG']
    tit0 = '#   File         LOBT Shutter Date/Tim      MinCount    Frm#     SEC2MIN   SeqEvn  EvnSt   D/L P   FDB         CIS_File      Img#
           ;20010120_032330  20-JAN-01  03:23:23.750        73        41     47.0859  SPARE3  0xc000  2NDRY  0x610b03ff  seq_detect        16
end
if (code eq 5) then begin	;
    list = ['IMG_SN', 'F1FILTER', 'F2FILTER', 'EXPOSURE', 'SHUTMODE', 'SHUTENC', 'SHUTSTAT', 'IMG_AVG', 'IMG_DEV', 'IMG_MIN', 'IMG_MAX', 'CIS_FILE', 'CIS_IMG']
    tit0 = '#   File            Frm#        FW1     FW2       Expos    ExpTyp ShEn  ShSt   Img_Avg      Img_Dev     Img_Min   Img_Max   CIS_File      Img#
           ;20010328_004439        12       Open    Open      0.0400    Dark  0x20  0x01   3109.3600    435.9280       370      3859    sun_leak        12

end
if (n_elements(list) eq 0) then return
;
out0 = filnam + '  ' + get_infox(h, list, format=format)

out = [out, tit0, '#', out0]
;

prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, /land, nodelete=keyword_set(outfil)
end