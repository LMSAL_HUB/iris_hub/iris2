function ssw_extra2struct, _extra=_extra
return,_extra
end

function ssw_aia_gridfiles, t0, t1, waves=waves, $
   low_grid=low_grid, high_grid=high_grid, sec_grid=sec_grid, $
   array=array, $
   no_grid=no_grid, search_array=search_array, debug=debug, level=level,_extra=_extra
; 
;+
;   Name: ssw_aia_gridfiles
;
;   Purpose: return gridded file lists (only IMG_TYPE=LIGHT are considered)
;
;   Input Paramters:
;      t0, t1 - desired time range
;   
;   Output:
;      returns structure {t0:t0, t1:t1, wl<1>:list [,wl<2>:list....]}
;
;   Keyword Parameters:
;      waves - comma delimited list of one or more AIA waves of interest
;      low_grid (switch) - grid to low cadence W (There Will Be Gaps) 
;      high_grid (switch) - grid to high cadence W (There Will Be Duplicates)
;      sec_grid (switch) - grid to this CADENCE in SECONDS
;      no_grid (switch) - no grid, full lists for all waves, # different
;      array (switch) - if set, file lists are arrays, not comma delimited
;                       lists (this probably should have been, and maybe 
;                       become, the default (with a /noarray keyword)
;
;   History:
;      30-mar-2010 - S.L.Freeland - 1st cut at Ralph request;
;       4-apr-2010 - S.L.Freeeland - add /ARRAY 
;
;   Method: sdo_time2files +  usual suspects
;-
debug=keyword_set(debug)
case n_params() of
   0: begin
         box_message,'Gotta give me a time or time range....'
         return,-1
   endcase
   1: t1=reltime(t0,/hours) ; one hour from t0 is default
   else:
endcase

t0x=anytim(t0,/ecs)
t1x=anytim(t1,/ecs)
 
if n_elements(waves) eq 0 then waves='131,171,304' ; default??

if n_elements(waves) gt 1 then wavesx=waves else $
   wavesx=strtrim(str2arr(waves,/nomult),2)
swavesx=strtrim(wavesx,2)

estring='retval=ssw_extra2struct(' +  $
   't0=t0x,t1=t1x,xfiles=0l,' + $
   arr2str('wlist'+wavesx+ '=""') + ')'
estat=execute(estring)

; 1st pass - get all LIGHT per wave

nw=n_elements(wavesx)
nwave=lonarr(nw)
wtags='WLIST'+swavesx
for i=0,nw-1 do begin 
   box_message,'Listing wave= ' + swavesx(i)
   listx=sdo_time2files(t0x,t1x,/aia,level=level,waves=wavesx(i))
   if listx(0) ne '' then begin 
      mreadfits_shm,listx,iix,only_tags='img_type,date-obs'
      ssl=where(iix.img_type eq 'LIGHT',lcount)
      nwave(i)=lcount
      if lcount gt 0 then begin 
          retval.(tag_index(retval,wtags(i)))= $
          arr2str(listx(ssl))
      endif
   endif
endfor

lg=(where(nwave eq min(nwave)))(0)
hg=(where(nwave eq max(nwave)))(0)
case 1 of
   keyword_set(nogrid): 
   keyword_set(low_grid): tg=$
      file2time(str2arr(retval.(tag_index(retval,wtags(lg)))),out='int')
   keyword_set(high_grid): tg=$
      file2time(str2arr(retval.(tag_index(retval,wtags(hg)))),out='int')
   keyword_set(sec_grid): tg=$
      anytim(timegrid(t0x,t1x,seconds=sec_grid),out='int')
   else: tg=$
      file2time(str2arr(retval.(tag_index(retval,wtags(lg)))),out='int')
endcase

if n_elements(tg) gt 0 then begin
    for i=0,n_elements(wtags)-1 do begin 
       tx=str2arr(gt_tagval(retval,wtags(i)))
       if tx(0) ne '' then begin 
           xtime=file2time(tx,out='int')
           ss=tim2dset(xtime,tg) ; times -> desired grid
           retval.(tag_index(retval,wtags(i)))=arr2str(tx(ss))
       endif else box_message,'No files for wtags(i)
    endfor
endif

retval.xfiles=n_elements(tg)
if keyword_set(array) and retval.xfiles gt 0 then begin 
   
   wlstring=arr2str('wlist'+wavesx+'=str2arr(retval.wlist'+wavesx+')')
   estring='retval=ssw_extra2struct(t0=t0x,t1=t1x,xfiles=retval.xfiles,' + $ 
      wlstring+')' 
   estat=execute(estring) 
endif
if debug then stop,'retval'

return, retval

end
