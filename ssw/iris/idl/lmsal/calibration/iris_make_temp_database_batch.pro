;+
;
; SSW Batch file for generating IRIS temperature database
;
;-

;+
;
; SSW Batch file for generating IRIS quaternion database
;  3/20/19 RPT adding another batch 
;-

timenow = TIME2FILE(RELTIME(/now), /sec)

SET_LOGENV,'GSE_PKT_FILE_DIR', '/irisa/moc/hk_archive-in/packets/vc2/'
case 1 of
	FILE_EXIST('/irisa/data/prep/aux/')	:	finalpath = '/irisa/data/prep/aux/'
	FILE_EXIST('/net/brooklyn/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/brooklyn/Volumes/earth/iris/data/prep/aux/'
	FILE_EXIST('/net/bay/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/bay/Volumes/earth/iris/data/prep/aux/'
	else	:	begin
		PRINT, 'Cannot reach irisa...'
		finalpath = '/irisa/data/prep/aux/'
	end
endcase
logpath = CONCAT_DIR(CONCAT_DIR('log', STRMID(timenow, 0, 4)), STRMID(timenow, 4, 2))
logpath = CONCAT_DIR(finalpath, logpath)
finalpath = CONCAT_DIR(finalpath, 'temp')

case 1 of
	FILE_EXIST('/sanhome/boerner/public_html/iris/temps')	:	pbpath = '/sanhome/boerner/public_html/iris/temps'
	else	:	begin
		PRINT, 'Cannot reach sanhome public_html...'
		pbpath = '~/iris/cronlog/'
	end
endcase

OPENW, lun, /get, CONCAT_DIR(logpath, timenow + '_temp.txt')
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TEMP_DATABASE_BATCH running... ', timenow
PRINTF, lun

;Generate data for right now.

PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for hours = 0'
PRINTF, lun, '*************************************
daynow = RELTIME(hours = 0)
IRIS_MAKE_TEMP_DATABASE, daynow, outpath = '/tmp', fname = fname, /short
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
SPAWN, 'grep 9999 ' + oldfile + ' | wc -l', ofsize, oferr
SPAWN, 'grep 9999 ' + newfile + ' | wc -l', nfsize, nferr
PRINTF, lun, 'Old file: ', 1440-FIX(ofsize), 'New file: ', 1440-FIX(nfsize), '(good lines)', $
	form = '(a15,i8,a15,i8,a15)'
;if ofsize lt nfsize then begin
;	PRINTF, lun, 'Keeping old file...'
;	SPAWN, 'rm ' + newfile, res, err
;	PRINTF, lun, 'rm result: ', res
;	PRINTF, lun, 'rm errcode: ', err
;endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
;endelse






; Generate data for the previous day
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for hours=-7'
PRINTF, lun, '*************************************
daynow = RELTIME(hours = -7)
IRIS_MAKE_TEMP_DATABASE, daynow, outpath = '/tmp', fname = fname, /short
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
SPAWN, 'grep 9999 ' + oldfile + ' | wc -l', ofsize, oferr
SPAWN, 'grep 9999 ' + newfile + ' | wc -l', nfsize, nferr
PRINTF, lun, 'Old file: ', 1440-FIX(ofsize), 'New file: ', 1440-FIX(nfsize), '(good lines)', $
	form = '(a15,i8,a15,i8,a15)'
;if ofsize lt nfsize then begin
;	PRINTF, lun, 'Keeping old file...'
;	SPAWN, 'rm ' + newfile, res, err
;	PRINTF, lun, 'rm result: ', res
;	PRINTF, lun, 'rm errcode: ', err
;endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
;endelse


; Once per day at 10 pm, go back and generate 2-day old and 30-day old files
if STRMID(SYSTIME(), 11, 2) eq '22' then begin

	; Generate data for 2 days ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for hours=-48'
	PRINTF, lun, '*************************************
	daynow = RELTIME(hours = -48)
	IRIS_MAKE_TEMP_DATABASE, daynow, outpath = '/tmp', fname = fname, /short
	newfile = CONCAT_DIR('/tmp', fname)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	SPAWN, 'grep 9999 ' + oldfile + ' | wc -l', ofsize, oferr
	SPAWN, 'grep 9999 ' + newfile + ' | wc -l', nfsize, nferr
	PRINTF, lun, 'Old file: ', 1440-FIX(ofsize), 'New file: ', 1440-FIX(nfsize), '(good lines)', $
		form = '(a15,i8,a15,i8,a15)'
;	if ofsize lt nfsize then begin
;		PRINTF, lun, 'Keeping old file...'
;		SPAWN, 'rm ' + newfile, res, err
;		PRINTF, lun, 'rm result: ', res
;		PRINTF, lun, 'rm errcode: ', err
;	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
;	endelse

	; Generate data for one month ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for days=-30'
	PRINTF, lun, '*************************************
	daynow = RELTIME(days = -30)
	IRIS_MAKE_TEMP_DATABASE, daynow, outpath = '/tmp', fname = fname, /short
	newfile = CONCAT_DIR('/tmp', fname)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	SPAWN, 'grep 9999 ' + oldfile + ' | wc -l', ofsize, oferr
	SPAWN, 'grep 9999 ' + newfile + ' | wc -l', nfsize, nferr
	PRINTF, lun, 'Old file: ', 1440-FIX(ofsize), 'New file: ', 1440-FIX(nfsize), '(good lines)', $
		form = '(a15,i8,a15,i8,a15)'
;	if ofsize gt nfsize then begin
;		PRINTF, lun, 'Keeping old file...'
;		SPAWN, 'rm ' + newfile, res, err
;		PRINTF, lun, 'rm result: ', res
;		PRINTF, lun, 'rm errcode: ', err
;	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
;	endelse

endif

PRINT, 'Updating temperature database for ISP2SOLAR...'
IRIS_UPDATE_TEMP_POINTDB

timeend = TIME2FILE(RELTIME(/now), /sec)
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TEMP_DATABASE_BATCH done... ', timeend
PRINTF, lun

end
