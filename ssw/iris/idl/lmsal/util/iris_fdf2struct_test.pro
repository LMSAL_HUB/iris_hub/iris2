pro iris_fdf2struct_test, fdf, fdt, _extra=_extra, first=first, last=last, append_file=append_file
;
;+
;   Name: iris_fdf2struct_test
;
;   Purpose: verify some iris_fdf2struct.pro stuff
;
;   Input Paramters:
;      NONE
;
;   Output Parameters:
;      fdf - fdf structure image of last FDF file conversion
;      fdt - fdf structure image of last FDT (team) conversion
;   
;   Keyword_paramters:
;      _EXTRA - optional pattern(s) - for selection of TYPE(s) for example - /view_10deg, /aaz , /events ... etc (keyword inherit)
;      last - if set, choose Last file for type list (default is file[0])
;      append_file - if set, append FILENAME:<filename> to original struct
;
;   Calling Examples:
;      IDL> iris_fdf2struct_test  ; spins through one example for each TYPE, in & out
;      IDL> iris_fdf2struct_test, fdfv10, fdtv10 , /view_10          ; return structs for the 10degree view files, fdf-in, fdt-out resp.
;      IDL> iris_fdf2struct_test, fdfv10, fdtv10 , /view_10, /append ; same, but append .FILENAME to output structures
;
append_file=keyword_set(append_file)

topdir='/net/leffe/Volumes/disk1/Users/bdp/iris/fdf/comp/'
fdfvars=str2arr('fdf,fdt')
fdffdt=concat_dir(topdir,fdfvars+['-in','-out'])

if data_chk(_extra,/struct) then types=strlowcase(tag_names(_extra)) else $
   types=str2arr('events,eclipse,aaz,view_5deg,view_10deg,star,radzones,ground')

for t=0,n_elements(types)-1 do begin 
   delvarx,fdf,fdt ; start with clean slate
   box_message,types[t]
   pattern='*'+types[t]+'*'
   for i=0,1 do begin 
      fdffile=file_search(fdffdt[i],pattern)
      ssf=([0,n_elements(fdffile)-1])(keyword_set(last))
      fdfx=iris_fdf2struct(fdffile[ssf])
      if append_file then fdfx=add_tag(fdfx,fdffile[ssf],'filename')
      help,fdfx,/str
      estat=execute(fdfvars[i]+'=fdfx')
   endfor
endfor

return
end



