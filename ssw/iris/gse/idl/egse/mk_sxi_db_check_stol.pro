;
;
;HISTORY:
;	Written Nov-99 by M.Morrison
;	25-Oct-00 (MDM) - Changed command count flag from Ex to Cx
;	26-Oct-00 (MDM) - Only flag command count if SXI telecommand is sent
;			- Wait 5 seconds if no command but increment case is specified
;	26-Oct-00 (MDM) - Mod to flag when the measurement is a delta
;	30-Oct-00 (MDM) - Made input file selection a menu option
;
;;if (n_elements(infil) eq 0) then infil='$SAG_TELEM_INFO/sxi_db_check.tab'
;;input, 'Enter file name', infil, infil
;
menu = ['MANUAL', '$SAG_TELEM_INFO/' + ['sxi_db_check5.tab', 'sxi_db_check_cmd5.tab', $
			'sxi_sat_eeprom_write.tab', 'sxi_sat_critical_commands.tab', $
			'sxi_db_check_illegal.tab', 'sxi_sat_mem_copy.tab']]
ii = xmenu_sel(menu, /one)
if (ii ge 1) then infil = menu(ii) $
		else input, 'Enter file name', infil, '$SAG_TELEM_INFO/sxi_db_check.tab'
;
break_file, infil, dsk_log, dir, filnam, ext
filnam = str_replace(filnam, 'sxi_', '')
outfil = concat_dir('/sxisw/cfl/egse_v5.x/test/src', filnam + '.src')
;
if (not file_exist(infil)) then stop, 'No input file: ' + infil
mat = rd_ulin_col(infil, /nohead, nocomment='#')
n = n_elements(mat(0,*))
;
ss = where(mat(1,*) eq '+', nss)	;MDM added 23-Oct-00 (continuation line)
if (nss ne 0) then begin
    print, nss, '  lines that have to be concatenated
    for i=nss-1,0,-1 do begin
	mat(2, ss(i)-1) = mat(2, ss(i)-1) + ' ' + mat(2, ss(i))
    end
    ss2 = rem_elem(indgen(n), ss)
    mat = mat(*,ss2)
    n = n_elements(mat(0,*))
end
;
out = ['program "' + filnam + '.src" "Database check"', $
	'# Created by MK_SXI_DB_CHECK_STOL.PRO  ' + !stime, $
	'# ', $
	'begin', $
	' ', $
	'setstr $GSLOGNAME = "' + filnam + '"', $
	'perf openlog', $
	'setstr $LSTIME = systime 0', $
	'setstr $LSHDR = "STARTED PROCEDURE - ' + filnam + '.src - "', $
	'setstr $LSHDR = $LSHDR + $LSTIME', $
	'write "LFFT_LOG" $LSHDR -n', $
	' ']
;
for i=0,n-1 do begin
    mnem 	= strcompress(mat(0,i))
    mnem2	= strmid(strupcase(mnem)+'                             ', 0, 20)
    typ 	= strcompress(mat(1,i))
    cmd 	= strupcase(strcompress(mat(2,i)))
    cmd = str_replace(cmd, '=0X', '=0x')
    cmd = str_replace(cmd, 'WAIT TIME', 'wait time')
    cmd = str_replace(cmd, 'GPIB', 'gpib')
    imin 	= strcompress(mat(3,i))
    imax 	= strcompress(mat(4,i))
    cmd2 = strtrim( str2arr(cmd, delim=';') )
    ;
    qinc = 0	;telemetry mnemonic verification that it increments
    if (strmid(imin, 0, 1) eq '+') then begin
	qinc = 1
	imin = strmid(imin, 1, 99)
	imax = strmid(imax, 1, 99)
    end
    qstr = (typ eq 'D')
    qhex = (strmid(imin, 0, 2) eq '0x')

    if (qstr) then begin
	simin = string(imin,  format='(a7)')
	simax = string('N/A', format='(a7)')
    end else begin
	imin2 = 0.
	imax2 = 0.
	if (imin ne '.') then imin2 = float(imin)
	if (imax ne '.') then imax2 = float(imax) 
	if (strmid(imin, 0, 2) eq '0x') then reads, imin, imin2, format='(2x, z)'
	if (strmid(imax, 0, 2) eq '0x') then reads, imax, imax2, format='(2x, z)'
	if (typ eq 'A') then begin
	    simin = string( imin2, format='(f7.2)')
	    simax = string( imax2, format='(f7.2)')
	end else begin
	    ;;simin = string( long(imin2), format='(i7)')
	    ;;simax = string( long(imax2), format='(i7)')	  
	    simin = string( imin, format='(a7)')
	    simax = string( imax, format='(a7)')	  
	    if (qinc) then simin = string('+' + strtrim(simin,2), format='(a7)')
	end
    end
    ;
    qprint = (imin ne '') and (imin ne '.') and (imin ne '. .')
    qcmd   = (cmd ne '') and (cmd ne '.')
    qnomax = (imax eq '') or (imax eq '.')
    if (strpos(cmd2(0), 'PERF') ne -1) then cmd2 = strlowcase(cmd2)
    ;
    if (qcmd and (not qprint)) then begin
	if (qcmd) then out = [out, $
			cmd2, $
			'wait time 9']
    end
    ;
    if (qnomax) then simax = '  N/A  '
    ;
    if (qprint) then begin
	out = [out, '#', $
			'# ------------------------------ ' + mnem2, $
			'# ', $
;			'display "Checking ' + mnem2 + '                                " at 0']
			'#']
	;
	case typ of
	    'A': begin & var = '$LR_val' & gcmd = 'getreal' & end
	    'R': begin & var = '$LI_val' & gcmd = 'getint'  & end
	    'D': begin & var = '$LS_val' & gcmd = 'getdig'  & end
	    'W': begin & var = '$LI_val' & gcmd = 'getraw'  & end
	endcase
	;
	;---- If incrementing command ver
	;
	if (qinc) then begin
    	    out = [out, $
		'set ' + var + '00 = ' +  gcmd + ' "' + mnem + '"']
	end
	;
	;---- Command portion
	;
	if (qcmd) then begin
	    out = [out, $
			'set $LI_CMD_CNT0 = getint "SXI_CHTCCOUNT"', $
			cmd2, $
			;'wait time 9', $
			'wait time 5', $
			'set $LI_CMD_CNT1 = getint "SXI_CHTCCOUNT"']
	end else begin
	    if (qinc) then out = [out, 'wait time 5']
	end
        qcmd_check = (strupcase(mnem) ne 'SXI_CHTCERRCNT') and (strupcase(mnem) ne 'SXI_CHTCCOUNT') and (strpos(cmd,'SXI') ne -1)
	out = [out, 'setstr $LS_CMD_ERR = "  "']
	if (qcmd) and (qcmd_check) then begin
	    out = [out, 'set $LI_CMD_CNT0 = $LI_CMD_CNT1 - $LI_CMD_CNT0', $
			'if $LI_CMD_CNT0 != 1 then', $
			'    setstr $LS_CMD_ERR = "C"', $
			'    setstr $LS_TEMP = makestr $LI_CMD_CNT0 "%1.1d"', $
			'    setstr $LS_CMD_ERR = $LS_CMD_ERR + $LS_TEMP', $
			'end if']
	end
	;
	;---- Get telemetry portion
	;
	if (qstr) then out = [out, 'setstr ' + var + ' = ' +  gcmd + ' "' + mnem + '"'] $
		else  out = [out, 'set ' + var + ' = ' +  gcmd + ' "' + mnem + '"']
	;
	if (qinc) then out = [out, $
		'set ' + var + ' = ' + var + " - " + var + '00']
	;
	case typ of
	    'A': out0 = 'setstr $LS_sval = makestr $LR_val "%8.3f"'
	    'R': out0 = 'setstr $LS_sval = makestr $LI_val "%8.4d"'
	    'W': out0 = 'setstr $LS_sval = makestr $LI_val "%8.4d"'
	    'D': out0 = 'setstr $LS_sval = $LS_val
;;	    'D': out0 = 'setstr $LS_sval = makestr $LS_val "%8s"'
	endcase
	if (qhex) then out0 = 'setstr $LS_sval = makestr $LI_val "0x%6.6x"'
	out = [out, out0]
	;
	;---- Perform the check
	;
	out = [out, 'setstr $LS_PassFail = "     OK"']
	if (typ eq 'D') or (qnomax) then begin
	    case typ of
		'D': out = [out, $
			'if ' + var + ' == "' + imin + '" then', $
			'else', $
			'    setstr $LS_PassFail = "   FAIL"', $
			'end if']

		else: out = [out, $
			'if ' + var + ' == ' + imin + ' then', $
			'else', $
			'    setstr $LS_PassFail = "   FAIL"', $
			'end if']
	    endcase
	end else begin
	    out = [out, $
			'if ' + var + ' < ' + imin + ' then', $
			'    setstr $LS_PassFail = "   FAIL"', $
			'end if', $
			'if ' + var + ' > ' + imax + ' then', $
			'    setstr $LS_PassFail = "   FAIL"', $
			'end if']
	end
	    
	;
	;---- output the results
	;
	tmp = strmid( str_replace(cmd, '"', ''), 0, 58)
    	out = [out, $
		'setstr $LS_STAT = "' + mnem2 + ' "', $
		'setstr $LS_STAT = $LS_STAT + $LS_CMD_ERR', $
		'setstr $LS_STAT = $LS_STAT + $LS_PassFail']
	if (qinc) then out = [out, 'setstr $LS_STAT = $LS_STAT + " Dmn=' + simin + ' Dmx='+simax + ' Val="'] $
		  else out = [out, 'setstr $LS_STAT = $LS_STAT + " Min=' + simin + ' Max='+simax + ' Val="']
    	out = [out, $
		'setstr $LS_STAT = $LS_STAT + $LS_sval', $
		;;'setstr $LS_STAT = $LS_STAT + "  ' + strmid(cmd, 0, 10) + '"', $
		'display $LS_STAT at 0', $
		;;'write "LFFT_LOG" $LS_STAT -n']	
		'write "LFFT_LOG" $LS_STAT', $	
		'write "LFFT_LOG" " ' + tmp + '" -n']	
    end
end
;
out = [out, ' ', ' ', ' ', $
	'setstr $LSTIME = systime 0', $
	'setstr $LSFTR = "FINISHED PROCEDURE - ' + filnam + '.src - "', $
	'setstr $LSFTR = $LSFTR + $LSTIME', $
	'write "LFFT_LOG" $LSFTR -n', $
	'write "LFFT_LOG" " " -n', $
	'perf closelog', $
		'end proc']
;
prstr, out, /nomore
prstr, out, file=outfil
print, outfil
end
