pro sdo_rt_emi_monitor, smoot=smoot, infile=infile, lookback=lookback, limits=limits, color=color, $
			qlinestyle=qlinestyle, filter1d=filter1d, statTlm=statTlm, filestat=filestat,ounit=ounit, $
			outstatfile=outstatfile, boutstatfile=boutstatfile, inst=inst, onedialog=onedialog
;+
;	$Id: sdo_rt_emi_monitor.pro 4942 2012-01-13 03:52:02Z sabolish $
;	$Name:  $
;
; 	Name: sdo_rt_emi_monitor
;
;	Purpose: Read the tlm input file, then extract the data from packets and check
;		limits if limit flag is set.
;
;	Parameters:
;		smoot		enable "box car" smoothing function, of size specified
;		infile		file with list of TLM points to extract from packets and plot
;		lookback	time window width in minutes to plot
;		color		enable color plots
;		qlinestyle	enable qlinestyle
;       	filter1d 	Remove bad data by finding where values exceed the
;                 		"smoothed 100" values by more than 3 sigma
;		statTlm		list of raw TLM points to check stats on for noise
;		filestat	switch to output stat checks on raw tlm.
;		ounit		unit number for output stat file
;		inst		SDO instrument name: aia or hmi 
;		onedialog	switch to limit the number of dialog alarms on stats
;		limits		switch to check specified limits from infile
;		
;
;	History: G.Linford, 12 July 2007
;		1-Apr-08, G.Linford, fixed UT time get
;
;	$Log: sdo_rt_emi_monitor.pro,v $
;	Revision 1.5  2008/04/16 21:19:04  linford
;	New default has continous dialog alarms, Added onedialog switch to limit this to one
;	
;	Revision 1.4  2008/04/13 16:40:14  linford
;	disabled 5-sigma alarm switch to color plots
;	
;	Revision 1.3  2008/04/12 17:08:30  linford
;	removed debug, running on old data
;	
;	Revision 1.2  2008/04/11 15:13:49  linford
;	name change for *proc_emi_str/stat routines
;	
;	Revision 1.1  2008/04/10 23:35:33  linford
;	Checkin to support SC EMI-EMC
;	
;
;-
;-- stats common block: mnemonics, min-stddev of dist., n-sigma val for outliers
common aia_tlm_stats, smnem, minsig, nsigout, fdialog, fnsigma

if keyword_set(lookback) then begin
	dt = lookback
endif else begin
	dt = 5				; default time window is 5 min
endelse

if keyword_set(infile) then begin
	tlm = rd_ulin_col(infile, /nohead, nocomment='#')
endif else begin
	;---- DEBUG CHANGE
	infile = '/home/sdotc/idl/'+inst+'_emi_tlm_pts.dat'
	;infile = '/export/home/sdotc/'+inst+'_emi_tlm_pts.dat'	; DEBUG Line for Kuna
	tlm = rd_ulin_col(infile, /nohead, nocomment='#')
endelse

tlm_pts = tlm[0,*]				; extract tlm points
tlm_ll =  float(tlm[1,*])       		; extract low limits
tlm_ul = float(tlm[2,*])        		; extract upper limits

pts = reform(tlm_pts)				; reform into linear array
ll = reform(tlm_ll)
ul = reform(tlm_ul)

	
;
;-- Using ut_time() to pick data read window:
;
sttim = anytim2ints(ut_time(), off= -(dt*60))
entim = anytim2ints(ut_time())

;-- DEBUG setup for fake time passage using tstime:
;sttim = anytim2ints(tstime, off= - (dt*60))
;entim = anytim2ints(tstime)

;-- Read the Selected Data
dat = sag_get_mnem(anytim(sttim,/yohkoh),anytim(entim,/yohkoh), pts)
;dat = sag_get_mnem('21-jul-07 22:50','21-jul-07 22:59', pts)	; Debug- Fixed data set for testing 

;-- debug time span
print, 'Start time of data check: '+anytim(sttim,/yohkoh)
print, 'End time of data check: '+ anytim(entim,/yohkoh)

;
;-- Stat Calculations using a 5 min window:
if keyword_set(statTlm) then begin
	tlm_stat = statTlm[0,*]
	statpts = reform(tlm_stat)
	tlm_ll = float(statTlm[1,*])		; extract low limit stddev
	statll = reform(tlm_ll)
	tlm_ul = float(statTlm[2,*])
	statul = reform(tlm_ul)

;	--- DEBUG CHANGE
	ssttim = anytim2ints(ut_time(), off= -(5*60))	; always a 5min lookback
	sentim = anytim2ints(ut_time())
	sdat = sag_get_mnem(anytim(ssttim,/yohkoh),anytim(sentim,/yohkoh), statpts)
	;sdat = sag_get_mnem(anytim(sttim,/yohkoh),anytim(entim,/yohkoh), statpts)	; DEBUG Line
endif

; --- Check stat data for all structure types
if keyword_set(filestat) then begin
  nsdat_tags = n_tags(sdat)
  sdat_tags = tag_names(sdat)
  for i=0, nsdat_tags-1 do begin
	  ;print,'Processing Data Tag for STATS:' + sdat_tags[i]
	  iStruct = struct_subset(sdat, sdat_tags[i])
	  exestr = 'help,iStruct.'+sdat_tags[i]+',/str'
	  prtmnem = 'print,"Mnemonics for this Tag: ", iStruct.'+sdat_tags[i]+'.mnem'
	  ;res = execute(prtmnem)			; print all mnemonics for this tag
	  ;res = execute(exestr)			; run help on extracted structure
	  sdo_proc_emi_stat, iStruct, color=color, qlinestyle=qlinestyle, $
	  filter1d=filter1d,ounit=ounit,outstatfile=outstatfile,boutstatfile=boutstatfile,$
	  inst=inst, onedialog=onedialog
	  
  endfor	
endif

n = n_elements(pts)

; --- check dat for all structure types!!!!
;All Structures: HKR:2, HMC:1, HPW:16, HSQ:7, SPW:2
; 9 plots into 10 slots
ndat_tags = n_tags(dat)		; get number of tags
dat_tags = tag_names(dat)	; get list of tags
for i=0, ndat_tags-1 do begin
	;print,'Processing Data Tag for Plots:' + dat_tags[i]
	iStruct = struct_subset(dat, dat_tags[i])
	exestr = 'help,iStruct.'+dat_tags[i]+',/str'
	prtmnem = 'print,"Mnemonics for this Tag: ", iStruct.'+dat_tags[i]+'.mnem'
	;res = execute(prtmnem)			; print all mnemonics for this tag
	;res = execute(exestr)			; run help on extracted structure
	if  keyword_set(filestat)  then begin		; n-sigma flag indicating outliers, change color of plots
		if  (fnsigma eq 1) then begin
			sdo_proc_emi_str, iStruct, color=color, qlinestyle=qlinestyle, filter1d=filter1d
			;The follow line removed cause Jake did not want color plots after a 5-sigma alarm
			;sdo_proc_emi_str, iStruct, /color, qlinestyle=qlinestyle, filter1d=filter1d
		endif else begin
			sdo_proc_emi_str, iStruct, color=color, qlinestyle=qlinestyle, filter1d=filter1d
		endelse
	endif else begin
		sdo_proc_emi_str, iStruct, color=color, qlinestyle=qlinestyle, filter1d=filter1d ; plot the extacted tag data
	endelse
endfor

	
end
