

PRO sdo_jsoc_check_status, requestid,  request_output=request_output,  $
                           outdir=outdir, status=status, download=download, $
                           progress=progress


;+
; NAME:
;      SDO_JSOC_CHECK_STATUS
;
; PURPOSE:
;      This routine uses a JSOC requestid and checks to see if the tar
;      file is available and (optionally) downloads and unpacks it.
;
; INPUTS:
;      Requestid:  The request ID returned by JSOC. It has the form
;                  "JSOC_20160406_626". 
;
; OPTIONAL INPUTS:
;      Request_output:  This is the structure returned by
;                       sdo_orderjsoc when a request has been
;                       placed. It contains the requestid and so can
;                       be used instead of the requestid input.
;
;      Outdir:   By default the tar file is unpacked in user's
;                working directory. Setting OUTDIR will send the tar
;                file there. 
;
; KEYWORDS:
;      DOWNLOAD: If set, then the tar file will be downloaded and
;                unpacked to the current working directory or OUTDIR.
;      PROGRESS: If set, then a widget will appear during downloading,
;                showing the progress of the download.
;
; OPTIONAL OUTPUTS:
;      Status:  Returns 1 if the location of the tar file is
;               successfully found, 0 otherwise.
;
; OUTPUTS:
;      Information about the result of the status request is printed
;      to the IDL window. If /download is set, then the data tar file
;      will be downloaded and unpacked.
;
; MODIFICATION HISTORY:
;      Beta 1, 6-Apr-2016
;         Successfully downloaded and unpacked a tar file.
;      Beta 2, 12-Apr-2016
;         Added information message.
;      Beta 3, 12-Apr-2016
;         Added extra information messages.
;      Beta 4, 19-Apr-2016
;         Added /download keyword.
;      Ver.1, 23-May-2016, Peter Young
;         Added /progress keyword.
;-


IF n_elements(requestid) EQ 0 THEN requestid=request_output.requestid

url='http://jsoc.stanford.edu/cgi-bin/ajax/jsoc_fetch?op=exp_status&requestid='+requestid

sock_list,url,page

;
; Need to concatenate the output string for it to be interpreted by
; json_parse. 
;
n=n_elements(page)
single_string=''
FOR i=0,n-1 DO single_string=single_string+page[i]

s=json_parse(single_string,/tostr)

IF n_elements(outdir) NE 0 THEN BEGIN
  chck=file_info(outdir)
  IF chck.directory EQ 0 THEN file_mkdir,outdir
ENDIF 

IF tag_exist(s,'tarfile') THEN BEGIN
  status=1
  IF keyword_set(download) THEN BEGIN 
    download_file='http://jsoc.stanford.edu'+s.tarfile
    print,'% SDO_JSOC_CHECK_STATUS: tar file found. Begin downloading...'
    sock_get,download_file,out_dir=outdir,progress=progress
    print,'% SDO_JSOC_CHECK_STATUS: Downloading completed.'
    file=file_basename(download_file)
    IF n_elements(outdir) NE 0 THEN file=concat_dir(outdir,file)
    chck=file_search(file,count=count)
    IF count NE 0 THEN BEGIN
      file_untar,file,/verbose
      file_delete,file
    ENDIF
  ENDIF ELSE BEGIN
    dir=s.dir
    url='http://jsoc.stanford.edu'+dir
    print,'The data are available at the webpage:'
    print,'   ',url
  ENDELSE 
ENDIF ELSE BEGIN
  status=0
  print,'% SDO_JSOC_CHECK_STATUS: data are not ready yet. Please try later.'
ENDELSE 

END
