pro read_iris_l2_multi_rast,raster,index,data, sdata=sdata, scale=scale, log=log, lable=lable, wavesx=wavesx, nrt=nrt, $
   loud=loud, allindex=allindex, debug=debug, sji_append=sji_append,silent=silent
;
;+
;   Name: read_iris_l2_multi_rast
;
;   Purpose: read one or more waves from a sit and stare or vector of raster files; optionally DN & spatial scale
;
;   Input Parameters:
;      raster - list of one or more local raster files (from single OBSID) -or- a fully qualified obsid like YYYYMMDD_HHMMSS_<obsid>
;
;   Output Parameters:
;      index,data ; concatenated 'index,data' for one or more spectral regions (warning - INDEX is not 1:1 for regions>1 see ALLINDEX 
;                 ; output DATA may have 3 or 4 dimentions - book keeping via ALLINDEX output keyword
;
;   Keyword Parameters:
;      waves - desired wave list -> read_iris_l2  - for example, WAVES='c ii,mg ii,si'
;      nrt (switch) - set this to force LEVEL2_NRT (in conjunction with obsid input)
;
;   History:
;      31-jan-2014 - S.L.Freeland - ssw IRIS raster utility
;-

delvarx,index,data,allindex ; start with a clean slate
debug=keyword_set(debug)
loud=keyword_set(loud)
silent=1-loud or keyword_set(silent)

case 1 of 
   file_exist(raster[0]):  rast=raster
   strlen(obsid) ne 26: begin 
      t0=ssw_round_times(file2time(obsid))
      t1=reltime(t0,/hour)
      obs=last_nelem(str2cols(obsid,'_',/trim))
      rast=iris_time2files(t0,t1,level=2,nrt=nrt,/rast,obsid=obs)
      if rast[0] eq '' then begin 
         box_message,'Cannot find raster file(s) for obsid> ' + obsid
         return
      endif
   endcase
   else: begin 
      box_message,'Expect L2 raster file list -or- fully qualified OBSID: yyyymmdd_hhmmss_<obsid>
      return ; !! EARLY EXIT on unexpected input
   endcase
endcase


if n_elements(wavesx) eq 0 then waves='c ii' else waves=wavesx; default
if n_elements(waves) eq 1 and strpos(waves,',') ne -1 then waves=strtrim(str2arr(waves))

; filter out invalid waves
mreadfits_header,rast[0],l2head
valid=intarr(n_elements(waves)) -1
for w=0,n_elements(waves)-1 do valid[w]=iris_l2_wave2extension(l2head,waves[w])
ssok=where(valid ne -1,vcnt)
case 1 of 
   vcnt eq n_elements(waves): ;  all ok, keep quiet
   vcnt eq 0: begin ; none valid - bail
      box_message,['None of your input waves occur in this file; bailing...',waves]
      return ; !!! EARLY Exit on invalid wavelist
   endcase
   else: begin 
      box_message,['At least one of your WAVES not found: ', waves[where(valid eq -1)]]
      waves=waves[ssok]
      valid=valid[ssok]
   endcase
endcase

rinfo=iris_l2_rastinfo(raster)

rinfo=rinfo[valid-1] ; map valid WAVES -> L2 info array

; presize concatenated array for down-line insertion operation (faster than per window concatenation)
xsize=total(rinfo.naxis1,/cum)
xins =[0,xsize]
bytdata=keyword_set(scale)
float=1-bytdata
data=make_array(last_nelem(xsize),rinfo[0].naxis2,rinfo[0].naxis3,n_elements(raster),float=float,byte=bytdata)

for w=0,n_elements(waves) -1 do begin 
   box_message,'Reading ' + strtrim(n_elements(rast),2) + ' raster files, wave='+waves[w]
   delvarx,ii
   read_iris_l2,rast,ii,dd,wave=waves[w],/silent
   ndim=data_chk(dd,/ndimen)
   if not data_chk(ii,/struct)  then begin 
      box_message,'No extension found for wave=' + waves[w] ; should not happen with added pre-filter...
   endif else begin 
      wname=rinfo[w].line
      if keyword_set(scale) then begin 
         dd=ssw_sigscale(ii,temporary(dd),log=log)
         dd=bytscl(temporary(dd))
      endif
      case 1 of
         ndim eq 3: begin 
            data[xins[w],0,0]=dd
         endcase 
         ndim eq 4: begin
            data[xins[w],0,0,0]=dd
         endcase
         else: begin
            box_message,'not taught this type yet - email Sam with full obsid YYYYMMDD_HHMMSS_<obsid>
            stop
         endcase
      endcase
      sname=str_replace(wname,' ','_')
      if is_number(strmid(sname,0,1)) then sname='W_'+sname
      allindex=add_tag(allindex,ii,sname)
      if debug then stop,'wname,ii,dd
   endelse
endfor

index=allindex.(0)
index=add_tag(index,strcompress(arr2str(rinfo.line)),'wavenames')
index=add_tag(index,strcompress(arr2str(rinfo.wave)),'waves')
index=add_tag(index,strcompress(arr2str(rinfo.wavemin)),'wavemins')
index=add_tag(index,strcompress(arr2str(rinfo.wavemax)),'wavemaxs')
index=add_tag(index,strcompress(arr2str(rinfo.naxis1)),'naxis1s')
index=add_tag(index,strcompress(arr2str(fix(xins[0:n_elements(xins)-2>0]))),'wavepix0')
index=add_tag(index,strcompress(arr2str(fix(xsize)-1)),'wavepixn')


return
end




