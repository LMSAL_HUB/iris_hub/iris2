iris_prep notes

Rotate/flip?
No

Outline of what IRIS_PREP will do
 - Convert to physical units
 - Geometric / wavelength correction
 - Keyword updates
 - Delta flat (/dark/spike)
 - Despike with time series
 - Correct spikes if they aren't already (presumably they're flagged?)
Unprep?
 
Geometric/wavelength correction information in FITS header
Image series despiking
Windows will grow

DESPIKING:
	- We may have a spike list, which we could then apply or not
	- If we have one, we should transform it using poly_2d
	- We could also generate one using an image series
	- How will we get the spike list -- look it up from DRMS? Require it be passed in?
	
WARPING
	- Where will the information come from?
	- How will we document it in the header? (what was done, and a version #)
	- Unprep: how will that be done?
	
SHIFTING
	- Spectral shifts to be applied based on "temperature table"
	- Pointing shifts? 
	
DELTAFLAT:
	- Is that what we want to do, or will the flats be updated in the pipeline?
	- Will delta flats(/darks) be distributed thru SSW, DRMS, ...?
	
PHYSICAL UNITS:
	- How will that information be distributed?
	- How is it documented in the header?
	- Include effective area, or just gain?
