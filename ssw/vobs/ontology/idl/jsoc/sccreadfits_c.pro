FUNCTION SccREADFITS, filename0, headers, NODATA=nodata,HEADER=str_header, EXTHDR=exthdr, $
    	_extra=_extra, LASCO=lasco
;+
; $Id: sccreadfits.pro,v 1.7 2010/02/01 23:17:21 nathan Exp $
;
; Project     : STEREO - SECCHI
;                   
; Name        : SccREADFITS
;               
; Purpose     : Read a SECCHI FITS file to obtain an image and header array, or
;               read a list of SECCHI FITS files to obtain a cube of images
;               and an array of header structures.
;               
; Explanation : This routine calls the SSW routine mreadfits. It uses a 
;		SECCHI-specific header structure
;               
; Use         : IDL> image = SccREADFITS('filename' [,header])
;		IDL> images = SccREADFITS(filenamelist [,headers, /NODATA])
;    
;      A useful sequence is:
;      --------------------------------------------------------------------
;      IDL> junk=sccreadfits( files, hdrs, /nodata)       ; headers only->struct 
;      IDL> ss=where(hdrs.naxis2 eq 512)   		; vector filter
;      IDL> images=sccreadfits(files[ss],hdrs) 		; read desired->3D 
;      --------------------------------------------------------------------
;
; Inputs      : filename	Name of the SECCHI FITS file or array of filenames.
;               
; Outputs     : image		The image array or array of images.
;               
; Opt. Outputs: headers		The image header(s) returned as a(n array of)
;				SECCHI header structure(s)
;               
; Keywords    : /NODATA  	Set this keyword if you only want the header returned.
;           	OUTSIZE = [nx,ny] - 1 or 2 element array specifying
;                                   the output size of data array 
;               HEADER=     	Set this keyword to a varible which will
;                               return the FITS string header. 
;   	    	EXTHDR=	    	Set equal to a variable which will contain an array
;   	    	    	    	of structures, 1 element for each exposure of summed image(s)
;   	    	    	    	Set variable=1 before calling sccreadfits.
;   	    	/LASCO	    	Use lasco header structure
;
;
; Calls       : MREADFITS, MRDFITS
;
; Category    : Data_Handling, I_O
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 2004.04.26
;               
; Modified    :$Log: sccreadfits.pro,v $
; Modified    :Revision 1.7  2010/02/01 23:17:21  nathan
; Modified    :added /LASCO option
; Modified    :
; Modified    :Revision 1.6  2007/05/21 18:44:25  colaninn
; Modified    :added silent keyword to def_secchi_hr call
; Modified    :
; Modified    :Revision 1.5  2007/04/03 21:05:33  nathan
; Modified    :add EXTHDR keyword for extended headers
; Modified    :
; Modified    :Revision 1.4  2007/02/06 17:50:53  colaninn
; Modified    :added header keyword for string header
; Modified    :
; Modified    :Revision 1.3  2006/09/08 16:45:39  nathan
; Modified    :use /DASH2UNDERSCORE
; Modified    :
; Modified    :Revision 1.2  2006/02/14 23:30:10  nathan
; Modified    :update backwards compatibility for def_secchi_hdr v1.18
; Modified    :
; Modified    :Revision 1.1  2005/02/02 16:14:06  nathan
; Modified    :move to new directory
; Modified    : 
;
;-            

COMMON secchi_header, sechdrstruct, sechdrtags

IF datatype(sechdrstruct) EQ 'UND' THEN sechdrstruct=def_secchi_hdr(sechdrtags, silent=silent)
IF keyword_set(LASCO) THEN BEGIN
    rootdir=getenv('SSW_LASCO')
    IF rootdir EQ '' THEN rootdir=getenv('NRL_LIB')
    RESTORE, FILEPATH('example_hdr.sav',ROOT_DIR=rootdir,SUBDIRECTORY=['idl','inout'])
    hdrstruct = lasco_hdr_struct
ENDIF ELSE hdrstruct=sechdrstruct
ims=-1

filename_temp=filename0
; check for compressed

wherecomp=is_tile_compressed(filename0,count=count,/wherex)

if count gt 0 then begin 
   box_message,'uncompressing ' + strtrim(count,2) + ' RICE compressed files...'
   mreadfits_tilecomp,filename0(wherecomp), iix, _extra=_extra, /only_uncompress, fnames_uncomp=fnames_uncomp,/noshell,/silent
   filename0(wherecomp)=fnames_uncomp
endif
mreadfits, filename0, headers, ims, STRTEMPLATE=hdrstruct, NODATA=nodata, /DASH2UNDERSCORE, header=str_header,_extra=_extra 
if n_elements(fnames_ucomp) gt 0 then ssw_file_delete,fnames_uncomp 

; Fill in DATE_OBS in headers
n=n_elements(filename0)
IF ~keyword_set(LASCO) THEN FOR i=0,n-1 DO BEGIN
	h=headfits(filename0[i])
	headers[i].date_obs=fxpar(h,'DATE-OBS')
	; Backwards compatability
	IF (headers[i].date LT '2006-02-08') THEN headers[i].ledcolor=fxpar(h,'LED')
	IF (headers[i].date LT '2006-02-14') THEN BEGIN
	    headers[i].biasmean=fxpar(h,'BIAS')
	    headers[i].gaincmd=fxpar(h,'GAIN')
	    headers[i].date_avg=fxpar(h,'DATE-MID')
	ENDIF
	;--populate extended header
	IF keyword_set(EXTHDR) THEN IF headers[i].extend EQ 'T' and headers[i].n_images GT 1 THEN BEGIN
	    xh=mrdfits(filename0[i],1)
	    IF i EQ 0 THEN exthdr=xh ELSE exthdr=[exthdr,xh]
	ENDIF
ENDFOR

filename0=filename_temp ; don't clobber input

return, ims

END
