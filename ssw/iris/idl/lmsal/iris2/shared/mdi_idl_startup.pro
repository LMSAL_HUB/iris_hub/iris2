pro mdi_idl_startup
;+
;NAME:
;	mdi_idl_startup
;PURPOSE:
;	Execute the MDI specific IDL startup.  It does the following:
;		* Calls WMENU_SEL_SET to define the x-terminal window size
;HISTORY:
;	Written 4-May-94 by M.Morrison
;	10-Aug-95 (MDM) - Added execution of UITDBLIB
;	30-Oct-96 (MDM) - Removed execution of UITDBLIB
;			- Added prepending MDI directories if
;			  SSW_MDI_PREPEND is set to "yes"
;	31-Oct-96 (MDM) - changed /PREPEND to /FULL_PREPEND
;	 8-Nov-96 (MDM) - Added astron dir to TRACE setup
;	15-Apr-97 (MDM) - Added idl_lib/astron for all setups
;			- Added SSW_CONTRIBUTED
;	21-May-97 (MDM) - Removed the code which was checking to
;			  see if we're running the SSW startup
;-
;
;-------------------------- Setup maximum WMENU screen size --------------
;
case get_tty_type() of
    'SGI': wmenu_sel_set,   105, 54
    'NCD17': wmenu_sel_set, 132, 46
    else:
endcase
;
;;loc = path_lib('uitdblib')
;;if (loc(0) ne '') then uitdblib
;
if (getenv('SSW_MDI_PREPEND') eq "yes") then begin
    ssw_path, '$SSW_MDI/idl', /full_prepend, /quiet
end
if (getenv('SSW_TRACE_PREPEND') eq "yes") then begin
    ssw_path, '$SSW_TRACE/idl', /full_prepend, /quiet
    ssw_path, '$SSW_TRACE/idl/trace', /full_prepend, /quiet
    ssw_path, '$SSW/gen/idl_libs/astron', /quiet
    ;;!path = '/tsw/idl/trace/egse:' + !path
end

ssw_path, '$SSW/gen/idl_libs/astron', /quiet
if getenv('SSW_CONTRIBUTED') ne '' then ssw_path, getenv('SSW_CONTRIBUTED'), /full_prepend, /quiet

end
