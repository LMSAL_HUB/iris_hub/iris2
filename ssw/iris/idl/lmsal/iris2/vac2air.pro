function vac2air, wl_data

;;; From STiC Code (J. de la Cruz Rodriguez)
;def _vac2air(alamb):
;    if(alamb < 2000.):
;        return alamb
;    else:
;        return alamb/(1.0+2.735182e-4+131.4182/alamb/alamb+ \
;                2.76249e8/alamb/alamb/alamb/alamb)
;def vac2air(alamb):
;    if(type(alamb) == np.ndarray):
;        n = len(alamb)
;        res = alamb * 0.0
;        for ii in range(n):
;            res[ii] = _vac2air(alamb[ii]*1)
;        return res
;    else:
;        return _vac2air(alamb*1)

dim = size(wl_data)
out = wl_data*0d
for j = 0, dim[1]-1 do begin
	alamb = wl_data[j]
	out[j]= alamb/(1.0+2.735182e-4+131.4182/alamb/alamb+ 2.76249e8/alamb/alamb/alamb/alamb)
endfor

return, out

end
