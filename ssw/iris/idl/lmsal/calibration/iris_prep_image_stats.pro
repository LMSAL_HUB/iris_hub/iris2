function IRIS_PREP_IMAGE_STATS, oindex, data

;+
;
; Updates the statistics in the FITS header for a batch of IRIS images
;
; WRITTEN: PB 2013/11/01
;
;
;-

nindex = oindex
n_imgs = N_ELEMENTS(oindex)
datasize = SIZE(data)
goodpoints = WHERE(FINITE(data), numgood)
goodframe = FIX(goodpoints / (datasize[1] * datasize[2]))
satpix = WHERE(data eq data and not FINITE(data), nsatpix)
if nsatpix gt 0 then satframe = FIX(satpix / (datasize[1] * datasize[2]))

for i = 0, n_imgs-1 do begin
	if nsatpix gt 0 then begin
		this_satpoints = satpix[WHERE(satframe eq i, numthis)]
		nindex[i].nsatpix = numthis
	endif else nindex[i].nsatpix = 0
	this_goodpoints = goodpoints[WHERE(goodframe eq i, numthis)]
	if numthis gt 0 then begin
		nindex[i].datamin  = MIN(data[this_goodpoints])
		nindex[i].datamax  = MAX(data[this_goodpoints])
		nindex[i].datamedn = MEDIAN(data[this_goodpoints])

		thismoment = MOMENT(data[this_goodpoints])
		nindex[i].datamean = thismoment[0]
		nindex[i].datarms  = SQRT(thismoment[1])
		nindex[i].dataskew = thismoment[2]
		nindex[i].datakurt = thismoment[3]

		thishist = HISTOGRAM(data[this_goodpoints], loc=hloc)
		chist = TOTAL(FLOAT(thishist), /cumulative)
		histlocs = VALUE_LOCATE(chist/numthis, [0.01, 0.10, 0.25, 0.75, 0.90, 0.95, 0.98, 0.99])
		nindex[i].datap01 = hloc[histlocs[0]]
		nindex[i].datap10 = hloc[histlocs[1]]
		nindex[i].datap25 = hloc[histlocs[2]]
		nindex[i].datap75 = hloc[histlocs[3]]
		nindex[i].datap90 = hloc[histlocs[4]]
		nindex[i].datap95 = hloc[histlocs[5]]
		nindex[i].datap98 = hloc[histlocs[6]]
		nindex[i].datap99 = hloc[histlocs[7]]
	endif else begin
		nindex[i].datamin  = !values.f_nan
		nindex[i].datamax  = !values.f_nan
		nindex[i].datamedn = !values.f_nan
		nindex[i].datamean = !values.f_nan
		nindex[i].datarms  = !values.f_nan
		nindex[i].dataskew = !values.f_nan
		nindex[i].datakurt = !values.f_nan
		nindex[i].datap01 = !values.f_nan
		nindex[i].datap10 = !values.f_nan
		nindex[i].datap25 = !values.f_nan
		nindex[i].datap75 = !values.f_nan
		nindex[i].datap90 = !values.f_nan
		nindex[i].datap95 = !values.f_nan
		nindex[i].datap98 = !values.f_nan
		nindex[i].datap99 = !values.f_nan
	endelse
endfor

RETURN, nindex

end