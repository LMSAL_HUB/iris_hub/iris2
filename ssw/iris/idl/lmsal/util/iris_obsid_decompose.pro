function iris_obsid_decompose,obsid

; 
;+
;      Name: iris_obsid_decompose
;
;      Purpose: decompose IRIS OBS ID into its components
;
;      Input parameters: obsid  -- long64 of lon64arr
;
;      Output: function returns array of 9 x n_elements(obsid) that
;              contains decomposition of 0; 100; 2000; 20,000;
;                                        250,000; 1e6; 10e6; 20e6; 200e6 
;              Sets all to 0 (except the 200e6 series) for OBSID GE
;              4.2d9 (calibration sequences)
;              Works for v38 and v40 of OBS tables
;      Calling Sequence: obsid_dc=iris_obsid_decompose(obsid)  
;
;      History:
;          9-Sep-2013 - B. De Pontieu


scheme = [long64(2d8),long64(2d7),long64(1d7),long64(1d6),long64(250000),long64(20000),long64(2000),long64(100),long64(0)]

;scheme_threshold=long64([0d0,1d2,2d3,2d4,2.5d5,1d6,1d7,2d7,2d8])
nscheme=n_elements(scheme)
nobsid=n_elements(obsid)

dum=lon64arr(nscheme,nobsid,2)
idr=obsid
for j=0,nscheme-1 do begin
   if scheme[j] ne 0 then begin
     dum[j,*,0]=long64(idr/scheme[j])*scheme[j]
     dum[j,*,1]=long64(idr-dum[j,*,0])
     idr=dum[j,*,1]  ;left-over part of obsid
  endif else begin
     dum[j,*,0]=idr
     dum[j,*,1]=0
  endelse 
endfor
out=reform(dum[*,*,0])
wbad=where(obsid ge 4.2d9,nbad)
if nbad gt 0 then out[1:*,wbad]=0
out=reverse(out,1)
return,out
end


; output parameters


