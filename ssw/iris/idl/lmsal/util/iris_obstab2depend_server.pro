function iris_obstab2depend_server,obsid, outdir=outdir, nmissing=nmissing, fourpix=fourpix, debug=debug
;
;+
;   Name: iris_obstab2depend_server,obsid
;
;   Purpose: plugin to iris_obstab2depend to access Ryan Timmons obs server 
;
;   Input Parameters:
;      obsid - desired obsid
;
;   Output:
;      local file list /<OUTDIR>/<OBSID>/*.xml 
;
;   Keyword Parameters:
;      outdir - optional output path for <OBSDIR> subdirectory 
;      nmissing = number of *.xmls missing after transfer (one hopes that this always 0...)
;
;   History;
;      29-jul-2013 - ssw interface to Ryans getdependencies/getxml
;
;-

server='http://www.lmsal.com/iris_science/irissciapp/irisplantoolserver?type='

obsidx=strtrim(obsid,2)
sobsid=string(ulong(obsid),format='(I10.10)')
debug=keyword_set(debug)
if debug then stop,'obsidx,sobsid'

; 1. get dependencies

scmd=server + 'getdependencies&obsids='+strtrim(obsid,2)
sock_list,scmd,depend
if depend[0] eq '' then begin
   box_message,['Problem with deppendencies method',scmd]
endif else begin 
   depend=strtrim(depend,2)
   depend=depend(where(depend ne ''))
   ss=where(strmatch(depend,'obs',/fold),ocnt)
   if ocnt eq 0 then depend=['OBS-'+sobsid +'.xml',depend]
   xmlurls=server+'getxml&id='+depend
   nurls=n_elements(xmlurls)
   if n_elements(outdir) eq 0 then outdir=curdir()
   obsdir=concat_dir(outdir,sobsid)
   mk_dir,obsdir
   xmlfiles=file_search(obsdir,'*.xml')
   ssw_file_delete,xmlfiles
   outfiles=concat_dir(obsdir,depend)
   for f=0,nurls-1 do begin 
      sock_list,xmlurls[f],xmldat
      file_append,outfiles[f],xmldat,/new
   endfor
   xmlfiles=file_search(obsdir,'*.xml')
   nout=n_elements(xmlfiles) * (xmlfiles[0] ne '')
   nmissing=n_elements(xmlurls) - nout
   retval=xmlfiles
endelse
 
if debug then stop,'scmd,xmlurls'


return,retval
end


