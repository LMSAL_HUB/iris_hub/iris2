pro ssw_tile_compress, ufiles, cfiles, $
   rice=rice, gzip=gzip, plio, tile_size=tile_size, noisebits=noisebits, $
   parent_dir=parent_dir, chron_tree=chron_tree, _extra=_extra
;
;+
;   Name: ssw_tile_compress
;
;   Purpose: tile compress FITS files; optionally clobber or new
;
;   Input Paramters:
;      ufiles - list of one or more uncompressed fits files
;      cfiles - optional output file names (default derived from input)
;
;   Keyword Parameters:
;      parent_dir - top path for file(s)
;      chron_treee - if set, populate a chronologically organized tree 
;                    (see ssw_time2paths.pro header)
;
;      _extra - optional descriptions of chron_tree via ssw_time2paths
;               deault = <parent_dir>/yyyy/mm/dd/<cfiles>
;      rice/gzip/plio - type of compression to apply, default= /RICE
;      noisebits - optionally, improve compress but degraded fidelity
;      tile_size - optional tile size in pixels - default=[100,100]
;
;   See: HEARSARC tile format fits description:
;   http://heasarc.nasa.gov/docs/software/fitsio/compression.html
;
;   History:
;      10-apr-2010 - S.L.Freeland - SDO et al
;
imcopy=concat_dir(concat_dir(concat_dir('$SSW_ONTOLOGY','binaries'),!version.os+'_'+!version.arch),$
   'imcopy')
if not file_exist(imcopy) then begin 
   box_message,'Do not have imcopy binary for this os/arch...'
   return
endif

break_file,ufiles,ll,pp,fname,ee,vv
ftimes=file2time(ufiles)
nf=n_elements(ftimes)
if n_elements(parent_path) eq 0 then parent_path=curdir()
outpaths=replicate(parent_path,nf)
if keyword_set(chron_tree) then begin 
    for i=0,nf-1 do begin 
       outpaths(i)=ssw_time2paths(ftimes(i),ftimes(i),_extra=_extra)
       if not file_exist(outpaths(i)) then mk_dir,outpaths(i)
    endfor
endif 

case n_elements(tile_size) of
   2: tsize=tile_size
   1: tsize=replicate(tile_size,2)
   else: tsize=[100,100]
endcase

tiles=arr2str(tsize,/trim)
nbits=''
if keyword_set(noisebits) then nbits=';'+strtrim(noisebits,2)
case 1 of
   keyword_set(gzip): comp='GZIP'
   keyword_set(plio): comp='PLIO'
   else: comp='RICE'
endcase

if n_elements(cfiles) ne nf then $
   cfiles=concat_dir(outpaths,fname+ee+vv)

compcmd="[compress " + comp + " "  + tiles + nbits + "]'" 
for i=0,nf-1 do begin
   imccmd=imcopy + ' ' + ufiles(i) + " '" + cfiles(i) + compcmd
   spawn,imccmd  
endfor
return
end






;    
