
FUNCTION iris_find_file, ttime, $
                         count=count, quiet=quiet, $
                         all=all, $
                         sji=sji, sequence=sequence, $
                         nearest=nearest

;+
; NAME:
;     IRIS_FIND_FILE
;
; PURPOSE:
;     This routine returns the name (or names) of IRIS raster files
;     (or optional SJI files) that correspond to the specified
;     time. The logic for what filename(s) is returned is as follows.
;
;     The data for a given observation can either be contained in a
;     single file for sit-and-stare or single raster observations, or
;     in multiple files corresponding to multiple raster repeats. In
;     the latter case, the routine will return the complete list of
;     rasters for the observation. Note that these are files with
;     r00000, r00001, r00002, etc. in their filenames.
;
;     A given observation has a start time (t0) and an end time
;     (t1). If TTIME lies between these two times, and the files are
;     present in the user's $IRIS_DATA directory, then the
;     raster files for this period are returned.
;
;     If the files are not present on the computer, then the user can
;     search for the nearest file sequence to the input time by
;     setting /NEAREST.
;     
;     The routine only checks the specified day and the previous day
;     for the existence of files.
;
; CATEGORY:
;     IRIS; file-handling.
;
; CALLING SEQUENCE:
;     Result = IRIS_FIND_FILE(Time)
;
; INPUTS:
;     Time:  This can be in any format accepted by the ANYTIM suite of
;            routines. For example, '1-jan-2010', '2010-01-01 05:00'.
;
; KEYWORD PARAMETERS:
;     ALL:      If set, then all filenames for the specified day will
;               be returned.
;     QUIET:    If set, then no information messages are printed.
;     SEQUENCE: This keyword is now obsolete.
;     SJI:      By default IRIS_FIND_FILE searches only for spectrum
;               files. Setting /sji forces a search for slitjaw
;               files. By using, e.g., sji=2832 then the routine will
;               search only for SJI files of the specified wavelength.
;     NEAREST:  If the standard call does not return a match, then
;               specifying this keyword will give the nearest
;               match. The "nearest" is defined as the sequence for
;               which the start time is closest to the input time. 
;
; OUTPUTS:
;     A string containing the full path to an IRIS file or files. If
;     there are no matches, then an empty string is returned. 
;
; OPTIONAL OUTPUTS:
;     Count: An integer containing the number of matching files.
;
; EXAMPLE:
;     IDL> file=iris_find_file('3-oct-2014 23:30')
;     IDL> file=iris_find_file('3-oct-2014 23:30',/sji)
;     IDL> file=iris_find_file('3-oct-2014 23:30',sji=2832)
;
; CALLS:
;     TIME2FID, ANYTIM2UTC, IRIS_OBJ
;
; MODIFICATION HISTORY:
;     Ver.1, 2-May-2014, Peter Young
;     Ver.2, 18-Jun-2014, Peter Young
;         Now searches for file on the requested and previous days.
;     Ver.3, 17-Oct-2014, Peter Young
;         Allows for $IRIS_DATA to contain multiple paths.
;     Ver.4, 23-Feb-2015, Peter Young
;         Overhauled this routine as multiple path feature
;         wasn't working correctly; routine now works for SJI data. 
;     Ver.5, 18-Mar-2015, Peter Young
;         Now returns a file sequence by default as the previous
;         operation could be very slow.
;     Ver.6, 25-Mar-2015, Peter Young
;         Updated header; tidied up code.
;     Ver.7, 23-Jun-2015, Peter Young
;         Modified implementation of the SJI keyword.
;     Ver.8, 17-Sep-2015, Peter Young
;         Revised method for finding raster so that it returns the
;         file nearest to the requested time; also updated the SJI
;         finder to return correct matches.
;     Ver.9, 15-Jan-2016, Peter Young
;         The count= output wasn't giving the right answer, so I've
;         fixed this.
;     Ver.10, 16-Mar-2017, Peter Young
;         Fixed bug with count when the data directories don't
;         exist; changed to using iris_find_obs_dir in order to speed
;         up searching.
;     Ver.11, 6-Jun-2017, Peter Young
;         Changed behavior so that only an exact match is returned. If
;         an exact match is not found, then it's necessary to
;         give /NEAREST to get the nearest sequence.
;     Ver.12, 8-Nov-2017, Peter Young
;         Now rounds the file start times down to the nearest minute
;         to make finding files less strict.
;     Ver.13, 17-Nov-2017, Peter Young
;         Now call HCR to get file end times.
;-

IF n_params() LT 1 THEN BEGIN
  print,'Use:  IDL> file=iris_find_file(time [, /sji, /quiet, /all, /nearest, count= ])'
  print,''
  print,' By default, only returns raster files (use /sji for SJI files).'
  print," Example time formats:  '1-jan-2010 05:00', '2010-01-01 05:00'"
  print,' Keywords:'
  print,'   /sji or sji=2832  Return SJI files instead of raster files.'
  print,'   /quiet Do not print messages.'
  print,'   /all   Return all files for the specified day.'
  print,'   count= The number of files found.'
  print,'   /nearest Search for nearest file sequence.'
  return,''
ENDIF 

iris_data=getenv('IRIS_DATA')
IF iris_data EQ '' THEN BEGIN
  print,'% IRIS_FIND_FILE: Please define the environment variable $IRIS_DATA to point to the top directory'
  print,'                  of your IRIS data directory.'
  print,'                  Note that level-2 files are expected to be in $IRIS_DATA/level2.'
  return,''
ENDIF 

;
; Set to count to zero to avoid "remembering" it from a previous call.
;
count=0


date_tai=anytim2tai(ttime)


iris_paths=BREAK_path(iris_data,/nocurrent)

l2dir=concat_dir(iris_paths,'level2')
dir=l2dir

np=n_elements(dir)

;
; Here I check if the directories exist. If not, then I remove them
; from the list. 
;
FOR i=0,np-1 DO BEGIN
  chck=file_info(dir[i])
  IF chck.directory EQ 0 THEN BEGIN
    str='not found' 
  ENDIF ELSE BEGIN
    str='found'
    IF n_elements(fdir) EQ 0 THEN fdir=dir[i] ELSE fdir=[fdir,dir[i]]
  ENDELSE 
  IF NOT keyword_set(quiet) THEN print,'- Directory '+dir[i]+' '+str
ENDFOR 
;
IF n_elements(fdir) EQ 0 THEN BEGIN
  print,'% IRIS_FIND_FILE:  none of your data directories was found. Returning...'
  return,''
ENDIF ELSE BEGIN
  dir=fdir
  np=n_elements(dir)
ENDELSE



;
; If a raster is repeated 25 times beginning at 23:00 on 31-Dec-2013,
; then all 25 files will be stored under the 2013/12/31 directory even
; though some of them were run on 1-Jan-2014. For this reason I have
; to search the previous day to the one requested in order to pick up
; the files correctly.
;
 

IF keyword_set(all) THEN BEGIN
  date_start=ttime
  date_end=ttime
ENDIF ELSE BEGIN
  tt_mjd=anytim2utc(/mjd,ttime)
  tt_mjd.mjd=tt_mjd.mjd-1
  date_start=anytim2utc(/ccsds,tt_mjd)
  date_end=ttime
ENDELSE 


IF keyword_set(sji) THEN BEGIN
  search_str='_SJI_'
  sji_swtch=1
ENDIF ELSE BEGIN
  search_str='_raster_'
  sji_swtch=0
ENDELSE 

files=iris_find_obs_dir(date_start,date_end,/files)
chck=strpos(files,search_str)
k=where(chck GE 0,count)
IF count GT 0 THEN BEGIN
  all_list=files[k]
ENDIF ELSE BEGIN
  IF NOT keyword_set(quiet) THEN print,'%IRIS_FIND_FILE: no files found.'
  return,''
ENDELSE 

count=n_elements(all_list)

IF count EQ 0 THEN BEGIN
  IF NOT keyword_set(quiet) THEN print,'% IRIS_FIND_FILE: no files found.'
  return,''
ENDIF 

;
; If /ALL then just return all the found files.
;
IF keyword_set(all) THEN BEGIN
  IF NOT keyword_set(quiet) THEN BEGIN
    print,'% IRIS_FIND_FILE: '+trim(count)+' files found: '
    FOR j=0,count-1 DO BEGIN
      print,format='(i3,2x,a60)',j,file_basename(all_list[j])
    ENDFOR 
  ENDIF 
  IF count NE 0 THEN return,all_list ELSE return,''
ENDIF


;
; Find the SJI files
;
IF sji_swtch EQ 1 THEN BEGIN
  i_match=intarr(count)

  t_start_utc=iris_file2date(all_list)
  t_end_utc=strarr(count)
  
  FOR j=0,count-1 DO BEGIN
    d=iris_sji(all_list[j])
    t_end_utc[j]=d->getinfo('DATE_END')
    obj_destroy,d
  ENDFOR

  t_start_ex=anytim2utc(/ex,t_start_utc)
  t_start_ex.second=0
  t_start_tai=anytim2tai(t_start_ex)
  t_end_tai=anytim2tai(t_end_utc)

  k=where(date_tai GE t_start_tai AND date_tai LE t_end_tai,nk)

  IF nk GE 1 THEN BEGIN
    outfiles=all_list[k]
  ENDIF ELSE BEGIN
    getmin1=min(abs(date_tai-t_start_tai),imin1)
    getmin2=min(abs(date_tai-t_end_tai),imin2)
    IF getmin1 LT getmin2 THEN k=imin1 ELSE k=imin2
    t_start_match=t_start_utc[k]
    j=where(t_start_utc EQ t_start_match)
    outfiles=all_list[j]
    IF NOT keyword_set(quiet) THEN BEGIN
      print,'% IRIS_FIND_FILE: no exact match. Returning nearest files.'
    ENDIF 
  ENDELSE 

  IF fix(sji) GT 1 THEN BEGIN
    chck=iris_file2date(outfiles,sji_wvl=sji_wvl)
    k=where(fix(sji) EQ sji_wvl,nk)
    IF nk NE 0 THEN BEGIN
      outfiles=outfiles[k]
    ENDIF ELSE BEGIN
      print,'% IRIS_FIND_FILE: there are no SJI files matching query. Returning...'
      return,''
    ENDELSE 
  ENDIF 
  
  n=n_elements(outfiles)
  print,'% IRIS_FIND_FILE: '+trim(n)+' SJI files found.'
  FOR j=0,n-1 DO BEGIN
    print,format='(i6,2x,a60)',j,strpad(file_basename(outfiles[j]),60,fill=' ',/after)
  ENDFOR
  count=n
  return,outfiles

ENDIF 



;
; Now deal with the raster files.
; -------------------------------
;
; Extract the start times of the start file of each sequence
; (t_uniq_utc, t_uniq_tai).
;
; Note that I use the ex format in order to round the start time down
; to the nearest minute. This allows the routine to find a file if the
; requested time is 20:52 and the start time is 20:52:22, for example.
;
t_list_utc=iris_file2date(all_list)
i_uniq=uniq(t_list_utc)
t_uniq_utc=t_list_utc[i_uniq]
t_uniq_ex=anytim2utc(/ex,t_uniq_utc)
t_uniq_ex.second=0
t_uniq_tai=anytim2tai(t_uniq_ex)
;
; List of all start files
;
file_uniq=all_list[i_uniq]

;
; 17-Nov-2017: I'm now getting the end time by calling the HCR,
; to save having to read the FITS file. I need to check that the user
; has a network connection, though. If not, then I use the old
; method. 
;
n=n_elements(file_uniq)
t_end_utc=strarr(n)
t_end_tai=lonarr(n)
;
; Check if we have an internet connection.
;
net_chck=have_network()
IF net_chck EQ 1 THEN BEGIN 
  FOR i=0,n-1 DO BEGIN
    t0=anytim2utc(/ccsds,t_uniq_tai[i])
    t1=anytim2utc(/ccsds,t_uniq_tai[i]+60.)
    hcr=iris_obs2hcr(t0,t1)
    t_end_utc[i]=anytim2utc(/ccsds,hcr[0].stoptime)
    t_end_tai[i]=ceil(anytim2tai(hcr[0].stoptime))
  ENDFOR
ENDIF ELSE BEGIN 
  FOR i=0,n-1 DO BEGIN
    d=iris_obj(file_uniq[i])
    t_end_utc[i]=d->getinfo('DATE_END')
    obj_destroy,d
    t_end_tai[i]=ceil(anytim2tai(t_end_utc[i]))
  ENDFOR 
ENDELSE


;
; Check if input date-time matches one of the sequences.
; Note that I give some leeway on the start time.
;
;
; There are two options for finding the right sequence:
; (1) [Default] Match the sequence for which the input time lies
;     within the sequence's start and end times.
; (2) [/nearest keyword] Match the sequence for which the start time
;     is nearest to the input time.
; For example, suppose the input time is 11:27. One sequence begins at
; 10:00 and finishes at 11:28, and the next sequence begins at
; 11:29. The default call, will return the first sequence while
; /nearest returns the second sequence.
;
k=where(date_tai GE t_uniq_tai AND date_tai LE t_end_tai,nk)
IF keyword_set(nearest) THEN BEGIN
  getmin=min(abs(date_tai-t_uniq_tai),imin)
;  getmin2=min(abs(date_tai-t_end_tai),imin2)
;  IF getmin1 LT getmin2 THEN k=imin1 ELSE k=imin2
  j=where(t_uniq_utc[imin] EQ t_list_utc)
 ;
  ;; IF NOT keyword_set(quiet) THEN BEGIN
  ;;   print,'% IRIS_FIND_FILE: no exact match. Returning nearest file sequence.'
  ;; ENDIF
  tstart=anytim2utc(/ccsds,t_uniq_utc[imin],/trunc)
  tend=anytim2utc(/ccsds,t_end_utc[imin],/trunc)
ENDIF ELSE BEGIN
  IF nk EQ 0 THEN BEGIN
    IF NOT keyword_set(quiet) THEN BEGIN
      print,'% IRIS_FIND_FILE: No exact match found. Use /NEAREST to find the nearest file sequence to input time.'
      return,''
    ENDIF
  ENDIF ELSE BEGIN 
    j=where(t_uniq_utc[k[0]] EQ t_list_utc)
    tstart=anytim2utc(/ccsds,t_uniq_utc[k[0]],/trunc)
    tend=anytim2utc(/ccsds,t_end_utc[k],/trunc)
  ENDELSE 
ENDELSE 

;; IF nk EQ 1 THEN BEGIN
;; ENDIF ELSE BEGIN
;;   IF keyword_set(nearest) THEN BEGIN
;;   ENDIF ELSE BEGIN 
;;   ENDELSE 
;; ENDELSE


nj=n_elements(j)
IF NOT keyword_set(quiet) THEN BEGIN
  IF nj EQ 1 THEN nstr='is '+trim(nj)+' file' ELSE nstr='are '+trim(nj)+' files'
  print,'% IRIS_FIND_FILE: there '+nstr+' in the sequence.'
  print,format='("    Matched sequence: ",a20," to ",a20)', $
        anytim2utc(/ccsds,tstart,/trunc), $
        anytim2utc(/ccsds,tend,/trunc)
ENDIF 

count=nj

return,all_list[j]




END

