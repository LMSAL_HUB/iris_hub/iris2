;             NAME : tgf_1lp
; 
;          PURPOSE : Perform single and triple Gaussian fit of a line profile. 
;
; CALLING SEQUENCE : tgf_1lp,wvl,lp,ee,ini,range0,range1,range2
; 
;           INPUTS : lp - the line profile
;                    
;                    wvl - wavelength vector
;                    
;                    ee - error vector  
;                    
;                    ini - 6-element vector:  initial guess of the
;                    2nd/core intensity ratio, 2nd component speed (relative to wave0) and
;                    width (km/s), 3rd/core intensity ratio, 3rd component speed (relative to wave0) and
;                    width (km/s), e.g., [0.5,70,35,0.5,-80,55]
;                    
;                    range0 - the allowed range of the fitting parameters of the 1st component, e.g., range0=[0.5,2,1] 
;                    means that the allowable range for the peak intensity values is 0.5-1.5 times of the initial peak intensity, 
;                    for the center is 2 times the spectral pixel size away from the initial line center,
;                    for the width is the initial line width plus minus 1 times the spectral pixel size 
;                    
;                    range1 - the allowed range of the fitting parameters of the 2nd component, similar to range0    
;                    
;                    range2 - the allowed range of the fitting parameters of the 3rd component, similar to range0 
; 
;         KEYWORDS : wave0 - reference wavelength, use the SGF centroid of the avearge profile if not set 
;         
;                    ini0 - initial values of the first component speed (relative to wave0) and width (km/s), e.g., [100,40], default is [0,ini[2]]
;                    
;                    posi - position of the upper most panel of the line profile plots
;                    
;                    xtitle - lable of x-cooridinate of the line profile plot and RB profile plot
;                    
;                    xtitleRB -  lable of y-cooridinate of the RB profile plot
;                    
;                    ytitle -  lable of y-cooridinate of the line profile plot
;                    
;                    lineid - name of the line
; 
;                    para_sgf -  output of SGF line parameters: peak intensity, Doppler shift, line width, background intensity
;          
;                    chisq_sgf - output of chisq value for SGF
;                    
;                    para_tgf -  output of TGF 3-component line parameters[4,3]: peak intensity, Doppler shift, line width, background intensity
;          
;                    chisq_tgf - output of the chisq value for TGF
;                    
;          OUTPUTS : 
;
;          HISTORY : Written by Hui Tian at CfA, April 4, 2013
;
;; **********************************************************************


function mp_tgf_tmp, x, y, e, fit0, fit1, fit2, range0, range1, range2, dlambda, good, double = double
; f = x0 + x1*exp((x-x2)/x3)^2 + x4*exp((x-x5)/x6)^2 + x7*exp((x-x8)/x9)^2
xlimited=[1,1]
; Background Intensity
x0={limited:xlimited, limits:fit0[0] + 0.5*[-1,1]*fit0[0], value:fit0[0]}
; First Component Peak Intensity
x1={limited:xlimited, limits:fit0[1] + range0[0]*[-1,1]*fit0[1], value:fit0[1]} 
; First Component Line Center Position
x2={limited:xlimited, limits: fit0[2] +range0[1]*[-dlambda, dlambda], value:fit0[2]}
; First Component Gaussian Width
x3={limited:xlimited, limits:fit0[3] + range0[2]*[-dlambda, dlambda], value:fit0[3]} ;
; Second Component Peak Intensity
x4={limited:xlimited, limits:fit1[1] + range1[0]*[-1,1]*fit1[1], value:fit1[1]}
; Second Component Line Center Position
x5={limited:xlimited, limits:fit1[2] +range1[1]*[-dlambda, dlambda], value:fit1[2]}
; Second Component Gaussian Width
x6={limited:xlimited, limits:fit1[3] +range1[2]*[-dlambda, dlambda], value:fit1[3]} 
; Third Component Peak Intensity
x7={limited:xlimited, limits:fit2[1] + range2[0]*[-1,1]*fit2[1], value:fit2[1]}
; Third Component Line Center Position
x8={limited:xlimited, limits:fit2[2] +range2[1]*[-dlambda, dlambda], value:fit2[2]}
; Third Component Gaussian Width
x9={limited:xlimited, limits:fit2[3] +range2[2]*[-dlambda, dlambda], value:fit2[3]} 
parinfo=[x0,x1,x2,x3,x4,x5,x6,x7,x8,x9]
param = parinfo.value
res = mpfitfun('triplegauss', x[good], y[good], e[good], param, parinfo=parinfo,$
  maxiter = 10000, dof = dof, bestnorm = bestnorm, yfit = yfit, double = double,status = status, /quiet, perror=perror)
result = {b:res[0], i1:res[1], p1:res[2], w1:res[3], i2:res[4], p2:res[5], w2:res[6], i3:res[7], p3:res[8], w3:res[9], fit:yfit, status:status,sigma:perror}
return, result
end



pro tgf_1lp,wvl,lp,ee,ini,range0,range1,range2,ini0=ini0,$
    lineid=lineid,wave0=wave0,posi=posi,xtitle=xtitle,xtitleRB=xtitleRB,ytitle=ytitle,$
    para_sgf=para_sgf,chisq_sgf=chisq_sgf,para_tgf=para_tgf,chisq_tgf=chisq_tgf

!p.font=-1
window,xs=900,ys=700, retain = 2
loadct,0  & tvlct,255L,0L,0L,2L  & tvlct,0L,255L,0L,3L   & tvlct,0L,0L,255L,4L

if not(keyword_set(xtitle)) then xtitle = 'Wavelength (!N!6!sA!r!u!9 %!6 !n!N!3)'
if not(keyword_set(xtitleRB)) then xtitleRB = 'Velocity (km/s)'
if not(keyword_set(ytitle)) then ytitle = 'Counts';'Radiance (erg !Ns!E-1!Ncm!E-2!Nsr!E-1 !N!6!sA!r!u!9 %!6!n!U-1!N!3)'
if not(keyword_set(posi)) then posi=[0.07,0.07,0.95,0.95]
if not(keyword_set(lineid)) then lineid='iris'

dlambda=mean(deriv(wvl)) ;spectral pixel size
nw=n_elements(wvl)

if not(keyword_set(wave0)) then begin
  res = mpfitpeak(wvl,lp, a, nterms = 4, /double, /positive) 
  wave0= a[1] ;set the rest wavelength by assuming 0 shift of the line profile
endif


para_sgf=fltarr(4)
para_tgf=fltarr(4,3)


;single Gaussian fit
x=wvl
y=lp
err_ave=ee>0.001<(y*0.9) ;make sure that the error value is larger than 0 and smaller than the data value
plot_io,x,y,title='SGF & TGF to '+lineid+' line profile',position=posi,xtitle=xtitle,ytitle=ytitle,psym=4,$;/noerase,$
      xrange=[min(x)-dlambda/2,max(x)+dlambda/2],xstyle=1,yrange=[min(y)*0.95>0.1,max(y)*1.5],ystyle=1,charsize=1.5
errplot,x,y-err_ave,y+err_ave,width=0.005,thick=1
bpp = where(y gt 0., bppc)  &  ee = err_ave
if bppc le 4 then begin ;do not do fit
  a=fltarr(4)-999. & chisq=-999. 
  i_rba1=-999. & v_rba1=-999. & w_rba1=-999. & rba1=fltarr(15)-999.
  i_rba2=-999. & v_rba2=-999. & w_rba2=-999. & rba2=fltarr(15)-999.
  goto,endprogram
endif
fitsg = mpfitpeak(x[bpp],y[bpp], a, nterms = 4, /double, /positive)   ;fitsg=a[3]+a[0]*exp(-0.5*(x-a[1])^2./(a[2]^2))
chisq = (1./(bppc - 4)) * total(((y[bpp] - fitsg[bpp])/err_ave[bpp])^2)
oplot,x,spline(x[bpp],fitsg,x),color=3L
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'SGF',charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.86),strmid(strtrim(string((a[1]-wave0)/wave0*3e5),2),0,4),charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.80), strmid(strtrim(string(a[2]/wave0*2.999e5*sqrt(2)),2),0,4),charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.68),strmid(strtrim(string(chisq),2),0,4),charsize=1.5,color=3L,align=1


;triple Gaussian fit
if not(keyword_set(ini0)) then ini0=[0,ini[2]]
fit0 = double([min(y[bpp]), max(y), wave0+ini0[0]/(3.e5)*wave0, ini0[1]/(3.e5)*wave0]) ;initial guess of the background, peak, centroid, width, and background for 1st component
fit1 = double([min(y[bpp]), max(y)*ini[0], wave0+ini[1]/(3.e5)*wave0, ini[2]/(3.e5)*wave0])  ;initial guess for 2nd component
fit2 = double([min(y[bpp]), max(y)*ini[3], wave0+ini[4]/(3.e5)*wave0, ini[5]/(3.e5)*wave0])  ;initial guess for 3rd component
re = mp_tgf_tmp(x,y, ee, fit0, fit1, fit2, range0, range1, range2, dlambda[0], bpp, /double)
fittg=re.b + re.i1*exp(-((x - re.p1)/re.w1)^2) + re.i2*exp(-((x - re.p2)/re.w2)^2) + re.i3*exp(-((x - re.p3)/re.w3)^2)
chisq3 = (1./(bppc - 10)) * total(((y[bpp] - fittg[bpp])/err_ave[bpp])^2)
oplot,interpol(x,nw*10,/spline),interpol(re.b + re.i1*exp(-((x - re.p1)/re.w1)^2),nw*10,/spline),linestyle=2,color=2L
oplot,interpol(x,nw*10,/spline),interpol(re.b + re.i2*exp(-((x - re.p2)/re.w2)^2),nw*10,/spline),linestyle=2,color=2L
oplot,interpol(x,nw*10,/spline),interpol(re.b + re.i3*exp(-((x - re.p3)/re.w3)^2),nw*10,/spline),linestyle=2,color=2L
oplot,interpol(x,nw*10,/spline),interpol(fittg,nw*10,/spline),color=2L
if abs(re.p1) lt 0.0001 then re.p1=0. & if abs(re.p2) lt 0.0001 then re.p2=0.  & if abs(re.p3) lt 0.0001 then re.p3=0. 
xyouts,x[2],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'1!Est!N     2!End!N     3!Erd!N',charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.86),'v: '+strmid(strtrim(string((re.p1-wave0)/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string((re.p2-wave0)/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string((re.p3-wave0)/wave0*3e5),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.80),'w: '+strmid(strtrim(string(re.w1/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string(re.w2/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string(re.w3/wave0*3e5),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.74),'i!i2!N/i!i1!N:    '+strmid(strtrim(string(re.i2/re.i1),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.68),'i!i3!N/i!i1!N:    '+strmid(strtrim(string(re.i3/re.i1),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.62),'!4v!N!3!ir!E2!N:     '+strmid(strtrim(string(chisq3),2),0,4),charsize=1.5,color=2L


para_tgf[0,0]=re.i1 & para_tgf[0,1]=re.i2 & para_tgf[0,2]=re.i3
para_tgf[1,0]=(re.p1-wave0)/wave0*3e5 & para_tgf[1,1]=(re.p2-wave0)/wave0*3e5 & para_tgf[1,2]=(re.p3-wave0)/wave0*3e5
para_tgf[2,0]=re.w1/wave0*3e5 & para_tgf[2,1]=re.w2/wave0*3e5 & para_tgf[2,2]=re.w3/wave0*3e5
para_tgf[3,0]=re.b
chisq_tgf=chisq3
para_sgf[0]=a[0] ;peak intensity
para_sgf[1]=(a[1]-wave0)/wave0*3e5 ;Doppler shift
para_sgf[2]=a[2]/wave0*3e5*sqrt(2) ;exponential width
para_sgf[3]=a[3] ;background
chisq_sgf=chisq


endprogram:
end
