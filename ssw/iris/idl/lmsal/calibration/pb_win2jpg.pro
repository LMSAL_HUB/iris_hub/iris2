pro PB_WIN2JPG, filename, $
	_extra = extra

;+
;
; Takes the contents of an IDL plot window and writes them to a .JPG file
;
;-

if N_ELEMENTS(filename) eq 0 then filename = 'idl.jpg'

case !d.name of
	'Z'	:	begin
		image = TVRD()
		imsize = SIZE(image)
		TVLCT, r, g, b, /get
		trueimage = BYTARR(3, imsize[1], imsize[2])
		trueimage[0,*,*] = r[image]
		trueimage[1,*,*] = g[image]
		trueimage[2,*,*] = b[image]
		WRITE_JPEG, filename, trueimage, /true, _extra = extra
	end
	'X'	:	begin
		image = TVRD(/true)
		WRITE_JPEG, filename, image, /true, _extra = extra
	end
	else:	begin
		PRINT, 'PB_WIN2PNG : Only works on X or Z plot device!'
	end
endcase


end