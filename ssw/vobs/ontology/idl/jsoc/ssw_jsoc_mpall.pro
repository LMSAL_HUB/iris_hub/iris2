function ssw_jsoc_mpall, ds, slow=slow, no_order=no_order, recnums=recnums, jrecnums=jrecnums
;+
;   Name: ssw_jsoc_mpall 
;
;   Purpose: return all records/versions of SDO master pointing series in rec# order
;
;   Input Parameters:
;      ds - optional series name - default=sdo.master_pointing
;
;   Keyword Parameters:
;      slow - if set, explicitly step through all records (one ssw_jsoc call per..)
;      recnums (output) - returns verbatim jsoc record numbers (used internally for recorder)

;      no_order - (switch) - if set, do not order by record number
;
;   Method:
;      couple of setup/calls to ssw_jsoc.pro
;      default uses "fast" w/call to return record numbers
;   
;   History:
;      19-sep-2012 - S.L.Freeland - ssw_sdo_master_pointing.pro helper 
;      28-sep-2012 - S.L.Freeland - use *recnum* -> json (via ssw_jsoc mod)
;                                   protect against duplicates 
;       7-nov-2012 - S.L.Freeland - allow for GAPS in jsoc REC#
;      27-mar-2015 - S.L.Freeland - exclude '=' from url encode (->ssw_jsoc->url_decode.pro) 
;-
;
if n_elements(ds) eq 0 then ds='sdo.master_pointing'

allrec='[!1=1!]'
fast=1-keyword_set(slow)
order=1-keyword_set(no_order)
urlencenv='url_encode_exclude'
envorig=get_logenv(urlencenv)
set_logenv,urlencenv,'='  

if fast then begin 
   retval=ssw_jsoc(/rs_list,ds=ds+allrec)
   recnums=ssw_jsoc(/plain_text,ds=ds+allrec+'&r=1&q=1&key=t_start,version,date,t_stop')
;  jrecnums=ssw_jsoc(ds=ds+'&key=t_start,version,*recnum*',/rs_list) ; need to vet on Monday
   if order then begin ; return in rec# order (#1->#MAXREC) ; 
      strtab2vect,str2cols(recnums,' ',/trim),rn,times,versions,date,t_stop
      rn=fix(rn) & versions=long(versions)
      temp=replicate(retval(0),max(rn)) ; 7-nov-2012 SLF - gaps in SDO master pointing 
      temp.version=-1                          ; dummy
      temp.t_start=anytim('1-jan-1999',/ccsds) ; dummy
;     I'll vectorize this tomorrow... sanity checking loop until I "understand this"
      for i=0,n_elements(temp)-1 do begin
         ssi=(where(rn eq i+1,icnt))(0) 
         if icnt gt 0 then begin
            ssn=where(times(ssi(0)) eq retval.t_start and versions(ssi) eq retval.version $
               and date(ssi(0)) eq retval.date and t_stop(ssi(0)) eq retval.t_stop,ncnt)
            ;if ncnt ne 1 then stop,'wtf??'
            temp(i)=retval(ssn(0))
          end
      endfor
      retval=temp
   endif
endif else begin 
   serstr=ssw_jsoc(/series_struct,ds=ds)
   maxrec=strtrim(gt_tagval(serstr,/maxrecnum,missing=1),2)
   recn=strtrim(indgen(fix(maxrec-1)+1)+1,2)
   recq=ds+'[:#'+recn+']' ; one query per rec#
   retval=ssw_jsoc(/rs_list,ds=recq(0)) ; init with rec#1

   for i=1,n_elements(recq)-1 do begin
      box_message,recq(i)
      retval=[temporary(retval),ssw_jsoc(/rs_list,ds=recq(i))]
   endfor
endelse

set_logenv,urlencenv,envorig
return,retval
end
