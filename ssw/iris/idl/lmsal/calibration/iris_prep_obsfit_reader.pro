function IRIS_PREP_OBSFIT_READER, header, $
	pstop = pstop

;+
;
; Given an image header structure (or vector of structures), returns a structure
; giving the obsfit (fiducial, wavelength, aia cross-correlation) parameters for
; that image's OBS. 
; 
; If a vector is passed in, it checks to ensure that all the images are from the 
; same OBS. If they are, then it returns a scalar structure with the fit 
; parameters for the OBS). If they are not, it prints a warning and returns a
; null string instead of a structure.
;
;-

nimg = N_ELEMENTS(header)

taiimg = ANYTIM2TAI(header.t_obs)

TIME_WINDOW, minutes = [-1440, 1], header.t_obs, ht0, ht1
daypaths = SSW_TIME2PATHS(ht0, ht1, '/irisa/data/prep/', /daily)
filelist = FILE_SEARCH( CONCAT_DIR(daypaths, '*/description.genx') )
;filelist = SSW_TIME2FILELIST(ht0, ht1, parent = '/irisa/data/prep/', patt = '*description.genx')
pathlist = FILE_DIRNAME(filelist)
nfiles = N_ELEMENTS(filelist)
filetime = STRMID(FILE_BASENAME(FILE_DIRNAME(filelist)), 0, 15)
taifile = ANYTIM2TAI(FILE2TIME(filetime))

preimg = WHERE(taifile le MIN(taiimg[0]), numpre)
obsfile_index = WHERE( (taiimg[0] - taifile[preimg]) eq MIN(taiimg[0] - taifile[preimg]), numind )
if numind gt 1 then obsfile_index = obsfile_index[-1]
obsfile = CONCAT_DIR(pathlist[obsfile_index], 'obsfits.genx')

if KEYWORD_SET(pstop) then STOP

; Check that all the images in the input are from the same OBS
; (This is currently a shortcut check; it seems to work ok)
isok = 1
if nimg gt 1 then begin
	obsfile_index_max = WHERE( (taiimg[-1] - taifile[preimg]) eq MIN(taiimg[-1] - taifile[preimg]), nummax )
	if nummax gt 1 then obsfile_index_max = obsfile_index_max[-1]
	if obsfile_index_max ne obsfile_index then begin
		isok = 0
		BOX_MESSAGE, ['IRIS_PREP_OBSFIT_READER: ', 'mix of OBS in input!']
	endif
endif
; Check that the obsfile exists
if not FILE_EXIST(obsfile) then begin
	isok = 0
	BOX_MESSAGE, ['IRIS_PREP_OBSFIT_READER: ', obsfile, 'obsfile not found!']
endif

if isok eq 0 then begin
	result = ''
endif else begin
	RESTGEN, file = obsfile, str = result
endelse

RETURN, result

end