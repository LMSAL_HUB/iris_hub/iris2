;+
; NAME:
;       iris_sjiflat_calc()
;
; PURPOSE:
;       For making the SJI flat field.
;       The multiple save files produced by sjiflat_prep can be passed
;       directly to this routine which prepares the data and runs the
;       Chae flat field code on each sequence.  This code extracts the
;       (stationary) flat field pattern from the (moving) image
;       pattern.
;
;       The relative image shifts are mined from the index.xcen and
;       index.ycen parameters and passed off to the chaeflat routines,
;       these act as a first guess and greatly increase the robustness
;       of the results in the case of the NUV SJI channels which have
;       a large dither pattern.
;
; CALLING SEQUENCE:
;       result=iris_sjiflat_calc(filename=filename, [/pickfile,
;                                datadir=, /geofile])
;
; INPUTS:
;       filename - the path and name of an IDL save file with the
;                  Level 1.5 data in the form of index, data.  If the
;                  user supplies an array, then each file is processed.
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filename of the IDL save file containing the results
;                which will be passed to the final stage of processing
;                where multiple flatfield results are combined.
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       iris_chae_gaincalib
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result1=iris_sjiflat_getdata(time_start, time_end, index, data, $
;     >                             datadir=datadir, /nosave, /jsoc2)
;     > result2=iris_sjiflat_prep(index, data, datadir=datadir)
;     > result3=iris_sjiflat_calc(filename=result2, datadir=datadir)
;
; MODIFICATION HISTORY:
;       Started 2014-Jul-1  by Sarah A. Jaeggli, Montana State
;                           University
;-

function iris_sjiflat_calc, filename=filename, datadir=datadir, $
                            pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1p5.sav', $
                              /multiple_files, $
                              title='Pick L1.5 files to process')

  if keyword_set(filename) then npaths=n_elements(filename)

  savename=strarr(npaths)

  for n=0,npaths-1 do begin

     restore, filename[n]

     nx=(size(data))[1]
     ny=(size(data))[2]
     nz=(size(data))[3]

     ;define effective areas of NUV and FUV slit-jaws
     if index[0].img_path eq 'SJI_5000W' or $
        index[0].img_path eq 'SJI_2832'  or $
        index[0].img_path eq 'SJI_2796'  then begin
        x0=0  & x1=1006
        y0=20 & y1=1075
     endif else begin
        x0=12 & x1=1031
        y0=20 & y1=1075
     endelse

     data=data[x0:x1,y0:y1,*]

     ;supply exposure deviation
     c=replicate(1.0, nz)

     ;supply pointing coordinates (in pixels)
     xoff=-(index.xcen - mean(index.xcen)) / index.cdelt1
     yoff=-(index.ycen - mean(index.ycen)) / index.cdelt1

     ;build mask
     mask=byte(data)*0+1
     mask[where(finite(data) ne 1)]=0
     data[where(finite(data) ne 1)]=0.0

     flat=iris_make_chae_flat(data, xoff, yoff, object=object, $
                              c=c, shift_flag=1, mask=mask)
     
     window, 0, xsize=(x1-x0+1)*2, ysize=y1-y0+1
     tv, [bytscl(flat), bytscl(object)]

     tmp=replicate(!values.f_nan, nx, ny)
     tmp[x0:x1,y0:y1]=flat
     flat=tmp

     tmp=replicate(!values.f_nan, nx, ny)
     tmp[x0:x1,y0:y1]=object
     object=tmp

     dt=index[0].date_obs
     timestring=strmid(dt, 0,4) + $ ;year
                strmid(dt, 5,2) + $ ;month
                strmid(dt, 8,2) + $ ;day
                '_' + $
                strmid(dt,11,2) + $ ;hour
                strmid(dt,14,2) + $ ;min
                strmid(dt,17,2)     ;sec

     savename[n]=datadir+timestring+'_'+index[0].img_path+'_chaeflat.sav'

     print, 'Saving level 1.5 '+index[0].img_path+' flat data in '+savename[n]
     save, flat, object, xoff, yoff, index, filename=savename[n]

  endfor

  return, savename

end
