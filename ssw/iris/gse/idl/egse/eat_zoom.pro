;
;+
;NAME:
;	eat_zoom
;PURPOSE:
;	Show EGSE Acceptance Test requirements are met
;HISTORY:
;	Written 5-May-01 by M.Morrison
;-
;
if (not keyword_set(qskip)) then begin
    today = gt_day( ut_time(), /str)
    ff = sxi_sel_images(today, /one)
    if (ff eq '') then stop
end
;
img = rfits(ff, h=head)
wdef,image=img
nx = n_elements(img(*,0))
ny = n_elements(img(0,*))
tvscl, img
;
box_cursor, x0, y0, nnx, nny
img2 = img(x0:x0+nnx-1, y0:y0+nny-1)
tvscl, congrid(img2, nx, ny)
;
nxt = 50.
nyt = 50.
thumb = congrid(img, nxt, nyt)
tvscl, thumb
xx = [x0, x0+nnx, x0+nnx, x0, x0] * nxt / nx
yy = [y0, y0, y0+nny, y0+nny, y0] * nyt / ny
plots2, xx, yy, /dev
;
end