if (n_elements(time0) eq 0) then time0 = ut_time()
if (n_elements(apid) eq 0) then apid = '0x0010'
;
input, 'Enter GMT time of desired record (NOW for current)', time0, time0
if (strupcase(time0) eq 'NOW') then time = ut_time() $
				else time = time0
;
fid = time2file(time, /date)
dir = concat_dir(getenv('GSE_PKT_FILE_DIR'), apid)
ff = file_list(dir, fid + '.' + apid)
;
infil = ff(0)
if (infil eq '') then stop, 'No input file: ' + infil
;
map = sag_rd_hk(infil, /map)                       ;read the roadmap
ptime = map.egse_time                               ;packet time
eepoch = getenv('SAG_EGSE_EPOCH')
iepoch = getenv('SAG_INSTRUMENT_EPOCH')
dectim = int2secarr(time, eepoch)
dummy = min( abs(ptime-dectim), strec)  ;find the closest record
print, infil, strec

rec = sag_rd_hk(infil, strec, strec)
rec_etime = fmt_tim(anytim2ints(eepoch, off=double(map(strec).egse_time)))
rec_lobt = rec(0).pkt_head.time
rec_itime = fmt_tim(anytim2ints(iepoch, off=double(rec(0).pkt_head.time)))
;-------
;
data = rec(0).data
out = ['SXI_DUMP_HK  Ver 1.00  Program run ' + !stime, $
	'Input time: ' + fmt_tim(time), $
	'Input file: ' + infil + '  (Record: ' + strtrim(strec,2) + ')', $
	'EGSE Packet time: ' + rec_etime, $
	'LOBT Raw: ' + string(rec_lobt, format='("0x", z8.8)'), $
	'LOBT: ' + rec_itime, $
	' ', $
	'Data displayed AFTER secondary header', $
	' ']
nplin = 20
n = n_elements(data)*1.
nlin = ceil( n/nplin )
dfmt = '(1x, i3)'
dfmt = '(1x, z2.2)'
lfmt = '(i6, 4x)'
offset = 0
fmt = '(' + lfmt + strtrim(nplin,2) + dfmt + ')'
out0 = strarr(nlin)
for i=0,nlin-1 do begin
    ist = i*nplin
    ien = (ist + nplin-1) < (n-1)
    out0(i) = string(i*nplin+offset, data(ist:ien), format=fmt)
end
out = [out, out0]
;
prstr, out
end