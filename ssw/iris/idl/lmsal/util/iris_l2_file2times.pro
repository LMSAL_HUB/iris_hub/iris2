function iris_l2_file2times,l2files, out_style=out_style
;+
;   Name: iris_l2_file2times
;
;   Purpose: quick & dirty (& fast) IRIS L2 file -> times
;
;   Input Parameters:
;      l2file - one (for now?) IRIS Level2 file names, assumed local
;
;   Keyword Parameters:
;      out_style - desired output format -> anytim ; default=CCSDS
;-
if n_elements(out_style) eq 0 then out_style='ccsds'

retval=-1 ; assume failure
if not file_exist(l2files[0]) then begin
   box_message,'input must be existing fits file
endif else begin 
   l2file=l2files ; only one for now
   mreadfits_header,l2file[0],ii,only_tags='startobs'
   if not required_tags(ii,'startobs') then begin
      box_message,"No .STARTOBS... you sure this is an IRIS L2 file?? I'm not..."
      return,retval ; EARLY EXIT on ?? input file
   endif
   next=get_fits_nextend(l2file[0]) ; assume consistent for all input l2 (sji or raster(s)) 
   edata=mrdfits(l2file,next-1,head,/silent)
   retval=anytim(anytim(ii.startobs) + reform(edata[0,*]), out_style=out_style)
endelse
return,retval
end
   
