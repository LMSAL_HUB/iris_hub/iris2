function IRIS_PREP_DARK, idata, iindex, $
	time_tab = time_tab, temp_tab = temp_tab, $
	dmsg = dmsg, dversion = dversion, tempstring = tempstring

;+
;
; Given a data array (single image) and header, removes the dark current + pedestal
; Uses Steve Saar's IRIS_MAKE_DARK
;
; INPUT:
;	idata	-	image array (2D only)
;	iindex	-	structure FITS headers
;
; OUTPUT:
;	oindex	-	a modified header with the history updated to reflect dark subtraction
;
; KEYWORDS:
;	time_tab	-	(Optional input) Set to a vector of ANYTIM time values for the
;					hour or so preceding the image t_obs. Passed through to iris_make_dark
;	temp_tab	-	(Optional input) Set to an array of temperatures corresponding
;					to the time_tab times; passed through to iris_make_dark
;	If time_tab and temp_tab are not set, then iris_make_dark will operate on the 
;	temperatures in the FITS header (if any)
;
;	ds_in	-	(Optional input) Set to the string name of the data series; 
;				useful only for preliminary	data series that had a fixed 
;				pedestal subtracted
;
;	dmsg	-	(Optional output) a string describing the dark subtraction	
;
;-


common IRIS_PREP_DARK_CB, dark
common IRIS_DARK_CB, darkstr

if (N_ELEMENTS(time_tab) gt 0) and (N_ELEMENTS(temp_tab) gt 0) then begin
	IRIS_MAKE_DARK, iindex, dark, temps, time_tab, temp_tab
endif else begin
	IRIS_MAKE_DARK, iindex, dark, temps
endelse

flip_rotate_dir = [0, 7, 5, 2]	;	map win_flip keyword to IDL's ROTATE function
flipper = flip_rotate_dir[iindex.win_flip]
dark = ROTATE(TEMPORARY(dark), flipper)

odata = idata - dark

tempstring = STRCOMPRESS(STRJOIN(STRING(temps, format = '(f7.1)'), ','), /remove_all)
dversion = darkstr.version
dmsg = STRJOIN( ['Dark v', dversion, '; T=[', tempstring, ']'], '')
	
RETURN, odata

end
