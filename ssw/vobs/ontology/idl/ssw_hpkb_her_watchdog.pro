function ssw_hpkb_her_watchdog, status_struct, nowx=nowx, post=post, login_only=login_only
;+
;   Name: ssw_hpkb_her_watchdog
;
;   Purpose: generate & upload a test event to HER and confirm readback via ssw client
;
;   Input Parameters:
;      NONE
;
;   Output:
;      Function returns overall status; 1=OK, 0=PROBLEM
;
;   Output Parameters:
;      status_struct - structure containing process status info
; 
;   Keyword Parameters:
;      nowx - optional event time - default is 'now' UT
;
;   Calling Sequence:
;      IDL> status=ssw_hpkb_her_watchdog([status_struct])
;
;   History:
;      3-December-2009 - S.L.Freeland - SSW HER piece of HPKB "heartbeat"/health monitor
;     19-December-2009 - S.L.Freeland - add explicit .LOGIN_STATUS chk/flag
;     24-Janurary-2010 - S.L.Freeland - add /POST keyword and function , /LOGIN_ONLY
;
;-=
;
;  
;
;

status_struct={process:get_caller(),runtime:'',login_status:0, ingest_status:0, readback_status:0}

if status_struct.process eq '' then begin 
   help,/trace,out=out
   status_struct.process=strextract(out(0),'At ',' ')
endif

status=0

postit=keyword_set(post)
gev=last_nelem(get_gevloc_data())
if keyword_set(nowx) then now=anytim(nowx,/ecs) else  now=reltime(/now,out='ecs')
status_struct.runtime=now
ename='her_'+time2file(now)
tenmin=reltime(now,minutes=10,out='ecs')
tenminago=reltime(now,minutes=-10,out='ecs')
gev.ename=ename
gev.date_obs=anytim(now,out='ccsds')
gev.xcen=0 & gev.ycen=0 & gev.helio='N00W00'
gev.fstart=now & gev.fpeak=now & gev.fstop=tenmin & gev.class='Z'

voe=ssw_le2voe(gev, daterun=anytim(now,/vms))
voe.optional.event_testflag='T'
voe.required.frm_humanflag='F'
voe.optional.event_expires=anytim(tenmin,/ccsds) ; force expire ~soon
voe.required.frm_name='SSW HER Watchdog Test'

; generate VOE..
outdir=concat_dir('$ssw_path','hpkb/watchdog')
export_event,voe,outdir=outdir, /write,suffix=ename,filenameout=fnout
; ingest
ssw_hpkb=get_logenv('ssw_hpkb')
if ssw_hpkb eq '' and get_user() eq 'freeland' then $
   set_logenv,'ssw_hpkb','sdo50JSOCSDO'


cgiserv='http://www.lmsal.com/cgi-ssw/ssw_service_hpkb_status.sh?'

ssw_hpkb_voeupload,fnout,/curl, status=ustat, lstatus=lstatus, $
   login_only=login_only, istatus=istatus
if get_logenv('heartbeat_check') ne '' then stop,ustat

smess=['<font color=red>Problem</font>','<font color=green>OK</font>']
status_struct.login_status=lstatus
if keyword_set(login_only) then begin 
   if postit then begin 
      query=ssw_make_query( $
                        PROCESS_ORIG="SSW HPKB Login", $
			CADENCE='10m', $
                        MESSAGE="HPKB Login "+smess(lstatus), $
                        SUBJECT="SSW HPKB Login Status", $
          		STATUS=(['Failed','Running'])(lstatus), $
          		CONTACT="freeland@lmsal.com", $
			BACKUP="kobashi", $
                        TIME_DATE=reltime(/now,out='ccsds'))
      sock_list,cgiserv + query
   endif
   box_message,'Login Only Request... returning' ; !!! unstructured Early Exit
   return,lstatus
endif 

if ustat eq 0 then begin 
   box_message,'Problem with "ingest"
endif

if ustat then begin 
status_struct.ingest_status=1  ; TBD - need status from ssw_hpkb_voeupload

; readback test
qq=ssw_hpkb_query(ssw_hpkb_make_query(tenminago, tenmin, /flare, showtests='only'))
fl=gt_tagval(qq,/fl)

if data_chk(fl,/struct) then begin 
   start=gt_tagval(fl,/event_starttime)
   dt=ssw_deltat(start,ref=now)
   ss=where(dt eq 0, sscnt)
   if sscnt ne 1 then box_message,'No event matches expected test event' else begin 
      status_struct.readback_status = 1
      box_message,'readback ok...'
   endelse
endif else box_message,'No HEK records in expected time window'
endif 

if postit then begin 
   query=ssw_make_query( $
                        PROCESS_ORIG="HER Query SSW - SF", $
			CADENCE='10m',$
                        MESSAGE="Readback "+smess(status_struct.readback_status), $
                        SUBJECT="SSW HER Readback Status", $
          		STATUS=(['Failed','Running'])(status_struct.readback_status), $
          		CONTACT="freeland@lmsal.com", $
			BACKUP="kobashi", $
                         TIME_DATE=reltime(/now,out='ccsds'))
   sock_list,cgiserv+query
   query=ssw_make_query( $
                        PROCESS_ORIG="HER Upload SSW - SF", $
			CADENCE='10m', $
                        MESSAGE="Upload " + smess(status_struct.ingest_status) , $
                        SUBJECT="SSW HER Upload Status", $
          		STATUS=(['Failed','Running'])(status_struct.ingest_status), $
          		CONTACT="freeland@lmsal.com", $
			BACKUP="kobashi", $
                        TIME_DATE=reltime(/now,out='ccsds'))
    sock_list,cgiserv+query
endif 
status=status_struct.readback_status * status_struct.ingest_status
return,status
end 



