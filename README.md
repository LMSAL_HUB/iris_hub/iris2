Full documentation about IRIS² can be found at:
     http://iris.lmsal.com/iris2/index.html

Acknowledging IRIS²:
  - Publication about IRIS²:
    https://ui.adsabs.harvard.edu/abs/2019ApJ...875L..18S/abstract
  - Publication about STiC:
    https://ui.adsabs.harvard.edu/abs/2016ApJ...830L..30D/abstract
    
Comments, questions and suggestions can be sent to:
  - Alberto Sainz Dalda: 
    asainz.solarphysics@gmail.com
  - Bart De Pontieu: 
    bdp@lmsal.com
