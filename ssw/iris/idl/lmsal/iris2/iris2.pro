function iris2, file_irisl2, lmsal=lmsal, raster=raster, $
	cpu_percentage = cpu_percentage, nosave=nosave, dir_save=dir_save, $
	name_save = name_save, show_cal=show_cal, version_db=version_db, $
	delta_mu = delta_mu, pca=pca, level=level, weights_windows=weights_windows, $
    sel_roi = sel_roi , in_parts=in_parts, my_mask=my_mask
;+ 
;
; NAME:
;   IRIS2
;
; PURPOSE:
;      To recover the model atmosphere (T, vlos, vturb, electron density) from IRIS 
;      Mg II h&k lines using IRIS^2 - or just IRIS2 - inversion code 
;      (Sainz Dalda et al., ApJL 875 L18 (2019)).
;
;      The code calibrates the data to c.g.s units [erg cm^-2 s^-1 sr^-1 Hz^-1], 
;      interpolates the RPs in the IRIS2 database to the spectral sampling and positions 
;      observed, and look for the closest RP in that database.
;     
;      It returns the model atmosphere for the IRIS Mg II h&k lines raster(s), and 
;      other information about the inversion depending on the level of information 
;      asked for.
;       
; CATEGORY:
;   Data processing/Inversion   
;
; SAMPLE CALLS:
;   IDL> raster_files = file_search(path_to_IRIS_l2+'/*iris_l2*raster*fits')
;   IDL> iris2model = IRIS2(raster_files)
;   IDL> iris2model = IRIS2(raster_files, level=2)
;
; INPUTS:
;   raster_files: an IRIS Level 2 raster file or a list of these kind of files belonging 
;      to the same raster observation, i.e. with the same observation parameters and 
;      IRIS OBSID.   
;
; OUTPUTS:
;   An iris2model structure or an array of iris2model structures as large as the 
;   list given by raster_files.  Depending on the level_info value, the structure 
;   will contain more detailed information about the way the inversion was made. See 
;   description of level_info keywords for more details.
;
; KEYWORDS:
;   cpu_percentage: A value between 1-100. A percentage of the CPUs used during the 
;      inversions. By default, 90% of the CPUs is used.
;
;   nosave: by deafult, the iris2structure is saved in the current working directory. 
;      If nosave is set (different than 0) nothing is saved.
;   
;   name_save: alternative name of the file where the output is saved. Do not include 
;      the directory. The default name is the IRIS Level 2 where 'iris_l2' has been 
;      changed to 'iris2model_l2'. If the input was a list of rasters, then 
;      'raster_xxxxx' is changed to 'raster_multi'.
;
;   dir_save: directory where the output file is saved instead of the current working 
;      directory (default).
;
;   delta_mu: delta in mu value with respect to the mu value of the observation considered 
;      during the inversion, e.g. for an observation at mu = 0.73 and delta_mu = 0.2, 
;      IRIS2 code only considers those RPs coming from observations in the IRIS2 
;      database located in 0.53 < mu_in_db < 0.93.  If delta_mu=1 (defult), then 
;      all the RPs in IRIS2 database are considered.
;
;   version_db: Currently (20190420) there are 2 version of IRIS2 database. The default 
;      value is the latest IRIS2 database, up to date v2.0, which has ~50,000 RPs/RMAs 
;      /RFs, that come from the clustering of 305 observations in 160 RPs per data set 
;      . IRIS2 database v1.0 has ~15,000 RPs/RMAs/RFs, which come from the clustering 
;      of 250 observations in 60 RPs per data set. The accepted values are:
;         - 'v1.0': for the version 1.0 of IRIS2 database.
;         - 'v2.0': (default) for the version 2.0 of IRIS2 database.
;
;   pca: If not 0, the observed data will be projected on the PCA components of the 
;      IRIS2 database. The number of eigenvectors used are the number passed through 
;      this keyword if it is larger than 20. This method determine the closest RP in 
;      the IRIS2 database to an observed profile calculting the distance between the 
;      eigenvalues of the observed profile projected on the PCA components of the IRIS2 
;      database and the eigenvalues of the RPs in the IRIS2 database.  
;
;   level_info: level of information stored in the iris2model structure. It has three 
;      possible values:
;      Level 0: Default option. The iris2model structure will contain:
;         - filename: the raster(s) filename(s) inverted.
;         - inv_db_fits: FITS file containing the RPs in the IRIS2 database.  
;         - model_db_fits: FITS file containing the Representative Model Atmospheres 
;              (RMAs) of IRIS2 database.
;         - model: model atmospher recovered by IRIS2. It consist of T, vlos, vturb, 
;	      and electron density.
;         - model_dimension_info: information of the dimensions in model. They are: 
;              [X [px], Y [px], log(\tau), physical_variable]
;         - physical_variable: physical variable and units stored in the model.
;	      These are: ['T [k]', 'v_los [m/s]', 'v_turb [m/s]', 'n_e [cm^-3]']
;         - uncertainty: uncertainty of the model, given in the same units that in 
;              model.
;         - uncertainty_dimension_info: information of the dimensions in uncertainty. 
;	      These are: [X [px], Y [px], log(\tau), uncertainty_in_physical_variable]
;         - uncertainty_variable: uncertainty for each variable in the model: 
;	      These are: ['unc_T [k]', 'unc_v_los [m/s]', 'unc_v_turb [m/s]', 'unc_n_e 
;	      [cm^-3]']
;         - rf_db_fits: a list with the files containing the response function for 
;             T, v_los, v_turb, and n_e.
;         - nodes_db_fits: a list with of files containing the nodes used to calculated
;             the RFs for T, v_los, v_turb, and n_e.
;         - chi2: chi2 map calculates as Eq 1 in Sainz Dalda et al., ApJL 875 L18 (2019).
;         - header: header corresponding to the original IRIS_lev2 raster file.
;         - mu: mu of the observed data
;         - delta_mu_db: interval in mu (cos(theta), being theta the heliocentric angle)
;               used to look for RPs in the IRIS2 database. 
;         - level_info: level of information demanded via level keyword: 0 (default), 1 or 2.
;         - qc_flag: quality control flag: 
;	       0 - No raster file, nothing is inverted 
;              1 - inversion completed succesfully 
;	      -1 - Something went wrong, none data were inverted.
;   Level 1: the information obtained with Level 0 plus:
;         - nodes_positions: a structure with the values of the nodes used to calculated
;             the RFs for T, v_los, v_turb, and n_e.
;         - nodes_values_in_ltau: a structure with the values of the nodes in optical
;             depth (log(tau)) used to calculated
;             the RFs for T, v_los, v_turb, and n_e.
;         - wl: original wavelengths sampled in the IRIS observations used during 
;              the IRIS2 inversion.
;         - iris_cal: datacube with the original data calibrated in c.g.s units [] 
;	      in the wavelengths in wl.
;         - inv_iris2: datacube with the RP in the IRIS2 database that matches best 
;              the observed profile.  It is interpolated to the wavelengths in wl.
;   Level 2: the information obtained with Level 1 plus:
;         - mask: A binary mask with value 1 in those pixels considered in the inversion.
;         - n_w_mask: number of pixels in the mask with value 1, i.e., number of 
;              observed profiles inverted.
;         - map_index_db: a map with the index of the RPs in the IRIS2 database that 
;              best matches the observed profile in that pixel.
;         - map_mu_db: a map with the mu value corresponding to the RPs in the IRIS2 
;              database that best matches the observed profile in that pixel.
;         - factor: structure containing all the information used for the calibration.
;         - partial_execution_time: execution time taken for the inversion of an 
;              input raster.
;         - total_execution_time: execution time taken for the inversion of all input 
;              rasters.
;         - threads_used: CPUs or threads used during the inversion.
;	  - batch_size: batch size used to split the observed data during the inversion.
;
; AUTHOR: 
;    A. Sainz Dalda (asainz.solarphysics@gmail.com)
; 
; DATE: 
;    - First version updated to SolarSoft: 2019/05/15
;
;- 


;;; Loading the IRIS2 database
version='latest'  ;;; Latest version of IRIS2 database.
iris_db = getenv('SSWDB')
donde_db = iris_db+'/iris/iris2/'
if keyword_set(version_db) then version = version_db 
donde_fits  = donde_db+version   ;;; Location of the IRIS2 database. 
inv_db_fits = (find_file(donde_fits+'/inv_mu_iris2.*.fits'))[0]
model_db_fits = (find_file(donde_fits+'/mod_iris2.*.fits'))[0]
rf_temp_fits = (find_file(donde_fits+'/rf_temp_n*.fits'))[0]
rf_vlos_fits = (find_file(donde_fits+'/rf_vlos_n*.fits'))[0]
rf_vturb_fits = (find_file(donde_fits+'/rf_vturb_n*.fits'))[0]
rf_nne_fits = (find_file(donde_fits+'/rf_nne_n*.fits'))[0]
nodes_temp_fits = (find_file(donde_fits+'/nodes_temp_n*.fits'))[0]
nodes_vlos_fits = (find_file(donde_fits+'/nodes_vlos_n*.fits'))[0]
nodes_vturb_fits = (find_file(donde_fits+'/nodes_vturb_n*.fits'))[0]
nodes_nne_fits = (find_file(donde_fits+'/nodes_nne_n*.fits'))[0]
rf_files = [rf_temp_fits, rf_vlos_fits, rf_vturb_fits, rf_nne_fits]
nodes_files = [nodes_temp_fits, nodes_vlos_fits, nodes_vturb_fits, nodes_nne_fits]

print
print, 'Loading IRIS2 database...'
print, 'Location of IRIS2 database: ', donde_fits
print, 'Inverted IRIS2 RPs: ', inv_db_fits
print, 'IRIS2 RMAs: ', model_db_fits
print, 'IRIS2 nodes: ', nodes_temp_fits, nodes_vlos_fits, nodes_vturb_fits, nodes_nne_fits
print, 'IRIS2 RFs: ', rf_temp_fits, rf_vlos_fits, rf_vturb_fits, rf_nne_fits
print

if inv_db_fits eq '' or model_db_fits eq '' or (where(rf_files eq ''))[0] ne -1 or (where(nodes_files eq ''))[0] ne -1 then begin
   print,""
   print,"****************************** WARNING ******************************"
   print,""
   print,"Some or all of the files of the IRIS2 database have not been found."
   print,""
   print,"These files shold be present in the IRIS2 database:"
   print," - inv_mu_iris2.vN.M (N and M refers to the number of version and revision respectively) "
   print," - mod_iris2.vN.M "
   print," - nodes_var_nPP_iris2.vN.M (4 files, with 'var' being: temp, vlos, vturb, nne, and nPP the number of nodes)"
   print," - rf_var_nPP_iris2.vN.M (4 files, with 'var' being: temp, vlos, vturb, nn, and nPP the number of nodese)"
   print,""
   print,"Please, verify that the IRIS2 database from the SolarSoft distribution is upgraded in your local"
   print,"distribution. More details can be found at: http://iris.lmsal.com/iris2/index.html"
   print,""
   print,"Nothing has been done."
   print,""
   print,"****************************** WARNING ******************************"
   print,""
   return, -1
endif

;;; CPU usage
save_cpu = !CPU
cpu_pctg = 90
if keyword_set(cpu_percentage) then cpu_pctg = min([cpu_percentage>0, 100])
TPOOL_NTHREADS = round(!cpu.HW_NCPU*cpu_pctg/100.)
batch_size = 3500.
TPOOL_MIN_ELTS = min([batch_size-1., TPOOL_NTHREADS*100.])
CPU, TPOOL_NTHREADS = TPOOL_NTHREADS, TPOOL_MIN_ELTS = TPOOL_MIN_ELTS

n_files  = 1
txt_multi = ''
total_execution_time = 0.
list_mask_arrays = LIST()
list_mask_data = LIST()

;;; Loading tghe file(s)
if keyword_set(lmsal) eq 1 then begin

   iris_date_obsid = lmsal

   year  = strmid(iris_date_obsid, 0,4)
   month = strmid(iris_date_obsid, 4,2)
   day   = strmid(iris_date_obsid, 6,2)
   
   raster_num = 0
   if keyword_set(raster) then raster_num = raster
   raster_num_str = string(fix(raster_num),format='(I05)')

   l2_sp_file = '/irisa/data/level2/'+year+'/'+month+'/'+day+'/'+iris_date_obsid+'/iris_l2_'+iris_date_obsid+'_raster_t000_r'+raster_num_str+'.fits'
   name_sav = 'iris2model_'+iris_date_obsid+'_raster_t000_r'+raster_num_str+txt_multi+'.sav'

endif else begin

    l2_sp_file = file_irisl2
    dim_l2_sp_file = size(l2_sp_file)
    pos_ref = strpos(l2_sp_file[0], 'iris_l2_')+strlen('iris_l2_')
    year  = strmid(l2_sp_file[0], pos_ref+0,4)
    month = strmid(l2_sp_file[0], pos_ref+4,2)
    day   = strmid(l2_sp_file[0], pos_ref+6,2)
    time_obs = strmid(l2_sp_file[0], pos_ref+9,6)
    iris_obsid = strmid(l2_sp_file[0], pos_ref+16,10)
    iris_date_obsid = strmid(l2_sp_file[0], pos_ref,26)
    raster_num_str = strmid(l2_sp_file[0], pos_ref+40,5)
    name_sav = 'iris2model_'+iris_date_obsid+'_raster_t000_r'+raster_num_str+txt_multi+'.sav'

    if n_elements(file_irisl2) gt 1 then begin
       txt_multi = 'multi'
       n_files = dim_l2_sp_file[1]
       name_sav = 'iris2model_'+iris_date_obsid+'_raster_t000_'+txt_multi+'.sav'
       sel_roi = 0 ; No selection mask on a list of files! 
       in_parts = 0
       my_mask = 0
    endif   
endelse


;;; Where the ouptut will be saved
dir2save = './'
if keyword_set(dir_save) then dir2save=dir_save
if keyword_set(name_save) then name_sav=name_save
output_filename = name_sav
name_sav = dir2save+name_sav
print, 'Output file: ', name_sav
print

;;; Run the code for n_files files
for j_files = 0, n_files-1 do begin

    l2_sp_file_j = l2_sp_file[j_files]	
    
    qc_flag =0 

    if file_exist(l2_sp_file_j) then begin
    
    	exec_iris2  = tic()
    	exec_cal = tic()
    
        print
    	print, 'WARNING: Using '+string(TPOOL_NTHREADS)+' CPUs  when a computation requires more than '+string(TPOOL_MIN_ELTS)+' elements.'
        print
    	print, 'Loading... '+l2_sp_file_j 
    	;tic

	;;; Getting the information, the data, and the calibration information
    	info =  get_info_irisl2(l2_sp_file_j, /factor, /mgii_only, nosave=1)
        
    	; if info.factor.TEXP_MEAN_MG_II_K_2796 le 0. then qc_flag = -1
     
    	if qc_flag ne -1 then begin             ;;; qc_flag condition
           data_mgii = info.factor
           header = info.index
           dim_data_ori =  size(data_mgii.DATA_MG_II_K_2796)
           ;toc
           
           ;;; Blocking windows for weights
           cont_level = 3.454e-6
           noi_stic = 5e-3 
           blocked_windows = [[2794.7, 2794.95], $
           		   [2796.75, 2797.10], $
           		   [2797.66, 2797.845], $
           		   [2798.13, 2798.40], $
           		   [2798.55, 2798.75], $
           		   [2799.01, 2799.45], $
           		   [2799.720726606522, 2800.0261722831183], $
           		   [2800.38, 2800.5861560159883], $
           		   ; [2800.7643326562147, 2801.3243163762049], $
           		   ; [2800.8661478787576, 2801.0697783228743], $
           		   ; [2801.1715935444486, 2801.4006777918103], $
			   [2800.695, 2801.4006777918103], $
           		   [2804.1242348299002, 2804.3787728590805], $
           		   [2804.4296804646751, 2804.73], $
           		   [2804.81, 2804.9642103185765], $
           		   [2805.0151179232489, 2805.193294538969]]

           ;;; Mg II h&k and Mg UV triplet positions		   
           mgII_k_pos_k3 = 2795.528
           mgII_h_pos_h3 = 2802.704
           mgII_uv_triplet_pos23 = 2797.930 + (2797.998 - 2797.930)
           linewidth = 1.

           ;;; Wavelengths in the air
           wl_air = vac2air(data_mgii.wl_mg_ii_k_2796)

	   ;;; Show calibration or not 
           if keyword_set(show_cal) then begin
                      plot, wl_air, total(total(data_mgii.DATA_MG_II_K_2796, 2, /nan),2, /nan)
              ver, mgII_k_pos_k3
              ver, mgII_h_pos_h3
              ver, mgII_uv_triplet_pos23
           endif   
           

           ;;; The weights in the right place		   
	       dim_wl_air = size(wl_air)
           dim_bw = size(blocked_windows)
	       weights = wl_air*0.
           pw = 1e16
	       count_blocks = 0
	       for l = 0, dim_wl_air[1]-1 do begin
               cond = ~(wl_air[l] ge blocked_windows[0,count_blocks] and wl_air[l] le blocked_windows[1, count_blocks])
	           if cond then begin 
		           if wl_air[l] ge blocked_windows[1, count_blocks] and count_blocks ne dim_bw[2]-1 then count_blocks+=1
		           ; Any spectral position in other spectral range different that k, l, and UV ones.
		           ; All points. However, after the following lines,  these values are mainly given 
		           ; to locations in the bump.
		           weight_window = -4.  
		           ; Blue wing of k line
		           if count_blocks eq (where(2794. lt blocked_windows[1,*]))[0] then weight_window = -1. 
		           ; k line and its red wing
		           if count_blocks eq (where(mgII_k_pos_k3 lt blocked_windows[1,*]))[0] then weight_window = -1. 
		           ; UV triplet 
		           if count_blocks eq (where(mgII_uv_triplet_pos23 lt blocked_windows[1,*]))[0] then weight_window = -2. 
		           ; h line including its blue and red wings 
		           if count_blocks eq (where(mgII_h_pos_h3 lt blocked_windows[1,*]))[0] then weight_window = -3. 
		           weights[l] = weight_window
               endif
               ; print, l, cond, count_blocks, wl_air[l], blocked_windows[0,count_blocks], blocked_windows[1, count_blocks], weights[l]
           endfor 
           ;;; Creating spectral and spatial masks
           aux =  total(data_mgii.DATA_MG_II_K_2796>0., 3, /nan)

           w = where(aux gt 0)
           mask_spectra = aux*0 - 1
           mask_spectra[w] = 1
           if keyword_set(show_cal) then tvscl, mask_spectra
           ;
           mean_mask_profile = mean(mask_spectra, dim=2)
           median_mask_spectra = median(mean_mask_profile>0)
           w_pos = (mean_mask_profile >= median_mask_spectra*.9)
           w_neg = (mean_mask_profile <= median_mask_spectra*1.1)
           ind = where(mean_mask_profile ge median_mask_spectra*.9 and mean_mask_profile le median_mask_spectra*1.1)
           ;
           pos_first_wl_data = ind[0]
           pos_last_wl_data  = ind[-1]
           flag_spectral_range = 0
           pos_first_wl = (where(wl_air ge 2794.))[0]
           if pos_first_wl ne -1 then begin
              pos_first_wl = pos_first_wl[0]
           endif else begin
              pos_first_wl =  0
              flag_spectral_range = -1
           endelse 
           ;
           if pos_first_wl_data gt pos_first_wl then  pos_first_wl = pos_first_wl_data
           pos_last_wl = (where(wl_air gt 2805.))[0]
           if pos_last_wl ne -1 then begin
              pos_last_wl = pos_last_wl[0]
           endif else begin
              pos_last_wl =  (size(wl_air))[1]-1
              flag_spectral_range = +1
           endelse
           if pos_last_wl_data lt pos_last_wl then  pos_last_wl = pos_last_wl_data
           first_wl = wl_air[pos_first_wl]
           last_wl = wl_air[pos_last_wl]
           if keyword_set(show_cal) then  begin
              plot, (wl_air)[pos_first_wl:pos_last_wl], total(total((data_mgii.DATA_MG_II_K_2796)[pos_first_wl:pos_last_wl,*,*], 2, /nan),2, /nan)
              ver, mgII_k_pos_k3
              ver, mgII_h_pos_h3
              ver, mgII_uv_triplet_pos23
           endif 	
           ;
           ima_ref_mask = mean((data_mgii.DATA_MG_II_K_2796)[pos_first_wl:pos_last_wl, *,*], dim=1)
           mask = ima_ref_mask*0. -1.
           w= where(ima_ref_mask ge  0.)
           mask[w] = 1.
           number_total_masks = 0
           if keyword_set(show_cal) then tvscl, mask
           ;
           ;if keyword_set(sel_roi) then user_mask = mask_image(transpose(mask*ima_ref_mask))
           dim_mask = size(mask)
           mask_ok = reform(mask, dim_mask[1]*dim_mask[2])
           w_mask = where(mask_ok eq  1.)
	       ; Number of elements to be inverted
	       n_w_mask = n_elements(w_mask)
           ;if keyword_set(show_cal) then begin
           	  ;plot, (wl_air)[pos_first_wl:pos_last_wl], total(total((data_mgii.DATA_MG_II_K_2796)[pos_first_wl:pos_last_wl,*,*], 2, /nan),2, /nan)
              ;ver, mgII_k_pos_k3
              ;ver, mgII_h_pos_h3
              ;ver, mgII_uv_triplet_pos23
           ;endif 	
           wl_sel = wl_air[pos_first_wl:pos_last_wl]

           ; Default list of masks
           mask_ori = mask
           dim_mask_ori = size(mask)
           name_sav_ori = name_sav
           list_mask_arrays.Add, mask
           list_mask_data.Add,  {xb:0, nxb:dim_mask[1], yb:0, nyb:dim_mask[2]} 
           number_total_masks = 1

           if keyword_set(my_mask) then begin
              dim_my_mask = size(my_mask)
              if dim_my_mask[1] eq dim_mask_ori[2] and dim_my_mask[2] eq dim_mask_ori[1] then begin
                 list_mask_arrays = LIST()
                 list_mask_data = LIST()
                  if dim_my_mask[0] eq 2 then begin 
                      mask =  mask_ori*transpose(my_mask)
                      list_mask_arrays.Add, mask
                      list_mask_data.Add,  {xb:0, nxb:dim_mask_ori[1], yb:0, nyb:dim_mask_ori[2]} 
                  endif
                  if dim_my_mask[0] eq 3 then begin 
                      for j_my_mask = 0, dim_my_mask[3]-1 do begin
                          mask =  mask_ori*transpose(my_mask[*,*,j_my_mask])
                          list_mask_arrays.Add, mask
                          list_mask_data.Add,  {xb:0, nxb:dim_mask_ori[1], yb:0, nyb:dim_mask_ori[2]} 
                      endfor 
                  endif
                  number_total_masks = list_mask_arrays.Count()
             endif else begin
                  number_total_masks = 0
             endelse 
           endif 

           if keyword_set(sel_roi) then begin
              user_mask = mask_image(transpose(mask*ima_ref_mask)) 
              list_mask_arrays = user_mask.list_mask_arrays 
              list_mask_data = user_mask.list_mask_data
              number_total_masks = list_mask_arrays.Count() 
              txt_number_mask = string(fix(number_total_masks), format='(I02)')
              ; For consistency with in_parts and default case
              ;stop
              for j_mask = 0, number_total_masks-1 do begin
                  aux = list_mask_data[j_mask]
                  list_mask_data[j_mask]= {xb:aux.yb, nxb:aux.nyb, yb:aux.xb, nyb:aux.nxb}
                  ;(list_mask_data[j_mask]).xb = aux.yb
                  ;(list_mask_data[j_mask]).nxb = aux.nyb
                  ;(list_mask_data[j_mask]).yb = aux.xb
                  ;(list_mask_data[j_mask]).nyb = aux.nxb
                  list_mask_arrays[j_mask] = transpose(list_mask_arrays[j_mask])
              endfor
              mask_ori = mask
              dim_mask_ori = size(mask)
           endif 

           if keyword_set(in_parts) then begin
              list_mask_arrays = LIST()
              list_mask_data = LIST()
              if in_parts le 1 then begin
                 number_total_masks = 3
              endif else begin    
                 number_total_masks = in_parts
              endelse 
              mask_ori = mask
              dim_mask_ori = size(mask)
              ;size_parts = ceil(dim_mask[1]/float(number_total_masks))
              size_parts = ceil(dim_mask[2]/float(number_total_masks))
              for j_parts = 0, number_total_masks-1 do begin
                  mask_part = mask*0.
                  ;mask_part[j_parts*size_parts:(j_parts+1)*size_parts<(dim_mask[1]-1),*]=1.
                  mask_part[*,j_parts*size_parts:((j_parts+1)*size_parts)<(dim_mask[2]-1)]=1.
                  list_mask_arrays.Add, mask_part*mask_ori
                  ; list_mask_data.Add, {xb:j_parts*size_parts, nxb:((j_parts+1)*size_parts)<(dim_mask[1]-1) - (j_parts*size_parts),yb:0, nyb:dim_mask[2]}
                  list_mask_data.Add, {xb:0, nxb:dim_mask[1], yb:j_parts*size_parts, nyb:(((j_parts+1)*size_parts)<(dim_mask[2])) - (j_parts*size_parts)}
              endfor     
              txt_number_mask = string(fix(number_total_masks), format='(I02)')
           endif

           txt_number_mask = string(fix(number_total_masks), format='(I02)')


           for j_mask = 0, number_total_masks-1 do begin

               ; Reading the calibrated data
               if keyword_set(sel_roi) or keyword_set(in_parts) or keyword_set(my_mask) then begin
                   mask =  mask_ori*list_mask_arrays[j_mask]
                   mask_ok = reform(mask, dim_mask_ori[1]*dim_mask_ori[2])
                   w_mask = where(mask_ok eq  1.)
                   if ~keyword_set(my_mask) then w_mask = where(w_mask ge 0)
	               n_w_mask = n_elements(w_mask)
                   xb = list_mask_data[j_mask].xb
                   nxb = list_mask_data[j_mask].nxb
                   yb = list_mask_data[j_mask].yb
                   nyb = list_mask_data[j_mask].nyb
                   data_sel = (data_mgii.DATA_MG_II_K_2796)[pos_first_wl:pos_last_wl,xb:(xb+nxb-1),yb:(yb+nyb-1)]
                   dim_data_ori = size(data_sel)
                   txt_j_mask = string(fix(j_mask+1), format='(I02)')
                   if keyword_set(sel_roi) then txt_mask = '_roi_'
                   if keyword_set(in_parts) then txt_mask = '_part_'
                   if keyword_set(my_mask) then txt_mask = '_mymask_'
                   name_sav = (string(name_sav_ori)).Replace('.sav', txt_mask+txt_j_mask+'_'+txt_number_mask+'.sav')
               endif else begin     
                   data_sel = (data_mgii.DATA_MG_II_K_2796)[pos_first_wl:pos_last_wl, *, *]
               endelse     
               dim_data_sel = size(data_sel, /dim)
               ; Calculating the noise
	           print, ''
               print, 'Calculating noise... '
	           noise_data = data_sel*0
	           factor_flux2photon  = (data_mgii.FACTOR_MG_II_K_2796[pos_first_wl:pos_last_wl]/data_mgii.DN2PHOTON_MG_II_K_2796)
	           for i_noise = 0, dim_data_sel[2]-1 do begin 
	               for j_noise = 0, dim_data_sel[1]-1 do begin 
		           noise_data[*,j_noise,i_noise] = sqrt(data_sel[*,j_noise,i_noise]*factor_flux2photon/data_mgii.TEXP_ARR_MG_II_K_2796[i_noise])
                   endfor
               endfor
               
	           ; Noise data [wl, Y, X] 
	           noise_data = noise_data/cont_level

               data_sel = reform(data_sel, dim_data_sel[0], dim_data_sel[1]*dim_data_sel[2])
               data_sel = data_sel[*,w_mask]
               dim_data_sel = size(data_sel, /dim)
               data_sel =  data_sel/cont_level

	           ; Comes from the blocking windows
	           weights_mask_labels = weights           ; Step-function in lambda with labels 0,-1,-2,-3,-4 corresponding to other, k, UV, and h Mg II lines 
	           weights_mask_values = weights*0.+1d16   ; Step-function in lambda with the weights_windows values in the corresponding weighting window
	           weights_mask_labels = weights_mask_labels[pos_first_wl:pos_last_wl] 
	           weights_mask_values = weights_mask_values[pos_first_wl:pos_last_wl] 
	           weights_mask_01 = weights_mask_labels
	           weights_mask_01[where(weights_mask_labels lt 0.)] = 1.
               weights2noise_2D = rebin(weights_mask_values, dim_data_sel[0], dim_data_ori[2], dim_data_ori[3])
	
	           wei_uv = fltarr(dim_data_ori[2], dim_data_ori[3])
	           wei_k = wei_uv 
	           wei_h = wei_uv 
	           wei_o = wei_uv 
        
               ; Default option is weights/noise
               weights_mask_values = weights_mask_values*0.+1.
               flag_weights_windows = 0
	           if keyword_set(weights_windows) eq 1 then begin
                  if n_elements(weights_windows) eq 1 then begin
	                 if weights_windows eq 1 then begin 
                        flag_weights_windows = 1		 
	                    weights_windows = [1.,1.,1.,1.]
                     endif 
                  endif   
                  if n_elements(weights_windows) eq 4 then begin 
	                 weights_mask_values = weights_mask_01
	                 weights_mask_values[where(weights_mask_labels eq 0.)] = 1e-16
	                 weights_mask_values[where(weights_mask_labels eq -1.)] = float(weights_windows[0])
	                 weights_mask_values[where(weights_mask_labels eq -2.)] = float(weights_windows[1])
	                 weights_mask_values[where(weights_mask_labels eq -3.)] = float(weights_windows[2])
	                 weights_mask_values[where(weights_mask_labels eq -4.)] = float(weights_windows[3])
                     if flag_weights_windows ne 1 then flag_weights_windows = 4		 
		             weights_values_txt = strcompress(weights_windows, /rem)
	              endif 	       
               endif  
         
;stop
	  ;;print, ''
          ;print, 'Calculating noise per profile... '
;          for j_noise = 0, dim_data_ori[2]-1 do begin 
;	      for i_noise = 0, dim_data_ori[3]-1 do begin 
;		     w_weights_mask_k = where(weights_mask_labels eq -1.)
;		     w_weights_mask_uv = where(weights_mask_labels eq -2.)
;		     w_weights_mask_h = where(weights_mask_labels eq -3.)
;		     w_weights_mask_other = where(weights_mask_labels eq -4.)
;		     val_wei_k  = 0.
;		     val_wei_uv = 0.
;		     val_wei_h = 0.
;		     val_wei_o = 0.
;		     if w_weights_mask_uv[0] ne -1 then begin
;                        val_wei_uv = mean(noise_data[w_weights_mask_uv,j_noise,i_noise], dim=1, /nan)
;        	     endif    
;		     if w_weights_mask_k[0] ne -1 then begin
;	                val_wei_k = mean(noise_data[w_weights_mask_k,j_noise,i_noise], dim=1, /nan)
;        	     endif    
;		     if w_weights_mask_h[0] ne -1 then begin 
;	                val_wei_h = mean(noise_data[w_weights_mask_h,j_noise,i_noise], dim=1, /nan)
;        	     endif    
;		     if w_weights_mask_other[0] ne -1 then begin
;	                val_wei_o = mean(noise_data[w_weights_mask_other,j_noise,i_noise], dim=1, /nan)
;        	     endif    
;		     ; w = where([val_wei_k, val_wei_h, val_wei_uv] ne 0., n_val)
;                     ;weights2noise_2D[*, j_noise,i_noise] = weights_mask_values/((val_wei_k + val_wei_uv + val_wei_h)/float(n_val))
;		     mean_noise = [val_wei_k, val_wei_h, val_wei_uv]
;		     w = where(finite(mean_noise))
;		     if w[0]  ne -1 then begin
;		        mean_noise = mean(mean_noise[w])
;                        if mean_noise ne 0. then weights2noise_2D[*, j_noise,i_noise] = weights_mask_values/mean_noise
;	             endif 		
;              endfor
;           endfor
;	   weights = rebin(weights2noise_2D,  dim_data_sel[0], dim_data_ori[2]*dim_data_ori[3])
;	   weights = weights[*,w_mask]
;
	    
;            w_weights_mask_k = where(weights_mask_labels eq -1.)
;            w_weights_mask_uv = where(weights_mask_labels eq -2.)
;            w_weights_mask_h = where(weights_mask_labels eq -3.)
;            w_weights_mask_other = where(weights_mask_labels eq -4.)
;	    val_wei_val_wei_uv = 0.
;	    val_wei_val_wei_k = 0.
;	    val_wei_val_wei_h = 0.
;	    val_wei_val_wei_o = 0.
;	    num_intervals = 0.
;	    if w_weights_mask_uv[0] ne -1 then begin
;               val_wei_uv = mean(noise_data[w_weights_mask_uv,*,*], dim=1, /nan)
;	       num_intervals+=1. 
;            endif    
;	    if w_weights_mask_k[0] ne -1 then begin
;	       val_wei_k = mean(noise_data[w_weights_mask_k,*,*], dim=1, /nan)
;	       num_intervals+=1. 
;            endif    
;	    if w_weights_mask_h[0] ne -1 then begin 
;               val_wei_h = mean(noise_data[w_weights_mask_h,*,*], dim=1, /nan)
;	       num_intervals+=1. 
;       	    endif    
;	    if w_weights_mask_other[0] ne -1 then begin
;               val_wei_o = mean(noise_data[w_weights_mask_other,*,*], dim=1, /nan)
;       	    endif    
;	    noise_per_profile_2D = (val_wei_k + val_wei_uv + val_wei_h)/num_intervals
;	    w = where(~finite(noise_per_profile_2D))
;	    noise_per_profile_2D[w] = 1e16
;	    noise_per_profile_1D = rebin(noise_per_profile_2D, dim_data_ori[2]*dim_data_ori[3])
;	    ; weights2noise_2D = (1./noise_per_profile_1D)##weights_mask_values
;	    weights2noise_2D = weights_mask_values#(1./noise_per_profile_1D)
;	    weights2noise_3D = rebin(weights2noise_2D, dim_data_sel[0], dim_data_ori[2], dim_data_ori[3]) ; lambda, X, Y
;	    weights2noise_2D = weights2noise_2D[*,w_mask]                                                 ; lamnda, sel_data_XY


	           noise_data_2D = reform(noise_data, dim_data_sel[0], dim_data_ori[2]*dim_data_ori[3])
	           delvar, noise_data
	           weights2noise_2D = rebin(weights_mask_values, dim_data_sel[0], dim_data_ori[2]* dim_data_ori[3])
	           weights2noise_2D = weights2noise_2D/noise_data_2D
	           delvar, noise_data_2D
	           weights2noise_3D = reform(weights2noise_2D, dim_data_sel[0], dim_data_ori[2], dim_data_ori[3]) ; lambda, X, Y
	           weights2noise_2D = weights2noise_2D[*,w_mask]                                                 ; lamnda, sel_data_XY
       
	           print, ''
	           print, 'The weight/noise has been calculated.'
       
	           case flag_weights_windows of
	              0 : weights_windows_txt = 'full spectral information (default) ' 
	              1 : weights_windows_txt = 'weights equals to 1 over the averaged noise per profile.'
	              4 : weights_windows_txt = 'weights equals to '+weights_values_txt[0]+', '+weights_values_txt[1]+', '+$
		                                weights_values_txt[2]+', '+weights_values_txt[3]+' over constant noise per profiles x ' 
	           endcase       

            
               if keyword_set(show_cal) then begin
                  plot, wl_sel, data_sel[*,100]*weights_mask_values[pos_first_wl:pos_last_wl]
                  ver, mgII_k_pos_k3
                  ver, mgII_h_pos_h3
                  ver, mgII_uv_triplet_pos23
                  plot, wl_sel, data_sel[*,5000], ychar=1.5
                  ver, mgII_k_pos_k3
                  ver, mgII_h_pos_h3
                  ver, mgII_uv_triplet_pos23
               endif
           
               ;;; Reading and interpolating inverted profiles in IRIS2 database
               inv_db = readfits(inv_db_fits, /silent)
               inv = inv_db[0:472,*]
	           inv = inv/cont_level
               mu_inv = abs(inv_db[473,*]-info.mu) 
	           ;;; For multimu
	           ;inv = inv/3.454e-6 
	           ;mu_inv =inv_db[0,*]*0. + info.mu
	           ;;; For multimu
               mu_interval = 1.
	           if keyword_set(delta_mu) then mu_interval =  delta_mu
               w_mu_interval = where(mu_inv le mu_interval, cuan_mu)
               inv = inv[*,w_mu_interval]
               wl_inv = dindgen(473)*0.0254599992186+2793.17909767
               dim_inv = size(inv)
               inv_ok = dblarr(dim_data_sel[0], dim_inv[2]) 
               for i = 0, dim_inv[2]-1 do inv_ok[*, i] = interpol(inv[*,i], wl_inv, wl_sel)
               if keyword_set(show_cal) then begin
                  plot, wl_inv, inv[*,100]
                  oplot, wl_sel, inv_ok[*,100], psym=2
               endif   


               ;;; Looking for the closest profile in IRIS2 database... or where the magic happens
               closest_pos = dblarr(dim_data_sel[1])
               inv_iris2 = fltarr(dim_data_sel[0], dim_data_ori[2]*dim_data_ori[3])
               map_pos = fltarr(dim_data_ori[2]*dim_data_ori[3])
               map_mu  = fltarr(dim_data_ori[2]*dim_data_ori[3])
               str = 'Partial progress bar: '
               offset = str.Strlen()        ; 22
               bar = strarr(30+offset) ;+"·"
               for j =0, offset-1 do bar[j] = str.CharAt(j)
               bar[offset+0] = '['
               bar[offset+1] = '>'
               bar[offset+29] = ']'
               str = 'Total progress bar: '
               offset_total = str.Strlen()   ; 20
               bar_total = strarr(30+offset) ;+"·"
               for j =0, offset_total-1 do bar_total[j] = str.CharAt(j)
               bar_total[offset_total+0] = '['
               bar_total[offset_total+1] = '>'
               bar_total[offset_total+29] = ']'

               ;;; PCA 
               n_pca = -1 
               if keyword_set(pca) then begin
                  aux= inv_ok ;data_sel; inv_ok
                  mypca, transpose(aux), eval, evec
                  ;n_pca  = 50 ;mincomp99[0]
                  n_pca = pca>20 
                  print, 'Using PCA coefs: ', n_pca
                  project_pca, transpose(aux), eval, evec, n_pca, inv_pca, inv_coefs
                  inv_coefs = transpose(inv_coefs)
                  project_pca, transpose(data_sel), eval, evec, n_pca, obs_pca, obs_coefs
                  obs_coefs = transpose(obs_coefs)
               endif 
             
               ;;; Where the weights/noise is evaluated  
               w_01 = where(weights_mask_01 eq 1.)

	           ;;; Number of iterations for the look-up table process. 
               num_iter = ceil(dim_data_sel[1]/batch_size)-1
    
               ;;; Looking for the closest RP in IRIS2 db, either for the full profile 
	           ;;; or in the PCA space. 
	           print, ''
	           print, 'Here is where the magic happens!...'
               time_calib = toc(exec_cal)
               if n_files eq 1 then time_calib = 0.
               for j = 0, num_iter do begin
                   if j eq 0 then eta = tic()
                   ini = j*batch_size
                   fin = min([(j+1)*(batch_size)-1, dim_data_sel[1]-1])
	               if keyword_set(pca) eq 0 then begin 
		              if flag_weights_windows eq 0 then begin
	                     if j eq 0 then begin
			                print,''     
	                        print, 'Considering '+weights_windows_txt
                            ;print, mean(weights[*,ini:fin], dim=2, /nan)			
			                print,''     
	                    endif		
                        dist_mio = calc_dist(data_sel[*, ini:fin],inv_ok, mio=1)
                      endif else begin
	                    if j eq 0 then begin
			                print,''     
			                if flag_weights_windows eq 1 then begin
	                           print, 'Considering '+weights_windows_txt, weights_windows
                            endif else begin
	                           print, 'Considering '+weights_windows_txt, weights_windows
		                    endelse   
                            print, mean(weights2noise_2D[*,ini:fin], dim=2, /nan)			
			                print,''     
	                    endif		
		                ;if flag_weights_windows eq 0 then $
                        ;dist_mio = calc_dist(data_sel[w_01, ini:fin],inv_ok[w_01,*], mio=1)
		                if flag_weights_windows eq 1 or flag_weights_windows eq 4 then begin 
		                   aux_data_sel = data_sel[*, ini:fin]*rebin(weights_mask_values, dim_data_sel[0], fin-ini+1)
	                       aux_data_sel = 	aux_data_sel[w_01,*]
		                   if j eq 0 then begin 
		                      aux_inv_ok = inv_ok*rebin(weights_mask_values, dim_data_sel[0], dim_inv[2])
		                      aux_inv_ok = aux_inv_ok[w_01,*]
                 	       endif 
                           dist_mio = calc_dist(aux_data_sel, aux_inv_ok, mio=1)
            	        endif          
                      endelse    
                   endif else begin  
                      dist_mio = calc_dist(obs_coefs[*, ini:fin], inv_coefs, mio=1)
                   endelse 
                   ;;; Looking for the closest
                   w = min(dist_mio, aux, dim=2)
                   closest_pos[ini:fin] = w_mu_interval[aux/ (fin-ini+1.)]   ;;; 1st dimesion of dist_mio 
                   inv_iris2[*,w_mask[ini:fin]] = inv_ok[*, aux/ (fin-ini+1.)]  ;;; inv_ok is already referred to inv[w_mu_interval]
                   map_pos[w_mask[ini:fin]] = closest_pos[ini:fin] 
                   ;;; Creating the progress bar
                   pos_bar = min([fix(j/float(num_iter)*28.), 28])
                   pos_bar_total = min([fix(j_files/float(n_files)*28.), 28])
                   pos_ini_pbar = offset+1
                   pos_ini_pbar_total = offset_total+1
                   if pos_bar gt 1 then begin
                      pos_fin_pbar = offset+pos_bar-1
                      bar[pos_ini_pbar:pos_fin_pbar] = '='
                      bar[pos_fin_pbar+1] = '>'
                      pos_ini_bar = pos_fin_pbar+1
                   endif   
                   if pos_bar_total gt 1 then begin
                      pos_fin_pbar_total = offset_total+pos_bar_total-1
                      bar_total[pos_ini_pbar_total:pos_fin_pbar_total] = '='
                      bar_total[pos_fin_pbar_total+1] = '>'
                      pos_ini_bar_total = pos_fin_pbar_total+1
                   endif   
	               if j eq 0 then begin
		              partial_time = toc(eta)   
                      print
                      print,'--------------------------------------------------------------------------------'
                      print,'------------------- Inverting data '+strcompress(j_files+1)+' of '+strcompress(n_files)+' with IRIS2 -------------------------'
                      print
                      print, 'Matching '+strcompress(n_w_mask)+ ' with '+strcompress(cuan_mu)+' profiles in the IRIS2 database'
                      print
		              if n_files gt 1 then begin
                         print, 'Estimated time to get the results [s]: ', (partial_time*num_iter+time_calib)*(n_files-j_files) ; - total_execution_time
                         print, string(13b), bar_total, format='(a, 50a1)' ; 50 = offset_total+30
                         print, 'Estimated time to get the partial results [s]: ', partial_time*num_iter+time_calib
                         print, string(13b), bar, format='(a, 52a1, $)' ; 52 = offset+30
         	          endif else begin      
                         print, 'Estimated time to get the results [s]: ', (partial_time*num_iter+time_calib)*(n_files-j_files) ; - total_execution_time
                      endelse 
	               endif 
                   print, string(13b), bar, format='(a, 52a1, $)' ; 52 = offset+30
               endfor
               print
               ;;; End of Looking for the closest RP in IRIS2 db, either for the full profile 
	           ;;; or in the PCA space. 
           
               ;;; Reading the model atmospheres in IRIS2 database
               model = readfits(model_db_fits, /silent)
               dim_model =  size(model, /dim)
               model_out = fltarr(dim_model[0], dim_model[1], dim_data_sel[1])
               for j = 0, dim_data_sel[1]-1 do model_out[*,*,j] = model[*,*,closest_pos[j]]
               ;
               model_rebuilt = fltarr(dim_model[0], dim_model[1], dim_data_ori[2]*dim_data_ori[3])
               model_rebuilt[*,*,w_mask] = model_out
               model_rebuilt = reform(model_rebuilt, dim_model[0], dim_model[1], dim_data_ori[2], dim_data_ori[3])
               model_rebuilt = transpose(model_rebuilt, [3,2,1,0])
          
	           ;;; Building the original calibrated data from the selected(masked) data. 
               iris_cal = fltarr( dim_data_sel[0], dim_data_ori[2]*dim_data_ori[3])
               iris_cal[*, w_mask] = data_sel
    
	           ;;; Degrees of freedom: number of nodes used in the last cycle of the inversion -> nodes in T=7,
	           ;;; nodes in vlos = 4, nodes in vturb =4. nne, pgas, rho... and derived from T, therefore do not contribute to deg_free
	           ltau = (findgen(39)*.2-7.6)
	           nodes_temp = readfits(nodes_temp_fits, /silent)
	           nodes_vlos = readfits(nodes_vlos_fits, /silent)
	           nodes_vturb = readfits(nodes_vturb_fits, /silent)
	           nodes_nne = readfits(nodes_nne_fits, /silent)
	           nodes_positions = {nodes_temp:nodes_temp, nodes_vlos:nodes_vlos, nodes_vturb:nodes_vturb, nodes_nne:nodes_nne} 
	           nodes_values = {nodes_temp_ltau:ltau[nodes_temp], nodes_vlos_ltau:ltau[nodes_vlos], nodes_vturb_ltau:ltau[nodes_vturb], nodes_nne_ltau:ltau[nodes_nne]} 
               deg_freedom = n_elements(nodes_temp) + n_elements(nodes_vlos) + n_elements(nodes_vturb)
          
	           ;;; Calculating the chi2-map.
	           print, ''
	           print, 'Calculating the chi^2... '
	           print, ''
               chi2 = (iris_cal - inv_iris2)
               chi2[*, w_mask] =  chi2[*, w_mask]*(weights2noise_2D)
	           if flag_weights_windows ne 0 then chi2 = chi2[w_01,*]
	           dim_chi2 = size(chi2) 
               chi2 = chi2*chi2
               chi2 = total(chi2, 1, /nan)/float(dim_chi2[1]-deg_freedom)
               chi2 = reform(chi2,  dim_data_ori[2], dim_data_ori[3])
    
               ;;; Calculating the uncertainties 
               ;;; Reading integrated RF
	           print, ''
	           print, 'Calculating the uncertainties:'
               num = rebin(chi2*float(dim_chi2[1]-deg_freedom), dim_data_ori[2] ,dim_data_ori[3], dim_model[1], dim_model[0])
	           ;;; Reading the RFs for Temp
               den_4 = fltarr(dim_model[0], dim_model[1], dim_data_sel[1])
	           print, ''
	           print, ' - Reading the file with the response function for the temperature... '
	           rf_temp = readfits(rf_temp_fits, /silent)
               dim_rf_temp = size(rf_temp)
               rf_temp_ok  = dblarr(dim_data_sel[0], dim_rf_temp[2], dim_rf_temp[3])
	           print, '   Calculating the uncertainties for the temperature... '
               for j = 0, dim_rf_temp[3]-1 do begin
                 for i = 0, dim_rf_temp[2]-1 do begin   
                   rf_temp_ok[*,i,j] = interpol(rf_temp[*,i,j], wl_inv, wl_sel)
                   endfor   
               endfor   
               den_temp = fltarr(dim_rf_temp[2], dim_data_sel[1])
               for j = 0, dim_data_sel[1]-1 do begin
                  for  k = 0, dim_rf_temp[2]-1 do begin
                   aux_rf = rf_temp_ok[w_01,k,closest_pos[j]]*weights2noise_2D[w_01,j]
                   aux_rf = aux_rf*aux_rf   
                   den_temp[k,j] = total(aux_rf, /nan)
                   endfor
                   den_4[0,*,j] = interpol(den_temp[*,j], ltau[nodes_temp], ltau)
               endfor
               delvar, rf_temp, rf_temp_ok, den_temp, aux_rf
               ;;; Reading the RFs for vlos
               print, ''
               print, ' - Reading the file with the response function for the v_los... '
               rf_vlos = readfits(rf_vlos_fits, /silent)
                   dim_rf_vlos = size(rf_vlos)
                   rf_vlos_ok  = dblarr(dim_data_sel[0], dim_rf_vlos[2], dim_rf_vlos[3])
               print, '   Calculating the uncertainties for the vlos... '
               for j = 0, dim_rf_vlos[3]-1 do begin
                   for i = 0, dim_rf_vlos[2]-1 do begin   
                   rf_vlos_ok [*,i,j] = interpol(rf_vlos[*,i,j], wl_inv, wl_sel)
                   endfor   
               endfor   
               den_vlos = fltarr(dim_rf_vlos[2], dim_data_sel[1])
               for j = 0, dim_data_sel[1]-1 do begin
                  for  k = 0, dim_rf_vlos[2]-1 do begin
                   aux_rf = rf_vlos_ok[w_01,k,closest_pos[j]]*weights2noise_2D[w_01,j]
                   aux_rf = aux_rf*aux_rf   
                   den_vlos[k,j] = total(aux_rf, /nan)
                   endfor
                   den_4[1,*,j] = interpol(den_vlos[*,j], ltau[nodes_vlos], ltau)
               endfor
               delvar, rf_vlos, rf_vlos_ok, den_vlos, aux_rf
               ;;; Reading the RFs for vtrub 
               print, ''
               print, ' - Reading the file with the response function for the v_turb... '
               rf_vturb = readfits(rf_vturb_fits, /silent)
                   dim_rf_vturb = size(rf_vturb)
                   rf_vturb_ok  = dblarr(dim_data_sel[0], dim_rf_vturb[2], dim_rf_vturb[3])
               print, '   Calculating the uncertainties for the vturb... '
               for j = 0, dim_rf_vturb[3]-1 do begin
                   for i = 0, dim_rf_vturb[2]-1 do begin   
                   rf_vturb_ok [*,i,j] = interpol(rf_vturb[*,i,j], wl_inv, wl_sel)
                   endfor   
               endfor   
               den_vturb = fltarr(dim_rf_vturb[2], dim_data_sel[1])
               for j = 0, dim_data_sel[1]-1 do begin
                  for  k = 0, dim_rf_vturb[2]-1 do begin
                   aux_rf = rf_vturb_ok[w_01,k,closest_pos[j]]*weights2noise_2D[w_01,j]
                   aux_rf = aux_rf*aux_rf   
                   den_vturb[k,j] = total(aux_rf, /nan)
                   endfor
                   den_4[2,*,j] = interpol(den_vturb[*,j], ltau[nodes_vturb], ltau)
               endfor
               delvar, rf_vturb, rf_vturb_ok, den_vturb, aux_rf
               ;;; Reading the RFs for nne
               print, ''
               print, ' - Reading the file with the response function for the nne... '
               rf_nne = readfits(rf_nne_fits, /silent)
               dim_rf_nne = size(rf_nne)
               rf_nne_ok  = dblarr(dim_data_sel[0], dim_rf_nne[2], dim_rf_nne[3])
               print, '   Calculating the uncertainties for the nne... '
               for j = 0, dim_rf_nne[3]-1 do begin
                   for i = 0, dim_rf_nne[2]-1 do begin   
                   rf_nne_ok [*,i,j] = interpol(rf_nne[*,i,j], wl_inv, wl_sel)
                   endfor   
               endfor   
               den_nne = fltarr(dim_rf_nne[2], dim_data_sel[1])
               for j = 0, dim_data_sel[1]-1 do begin
                  for  k = 0, dim_rf_nne[2]-1 do begin
                   aux_rf = rf_nne_ok[w_01,k,closest_pos[j]]*weights2noise_2D[w_01,j]
                   aux_rf = aux_rf*aux_rf   
                   den_nne[k,j] = total(aux_rf, /nan)
                   endfor
                   den_4[3,*,j] = interpol(den_nne[*,j], ltau[nodes_nne], ltau)
               endfor
               delvar, rf_nne, rf_nne_ok, den_nne, aux_rf
               ; Denominator for uncertainty
                   den = fltarr(dim_model[0], dim_model[1], dim_data_ori[2]*dim_data_ori[3])
                   den[*,*,w_mask] = den_4
               delvar, den_4
                   den = reform(den, dim_model[0], dim_model[1], dim_data_ori[2] ,dim_data_ori[3])
                   den = transpose(den, [2,3,1,0])
                   unc = (2./deg_freedom)*(num/den)
                   unc = sqrt(unc)
                   unc = transpose(unc, [1,0,2,3])
               delvar, num, den 

               ;stop
                
               ;;; Reshaping the original calibrated data and inverted data to a cube. 
                   iris_cal = reform(iris_cal, dim_data_sel[0], dim_data_ori[2], dim_data_ori[3])
                   iris_cal = transpose(iris_cal, [2,1,0])
                   inv_iris2 = reform(inv_iris2, dim_data_sel[0], dim_data_ori[2], dim_data_ori[3])
                   inv_iris2= transpose(inv_iris2, [2,1,0])
                   level_info = 0

               ;;; Execution time 
                   partial_execution_time = toc(exec_iris2)
                   total_execution_time = total_execution_time + partial_execution_time 
                   print
                   if n_files gt 1 then print, 'Partial execution time (including the time for the calibration and calculation of the uncertainties) [s]: ', partial_execution_time
                   print, 'Total execution time (including the for the calibration and calculation of the uncertainties) [s]: ', total_execution_time
                   print
                   print,'----------------------- Inverting data with IRIS2 ------------------------------'
                   print,'--------------------------------------------------------------------------------'
                   print
                   qc_flag = 1 

               ;;; Outpout depending on the level information 
                   if keyword_set(level) then begin
                      level_info = level 
                      if level eq 1 then begin
                         iris2model = {input_filename:l2_sp_file_j, $
                         output_filename:output_filename, $     
                         inv_db_fits:inv_db_fits, model_db_fits:model_db_fits, $
                         rf_db_fits:rf_files, $
                         nodes_db_fits:nodes_files, $
                         nodes_positions:nodes_positions, $
                         nodes_values_in_ltau:nodes_values,$
                         wl:(wl_air)[pos_first_wl:pos_last_wl], $
                         ltau:(findgen(39)*.2-7.6), $
                         model:model_rebuilt, $
                         model_dimension_info:['[X [px], Y [px], log(\tau), physical_variable]'], $
                         physical_variable_in_model:['T [k]', 'v_los [m/s]', 'v_turb [m/s]', 'n_e [cm^-3]'], $
                         uncertainty:unc, $
                         unc_dimension_info:['[X [px], Y [px], log(\tau), uncertainty_in_physical_variable]'], $
                         uncertainty_variable:['unc_T [k]', 'unc_v_los [m/s]', 'unc_v_turb [m/s]', 'unc_n_e [cm^-3]'], $
                         iris_cal:iris_cal*cont_level, $
                         units_iris_cal: '[erg cm^-2 s^-1 sr^-1 Hz^-1]', $
                         iris2_inv:inv_iris2*cont_level, $
                         units_iris2_inv: '[erg cm^-2 s^-1 sr^-1 Hz^-1]', $
                         chi2:transpose(chi2,[1,0]), $
                         weight2noise: transpose(weights2noise_3D, [2,1,0]), $
                         weighting_windows_01:weights_mask_01, $
                         weights_in_windows:weights_mask_values, $
                         pca_components:n_pca, $
                         header:header, mu:info.mu, delta_mu_db:mu_interval, $
                         level_info:level_info, qc_flag:qc_flag}
                      endif
                      if level eq 2 then begin
                         iris2model = {input_filename:l2_sp_file_j, $ 
                         output_filename:output_filename, $     
                         inv_db_fits:inv_db_fits, model_db_fits:model_db_fits, $
                         rf_db_fits:rf_files, $
                         nodes_db_fits:nodes_files, $
                         nodes_positions:nodes_positions, $
                         nodes_values_in_ltau:nodes_values,$
                         ltau:(findgen(39)*.2-7.6), $
                         wl:(wl_air)[pos_first_wl:pos_last_wl], $
                         mask_ondata:transpose(mask_ori, [1,0]), $
                         mask_extra:transpose(list_mask_arrays[j_mask]), $
                         mask_extra_data:{xb:list_mask_data[j_mask].yb, nxb:list_mask_data[j_mask].nyb, $
                                          yb:list_mask_data[j_mask].xb, nyb:list_mask_data[j_mask].nxb}, $
                         ima_ref:transpose(ima_ref_mask), $
                         ima_ref_mask:transpose(ima_ref_mask+ima_ref_mask*list_mask_arrays[j_mask]), $
                         n_w_mask: n_w_mask, $
                         n_rp_db_used: cuan_mu, $
                         model:model_rebuilt, $
                         model_dimension_info:['[X [px], Y [px], log(\tau), physical_variable]'], $
                         physical_variable:['T [k]', 'v_los [m/s]', 'v_turb [m/s]', 'n_e [cm^-3]'], $
                         uncertainty:unc, $
                         uncertainty_dimension_info:['[X [px], Y [px], log(\tau), uncertainty_in_physical_variable]'], $
                         uncertainty_variable:['unc_T [k]', 'unc_v_los [m/s]', 'unc_v_turb [m/s]', 'unc_n_e [cm^-3]'], $
                         iris_cal:iris_cal*cont_level, $
                         units_iris_cal: '[erg cm^-2 s^-1 sr^-1 Hz^-1]', $
                         iris2_inv:inv_iris2*cont_level, $
                         units_iris2_inv: '[erg cm^-2 s^-1 sr^-1 Hz^-1]', $
                         chi2:transpose(chi2,[1,0]), $
                         weight2noise: transpose(weights2noise_3D, [2,1,0]), $
                         weighting_windows_01:weights_mask_01, $
                         weights_in_windows:weights_mask_values, $
                         flag_weights_windows:flag_weights_windows, $
                         pca_components:n_pca, $
                         map_index_db:transpose(reform(map_pos, dim_data_ori[2],dim_data_ori[3]), [1,0]), $
                         map_mu_db:transpose(reform(map_mu, dim_data_ori[2],dim_data_ori[3]), [1,0]), $
                         header:header, mu:info.mu, delta_mu_db:mu_interval, $
                         factor:info.FACTOR, $
                         partial_execution_time:partial_execution_time, $
                         total_execution_time:total_execution_time, $
                         threads_used:TPOOL_NTHREADS, batch_size:batch_size, level_info:level_info, qc_flag:qc_flag}
                      endif
                   endif else begin
                         iris2model = {input_filename:l2_sp_file_j, $
                         output_filename:output_filename, $
                         inv_db_fits:inv_db_fits, model_db_fits:model_db_fits, $
                         rf_db_fits:rf_files, $
                         nodes_db_fits:nodes_files, $
                         ltau:(findgen(39)*.2-7.6), $
                         model:model_rebuilt, $
                         model_dimension_info:['[X [px], Y [px], log(\tau), physical_variable]'], $
                         physical_variable:['T [k]', 'v_los [m/s]', 'v_turb [m/s]', 'n_e [cm^-3]'], $
                         uncertainty:unc, $
                         uncertainty_dimension_info:['[X [px], Y [px], log(\tau), uncertainty_in_physical_variable]'], $
                         uncertainty_variable:['unc_T [k]', 'unc_v_los [m/s]', 'unc_v_turb [m/s]', 'unc_n_e [cm^-3]'], $
                         chi2:transpose(chi2,[1,0]), $
                         weighting_windows_01:weights_mask_01, $
                         weights_in_windows:weights_mask_values, $
                         pca_components:n_pca, $
                         header:header, mu:info.mu, delta_mu_db:mu_interval, level_info:level_info, qc_flag:qc_flag}
                   endelse       
                     
                   if n_files gt 1 then begin
                      if j_files eq 0 then aux_iris2model = replicate(iris2model, n_files)
                      aux_iris2model[j_files] = iris2model
                   endif
            
                   
                   if keyword_set(sel_roi) or  keyword_set(in_parts) then begin
                      ;;; Saving the output
                      if keyword_set(nosave) ne 1 then begin
                         print,'--------------------------------------------------------------------------------'
                         print, '-------------------------------- WARNING ---------------------------------------'
                         print,''
                         print,'Saving model atmosphere, chi2, uncertainties, and other things in:'
                         print, name_sav
                         print,''
                         print,'--------------------------------------------------------------------------------'
                         print,'--------------------------------------------------------------------------------'
                         save, iris2model, filename = name_sav
                      endif
                   endif 

                endfor ;;; for j_mask


                endif else begin       ;;; qc_flag condition

                   ;;; Nothg has been done.		
                   print, '--------------------------------------------------------------------------------'
                   print, '-------------------------------- WARNING ---------------------------------------'
                   print,''
                   print, 'The mean exposure time for this raster is 0.'
                   print, 'Nothing has been done.'
                   print, ''
                   print,''
                   print, '--------------------------------------------------------------------------------'
                   print, '--------------------------------------------------------------------------------'
                   if n_files gt 1 then begin
                     iris2model = aux_iris2model[j_files-1]
                     iris2model.qc_flag = -1
                     iris2model.model = iris2model.model*0.
                     iris2model.uncertainty = iris2model.uncertainty*0.
                     if level_info gt 0 then begin
                         iris2model.inv_iris2 = iris2model.inv_iris2*0.
                         iris2model.chi2 = iris2model.chi2*0.
                         if level_info eq 2 then begin
                            iris2model.map_mu = iris2model.map_mu*0.-1.
                            iris2model.map_pos= iris2model.map_pos*0.-1.
                         endif
                     endif 
                     aux_iris2model[j_files] = iris2model
                   endif else begin
                     iris2model = -1
                     nosave =  1
                   endelse
            
                endelse                ;;; qc_flag condition

            endif else begin
                  
               ;;; None file(s) was/were matching  
                   print, '--------------------------------------------------------------------------------'
                   print, '-------------------------------- WARNING ---------------------------------------'
                   print,''
                   print, 'None file matches that date and IRIS OBSID: '+iris_date_obsid
                   print, 'Please verify the date and IRIS OBSID values.'
                   print, 'IRIS team wants to make you feel happy, but you have to put from your side :)'
                   print,''
                   print, '--------------------------------------------------------------------------------'
                   print, '--------------------------------------------------------------------------------'
                   iris2model = -1
               qc_flag = -1
                   
            endelse 


endfor
    	
if n_files gt 1 then begin
   iris2model = aux_iris2model 
   delvar, aux_iris2model
endif   
    
;;; Saving the output    
if keyword_set(nosave) ne 1 and keyword_set(sel_roi) eq 0 then begin
   print,'--------------------------------------------------------------------------------'
   print, '-------------------------------- WARNING ---------------------------------------'
   print,''
   print,'Saving model atmosphere, chi2, uncertainties, and other things in:'
   print, name_sav
   print,''
   print,'--------------------------------------------------------------------------------'
   print,'--------------------------------------------------------------------------------'
   save, iris2model, filename = name_sav
endif

    
;;; Restoring CPU initial profile
cpu, restore=save_cpu

;;; Returning the output structure
return, iris2model
    
end
