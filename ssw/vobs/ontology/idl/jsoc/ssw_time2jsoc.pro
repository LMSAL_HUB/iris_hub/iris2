function ssw_time2jsoc, times, tai=tai, ut=ut, jsoc2time=jsoc2time, out_style=out_style 
;+
;   Name: ssw_time2jsoc
;
;   Purpose: any SSW time/time vector -> JSOC YYYY.MM.DD_HHh:MMm:SSs[_{TAI,UT}] or reverse
;
;   Input Parameters:
;     times - any SSW time/time vector (or JSOC times if /jsoc2time set)
;
;   Output:
;      function returns corresponding JSOC style record names
;
;   Keyword Parameters:
;      tai, ut, - optionally, add jsoc _UT or _TAI suffix
;      jsoc2time (switch)  
;
;   History:
;      23-Jun-2008 - S.L.Freeland - likely useful SSW->JSOC helper
;       6-mar-2009 - S.L.Freeland - add /JSOC2TIME keyword and function
;      23-mar-2011 - S.L.Freeland - fix typo (thanks Hyeowon Jung & Pete Schuck)
;       4-sep-2014 - S.L.Freeland - tweak for series where time is last, not first key (all the *harp* for example)
;       9-sep-2014 - S.L.Freeland - oops, 4-sep tweak broke a couple of series - kludge for now to repair 
;                                   need a better fix for this
;-

common ssw_time2jsoc_blk, first

if keyword_set(jsoc2time) then begin ; assume JSOC time input
   jtimes=times
   if strpos(jtimes[0],'][20') ne -1 then jtimes='['+ssw_strsplit(jtimes,'[',/tail)
   ss=where(strpos(jtimes,'[') ne -1 and strpos(jtimes,']') ne -1, bcnt)
   if bcnt gt 0 then jtimes(ss)=strextract(jtimes,'[',']')
   ssn=where(strmid(jtimes,0,1) eq '-',ncnt)
   if ncnt gt 0 then begin 
      if n_elements(first) eq 0 then box_message,'Negative time in Interval structure'
      jtimes(ssn)=strmid(jtimes(ssn),1,1000)
      jtimes(ssn)=anytim( (['15-apr-2010',reltime(/now,out='yoh')])(ssn eq 1),/ccsds)
   endif
   suff=str2arr('_UT,_TAI')
   for i=0,n_elements(suff)-1 do begin 
      sss=where(strpos(jtimes,suff(i)) ne -1,scnt)
      if scnt gt 0 then jtimes(sss)=ssw_strsplit(jtimes(sss),suff(i),/head)
   endfor
   if strpos(jtimes(0),'T') eq -1 then begin
      dates=ssw_strsplit(jtimes,'_',/head,tail=stimes)
      jsoctimes=str_replace(dates,'.','/') + 'T' + stimes ; default=CCSDS
   endif else jsoctimes=jtimes
   if n_elements(out_style) eq 0 then out_style='CCSDS'
   hms=str2arr('h,m,s')
if get_logenv('check') ne '' then stop,'jsoctimes'
   for i=0,2 do jsoctimes=str_replace(jsoctimes,hms(i),' ')
   
   jsoctimes=strcompress(jsoctimes,/remove)
   sst=where(strmid(jsoctimes,0,1) eq 'T',tcnt)
   if tcnt gt 0 then jsoctimes(sst)=strmid(jsoctimes(sst),1,1000)
   jsoctimes=anytim(jsoctimes,out_style=out_style)
endif else begin
   ecs=anytim(times,/ecs,/truncate) ; univerisal ssw translator->ECS format

   case 1 of 
      keyword_set(ut): suffix='_UT'
      keyword_set(tai): suffix='_TAI'
      else: suffix=''
   endcase

   cols=str2cols(ecs,' ',/trim)
   strtab2vect,cols,jsdates,jstimes
   jsdates=str_replace(jsdates,'/','.')
   jstimes=strmid(jstimes,0,2) + 'h:' + strmids(jstimes,3,2) + 'm:' + $
   strmid(jstimes,6,2) +'s'
   jsoctimes=jsdates+'_'+jstimes+suffix
endelse
first=1

return,jsoctimes
end
   



