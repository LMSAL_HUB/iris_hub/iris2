

PRO iris_fit_viewer, windata, fitdata, _extra=extra


;+
; NAME:
;      IRIS_FIT_VIEWER
;
; PURPOSE:
;      This is a wrapper routine for calling EIS_FIT_VIEWER with IRIS
;      data-sets. 
;
; CATEGORY:
;      IRIS; Gaussian fitting; display.
;
; CALLING SEQUENCE:
;      IRIS_FIT_VIEWER, Windata, Fitdata
;
; INPUTS:
;      Windata:   Structure in the format produced by IRIS_GETWINDATA.
;      Fitdata:   Structure in the format produced by IRIS_AUTO_FIT.
;
; OPTIONAL INPUTS:
;      See the header for EIS_FIT_VIEWER.
;	
; KEYWORD PARAMETERS:
;      See the header for EIS_FIT_VIEWER.
;
; OUTPUTS:
;      Creates a GUI for inspecting Gaussian fit parameters.
;
; OPTIONAL OUTPUTS:
;      See the header for EIS_FIT_VIEWER.
;
; CALLS:
;      EIS_FIT_VIEWER.
;
; MODIFICATION HISTORY:
;      Ver.1, 22-May-2018, Peter Young.
;-

IF n_params() LT 2 THEN BEGIN
  print,'Use:  IDL> iris_fit_viewer, windata, fitdata'
  return
ENDIF 

;
; Here I set the intensity units that will be displayed by eis_fit_viewer. 
;
CASE trim(windata.units) OF
  'erg cm^-2 s^-1 sr^-1 pixel^-1': BEGIN
    units=[ 'erg cm!u-2!n s!u-1!n sr!u-1!n pix!u-1!n', $
            'erg cm!u-2!n s!u-1!n sr!u-1!n']
  END
  ELSE: BEGIN
    units=[ 'DN pix!u-1!n', $
            'DN' ]
  END
ENDCASE

;
; Call to eis_fit_viewer.
;
eis_fit_viewer, windata, fitdata, _extra=extra, units=units

END
