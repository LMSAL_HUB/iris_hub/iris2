;
;
;V1.00 20-Nov-99
;V1.01  11-Mar-00 (MDM) - Adjusted ranges to be ok
;V1.02	26-Mar-01 (MDM) - Removed hardwired lfft dir
;V1.03	18-Jan-02 (MDM) - Changed output dir to be the tmp dir
;
if (keyword_set(qskip)) then goto, skip
set_plot,'x
;
if (n_elements(date) eq 0) then date = ut_time()
nimg = 8
infil = sxi_stol2files(date, 'lfft_cam_clocks', nimg, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
if (n_elements(infil) ne nimg) then stop, 'Not enough images'

mreadfits, infil, head, img
pr_stats, img, 3, img_info
;
skip:
break_file, infil, dsk_log, dir, filnam, ext
list = filnam+ext+'  ' + get_infox(head, 'EGSE_TIM, naxis1, naxis2')
;
xsiz = intarr(nimg)+528
ysiz = intarr(nimg)+588
amin = [ 50,  20,  50,  50,  100,  20,  50,  100]
amax = [600, 300, 600, 600, 1500, 600, 600, 1500]
stat = strarr(nimg) + '  '
for i=0,nimg-1 do begin
    if (head(i).naxis1 ne xsiz(i)) then stat(i) = stat(i) + 'X_ERR_SIZ '
    if (head(i).naxis2 ne ysiz(i)) then stat(i) = stat(i) + 'Y_ERR_SIZ '
    if (img_info(i).avg lt amin(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_LOW '
    if (img_info(i).avg gt amax(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_HI  '
end
ss = where(strmid(stat,2) eq '', nss)
if (nss ne 0) then stat(ss) = stat(ss) + 'OK'
;
imgstr = get_infox(img_info, 'min,max,avg,dev')
out = ['LFFT_CAM_CLOCKS  Ver 1.03  Program run: ' + !stime, $
	'On machine: ' + get_host(), $
	' ', $
	' File/Image             EGSE Time               NX        NY      Min      Max      Avg      Dev  Stat', $
	;19991117_052554.fit  17-NOV-99  05:25:54       528       588     0.00  4095.00  1069.61   245.03  OK
	' ', $
	list + '  ' + imgstr + stat]

prstr, out
if (keyword_set(qstop)) then stop
outfil = getenv('SXI_LFFT_DIR') + '/tmp/lfft_cam_clocks_' + filnam(0) + '.txt
prstr, out, fil=outfil
sprint, outfil, /land
;
outdir = getenv('SXI_LFFT_DIR') + '/tmp/
disp12, infil, 'SXI run of LFFT_CAM_CLOCKS ' + filnam(0), option = 'STDEV', $
        hc=concat_dir(outdir, 'cam_clocks_'+filnam(0)+'disp12.ps'), $
        gif=concat_dir(outdir, 'cam_clocks_'+filnam(0)+'disp12.gif')

;
end
