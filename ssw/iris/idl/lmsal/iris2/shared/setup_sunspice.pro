;+
; Project     :	Multimission
;
; Name        :	SETUP_SUNSPICE
;
; Purpose     :	Set up the SunSPICE package.
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	Initializes the SunSPICE package by defining the environment
;               variable SSw_SUNSPICE_GEN and calling REGISTER_SUNSPICE_DLM.
;               Usually called from an instrument's IDL_STARTUP file.
;
; Syntax      :	SETUP_SUNSPICE
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	SET_LOGENV, CONCAT_DIR, REGISTER_SUNSPICE_DLM
;
; Common      :	None.
;
; Env. Vars.  : None
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 8-Aug-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro setup_sunspice
;
set_logenv, 'SSW_SUNSPICE_GEN', concat_dir('SSW_SUNSPICE','data')
register_sunspice_dlm
;
end
