function ssw_iris_struct2xml, strxml, xml_original=xml_original, $
   out_filename=out_filename, overwrite=overwrite
;+
;    Name: ssw_iris_struct2xml
;
;   Purpose: convert iris xml-structure -> xml array; optionally write .xml file
;
;   Input Parameters:
;      strxml - iris xml-structure like ssw_iris_xml2struct.pro output
;               
;   Output:
;      function returns xml-ready string array analog of input structure
;
;   Keyword Parameters:
;      xml_original - xml file (or strarr) from Original 
;      out_filename=out_filename - optional filename for write
;      overwrite (flag) - if set, overwrite existing OUT_FILENAME without comment
;
;   Calling Sequence:
;      xmlarr=ssw_iris_struct2xml(strxml [,xml_original=xml_original] )
;
;   History:
;      15-Jan-2013 - S.L.Freeland
;      25-Mar-2013 - S.L.Freeland - eliminate inner loop / use insertion instead of concatenation (+order mag faster for Large xml/structs)
;
;   Restrictions/Notes:
;      Best results if original XML data is availble to this routine
;      to allow header/trailer mimicry
;      supply either via strxml.XMLDATA (from ssw_iris_xml2struct(/ADD_XML..)
;      or XML_ORIGINAL keyword (xml file or string array contents)
;      
;-

if not data_chk(strxml,/struct) then begin 
   box_messge,'Need structure input... (hence name=..struct2xml.pro)
   return,''
endif
;
; check for original xmldata...

case 1 of 
   required_tags(strxml,/xmldata) : xmldata=strxml.xmldata ; structure.XMLDATA
   file_exist(xml_original): xmldata=rd_tfile(xml_original)
   data_chk(xml_original,/string): xmldata=xml_original
   else: box_message,'No original XML available; - you may have to add header/trailer 
endcase

things=tag_names(strxml)
things=things(rem_elem(things,'XMLDATA')) ; ignore value-added tags
cthings=strcapitalize(things)

header=''
trailer=''

if n_elements(xmldata) gt 0 then begin ; xml derived header/trailer
   ss0=where(strpos(xmldata,cthings(0)) ne -1)
   sse=where(strpos(xmldata,last_nelem(cthings)) ne -1)
   header=xmldata[0:ss0(0)-1]
   trailer=xmldata[last_nelem(sse)+1:*]
   tpad=strpad('',(strpos(xmldata[ss0(0)],'<'+cthings(0)))) ; mimic indentation
endif else tpad='  '

retval=header
for t=0,n_elements(things) -1 do begin 
   tthing=strxml.(t)
   tn=tag_names(tthing)
   ctn=strcapitalize(tn)
   txpad=tpad+tpad
   ntags=n_tags(tthing)
   lpe=ntags+2  ; lines per thing
   nthings=n_elements(tthing)
   tarray=strarr(lpe*nthings)
   t0=lindgen(nthings)*lpe
   tl=(lindgen(nthings)*lpe + lpe)-1
   tarray[t0]=tpad + '<'+cthings(t)+'>'
   tarray[tl]=tpad + '</'+cthings(t)+'>'
   for tx=0,n_elements(ctn)-1 do begin 
      ss=lindgen(nthings)*lpe+(tx+1)
      pnam=strmid(ctn[tx],0,strlen(ctn[tx])-is_member(ctn[tx],'repeat_',/ignore)) ; un-escape - add if required
      if n_elements(xmldata) gt 1 then begin ; param/case patch
            patt='<'+pnam+'>'
            ssp=where(strpos(xmldata,patt) ne -1, pcnt)
            if pcnt eq 0  then begin ; mimic parameter string-case verbatim
               ssp=where(strpos(strupcase(xmldata),strupcase(patt)) ne -1, pcnt)
               if pcnt gt 0 then pnam=strextract(xmldata[ssp[0]],'<','>')
            endif
      endif
      tarray[ss]= txpad + '<'+pnam + '>' + tthing.(tx) + '</' + pnam + '>' 
   endfor  
   retval=[retval,tarray]
endfor  
if trailer(0) ne '' or n_elements(trailer) gt 1 then $
   retval=[retval,trailer] ; glue original trailer

if keyword_set(out_filename) then begin
   overwrite=keyword_set(overwrite)
   if overwrite or (1-file_exist(out_filename)) then file_append,out_filename,retval,/new else $
      box_message,out_filename(0) + ' already exists - set /OVERWRITE to allow clobber
endif

return,retval
end
