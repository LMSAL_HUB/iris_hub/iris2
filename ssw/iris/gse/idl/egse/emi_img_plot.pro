pro emi_img_plot, infil, time_sweep, st_freq, freq_rate, dwell, time_end, $
		single=single, qstop=qstop, hc=hc, $
		ask_freq=ask_freq, code=code, $
		tit2=tit2, tit3=tit3
;+
;	emi_img_plot,ff,'1-dec 19:00:40',20000,1.003
;	emi_img_plot,outfil,'3-dec 18:35:30',3e+6,1.003,1,'3-dec 18:46:07', code='IMG2'	;30M to 200M
;	emi_img_plot,outfil,'3-dec 19:31:00',3e+6,1.003,1,'3-dec 19:41:35', code='IMG2'	;30M to 200M
;	emi_img_plot,outfil,'3-dec 20:07:30',35e+6,1.003,10,'3-dec 20:28:39', code='IMG2' ;35M to 55M
;	emi_img_plot,outfil,'3-dec 23:01:30',30e+6,1.003,1,'3-dec 23:05:26', code='IMG2'  ;30M to 60M
;	emi_img_plot,outfil,'4-dec 00:01:50',30e+6,1.003,1,'4-dec 00:13:05', code='IMG2'  ;30M to 200M
;INPUT:
;	infil
;OPTIONAL INPUT:
;	time_sweep- The GMT time of the start of the sweep
;	st_freq	- The starting frequency
;	freq_rate - The frequence rate (hz/sec)
;HISTORY:
;	Written 1-Dec-99 by M.Morrison
;V1.1	 2-Dec-99 (MDM) - Allow overplot of frequency
;V2.0	 3-Dec-99 (MDM) - Added dwell time and time_end
;V2.1	 3-Dec-99 (MDM) - Added overplot of reference failure mark
;			- Added printing starting/ending frequency
;V2.2	 3-Dec-99 (MDM) - Added printing start/end time/rate/dwell
;V2.3	 8-Dec-99 (MDM) - Allow reverse scans
;V2.4	13-Apr-01 (MDM) - Modified to use env vars for directories
;V2.5	15-Aug-01 (MDM) - Overplot 5x average instead of 2x
;			- Handled linear scan rates
;			- Added tit2 and tit3 keywords
;V2.6	 9-Apr-01 (MDM) - Added removal of partial images (min eq 0)
;-
;
if (n_elements(infil) eq 0) then begin
    ff = file_list('$SXI_EMI_DB_DIR', '*.tab')
    ff = reverse(ff)
    imenu = xmenu_sel(ff, /one)
    if (imenu eq -1) then return
    infil = ff(imenu)
end
;
print, 'Reading ', infil
;
if (n_elements(dwell) eq 0) then dwell = 1.
;
mat = rd_tfile(infil, 12)
if (n_elements(mat(0,*)) le 1) then return	;MDM 20-Sep-00
mat = mat(*,1:*)	;drop first line
n = n_elements(mat(0,*))
dattim = replicate(anytim2ints(!stime), n)
dattim.day  = long(reform(mat(0,*)))
dattim.time = long(reform(mat(1,*)))
;
;
if (keyword_set(ask_freq)) then begin
    input, 'Enter the date/time the scan started', time_sweep, fmt_tim(dattim(0))
    input, 'Enter the starting frequency (in Hz)', st_freq, 20000.
    input, 'Enter the sweep rate (% (factor) per second)', freq_rate, 1.003
    input, 'Enter the dwell (seconds)', dwell, 1
    input, 'Enter the date/time the scan ended', time_end, fmt_tim(dattim(n-1))
end
;
;ss = where(abs(mat(6,*)) lt 200, nss)
ss = where( (abs(mat(6,*)) lt 200) and (mat(2,*) ne 0), nss)
print,n,nss
if (nss eq 0) then return
if (keyword_set(hc)) then setps, /port
if (!d.name eq 'X') then tv2, 600, 800, /init, /already
clearplot
!p.multi = [0,1,2]
!p.charsize = 1.5
if (keyword_set(st_freq)) then !p.multi = [0,1,3]
if (keyword_set(single)) then begin
    i1 = 4
    i2 = 5
    ytit0 = 'Single Image '
end else begin
    i1 = 6
    i2 = 7
    ytit0 = 'Difference '
end
;
if (keyword_set(code)) then if (code eq 'IMG2') then begin
    i1 = 8
    i2 = 9
    ytit0 = 'Single Image Extracted '
end
;
if (keyword_set(code)) then if (code eq 'DIFF2') then begin
    i1 = 10
    i2 = 11
    ytit0 = 'Difference Extracted '
end
;
ref_tim = dattim(0)
utplot, dattim(ss), float(mat(i1,ss)), ref_tim, /ynozero, ytit=ytit0 + ' Average', tit=infil
;utplot, dattim(ss), float(mat(i2,ss)), /ynozero, ytit=ytit0 + ' Stddev'
utplot, dattim(ss), float(mat(i2,ss)), ref_tim, /ynozero, ytit=ytit0 + ' Stddev', xstyle=8, $	;,ymargin=[4,4]
		tit = tit2
;
if (keyword_set(time_sweep)) then begin
    dx = int2secarr(time_sweep, ref_tim)	;normally a positive value
    ss2 = where( int2secarr(dattim(ss), ref_tim) lt dx, nss2)
    if (nss2 ne 0) then bavg = total( float(mat(i2,ss(ss2))) ) / nss2
    plottimes, .05, .9, 'Sweep Start Time: ' + fmt_tim(time_sweep)
    if (keyword_set(freq_rate)) then plottimes, 0.05, .84, 'Freq rate: ' + string(freq_rate)
    if (keyword_set(dwell)) then plottimes, 0.05, .78, 'Dwell (secs): ' + string(dwell)
    if (keyword_set(time_end)) then plottimes, .05, .72, 'Sweep End Time:   ' + fmt_tim(time_end)
end
;
;if (keyword_set(bavg)) then oplot, !x.crange, [1,1]*bavg*2, linestyle=2
if (keyword_set(bavg)) then oplot, !x.crange, [1,1]*bavg*5, linestyle=2
;
if (keyword_set(st_freq)) then begin
    outplot, [time_sweep, time_sweep], !y.crange, /linestyle
    if (keyword_set(time_end)) then outplot, [time_end, time_end], !y.crange, /linestyle
    dx = int2secarr(time_sweep, ref_tim)	;normally a positive value
    ;
    n = n_elements(dattim)
    ;
    ;fmin = min(farr)
    ;fmax = max(farr)
    ffreq_rate = double(freq_rate)
    print, 'Freq_rate', freq_rate, ffreq_rate
    if (ffreq_rate ge 10) then begin
	;linear rate
	farr = double(st_freq) + ffreq_rate * (float( (findgen(n)-dx)/dwell ) )
	fmin = st_freq +  ffreq_rate * ((!x.crange(0)-dx)/dwell)  > 1
	fmax = st_freq +  ffreq_rate * ((!x.crange(1)-dx)/dwell)
	if (keyword_set(time_end)) then $
		en_freq = float( st_freq +  double(freq_rate) * (int2secarr(time_end, time_sweep)/dwell) )
    end else begin
	;exponential rate
	;farr = double(st_freq) * double(freq_rate) ^ (float(long( (findgen(n)-dx)/dwell ) ))
	farr = double(st_freq) * ffreq_rate ^ (float( (findgen(n)-dx)/dwell ) )
	fmin = st_freq *  ffreq_rate ^ ((!x.crange(0)-dx)/dwell)
	fmax = st_freq *  ffreq_rate ^ ((!x.crange(1)-dx)/dwell)
	if (keyword_set(time_end)) then $
		en_freq = float( st_freq *  double(freq_rate) ^ (int2secarr(time_end, time_sweep)/dwell) )
    end

    print,fmin,fmax
    axis,xaxis=1,xrange=[fmin,fmax],/xstyle,xtit='Frequency',/xtype
    ;
    farr2 = farr
    if (ffreq_rate gt 1) then farr2 = farr > st_freq $
			else farr2 = farr < st_freq
    plot, farr2(ss), float(mat(i2,ss)), /ynozero, ytit=ytit0 + ' Stdev',xtit='Frequency', tit=tit3		;, /xtype, /xstyle
    if (keyword_set(bavg)) then oplot, !x.crange, [1,1]*bavg*5, linestyle=2
    ;
    plottimes, .05, .90, 'Start Freq: ' + string(st_freq)
    if (keyword_set(time_end)) then begin
	plottimes, .05, .84, 'Ending Freq (predicted): ' + string(en_freq)
    end
end
;
;
plottime, 0, 0, 'EMI_IMG_PLOT  Ver 2.6
plottime
if (keyword_set(qstop)) then stop
if (keyword_set(hc)) then pprint, /reset
end