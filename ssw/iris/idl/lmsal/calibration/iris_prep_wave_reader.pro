function IRIS_PREP_SHIFT_READER, iindex, fidmsg, wmsg, $
	nomx = nomy, nomy = nomy

;+
;
;	Given an image header, look up the wavelength and fiducial shift that needs 
;	to be applied (in pixels) to take out orbital and temperature-dependent shifts
;	in X and Y based on JPW's IRIS_ISP2WCS
;
; INPUT:
;	iindex	-	The
;
; OUTPUT:
;	wmsg	-	String constant to put into the image header describing the
;				wavelength correction
;	fidmsg	-	String constant to put into the image header describing the
;				fiducial correction
;
; KEYWORDS:
;	nomx	-	The nominal (desired) value of CRPIX1; for spectra, this is the
;				target pixel for the reference wavelength (for SJIs it is the
;				standard X coordinate of the slit)
;
;	nomy	-	The nominal (desired) value of CRPIX2; this is the target pixel
;				for the center of the slit (midpoint between fiducials)
;
; RETURNS:
;	result	-	A 2-element FLOAT array giving the X and Y shift that should be 
;				applied
;	
;-


oindex = iindex
IRIS_ISP2WCS, oindex

xshift = nomx - oindex.crpix1
yshift = nomy - oindex.crpix2
result = [xshift, yshift]

case iindex.instrume of
	'NUV'	:	begin
		wmsg = STRING('Ni I line wavelength shift [pix]: ', xshift, form = '(a35,f9.2)')
		fidmsg = STRING('Fiducial midpoint Y shift [pix]: ', yshift, form = '(a35,f9.2)')
	end
	'FUV'	:	begin
		wmsg = STRING('O I line wavelength shift [pix]: ', xshift, form = '(a35,f9.2)')
		fidmsg = STRING('Fiducial midpoint Y shift [pix]: ', yshift, form = '(a35,f9.2)')
	end
	else	:	begin
		wmsg = 'No wavelength shift applied to SJI'
		fidmsg = STRING('Fiducial midpoint shift X,Y [pix]: ', xshift, yshift, form = '(a35,2f9.2)')
	end
endcase

RETURN, result

end