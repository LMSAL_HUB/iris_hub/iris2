pro ssw_previous_version, previous_versions, $
  date=date, _extra=_extra, outdir=outdir, lastn=lastn, $
   no_copy=no_copy, urls=urls, refresh_log=refresh_log, progress=progress
;+
;   Name: ssw_previous_version
;
;   Purpose: retrieve previous version(s) of desired SSW-GEN routine
;
;   Input Parameters:
;      none
;
;   Output Paramters:
;      previous_versions - list of previous version(s) - local or URLS
;
;   Keyword Parameters:
;      lastn - number of versions to return ( /lastn => 1,  or lastn=NN )
;      no_copy - if set, don't copy, just return names
;      urls - if set, previous_versions are urls on ssw master
;      refresh_log - if set, force update of online log->local
;      outdir - optional outdir for master->local copy (def=curdir())
;      date - optionally, particular date rather than routine name
;      _extra - name of desired routine via keyword inheritance      
;      same_day - also return other routines updated on same day
;                 (implies lastn=1 only)
;
;   Method:
;      looks at ssw master 'online' log and searches for desired routine
;      entries
;-

usedate=keyword_set(date)
if not data_chk(_extra,/struct) and 1-usedate then begin 
   box_message,'Need to supply routine name via /ROUTINE_NAME'
   return
endif

tempdir=get_temp_dir()

topssw='http://sohowww.nascom.nasa.gov/solarsoft/'
toplog=topssw+'site/logs/ssw_online.log'
topback=topssw+'offline/ssw_backup/'

loclog=concat_dir(tempdir,'ssw_online.log')

refresh=keyword_set(refresh_log)

if refresh then ssw_file_delete,loclog

; get online log
sock_copy,toplog, out_dir=tempdir
if not file_exist(loclog) then begin 
   box_message,'Problem with log copy??, returning'
   return
endif

if n_tags(_extra) gt 1 then box_message,'More than one routine?.. only 1st used'
case 1 of
   usedate: rstring='*.' + time2file(date,/date_only,/year2) +  '.*'
   else: begin 
      routine=(tag_names(_extra))(0)
      lroutine=strlowcase(routine)
      rstring='*/' + lroutine+'.pro.??????.??????'
   endcase
endcase

logcont=rd_tfile(loclog) 

rss=where(strmatch(logcont,rstring,/fold_case),sscnt)
if sscnt eq 0 then begin 
   box_message,'No previous versions found in online log..'
   return
endif
if usedate then lastn=100
if n_elements(lastn) eq 0 then lastn=1 ; default is most recent (only)
same_day=keyword_set(same_day)
if same_day then lastn=1 ; /same_day implies only most recent considered

routines=strextract(logcont(last_nelem(rss,lastn))+'**','ssw_backup/','**')

if same_day then begin 
   ssw_previous_version, routines, date=file2time(last_nelem(routines))
endif

recent_version=routines
rurls=topback+routines
if keyword_set(urls) then recent_versions=rurls

copyem=1-keyword_set(no_copy)

if copyem then begin 
   if n_elements(outdir) eq 0 then outdir=curdir()
   sock_copy,rurls,out_dir=outdir, progress=progress
endif


return
end



