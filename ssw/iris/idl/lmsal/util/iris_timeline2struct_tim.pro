function iris_timeline2struct_tim, timeline, out_style=out_style, debug=debug, loud=loud, timeline_names=timeline_names, $
   slew=slew, roll=roll, dump=dump, queue=queue
;+
;   Name: iris_timeline2struct_tim
;
;   Purpose: IRIS timeline file -> structure/info vector (tim, not science timelines
;
;   Input Paramters:
;      timeline - IRIS timeline.txt file
;
;   Keyword Paramters:
;      out_style - optional 'anytim' time format - default=CCSDS
;      timeline_names=timeline_names - if set, include path/name of timeline file

;   Calling Examples:
;      (generally called via iris_time2timeline wrapper)
;      IDL> struct=iris_timeline2struct(timeline [,/slew] [,/dump] [,/roll] )
;
;   History:
;      8-aug-2013 - S.L.Freeland - tim file plug in to iris_time2timeline 
;     12-aug-2013 - S.L.Freeland - add /DUMP & enable /ROLL
;     27-sep-2013 - S.L.Freeland - quiet this down, since was warning about a fairly common/expected piece of info
;     17-mar-2014 - J-P.Wuelser  - bugfix: added yy in call to doy2utc
;     28-aug-2014 - S.L.Freeland - added /QUEUE keyword & function
;   
;   Restrictions:
;-

obs=keyword_set(obs)
slew=keyword_set(slew)
roll=keyword_set(roll)
dump=keyword_set(dump)
queue=keyword_set(queue)

if not file_exist(timeline) then begin 
   box_message,'Need IRIS timeline file.. bailing
   return,-1
endif
debug=keyword_set(debug)
loud=keyword_set(loud)

dat=rd_Tfile(timeline)
if n_elements(out_style) eq 0 then out_style='ccsds' ; default time format
sst0=where(strmatch(dat,'*T*FIL*'),t0cnt)
sst01=where(strmatch(dat,'*T*REGOTL*'),t01cnt)

case 1 of 
   t0cnt eq 1: yydoy=strextract(ssw_strsplit(dat[sst0],':',/tail),'T','TIMFIL')
   t01cnt eq 1:yydoy=strextract(dat[sst01],'T','REG')
   else: begin 
      box_message,'Unexpected T0, bailing...'
      return,-1 ; early exit
   endcase
endcase 

yy=fix(strmid(yydoy,0,2))
doy=fix(strmid(yydoy,2,1000))
tldoy=doy
retval={date_obs:'',date_end:'',obsid:0ul,repeats:0,duration:0.,xcen:0.,ycen:0.,tracking:0, slewed:0, count:0,size:0l,pzta:0l,pztb:0l,pztc:0l,description:''}

case 1 of 
   keyword_set(obs): template=retval
   keyword_set(roll): template={roll_start:'',degrees:0.0, radians:0.0, duration:0.0}
   keyword_set(slew): template={date_obs:'',xoff:0.0, yoff:0.0, m1:0, m2:0, cmdx:0., cmdy:0, slewtime:0}
   keyword_set(dump): template={dump_start:'',band:'',duration:0,roll:0., station:'',station_code:''}
   keyword_set(queue): template={q_start:'',pid:0,qid:'',duration:0.,datarate:0.,dunit:''}
   else: begin 
      template={date_obs:'',xoff:0.0, yoff:0.0, m1:0, m2:0, cmdx:0., cmdy:0, slewtime:0}
   endcase
endcase

pattern=(['*OBSID=*rpt*endtime*','*SLEW*xoff*'])(1-obs)
if roll or dump then pattern=(['*DUMP_START*band*','*ACSROLL*'])(roll)
if queue then pattern='*queue*id=q*'

ss=where(strmatch(dat,pattern,/fold),startcnt)

if startcnt eq 0 then begin 
   ;box_message,'No OBS(??)
   return,-1  ; UNSTRUCTURED EXIT ON NO "thing" found in this tim file
endif

retval=replicate(template,startcnt)

data=dat[ss]

case 1 of 
   roll: pattern='ACSROLL'
   slew: pattern='SLEW'
   dump: pattern='DUMP_START'
   queue: pattern='pid='
   else: begin
      return,'' ; EARLY EXIT
   endcase
end

time=ssw_strsplit(data,pattern,/head,tail=stuff)
stuff=strtrim(stuff,2)
ssdesc=where(strpos(stuff,'desc=') ne -1, dcnt)
if dcnt gt 0 then stuff[ssdesc]=ssw_strsplit(stuff[ssdesc],/head,tail=desc,'desc=')
if slew then begin 
   stuff='*'+stuff+','
   stuff=strextract(stuff,'*','s,')+',' 
endif
if queue then stuff=str_replace(stuff,'QUEUE','')
cols=str2cols(stuff,/trim,/unal)
for c=1, n_tags(retval)-1 do begin
   cx=reform(cols[c-1,*])
   cx=ssw_strsplit(cx,'=',/tail)
   if strlowcase(cx[0]) eq strupcase(cx[0]) then cx=str2number(cx) else cx=str_replace(cx,',','')
   retval.(c)=cx
endfor

tcols=str2cols(time,' ',/trim)
strtab2vect,tcols,dtdoy,time

retval.(0)=anytim(anytim(doy2utc(tldoy+str2number(dtdoy),yy),/ecs,/date_only) + ' ' + time,out_style=out_style)

case 1 of 
   slew: retval=add_tag(retval,anytim( anytim(retval.date_obs) + retval.slewtime,out_style=out_style),'date_end')
   dump or roll: $
      retval=add_tag(retval,anytim( anytim(retval.(0)) + retval.duration,out_style=out_style),(['dump_end','roll_end'])(roll))
   queue: begin
      retval=add_tag(retval,anytim( anytim(retval.(0)) + retval.duration,out_style=out_style),'date_end')
      retval=add_tag(retval,retval.(0),'date_obs')
      retval=add_tag(retval,retval.date_end,'q_stop')
      retval=add_tag(retval,desc,'description')
   endcase
   else:
endcase

if dump then begin ; patch for now
  aroll=strtrim(fix(all_vals(retval.roll))  ,2)
  for r=0,n_elements(aroll)-1 do begin 
     rp='roll='+aroll[r]+','
     ssr=where(strpos(stuff,rp) ne -1,rcnt)
     retval[ssr].station=strtrim(strextract(stuff[ssr],rp,'('),2)
  endfor
  retval.station_code=strtrim(strextract(stuff,'(',')'),2)
  retval.band=strtrim(strextract(stuff,'band=',','),2)
endif

if keyword_set(timeline_names) then begin 
   retval=add_tag(retval,replicate(timeline,n_elements(retval)),'timeline_name')
endif

return,retval
end

