pro added_event, e
   

end


pro table_event, e

case e.type of 
   0: begin
      ; If carriage returned pressed
      ;if e.ch EQ 10 then begin
         wbase = widget_info(e.id,/parent)
         widget_control, e.id, get_value = showntable
         widget_control, wbase, get_uvalue=uvalue
         uvalue.votable->GetProperty, table=table, event_types=event_types, par_names=par_names
                                ;table[e.y,e.x] = fix(e.str)
         uvalue.votable->SetProperty, table=fix(showntable)
         uvalue.votable->ColorTable, e.id
      ;endif
   endcase
   else: 
end

end

pro add_par_name, e

wbase = widget_info(e.id,/parent)
widget_control, wbase, get_uvalue=uvalue

;Add row to table
widget_control, uvalue.table_id, get_value=table

new_table = intarr(n_elements(table[*,0]),n_elements(table[0,*])+1)
new_table[0:n_elements(table[*,0])-1,0:n_elements(table[0,*])-1] = table
new_table[*,n_elements(table[0,*])] = -1

; Get parameter name
widget_control, uvalue.new_par_name, get_value = new_par_name
widget_control, uvalue.new_par_type, get_value=new_par_type
widget_control, uvalue.new_par_votype, get_value=new_par_votype
widget_control, uvalue.new_par_source, get_value=new_par_source
widget_control, uvalue.new_par_votranslation, get_value=new_par_votranslation
widget_control, uvalue.new_par_description, get_value=new_par_description

widget_control, uvalue.table_id, insert_rows = 1

uvalue.votable->GetProperty, par_names=par_names, par_sources=par_sources, par_types=par_types, $
                             par_votypes=par_votypes, par_votranslations=par_votranslations, $
                             par_descriptions=par_descriptions
par_names = [par_names,new_par_name]
par_sources=[par_sources,new_par_source]
par_types = [par_types, new_par_type]
par_votypes=[par_votypes, new_par_votype]
par_votranslations=[par_votranslations,new_par_votranslation]
par_descriptions=[par_descriptions, new_par_description]

uvalue.votable->SetProperty, table=(new_table), par_names=par_names, par_sources=par_sources, par_types=par_types, $
                             par_votypes=par_votypes, par_votranslations=par_votranslations, $
                             par_descriptions=par_descriptions

widget_control, uvalue.table_id, set_value = (new_table), row_labels = par_names
uvalue.votable->ColorTable, uvalue.table_id

end

pro add_event_type, e

wbase = widget_info(e.id,/parent)
widget_control, wbase, get_uvalue=uvalue

;Add column to table
widget_control, uvalue.table_id, get_value=table

new_table = intarr(n_elements(table[*,0])+1,n_elements(table[0,*]))
new_table[0:n_elements(table[*,0])-1,0:n_elements(table[0,*])-1] = table
new_table[n_elements(table[*,0]),*] = -1

;RPT new code to do basic setting for most of them.  
for i=0,n_elements(table[0,*])-1 do begin
; yeah probably better idl ways for this.  
oldrow = table[*,i]
oldrow=oldrow[sort(oldrow)] 
wh=where(oldrow ne shift(oldrow,-1),cnt)
if cnt eq 0 then mode=oldrow[0] else begin
void=max(wh-[-1,wh],mxpos)
mode=oldrow[wh[mxpos]]
endelse 
new_table[n_elements(table[*,0]),i] = mode

endfor

; Ask for name and abbreviation of new event_type
widget_control, uvalue.new_event_name, get_value = new_event_name
widget_control, uvalue.new_event_abbr, get_value = new_event_abbr

widget_control, uvalue.table_id, insert_columns = 1
uvalue.votable->GetProperty, event_types=event_types, event_names=event_names
event_types = [event_types,new_event_abbr]
event_names = [event_names,new_event_name]
uvalue.votable->SetProperty, table=(new_table), event_types = event_types, event_names=event_names

widget_control, uvalue.table_id, set_value = (new_table), column_labels = event_types
uvalue.votable->ColorTable, uvalue.table_id
end


PRO VOEvent_Table::Export, outfile=outfile
  n_types = n_elements(*self.event_types)
  n_pars  = n_elements(*self.par_names)
  openw, u, outfile, /get_lun
  line = "Heliophysics Knowledgebase Features/Events Specification (Last updated:"+systim()+")"
  for i=0,n_types+5 do line = line+','
  printf, u, line
  line = "-------------------------------------------------------------------------------------"
  for i=0,n_types+5 do line = line+','
  printf, u, line
  line = 'Parameter,Type,Dimensions,'
  for i=0,n_types-1 do line = line+strupcase((*(self.event_types))[i])+','
  line = line+'Source,VOParamType,R/O,VOTranslation,Description'
  printf, u, line
  line = 'Full_Name,-,-,'
  for i=0,n_types-1 do line = line+(*(self.event_names))[i]+','
  line = line+'-,-,-,-,Feature Type'
  printf, u, line

  ; Now loop through parameters
  for j=0,n_pars-1 do begin
     line = strtrim((*self.par_names)[j],2)+','+strtrim((*self.par_types)[j],2)+','+strtrim((*self.par_dimensions)[j],2)+','
     for i=0,n_types-1 do begin
        line  += (string((*self.table)[i,j], format="(I1)")+',')
     endfor
     line += ((*self.par_sources)[j]+',')
     line += ((*self.par_votypes)[j]+',')
     if total((*self.table)[*,j]) EQ n_types*9 then begin
        line+='r,'
     endif else begin
        line+='o,'
     endelse
     line += ((*self.par_votranslations)[j]+',')

     line += (*self.par_descriptions)[j]
     printf, u, line
  endfor

  close, u
  free_lun, u
   
END

PRO VOEvent_Table::SetProperty, event_types=event_types, par_names=par_names, table=table, event_names=event_names,$
                                par_types=par_types, par_votypes=par_votypes, par_sources=par_sources, $
                                par_votranslations=par_votranslations, par_descriptions=par_descriptions

IF N_ELEMENTS(event_types) NE 0 THEN self.event_types = ptr_new(event_types)
IF N_ELEMENTS(event_names) NE 0 THEN self.event_names = ptr_new(event_names)
IF N_ELEMENTS(par_names) NE 0 THEN self.par_names = ptr_new(par_names)
IF N_ELEMENTS(table) NE 0 THEN self.table = ptr_new(table)
IF N_ELEMENTS(par_types) NE 0 THEN self.par_types = ptr_new(par_types)
IF N_ELEMENTS(par_sources) NE 0 THEN self.par_sources = ptr_new(par_sources)
IF N_ELEMENTS(par_votypes) NE 0 THEN self.par_votypes = ptr_new(par_votypes)
IF N_ELEMENTS(par_votranslations) NE 0 THEN self.par_votranslations = ptr_new(par_votranslations)
IF N_ELEMENTS(par_descriptions) NE 0 THEN self.par_descriptions = ptr_new(par_descriptions)



END

PRO VOEvent_Table::GetProperty, event_types=event_types, par_names=par_names, table=table, event_names=event_names,$
                                par_types=par_types, par_votypes=par_votypes, par_sources=par_sources, $
                                par_votranslations=par_votranslations, par_descriptions=par_descriptions
  event_types = (*self.event_types)
  event_names = (*self.event_names)
  par_names = (*self.par_names)
  table = (*self.table)
  par_types = (*self.par_types)
  par_sources= (*self.par_sources)
  par_votypes = (*self.par_votypes)
  par_votranslations=(*self.par_votranslations)
  par_descriptions = (*self.par_descriptions)
END

PRO VOEvent_Table::ColorTable, table_id
colors = make_array(3,N_ELEMENTS((*self.table)[*,0]), N_ELEMENTS((*self.table)[0,*]), /integer)
for i=0,N_ELEMENTS((*self.table)[*,0])-1 do begin
   for j=0,N_ELEMENTS((*self.table)[0,*])-1 do begin
      case (*(self.table))[i,j] of
         9: colors[*,i,j] = [250,40,40]
         5: colors[*,i,j] = [202,255,112]
         0: colors[*,i,j] = [0,170,208]
         else: colors[*,i,j] = [220,220,220]
      endcase
   endfor
endfor

widget_control, table_id, background_color=colors
END

PRO VOEvent_Table::Show

wbase  = widget_base(xsize=1600,ysize=768)
dx = 300

; Add table
column_widths=round((1200)/n_elements(*self.event_types)); 40
wtable = widget_table(wbase, value=(*(self.table)), column_labels=(*self.event_types), row_labels=(*self.par_names), column_widths=column_widths, /resizeable_columns, xsize=n_elements(*self.event_types), /all_events, event_pro='table_event', /editable)

; Add event button
new_event_name = widget_text(wbase, xoffset = 1600-dx, value="NewEvent", /editable)
new_event_abbr = widget_text(wbase, xoffset = 1600-dx, yoffset=30, value="Abbr.", /editable)
event_button = widget_button(wbase, xoffset = 1600-dx, yoffset=60, value='Add event type', event_pro='add_event_type')

; Add parameter button
new_par_name = widget_text(wbase, xoffset = 1600-dx, yoffset=120, value='Param name', /editable)
new_par_type = widget_droplist(wbase, xoffset = 1600-dx, yoffset=145, value=['float','integer','long','string'])
new_par_source  = widget_droplist(wbase, xoffset = 1600-dx, yoffset=175, value=['data','method'])
new_par_votype  = widget_droplist(wbase, xoffset = 1600-dx, yoffset=206, value=['why','what','wherewhen','how'])
new_par_votranslation=widget_text(wbase, xoffset = 1600-dx, yoffset=240, value="-", /editable)
new_par_description=widget_text(wbase, xoffset = 1600-dx, yoffset=270, value="Description", /editable,xsize=30,ysize=4)
new_par_button=widget_button(wbase, xoffset=1600-dx, yoffset=340, value='Add parameter', event_pro='add_par_name')

; Store uvalue
wbase_uvalue = {id:wbase, table_id:wtable, add_event_id:event_button, votable:self, new_event_name:new_event_name,$
                new_event_abbr:new_event_abbr, new_par_name:new_par_name, $
                new_par_type:new_par_type, new_par_source:new_par_source, new_par_votype:new_par_votype, $
                new_par_votranslation:new_par_votranslation, new_par_description:new_par_description}

widget_control, wbase, set_uvalue  = wbase_uvalue

widget_control, wbase, /realize
xmanager, 'wbase', wbase, /no_block

self->ColorTable, wtable

END

FUNCTION VOEvent_table::Event_types
   return, *(self.event_types)
END

PRO VOEvent_Table::Delete_Event_Type, event_type
   evs = *(self.event_types)
   pns = *(self.par_names)
   table = *(self.table)
   n_evs = n_elements(evs)
   n_pns = n_elements(pns)
   
   ie    = where(strlowcase(strtrim(evs,2)) EQ strlowcase(event_type))
   print, ie
   if (ie[0] ne -1) then begin
      new_evs = strarr(n_evs-1)
      new_table = intarr(n_evs-1, n_pns)
      j = 0
      for i=0,n_evs-1 do begin
         if (i NE ie) then begin
            new_evs[j] = evs[i]
            new_table[j,*] = table[i,*]
            j = j+1
         endif
      endfor
   endif
   ptr_free, self.event_types
   ptr_free, self.table
   self.event_types = ptr_new(new_evs, /no_copy)
   self.table = ptr_new(new_table,/no_copy)

END

FUNCTION VOEvent_Table::REQUIRED, event_type, param_name

   ie = where(strlowcase(strtrim(event_type,2)) EQ strlowcase(*(self.event_types)))
   ip = where(strlowcase(strtrim(param_name,2)) EQ strlowcase(*(self.par_names)))
   return, (*(self.table))[ip,ie]

END

FUNCTION VOEvent_Table::Init, infile=infile
IF (N_ELEMENTS(infile) EQ 0) THEN BEGIN
   dir   ="/Users/cheung/ontology/data/"
   infile=dir+"VOEvent_Spec.txt"
   outfile=dir+"new_VOEvent_Spec.txt"
ENDIF ELSE BEGIN
   dir = ''
   infile = dir+infile
   outfile= dir+"new_VOEvent_Spec.txt"
ENDELSE

lines = strarr(file_lines(infile))
openr, lun, infile, /get_lun
readf, lun, lines
close, lun
free_lun, lun

; Now Parse the ascii file
;stop

header=lines[0:1]
lines = lines[2:*]

event_types = (strsplit(lines[0],',',/extract))
source_index = where(strlowcase(event_types) EQ 'source')
event_types = event_types[3:source_index-1]
n_types = n_elements(event_types)

event_names = (strsplit(lines[1],',',/extract))
event_names = event_names[3:n_types+3-1]
print, event_names
;lines = lines[3:*]
lines = lines[2:*]
n_pars  = n_elements(lines)
par_names = strarr(n_pars)
par_sources= strarr(n_pars)
par_types= strarr(n_pars)
par_votypes= strarr(n_pars)
par_votranslations=strarr(n_pars)
par_descriptions = strarr(n_pars)
par_dimensions = strarr(n_pars)
table = intarr(n_pars, n_types)

for l=0,n_pars-1 do begin 
   temp = (strsplit(lines[l],',',/extract)) 
   par_names[l]=temp[0]
   par_types[l]=temp[1]
   par_sources[l] = temp[source_index]
   par_votypes[l] = temp[source_index+1]
   par_votranslations[l]=temp[source_index+3]
   par_descriptions[l]=temp[source_index+4]
   par_dimensions[l]=temp[2]
   table[l,*] = fix(temp[3:source_index-1])
   print, temp[3:source_index-1]
   ;stop
endfor
self.infile = infile
self.dir    = dir
table = transpose(table)
self.table = ptr_new(table, /no_copy)
self.event_types = ptr_new(event_types, /no_copy)
self.event_names = ptr_new(event_names, /no_copy)
self.par_names = ptr_new(par_names, /no_copy)
self.par_sources = ptr_new(par_sources, /no_copy)
self.par_votypes = ptr_new(par_votypes, /no_copy)
self.par_votranslations = ptr_new(par_votranslations, /no_copy)
self.par_descriptions = ptr_new(par_descriptions, /no_copy)
self.par_types =ptr_new(par_types, /no_copy)
self.par_dimensions = ptr_new(par_dimensions,/no_copy)
self.header = header
return, 1
END

PRO VOEvent_Table__define

struct = {VOEvent_Table, $
          infile:'', $
          dir:'', $
          table:ptr_new(),$
          event_types:ptr_new(),$
          event_names:ptr_new(),$
          par_names:ptr_new(), $
          par_sources:ptr_new(),$
          par_votypes:ptr_new(), $
          par_votranslations:ptr_new(),$
          par_descriptions:ptr_new(), $
          par_dimensions:ptr_new(), $
          par_types:ptr_new(), $
          header:strarr(2) $
         }

end
