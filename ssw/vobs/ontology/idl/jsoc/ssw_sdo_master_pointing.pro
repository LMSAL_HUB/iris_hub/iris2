
function ssw_sdo_master_pointing, t0, t1, ds=ds, no_jsoc2=no_jsoc2, $
   ssw=ssw, record_number=record_number, all_versions=all_versions , refresh=refresh
;+
;   Name: ssw_sdo_master_pointing
;
;   Purpose: SDO master pointing - jsoc series -> SSW structure vector
;
;   Input Paramters:
;      t0 - if passed without t0, then return mpo_rec with nearest earlier T_START to t0
;           (may be a vector)
;           if passed with second pos param t1 then t0 must be a scalar and all mpo recs with
;           T_START in range [t0,t1] returned
;      t1 - if passed as second pos param, end time for optional time range (see above).
;           Default is full mission (all mpo recs in mission)
;
;   Keyword Parameters:
;      ds - optional data series to use - default='sdo.master_pointing
;      ssw (switch) - set that to use local version instead of jsoc client
;      record_number - index record(s) w/MPO_REC -or- explicit record number(s)
;                      NOTE: jsoc record numbers which start at 1, not zero
;
;   Calling Seqeunce:
;      IDL> mpstr=ssw_sdo_master_pointing([t0,t1] [,ds=seriesname] [,record_number={index or #} ])
;
;   Calling Examples:
;
;      IDL> mp=ssw_sdo_master_pointing() ; all records, newest versions 
;
;      IDL> mp=ssw_sdo_master_pointing( /all_versions) ; mission long mp records, all versions
;
;      IDL> mp=ssw_sdo_master_pointing( record_number=index ) ; match index.MPO_REC : MP records
;
;      IDL> mp=ssw_sdo_master_ointing(t0 [,t1] [,/all_versions] ) ; closest or withing time range
; 
;   Method:
;      Setup and issue call(s) to ssw_jsoc.pro
;
;   History:
;      25-Sep-2010 - S.L.Freeland -
;      28-aug-2012 - S.L.Freeland - add RECORD_NUMBER & ALL_VERSIONS keyword&function
;                                   load full mp series -> cache 
;      20-sep-2012 - S.L.Freeland - use ssw_jsoc_mpall to assure rec# ordering
;      02-oct-2012 - G.L.Slater - vectorized input t0 and made returned rec(s) be nearest earlier
;                                 to t0
;
;   Restrictions:
;      use /SSW to use distributed dbase
;-
common ssw_sdo_master_pointing_blk, mpall, mplatest

refresh=keyword_set(refresh) or n_elements(mplatest) eq 0 

retval=''
fpat='sdo_master_point*'

userec=n_elements(record_number) gt 0 
siz_rec = size(record_number, /type)
all_versions=keyword_set(all_versions) or userec

if keyword_set(ssw) then begin ; use local/ssw db
   file=file_search(concat_dir('$SSW','sdo/gen/data'),fpat)
   if file(0) eq '' then file= $
      file_search(concat_dir('$SSW_ONTOLOGY','data'),fpat)
   if file(0) eq '' then box_message,'Cannot find local version...' else begin
      restgenx,file=last_nelem(file),retval
   endelse
endif else begin ; direct jsoc series access
   if n_elements(ds) eq 0 then ds='sdo.master_pointing'
   jsoc2= 1-keyword_set(no_jsoc2) ; requires jsoc2 as of day written
;
;
   if refresh then begin ; load cache w/full mission MP
      serstr=ssw_jsoc(/series_struct,ds=ds)
      if not data_chk(serstr,/struct) then begin 
         box_message,'Problem with JSOC access... bailing'
         return,''  ; !!! Early Exit
      endif
      maxrec=strtrim(gt_tagval(serstr,/maxrecnum,missing=1),2)
      mplatest=ssw_jsoc(/rs_list,ds=ds)
      mpall   = ssw_jsoc_mpall() ; assure rec# ordering
   endif       
   if all_versions then retval=mpall else retval=mplatest
   retval=add_tag(retval,retval.t_start,'date_obs')
   retval=add_tag(retval,retval.t_stop,'date_end')
   dt0=anytim(retval.t_start)
   dt1=anytim(retval.t_stop)
   maxrec=n_elements(mpall)-1
   case 1 of 
      n_params() eq 1: begin
         n_tim=n_elements(t0)
         ss_arr=strarr(n_tim)
         t0_sec=anytim(t0)
         for i=0,n_tim-1 do begin
            dtmp=t0_sec[i]-dt0
            ss_gz=where(dtmp gt 0, n_gz)
            if n_gz gt 0 then begin
               ss_arr[i]=ss_gz[(where(dtmp[ss_gz] eq min(dtmp[ss_gz])))[0]]
            endif else begin
               box_message,'No MP records in range. Returning.
               return, -1
            endelse
         endfor
         retval=retval[ss_arr]
      endcase
      n_params() eq 2: begin 
         ss=where(ssw_in_timerange(dt0,t0,t1),sscnt)
         if sscnt eq 0 then box_message,'No MP records in range' else retval=retval(ss)
      endcase
      n_elements(record_number) eq 0:   ; finished
      siz_rec eq 8: begin 
         if required_tags(record_number,/mpo_rec) then begin
             recs=str2number(ssw_strsplit(record_number.mpo_rec,'#',/tail))-1  ; jsoc#->idl subscript
             retval=retval(recs>0)
         endif else box_message,'index/structure input but no .MPO_REC tag...'
      endcase
      siz_rec eq 7: begin
         recs=str2number(ssw_strsplit(record_number,'#',/tail))-1 ; jsoc->idl
         if max(recs) gt maxrec then box_message,'RECORD_NUMBER exceeds #MP records'
         retval=retval[recs<maxrec]
      endcase
      (siz_rec ne 8) and (siz_rec ne 7): begin
         recs=record_number-1
         if max(recs) gt maxrec then box_message,'RECORD_NUMBER exceeds #MP records'
         retval=retval[recs<maxrec]
      endcase
      else: 
   endcase
endelse ; end JSOC

return,retval
end


