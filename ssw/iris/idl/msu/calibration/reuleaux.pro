;a function to calculate the positions of a given number of evenly
;distributed perimeter points of a Reuleaux triangle of a given size

;the triangle is formed from three overlapping circles whose centers
;are equidistant with spacing equal to the radius

function reuleaux, npts, L, verbose=verbose

  ;calculate circle centers/triangle vertices
  c2=[0., 0.5*sqrt(L^2 - (0.5*L)^2)]
  c3=[0.5*L, -0.5*sqrt(L^2 - (0.5*L)^2)]
  c1=[-0.5*L, -0.5*sqrt(L^2 - (0.5*L)^2)]

  ;determine arc length
  circum=!pi*L

  circumpts=circum/npts*findgen(npts)

  thetapts=!pi/npts*findgen(npts)
  xpts=fltarr(npts)
  ypts=fltarr(npts)

  idx1=where(circumpts ge 0 and circumpts lt circum/3.)
  idx2=where(circumpts ge circum/3. and circumpts lt 2.*circum/3.)
  idx3=where(circumpts ge 2.*circum/3. and circumpts lt circum)

  xpts[idx1]=L*cos(!pi/3.-thetapts[idx1])+c1[0]
  ypts[idx1]=L*sin(!pi/3.-thetapts[idx1])+c1[1]

  xpts[idx2]=L*cos(-thetapts[idx2])+c2[0]
  ypts[idx2]=L*sin(-thetapts[idx2])+c2[1]

  xpts[idx3]=L*cos(-!pi/3.-thetapts[idx3])+c3[0]
  ypts[idx3]=L*sin(-!pi/3.-thetapts[idx3])+c3[1]

  if keyword_set(verbose) then begin
     plot, xpts, ypts, psym=4, xran=[-2*l,2*l], yran=[-2*l,2*l], /isotropic, $
           xstyle=1, ystyle=1
     tvcircle, l, c1[0], c1[1], color=250, /data
     tvcircle, l, c2[0], c2[1], color=250, /data
     tvcircle, l, c3[0], c3[1], color=250, /data

     print, '              X-Coord      Y-Coord'
     print, '              --------------------'
     for i=0,npts-1 do print, i, xpts[i], ypts[i]
  endif

  return, [[xpts],[ypts]]

end
