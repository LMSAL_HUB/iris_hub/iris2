function ssw_patt_replace, instr, oldpatt, newpatt, debug=debug

debug=keyword_set(debug)

ss=where_pattern(instr,oldpatt,cnt)
binstr=byte(instr)

retval=instr
if cnt gt 0 then begin
  lold=strlen(oldpatt)
  lnew=strlen(newpatt)
  if lnew le lold then begin
     newstr=strpad(newpatt,lold) 
     newlin=byte(newstr)
     for i=0l,cnt-1 do begin
        binstr(ss(i))=newlin 
     endfor 
  retval=string(binstr)
  endif else box_message,'Replace>original, cannot apply this algorithm'

endif 

return,retval
end
