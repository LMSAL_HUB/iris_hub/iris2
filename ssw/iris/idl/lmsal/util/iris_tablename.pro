function iris_tablename,tparent, _extra=_extra,sswdb=sswdb, parent=parent, $
   xml=xml, csv=csv, count=count, debug=debug, pattern=pattern
;+
;  Name: iris_tablename
;
;  Purpose: return IRIS table names - ops/testing/??
;
;  Input Parameters:
;     tparent - parent directory for search (alternate for keyword PARENT)
;
;   Keyword Parameters:
;      /<pattern> - keyword inherit, tables matching PATTERN
;     pattern - optional string pattern (wild cards ok)
;
;   History:
;      6-mar-2013 - S.L.Freeland - SW/logistics helper function
;-

iris_tab=get_logenv('$IRIS_OPS_TABLES') 
debug=keyword_set(debug)

case 1 of 
   file_exist(tparent): parent=tparent ; positional 1st parameter
   data_chk(parent,/string): ; user supplied
   file_exist(iris_tab): parent=iris_tab ; SSW default
   keyword_set(sswdb): parent=concat_dir('$SSWDB','iris')
   else: parent=get_logenv('$SSW_IRIS')
endcase

case 1 of 
   keyword_set(pattern): pattern=pattern
   data_chk(_extra,/struct): pattern=(tag_names(_extra))(0)
   keyword_set(xml): pattern='*.xml'
   keyword_set(csv): pattern='*.csv'
   else: pattern='*.xml'
endcase

if strpos(pattern,'*') eq -1 then pattern='*'+pattern+'*'

if debug then stop,'parent/pattern'

tables=[file_search(parent,pattern), $
        file_search(parent,strlowcase(pattern))]
tables=all_vals(tables)       ; uniq set
tables=strarrcompress(tables) ; eliminate nulls

sst=where(tables ne '', count) ; number of returned table names

return,tables
end


