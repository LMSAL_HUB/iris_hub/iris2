function iris_fill_blanks,stuff
;
bstrings=['Partial Pass','Not in Pass'] 
for b=0,n_elements(bstrings)-1 do begin 
   ssb=where(strmatch(stuff,'*'+ bstrings[b] +'*'),bcount)
   if bcount gt 0 then stuff[ssb]=str_replace(stuff[ssb],bstrings[b],str_replace(bstrings[b],' ','_'))
endfor
return,stuff
end

function iris_fdf_patchdates, table,year

if n_elements(year) eq 0 then year=strmid(anytim(reltime(/now),/date_only,/ecs),0,4)
btable=byte(table)
years=strtrim((lindgen(3)-1)+fix(year)) ; include adjacent years
for y=0,2 do begin ; s.l.freeland 29-apr-2013 - include adjacent years  +[-1,0,1]
   ssp=where_pattern(table,' ' + strtrim(years[y],2) + ' ',pcnt) ; 
   if pcnt gt 0 then begin 
      sspss=where(btable[ssp+8] eq 58,pcnt) ; double check that this is an fdf-like date/time string
      btable[ssp[sspss]]=byte('-')
      btable[ssp[sspss]-4]=byte('-')
      btable[ssp[sspss]+5]=byte('T')
   endif
endfor ; years +/- 1
return,string(btable)
end

function iris_table2struct, stuff, template, colnums, ftype=ftype
;
;+ 
;    function to convert single table->structure

stuff=stuff(where(strpos(strtrim(stuff,2),'-') ne 0 and strlen(strtrim(stuff,2)) gt 5)) ; some "random" fdfs include last record "--" ? ; eliminate those 
slen=strlen(strtrim(stuff,2))
if get_logenv('iris_check') ne '' then stop,'stuff'
if ftype ne 'star' then $
   stuff=stuff(where(abs(slen - median(slen)) le 2)) ; kuludge to filter embedded crap (radzones for example)

ns=n_elements(stuff)
stuff=iris_fill_blanks(stuff)
stuff=iris_fdf_patchdates(stuff) ; hokey, since dates include embedded table delimiter (blank = ' ')
scols=str2cols(stuff,' ',/trim,/unaligned) ; 1D -> 2D table
retval=replicate(template,n_elements(stuff))
ntags=n_tags(retval) 

if n_elements(colnums) eq 0 then colnums=indgen(ntags) ; default - all columns -> structure tags

for i=0,ntags-1 do begin  ; for all desired columns (tags) 
   temp=retval.(i)
   colvect=reform(scols[colnums[i],*])
   if is_number(temp[0]) then begin ; is this column numeric?
      ssnotnumber=where(1-is_number(colvect),nncnt)
      if nncnt gt 0 then colvect[ssnotnumber]= -9999. ; bad data value
   endif
   reads,colvect,temp
   retval.(i)=temp
endfor

; convert _time fields -> ssw standards (optionally user supplied OUT_STYLE -> anytim.pro )
if n_elements(out_style) eq 0 then out_style='ccsds' ; default output for times
tn=tag_names(retval)
sst=where(strmatch(tn,'*TIME'),tcnt)
for t=0,tcnt-1 do begin
   temp=retval.(sst[t])
   sstext=where(1-is_number(temp),bcnt)
   if bcnt gt 0 then temp[sstext]=anytim(0,/ccsds)
   retval.(sst[t])=anytim(temp,out_style=out_style)
endfor

return, retval
end

function iris_fdf2struct, fdf, data, out_style=out_style, header=header, debug=debug
;
;+
;   Name: iris_fdf2struct
;
;   Purpose: convert IRIS Flight Dynamics Files -> ssw/idl structure
;
;   Input Parameters:
;      fdf - name of FDF file
;
;   Output:
;      funtion returns structure vector w/ssw standard times - some value added
;
;   Keyword Parameters:
;      out_style - optional format for time tags per anytim.pro - default='CCSDS'
;      header (output) - header lines from input FDF      
;   
;   History:
;      21-feb-2013 - S.L.Freeland 
;      22-feb-2013 - S.L.Freeland - logic for multi-table files -> nested structure
;      25-feb-2013 - S.L.Freeland - for numerical data types, set invalid data -> -99999. (for example "Not in Pass" -> -99999.)
;       4-apr-2013 - S.L.Freeland - protect against "TIMES" which are not times (for examplee "Not in Pass") - set to 1979-01-01T00:00:00.000 (aka anytim(0,/ccsds) )
;      12-apr-2013 - S.L.Freeland - change minimum required file lines from 7 to 4
;      29-apr-2013 - S.L.Freeland - year rollover hook years=year+[-1,0,1]
;                    Add "special cases" (aaz format change for example)
;                    removal of ACESS column in "some" radzones...
;       1-May-2013 - S.L.Freeland - tighten search string for aaz swap
;      13-may-2013 - S.L.Freeland - additional filter for null files
;
if not file_exist(fdf) then begin 
   box_message,'FDF file not found, bailing...'
   return,-1
endif
fdt=strpos(fdf,'FDF_IRIS') eq -1

ftype=strextract(fdf,'IRIS_','_')

case ftype of 
   'events': begin
       if fdt then $
         temp={pass:0l,start_time:'',apogee:0.d,apogee_time:'',period:0.d,lon_ascen_node:0.d,end_time:'',perigee:0.d,perigee_time:'', $
             right_ascen:0.d,lon_descen_node:0.d} else $ 
         temp={pass:0l,start_time:'',apogee:0.d,apogee_time:'',period:0.d,lon_ascen_node:0.d, ascen_node_time:'', $
                end_time:'',perigee:0.d,perigee_time:'', right_ascen:0.d,lon_descen_node:0.d,  descen_node_time:''}
   endcase
   'aaz': begin
       temp={aaz_direction:'', aaz_time:'', aaz_angle:0.d}
       colnums=[0,3,4]
   endcase
   'eclipse':temp={start_time:'',stop_time:'',duration:0.d,obstruction:'',start_pass:0l,stop_pass:0l,current_condition:'', $
       worst_condition:'',total_duration:0.d}
   'ground':temp={time:'',lat:0.d,lon:0.d,alt:0.d}
   'star': begin
      temp={time:'' ,aroll_min0:0.d , aroll_max0:0.d , aroll_min1:0.d, aroll_max1:0.d }
      colnums=[0,1,3,4,6]
   endcase
   'view': temp={pass:0l,start_time:'',end_time:'',duration:0.d,max_elevation:0.,max_elevation_time:''}
   'radzones': if fdt then temp={start_time:'',stop_time:'',duration:0.d} else $
                           temp={access:0L, start_time:'',stop_time:'',duration:0.d}
   else: begin
      box_message,'Not yet trained for fdf type>> ' + ftype +  ' , bailing...
      return,-1
   endcase
endcase

if n_elements(data) eq 0 then data=rd_tfile(fdf)
toofew=n_elements(strarrcompress(data)) le 4 
nocnt=where(strmid(strarrcompress(data),0,3) eq 'No ',ncnt)
head=where(strpos(strcompress(data,/remove),strpad('-',fill='-',50)) ne -1,hcnt)

if ncnt eq 1 or hcnt eq 0 or toofew then begin 
   box_message,'No valid records in >> ' +fdf[0]
   return,-1
endif

header=data[0:head[0]-1]

if hcnt eq 1 then lastrec=n_elements(data) else begin 
   lastrec=(where(strtrim(data[head[0]:*],2) eq ''))(0) + head[0]
endelse
stuff=strarrcompress(data[head[0]+1:lastrec-1]) 
ns=n_elements(stuff)

doubleup=is_member(ftype,'events') ; at least _events_ have two lines per thing - fix that
if doubleup then begin ;  
   dstuff=strarr(ns/2)
   dstuff=stuff[indgen(ns/2)*2] + '  ' + stuff[indgen(ns/2)*2+1]
   stuff=dstuff
endif
;
; special cases.... (unannounced format changes, etc...)
case 1 of 
   ftype eq 'aaz': begin 
      ssp=where(strmatch(header,'*AAZ*Time*UTCG*'),sscnt)
      if sscnt gt 0 then colnums=[0,4,3] ; order switched?
   endcase
   ftype eq 'radzones': begin
      ssa=where(strpos(strtrim(header,2),'Access') ne -1, acnt)
      if (1-fdt) and (acnt eq 0) and tag_exist(temp,'access') then $
         temp=rem_tag(temp,'access') ; access column not present
   endcase
   else:
endcase

testterm=strmid(strlowcase(stuff[0]),0,2)

if n_elements(stuff) eq 1 and is_member(testterm,'--,no')  then retval= -1 else $
   retval=iris_table2struct(stuff,temp,colnums,ftype=ftype) ; one table -> structure

if n_elements(head) gt 1 then begin  ; handle multi-table files -> nested structure
   rnames=ssw_strsplit(strtrim(data(head-1),2),'-',/head) ; tag names from header records
   retval=add_tag(xx,retval,rnames[0]) ; start the ball rolling with table/tag[0]
   for r=1,hcnt-1 do begin 
      lastrec=(where(strtrim(data[head[r]:*],2) eq ''))(0) + head[r]
      lastrec=([lastrec,n_elements(data)])(lastrec lt head[r])
      stuff=data[head[r]+1:lastrec-1]
      retval=add_tag(retval,iris_table2struct(stuff,temp,colnums,ftype=ftype),rnames[r])
   endfor
endif



return,retval
end





