outdir = getenv('SXI_TBTV_HTML_DIR')
;;dirs = file_list(outdir, 'case*', /cd)
sdirs = 'case_' + ['bo', 'cno', 'c1', 'ctb', 'ctb2', 'h1', $
			'htb', 'htb2', 'c2', 'h2', $
			'c3', 'h3', 'h3tb', 'c4', 'h4', 'c5', 'h5', $
			'c6', 'h6', 'c7', 'h7', 'h7tb', 'c8', 'h8']
;junk = file_list(outdir, 'case_*T', file=sdirs, /cd)
junk = file_list(outdir, 'case_*', file=sdirs, /cd)
dirs = concat_dir(outdir, sdirs)
ff = file_list(dirs, 'real_time*.txt')
;
n = n_elements(dirs)
;ff0 = file_list(dirs(n-1), 'real_time*.txt')
ff0 = file_list(dirs, 'real_time*.txt')
nnn = n_elements(ff0)
ff0 = ff0(nnn-2:nnn-1)	;take last two only
mnem = ''
for i=0,n_elements(ff0)-1 do begin
    mat = rd_tfile(ff0(i), 5)
    v = reform(mat(0,*))
    if (strpos(ff0(i), '_gse') ne -1) then v = v(sort(v))
    mnem = [mnem, v]
end
;
nn = n_elements(mnem)
out = strarr(n+1, nn)
out(0,*) = mnem
for idir=0,n_elements(dirs)-1 do begin
    ff = file_list(dirs(idir), 'real_time*.txt')
    if (ff(0) ne '') then for ifil=0,n_elements(ff)-1 do begin
	print, 'Reading: ', ff(ifil)
	mat = rd_tfile(ff(ifil), 5)
	v0 = reform(mat(0,*))
	v1 = reform(mat(1,*))
	ss = where_arr( v0, mnem, /map)
	out(idir+1, ss) = v1
	;
	ss2 = where(ss eq -1, nss2)
	if (nss2 ne 0) then out(idir+1, ss(ss2)) = 'N/A'
    end
end
;
ss = where(out eq '-3276.70', nss)
if (nss ne 0) then out(ss) = 'OPEN TC'
ss = where(out eq '9999.00', nss)
if (nss ne 0) then out(ss) = ''
ss = where(out eq '********', nss)
if (nss ne 0) then out(ss) = ''
ss = where(out eq '****************', nss)
if (nss ne 0) then out(ss) = ''

;
break_file, dirs, dsk_log, dir00, filnam
out(1:n, 0) = filnam
out2 = ['<html><head><title>Thermal Balance/Vacuum Temperatures</title><body>',$
        strtab2html(out), $
        ' ', $
        'Page Generated: ' + !stime, $
        '</body></html>']
;
prstr, out2, file=concat_dir(outdir, 'summary10cases.html')
;
end