;+
; Project     : VSO
;
; Name        : URL_VALID
;
; Purpose     : Validate a URL by sending a HEAD request to it.
;
; Category    : utility sockets
;
; Inputs      : URL = URL to validate
;
; Outputs     : OK = 1/0 is valid or not
;
; Keywords    : ERR = error message
;
; History     : 18-Jan-2019, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function url_valid,url,_ref_extra=extra

chk=sock_check(url,code=scode,response_code=rcode,verbose=0)
if ~chk then sock_error,url,scode,response_code=rcode,_extra=extra

return,chk
end
