;+
; NAME:
;       iris_make_chae_flat
;
; PURPOSE:
;       Produce a flat field image for arbitrarly shifted
;       observations of the same scene.
;
; CALLING SEQUENCES:
;         flat = iris_make_chae_flat(
;
; INPUT:
;         imgs - an array of images
;
; OPTIONAL INPUT:
;
; INPUT KEYWORDS:
;        min_frac   - if set, the data is enforced to have values
;                     greater than or equal to minfrac*median(data)
;        xr, yr     - ranges of x & y to use (2 element arrays, min & max)
;        mask0      - single bytearray mask (1=good data, 0=bad) used
;                     on all images
;   See gaincalib for the other keywords
;
; RETURNS:
;        flat field image, ie divide a raw image by this to correct it
;
; OUTPUTS:
;        xoffset, yoffset - arrays of x & y offsets of the input images
;        niter - # iterations used
;        last_error - final error achieved
;
;  HISTORY
;        2004 July, J. Chae. Added new keywords: setmask and minfrac
;  9-Dec-2009 TDT Hinode SOT version
;  9-Aug-2010 TDT 2nd Hinode SOT version
; 11-Sep-2012 TDT added niter & last_error
; 25-Sep-2012 TDT added notvshow keyword
; 25-Jul-2013 SAJ modified for IRIS
;-

function iris_make_chae_flat, imgs, xoffset, yoffset, niter, last_error, $
                              min_frac=min_frac, object=object, c=c, $
                              maxiter=maxiter, min_error=min_error, $
                              xr=xr, yr=yr, shift_flag=shift_flag, $
                              mask=mask, notvshow=notvshow

if not keyword_set(minfrac) then minfrac=0.001
if not keyword_set(notvshow) then notvshow = 0

nf=(size(imgs))[3]

for k=0, nf-1 do begin
   tmp = imgs[*,*,k]

   if k eq 0 then begin
      s=size(tmp)
      if n_elements(xr) ne 2 then xr=[0, s(1)-1]
      if n_elements(yr) ne 2 then yr=[0, s(2)-1]
      nx=xr[1]-xr[0]+1
      ny=yr[1]-yr[0]+1
      logimages=fltarr(nx, ny, nf)
   endif
   m1=median(tmp[xr[0]:xr[1], yr[0]:yr[1]])*minfrac > 0.001
   logimages(*,*,k) = alog(tmp[xr[0]:xr[1], yr[0]:yr[1]]>m1 )
   if (not notvshow) then begin
      if k eq 0 then begin
         mlogi = mean(logimages[*,*,0])
         slogi = mean(abs(logimages[*,*,0]-mlogi))
         tvlo = mlogi - 6.*slogi
         tvhi = mlogi + 4.*slogi
         window, 0, xsize=nx, ysize=ny
      endif
      tv, bytscl(logimages[*,*,k],tvlo,tvhi)
      wait,.2
   end
endfor

if keyword_set(mask) then begin
   if (size(mask))[0] lt 3 then mask=rebin(mask, nx, ny, nf)
endif

if keyword_set(c) then c=alog(c)

t1=systime(/second)

flat = iris_chae_gaincalib(logimages, xoffset, yoffset, niter, last_error, $
                           object=object, c=c, mask=mask, maxiter=maxiter, $
                           shift_flag=shift_flag, min_error=min_error, $
                           notvshow=notvshow)

t2=systime(/second)
print, t2-t1, ' seconds elapsed'

flat1=replicate(1., s(1), s(2))
flat1(xr(0):xr(1), yr(0):yr(1))=exp(flat)

object1=replicate(exp(median(object)), s(1), s(2))
object1(xr(0):xr(1), yr(0):yr(1))=exp(object)
object=object1

if keyword_set(c) then c=exp(c)

return, flat1

end
