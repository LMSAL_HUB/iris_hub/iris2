pro sxi_disp_hass_drec, recs, outfil=outfil, hc=hc, date=date, drectime=drectime
;
;
;HISTORY:
;	Written Oct-00 by M.Morrison
;V1.01	16-Nov-00 (MDM) - Added keyword option, drectime
;
if (n_elements(recs) eq 0) then begin
    if (n_elements(date) eq 0) then date = ut_time()
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/0x0018/' + fid + '.0x0018'
    recs = sag_rd_hk(infil, 0, 1000)
end
;
;
;
;
out = ['SXI_DISP_HASS_DREC  Ver 1.01  Program run: ' + !stime, ' ', $
	'     Date/Time           SerNo     LOBT Sec  FracSec  PktCount #PairInPkg #PairInSeries', ' ']
	; 1-JUN-00  03:09:45.796       0         0        0        0        0        0
;
for i=0,n_elements(recs)-1 do begin
    rec0 = recs(i)
    dattim = sxi_time_conv('INSTRUMENT', struct=rec0.pkt_head, /string, /msec)
    dattim2 = sxi_time_conv('INSTRUMENT', struct=rec0.drec, /string, /msec)
    out0 = dattim + get_infox(rec0.drec, 'serno,time,ftime,pcnt,npair_pkt,npair_ser')
    v = rec0.data(0,*)
    outY = string(min(v), max(v), format='("Y:", i6,"/",i6)')
    v = rec0.data(1,*)
    outZ = string(min(v), max(v), format='("Z:", i6,"/",i6)')
    out00 = out0 + "  " + outY + "  " + outZ
    if (keyword_set(drectime)) then out00 = out00 + ' ' + dattim2
    out = [out, out00]
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil), /land
end
