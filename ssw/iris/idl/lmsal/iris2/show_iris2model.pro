@ ./mierrplot.pro
@ ./miver.pro
@ ./colors_iris2model.pro


;  show_iris2model_multi, model, show_fit=1, arcsec=5, aux_ima = model.iris_cal[*,*,55]<3>0, color=3

function show_iris2model, iris_model, show_fit=show_fit, step=step, aux_ima=aux_ima, show_var=show_var, $
	show_chi2 = show_chi2, show_index = show_index, show_mu = show_mu, put_threshold=put_threshold, $
        color_index=color_index, zoom=zoom, arcsec=arcsec, raster_sel=raster_sel, asymmetric_vlos=asymmetric_vlos
;+
;
; NAME:
;   show_iris2model 
;
; PURPOSE:
;      To display a model atmosphere (T, vlos, vturb, electron density) recovered from IRIS Mg II h&k lines 
;      using IRIS^2 - or just IRIS2 - inversion code (Sainz Dalda et al., ApJL 875 L18 (2019)).
;      To recover the IRIS2 model atmosphere structures see IRIS2 function.
;
;      In adittion to display the model atmosphere, depending of the information level in the recovered
;      IRIS2MODEL model atmosphere structure, this function is able to display the observed and fit
;      profiles, the chi2 map, the index map, and the original data cube.
;
;      The function returns a structure which contains information of the points selected by the user.
;
; CATEGORY:
;  Visualization.
;
; SAMPLE CALLS:
;   Inputing file list:
;   IDL> sel = show_iris2model(iris2model)
;   IDL> sel = show_iris2model(iris2model, raster_sel=[1,3], show_fit=1) 
;
; INPUTS:
;   iris2model structure obtained with IRIS2MODEL. Depending of the information 
;   contained in this structure the behavior of show_iris2model may be different.
;
; OUTPUT:
;   By default, if none point has been stored, the output is an structure containing the information about 
;   the model atmospere, uncertainty and other values of the point where the mouse is located. 
;   If a position(s) is(are) has been stored by the user pressing "S", the output is an structure with the 
;   position(s) as it is in the displayed image, in the original data, the filenames of the data, and
;   other information for the selected points. Depending on the level of information or iris2model 
;   structure the output is:
;   For level_info equals 0:
;       - x_display: the X value of the selected point in the displayed image coordinates.
;       - y_display: the Y value of the selected point in the displayed image coordinates.
;       - x_in_raster: the X value of the selected point in the original data coordinates.
;       - y_in_raster: the Y value of the selected point in the original data coordinates.
;       - model_xy: the model atmosphere in the selected point.
;       - ltau: optical depth in log(tau) where the model atmosphere is sampled.
;       - uncertainty_xy: the uncertainty of the model atmosphere in the selected point.
;       - chi2_xy: the chi2 of the fit between the observed profile and the inverted RP in the selected point.
;       - raster_index_xy: the number of the raster for the selected point.
;       - raster_filename_xy: the file name of the raster for the selected point.
;       - all_rasters_filenames: all the file name rasters considered to build iris2model, i.e. during the inversion.
;       - selected_rasters: the number of the raster(s) selected through the keywor raster_sel.
;   For level_info equals 1, the variables in the output structure are those in the output 
;   for level_info equals 0 plus:
;       - obs_xy: the calibrated observed profile in the selected point.
;       - iris2_inv_xy: the inverted RP in the selected point.
;       - wl: wavelength in Angstrom where obs_xy was observed and iris2_inv_xy was interpolated.
;   For level_info equals 2, the variables in the output structure are those in the output 
;   for level_info equals 1 plus:
;       - index_db_xy: index of the inverted RP in the IRIS2 database for the selected point.
;       - mu_db_xy: mu value of the inverted RP in the IRIS2 database for the selected point.
;
;
; KEYWORDS:
;    show_chi2: it displays the chi2-map. 
;    show_index: if level_info in the iris2model structure is 2, it displays a map showing the
;         index of the representatvie profile (RP) used in the IRIS2 database during the inversion.
;         Thus, the user can know what RP and from what observation in the database it comes from.
;    show_mu: if level_info in the iris2model structure is 2, it displays a map of the mu position
;         of the RP in the IRIS2 database used for the inversion.
;    show_fit: if level_info in the iris2model structure is 1 or 2, a new window will show the 
;         the observed (dashed) and fit profiles (thick), with the chi2 information.
;    put_threshold: sets a threshold for the variables showed.
;    color_index: sets the colortable used to display the data, e.g. color_index = 3 is equivalent to 
;         loadct, 3. The IRIS2 model atmosphere variables have their own default colortables. 
;    zoom: If zoom is just a number, the data to be displayed are zoom in/out by a factor of zoom.
;         in both axes (X and Y). If zoom is a tuple, the data to be displayed are zoom in/out by a
;         factor zoom[0] in X axis, and by a factor zoom[1] in Y axis.
;    arcsec: If it equals 1, the data are zoom to the plate scale, i.e. arcsec/px. Thus, if an observation
;         covers an area of 120"x110", the data are displayed as 120x110 px^2. Because that size may be a small
;         image, if arcsec is a number > 1, it enlarges the data by that factor, e.g. arcsec=3 displays an
;         image of 360x330px^2. 
;    raster_sel: List of numbers of the rasters in the iris2model structure to be displayed, e.g. 
;         raster_sel=[0,3,5] will show the corresponding rasters in the IRIS2 model structure. The position
;         of a selected point is returned both relative to the displayed image and to the original raster.
;         
; COMMANDS:
;    Several commands are encoded in some letters (case-insensitive). This a list of letters and commands and
;    a description of their actions:
;
;    - To display the discription of these commands:
;      '?' -> Shows the help information.
;
;    - Moving along the 3rd dimension in the model atmosphere (optical depth) or the original calibrated data
;      (spectral information). A vertical line pointing out the current position is displayed in the optical
;      depth in the model variables plots or in thes spectral postion in the profile plots. It changes as:
;      'Z' -> Moves back along the 3rd dimension of the data cube.
;      'X' -> Moves forward along 3rd dimension of the data cube.
;
;    - Toggles between the model atmosphere and the original (calibrated) data or an auxiliar image (for level_info is 1 or 2):
;      'M' -> Toggles between the  model atmosphere and the calibrated observed data cube, or between the model 
;             atmosphere or an auxiliar image.
;
;    - Changes between the variables in the model atmosphere:
;      'V' -> Changes the variable displayed following this order: T -> vlos -> vturb -> elec. density -> T ...
;
;    - Changing the threshold of the image displayed (not of the values in the data):
;      'H' -> Turns DOWN the LOWER limit of displayed image.
;      'J' -> Turns UP the LOWER limit of displayed image.
;      'L' -> Turns UP the UPPER limit of displayed image.
;      'K' -> Turns DOWN the UPPER limit of displayed image.
;      'T' -> Resets the lower and upper limits (thresholds)  of the displayed image.
;
;    - Activate/Deactivate the profile+fit window:
;      'F' -> Shows/Hides the profile+fit window.
;
;    - Storing the position and other information:
;      'S' -> Stores the position, the corresponding values of model atmosphere and uncertainty in that
;      position and other values depending on the level_info in the iris2model structure. The position
;      is stored as it is displayed in the image (in the orignal scale of the data, i.e. without taking into
;      account the zoom) and as it is in the original raster.
;
;    - Changing the size of the displayed image:
;      '+' -> Makes the displayed image 10% larger.
;      '-' -> Makes the displayed image 10% smaller.
;      'a' -> Changes the displayed image to 1px/arcsec scale.
;
;    - Quit show_iris2model:
;      'Q' -> Quits show_iris2model.
;
; AUTHOR:
;    A. Sainz Dalda (asainz.solarphysics@gmail.com)
;
; DATE:
;    - First version updated to SolarSoft: 2019/05/15
;
;
;-

;;; WARNING: DO NOT REMOVE/CHANGE THIS LINE ;;;
device, True=24, Decomposed=0, Retain=2
;;; WARNING: DO NOT REMOVE/CHANGE THIS LINE ;;;
;;; Your life will become misarable if you do it.

;;; Reshaping the data
print	
print, 'Reshaping IRIS2 model structure data. This may take a few seconds, be patient... '	
print	


;;; Selecting the rasters or not.
n_iris_model =  n_elements(iris_model)
dim_data = size(iris_model.model)	
selected_rasters = dindgen(n_iris_model)
if keyword_set(raster_sel) then begin
   selected_rasters = raster_sel
   dim_data = size(iris_model[selected_rasters].model)	
endif

;;; Number of variables in the model atmos
n_var = 4

;;; Initialization of some important variables
level_info =  iris_model[0].level_info
ltau = iris_model[0].ltau
list_points = LIST() 
all_rasters_filename = iris_model.input_filename
x_scl = iris_model[0].header.cdelt3
y_scl = iris_model[0].header.cdelt2
active_box=0
count_box =0
x_before_box = 0
y_before_box = 0
zoom_before_box = [1,1]
;
if keyword_set(show_fit) eq 0 then show_fit = 0
if level_info eq 0 then begin
	show_fit = 0
	show_index = 0
	show_mu  = 0
	;show_monochromatic  = 0
endif	
if level_info eq 1 then begin
	show_index = 0
	show_mu  = 0
endif
show_monochromatic  = 0
;if keyword_set(show_obs) then show_monochromatic = 1
if keyword_set(show_fit) and level_info gt 0 then show_monochromatic = 1
;
;restore, './colors_iris2model.sav'
;;; Colors are hard-coded at colors_iris2model.pro
;
;
var_text = ['T [kK]', 'vlos [km/s]', 'vturb [km/s]', 'log(ne [cm^-3])']
var_text_inplot = ['T [K]', 'vlos [km/s]', 'vturb [km/s]', 'log(ne [cm^-3])']
var_factor = [1e3, 1e5, 1e5, 1.]
rgb= colors_iris2model('t_cool_hot')
;;; Limits for T, vlos, vturb, ne
lim_inf = [4.,-6.,0.01,8.]
lim_sup = [15.,6.,5.,15.]
;;; Limits for Intensity (calibrated data)
lim_inf_obs = 0.01
lim_sup_obs = 1.
;;; Limits given by the user
if keyword_set(put_threshold) then begin
   lim_inf = put_threshold[*,0]
   lim_inf = put_threshold[*,1]
endif 
if keyword_set(show_var) eq 0 then show_var ='T'
;
;
if keyword_set(show_var) then begin
   case strupcase(show_var) of
        'T': begin 
		var=0
	        rgb= colors_iris2model('t_cool_hot')
	     end	
        'VLOS': begin
		var=1
	        rgb = colors_iris2model('v_bgr')
	     end
        'VTURB': begin
		var=2
		loadct, 7, /silent
		tvlct, r, g, b, /get
		rgb[0] = r
		rgb[1] = g
		rgb[2] = b
             end
        'NE': begin
		var=3
		lims_ima=minmax(iris_model[selected_rasters].model[*,*,*,3])
	        rgb = colors_iris2model('rainbow')
	      end	
   endcase
endif    
;
lims_ima = [lim_inf[var], lim_sup[var]]
;
if keyword_set(show_monochromatic) then loadct, 3, /silent
;
if keyword_set(color_index) then begin
	loadct, color_index, /silent 
endif else begin
	;load_vel
        tvlct, rgb[*,0], rgb[*,1], rgb[*,2]
endelse
;;; Initialization of some important variables


;;; Loading some variables from iris2model depending on level_info
if n_iris_model eq 1 or n_elements(selected_rasters) eq 1 then begin
   model = iris_model[selected_rasters].model	
   unc = iris_model[selected_rasters].uncertainty
   for i_var = 0, n_var-1 do model[*,*,*,i_var] = iris_model[selected_rasters].model[*,*,*,i_var]/var_factor[i_var]
   for i_var = 0, n_var-1 do unc[*,*,*,i_var] = iris_model[selected_rasters].uncertainty[*,*,*,i_var]/var_factor[i_var]
   chi2 = iris_model[selected_rasters].chi2
   if keyword_set(show_index) eq 1 or level_info gt 1 then map_pos = iris_model[selected_rasters].map_index_db
   if keyword_set(show_mu) eq 1 or level_info gt 1 then map_mu = iris_model[selected_rasters].map_mu_db
   if keyword_set(show_fit) eq 0 then show_fit = 0
   if keyword_set(show_fit) or level_info ge 1 then begin
      wl_obs = iris_model[selected_rasters].wl
      iris2_inv = iris_model[selected_rasters].iris2_inv
      iris_cal = iris_model[selected_rasters].iris_cal
   endif
   if level_info gt 0 then begin
   ;if keyword_set(show_monochromatic) then begin
	   dim_data_ori = size(iris_model[selected_rasters].iris_cal)
	   ;data_cal = fltarr(dim_data_ori[1], dim_data_ori[2], dim_data_ori[3])
	   data_cal = iris_model[selected_rasters].iris_cal
    endif 	   
endif else begin
   dim_data = size(iris_model[selected_rasters].model)	
   model = fltarr(dim_data[1]*dim_data[5], dim_data[2], dim_data[3], dim_data[4])   
   unc = fltarr(dim_data[1]*dim_data[5], dim_data[2], dim_data[3], dim_data[4])   
   chi2 =fltarr(dim_data[1]*dim_data[5], dim_data[2])
   if level_info ge 1 then begin
	  dim_data_ori = size(iris_model[selected_rasters].iris_cal)
	  data_cal = fltarr(dim_data_ori[1]*dim_data_ori[4], dim_data_ori[2], dim_data_ori[3])
	  for k = 0, dim_data_ori[4]-1 do data_cal[dim_data_ori[1]*k:dim_data_ori[1]*(k+1)-1,*,*] = iris_model[selected_rasters[k]].iris_cal
	  if level_info gt 1 then begin
	     map_pos = fltarr(dim_data[1]*dim_data[5], dim_data[2])
             map_mu = fltarr(dim_data[1]*dim_data[5], dim_data[2])
	     for k = 0, dim_data[5]-1 do begin
                 map_pos[dim_data[1]*k:dim_data[1]*(k+1)-1,*] = iris_model[selected_rasters[k]].map_index_db
                 map_mu[dim_data[1]*k:dim_data[1]*(k+1)-1,*] = iris_model[selected_rasters[k]].map_mu_db
             endfor   
	  endif 
   endif	   
   for k = 0, dim_data[5]-1 do begin
	   model[dim_data[1]*k:dim_data[1]*(k+1)-1,*,*,*] = iris_model[selected_rasters[k]].model
	   unc[dim_data[1]*k:dim_data[1]*(k+1)-1,*,*,*] = iris_model[selected_rasters[k]].uncertainty
           chi2[dim_data[1]*k:dim_data[1]*(k+1)-1,*] = iris_model[selected_rasters[k]].chi2
   endfor
   for i_var = 0, n_var-1 do model[*,*,*,i_var] = model[*,*,*,i_var]/var_factor[i_var]
   for i_var = 0, n_var-1 do unc[*,*,*,i_var] = unc[*,*,*,i_var]/var_factor[i_var]
   if keyword_set(show_fit) or level_info ge 1 then begin
      wl_obs = iris_model[0].wl
      dim_inv_iris = size(iris_model[selected_rasters].iris2_inv)
      iris2_inv = fltarr(dim_inv_iris[1]*dim_inv_iris[4], dim_inv_iris[2], dim_inv_iris[3])
      iris_cal = fltarr(dim_inv_iris[1]*dim_inv_iris[4], dim_inv_iris[2], dim_inv_iris[3])
      for k = 0, dim_inv_iris[4]-1 do begin
	   iris2_inv[dim_inv_iris[1]*k:dim_inv_iris[1]*(k+1)-1,*,*] = iris_model[selected_rasters[k]].iris2_inv
	   iris_cal[dim_inv_iris[1]*k:dim_inv_iris[1]*(k+1)-1,*,*] = iris_model[selected_rasters[k]].iris_cal
      endfor	   
   endif
endelse

;;; Selected filenames. All by default, specific if raster_sel was given.
selected_filenames =  iris_model[selected_rasters].input_filename 

;;; Defining what (auxiliar) image to show
show_aux_ima = 0
if keyword_set(show_chi2) then begin
	threshold_chi2 = 100
	if show_chi2 gt 1 then threshold_chi2 = show_chi2
       	aux_ima = chi2<threshold_chi2
endif	
if keyword_set(show_index) then aux_ima = map_pos
if keyword_set(show_mu)  then aux_ima = map_mu
if keyword_set(aux_ima) then show_aux_ima = 1

;;; Deleting the iris_model from memory
delvar, iris_model


dim=size(model[*,*,*,var])
dim_ima2show = dim 
maxi=max(model[*,*,*,var])
mini=min(model[*,*,*,var])

nxb = dim[1]
nyb = dim[2]

n=0
step = 1 
if keyword_set(inc) then step = inc>0 

;;; Creating windows, and their location.
tampan = GET_SCREEN_SIZE(RESOLUTION=resolution)
;
; Default size plotting windows ("IRIS2 model atmosphere" & "IRIS2 Fit" window)
xsp=700 & ysp=500   
; Default size plotting windows ("IRIS2 model atmosphere" & "IRIS2 Fit" window)
xsp=tampan(0)/2. & ysp=tampan(1)/2.5   
; Position for "IRIS2 Physical Variable Information" window
width_window3 = ysp/4. & xpos3=0 & ypos3=tampan(1)-width_window3
; Position for "IRIS2 model atmosphere" window
xpos0=0 & ypos0=tampan(1)-ysp-width_window3-20
; Position for "IRIS2 3D Model Inspector" window
; xpos1=xpos0 + xsp + 10 & ypos1= ypos0+(ysp/2.)-dim[2]/2.
xpos1=xpos0 + xsp + 10 & ypos1= tampan(1)-dim[2]/2.
; Position for "IRIS2 Fit" window
xpos2=xpos0 & ypos2 = ypos0-ysp - 20 
;
title0='IRIS2 3D Model Inspector'
if keyword_set(title) then  title0=title
;
;
if keyword_set(arcsec) then begin
	zoom = [arcsec*x_scl, arcsec*y_scl]	
	if x_scl eq 0. then zoom[0]=1.
endif 	
;
colorbar_area = 1.
portion_eraser= 0.30
if keyword_set(zoom) eq 0 then zoom=[1.,1.]
window,1, xs=dim_ima2show[1]*zoom[0], ys=dim_ima2show[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
print, xpos0, ypos0, ypos0+ysp+25
window,3, xs=xsp, ys=width_window3,  xpos=xpos3, ypos=ypos3,  title='IRIS2 Physical Variable Information'
;eraser = intarr(dim[1], dim[2]*zoom[1]*(colorbar_area-1.10))
;eraser = intarr(dim[1]*zoom[0], (dim[2]*zoom[1]*(colorbar_area-1)*portion_eraser)>1.)
eraser = intarr(xsp, width_window3/2.);+1
;eraser[0,0] = 0.
;stop
;
window,0, xs=xsp, ys=ysp, xpos=xpos0, ypos=ypos0,  title='IRIS2 Model Atmosphere'
if keyword_set(show_fit) then window,2, xs=xsp, ys=ysp, xpos=xpos2, ypos=ypos2,  title='IRIS2 Fit'
;;; Creating windows, and their location.

;;; Initialization of some important variables for the visualization.
old_mouse = !mouse.button
!mouse.button=0
sw=0
y=0
x=1
!p.multi=[0,1,0]
!mouse.button=1
c=0
ini=n
j=ini
tc=""
enmouse=1
control_mouse=1
dev_kw  = 1
data_kw = 0
size_font_plot = 1.5
x_crs =0
y_crs =0
x_scr =0
y_scr =0

;;; Mg II h&k and Mg UV lines positions
mgII_k_pos_k3 = 2795.528
mgII_h_pos_h3 = 2802.704
mgII_uv_triplet_pos23 = 2797.930 + (2797.998 - 2797.930)


;;; Initialization of some important variables for the visualization.
lim_inf[var] = lims_ima[0]
lim_sup[var] = lims_ima[1]
j = 0
j_wl = 0
j_tau = 18 
chr = 'Z'
string_show = ''
STRING_SHOW_AUX_IMA = ''
x_crs_ant = 0
y_crs_ant = 0


;while ( (!mouse.button eq 1 or !mouse.button eq 4 or  !mouse.button eq 0 ) and tc ne "q") do begin
;;; Where the magic begins...
while (chr ne "Q") do begin

   ;;; Command-line control options	
   chr = strupcase(get_kbrd(0.1)) ;Read character
   
   ; Information about the key shortcuts
   if chr eq "?" or c eq 0 then begin
      print,""
      print,"==================== Key shortcuts for show_iris2model ===================="
      print,""
      print,"    - To display the discription of these commands:"
      print,"      '?' -> Shows the help information."
      print,""
      print,"    - Moving along the 3rd dimension in the model atmosphere (optical depth) or the original calibrated data"
      print,"      (spectral information). A vertical line pointing out the current position is displayed in the optical"
      print,"      depth in the model variables plots or in thes spectral postion in the profile plots. It changes as:"
      print,"      'Z' -> Moves back along the 3rd dimension of the data cube."
      print,"      'X' -> Moves forward along 3rd dimension of the data cube."
      print,""
      print,"    - Toggles between the model atmosphere and the original (calibrated) data, only if level_info is 2:"
      print,"      'M' -> Changes between the  model atmosphere and the data cube."
      print,""
      print,"    - Changes between the variables in the model atmosphere:"
      print,"      'V' -> Changes the variable displayed following this order: T -> vlos -> vturb -> elec. density -> T ..."
      print,""
      print,"    - Changing the thresholds of the image displayed (not of the values in the data) which are updated in the colorbar:"
      print,"      'H' -> Turns DOWN the LOWER limit of displayed image."
      print,"      'J' -> Turns UP the LOWER limit of displayed image."
      print,"      'L' -> Turns UP the UPPER limit of displayed image."
      print,"      'K' -> Turns DOWN the UPPER limit of displayed image."
      print,"      'T' -> Resets the lower and upper limits (thresholds)  of the displayed image."
      print,""
      print,"    - Cleaning the IRIS2 Physical Variable Information window. It my useful if that windows is resizedi by the user."
      print,"      'C' -> Clean the IRIS2 Physical Variable Information window."
      print,""
      print,"    - Activate/Deactivate the profile+fit window:"
      print,"      'F' -> Shows/Hides the profile+fit window."
      print,""
      print,"    - Storing the position and other information:"
      print,"      'S' -> Stores the position, the corresponding values of model atmosphere and uncertainty in that"
      print,"      position and other values depending on the level_info in the iris2model structure. The position"
      print,"      is stored as it is displayed in the image (in the orignal scale of the data, i.e. without taking into"
      print,"      account the zoom) and as it is in the original raster."
      print,""
      print,"    - Changing the size of the displayed image:"
      print,"      '+' or '=' -> Makes the displayed image 10% larger."
      print,"      '-' -> Makes the displayed image 10% smaller."
      print,"      'A' -> Changes the displayed image to 1arcsec/5px"
      print,""
      print,"    - Zoom-in an area in the original field of view (FoV) using 2-step selection:"
      print,"      'I' -> First corner of the zoom-in box selected in the FoV"
      print,"      'I' -> Second corner of the zoom-in box selected in the FoV. It has to be selected just after"
      print,"      the 1st corner of the box."
      print,""
      print,"    - Zoom-out to original FoV:"
      print,"      'O' -> Zoom-out to original FoV (keeping the zoom/arcsec previous to the zoom-in)."
      print,""
      print,"    - Quit show_iris2model:"
      print,"      'Q' -> Quits show_iris2model."
      print,""
      print,"==========================================================================="
      print,""
      c = 1
   endif

   
   ; Quitting 
   if chr eq 'Q' then print, " -> Quitting show_iris2model."

   ; Erasing colorbar dynamically
   if chr eq 'H' or chr eq 'J' or chr eq 'L' or chr eq 'K' or chr eq 'O' or chr eq 'V' or chr eq 'M' then begin
     wset, 3 & COLORBAR, POSITION=[0.1, 0.65, 0.90, 0.80], MAXRANGE=lim_sup_cb, MINRANGE=lim_inf_cb, title=tit_cb, color=0, format=format_cb
     wset, 1
   endif 

   ; Changing to 1px/arcsec scale 
   if chr eq 'A' then begin
      arcsec=5
      print, " -> Changing spatial scale to 1arcsec/5px "
      ;if keyword_set(zoom) ne 0 then arcsec = zoom[0]
      zoom = [arcsec*x_scl, arcsec*y_scl]
      if x_scl eq 0. then zoom[0]=5.
      wdelete, 1
      window,1, xs=dim_ima2show[1]*zoom[0], ys=dim_ima2show[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
   endif 

   ; Enlarge/shrink the image displayed
   if chr eq '+' or chr eq '=' or chr eq '-' then begin
      if chr eq '+' or chr eq '=' then begin
	     incr_zoom = 1.1	   
	     print, ' -> Making the displayed image 10% larger.'
      endif	 
      if chr eq '-' then begin
	     incr_zoom = 0.9	   
	     print, ' -> Making the displayed image 10% smaller.'
      endif	 
      if keyword_set(zoom) eq 0 then zoom=[1.,1.]
      zoom=float(zoom)
      zoom[0] = (zoom[0]*incr_zoom)>0.1
      zoom[1] = (zoom[1]*incr_zoom)>0.1
      ;;print, 'New...', zoom
      ;eraser = intarr(dim[1]*zoom[0], (dim[2]*zoom[1]*(colorbar_area-portion_eraser))>1.)
      ;eraser = intarr(dim[1]*zoom[0], (dim[2]*zoom[1]*(colorbar_area-1)*portion_eraser)>1.)
      ;eraser = eraser+1.
      ;eraser[0,0]=0.
      wdelete, 1
      ;window,1, xs=dim[1]*zoom[0], ys=dim[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
      window,1, xs=dim_ima2show[1]*zoom[0], ys=dim_ima2show[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
      ;tvcrs, x_crs*zoom[0], y_crs*zoom[1]
      ;x_crs = x_crs*zoom[0]
      ;y_crs = y_crs*zoom[1]
   endif 

   ; Moves down aling the 3rd dimension. In the model atmosphere that means to move 
   ; to higher layers in the solar atmosphere.
   if chr eq "Z" then begin
	   print, '-> Moving back along the 3rd dimension of the data cube...'
	   j = j-step
   endif
   ; Moves up down aling the 3rd dimension. In the model atmosphere that means to move 
   ; to lower layers in the solar atmosphere.
   if chr eq "X" then begin
	   print, '-> Moving forward along 3rd dimension of the data cube...'
	   j = j+step
   endif 	   

   ;if keyword_set(show_monochromatic) and control_mouse eq 1 then begin
   if show_monochromatic eq 1 then begin
      j_wl = j
   endif else begin
      j_tau = j
   endelse
   if (j le -1) then begin
                  print, "WARNING: The first image is numbered in 0"
                  ;j=0
                  j=dim[3]-1
                  if keyword_set(show_monochromatic) and control_mouse eq 1 then begin
                          j_wl = dim_data_ori[3]-1
                  endif else begin
                          j_tau = j
                  endelse
   endif
   lim_mouse = dim_data[3]
   if keyword_set(show_monochromatic) and control_mouse eq 1 then lim_mouse = dim_data_ori(3)
   if (j ge lim_mouse) then begin
                  print, "WARNING: There are "+strcompress(lim_mouse,/remove_all)+" images only"
                  ;j=dim[3]-1
                  j=0
                  ;if keyword_set(show_monochromatic) and control_mouse eq 1 then begin
                  if show_monochromatic eq 1 then begin
                          j_wl = j
                  endif else begin
                          j_tau = j
                  endelse
   endif

   ; Toggling between Model/Data or aux_ima
   if chr eq "M" and (level_info ne 0  or keyword_set(aux_ima)) then begin
      if  keyword_set(aux_ima) eq 0 then begin
       	   print, '-> Toggling between Model and Monochromatic or auxiliar image...'
	   show_monochromatic = ~show_monochromatic
	   if show_monochromatic eq 1 then loadct, 3, /silent
	   if show_monochromatic eq 0 then begin
		 ;wset, 1 & tvscl, eraser, 0, dim[2]*zoom[1]
		 ;char = 'V'
		 ;var = var-1>0 ; It will be compensated below 
                 tvlct, rgb[*,0], rgb[*,1], rgb[*,2]
	         wset, 1
           endif		 
           ;if keyword_set(show_monochromatic) and control_mouse eq 1 then begin
           if keyword_set(show_monochromatic) then begin
	      j = j_wl
           endif else begin
	      j = j_tau 
           endelse	  
      endif else begin 	   
	 show_aux_ima=~show_aux_ima
	 if show_aux_ima eq 1 then begin
            rgb= colors_iris2model('t_cool_hot')
            tvlct, rgb[*,0], rgb[*,1], rgb[*,2]
         endif   
      endelse 
      ; To change colortable when toggling back to the model 
      if show_monochromatic eq 0 and show_aux_ima eq 0 then begin 
         chr = 'V'
         var = var-1>(-1) ; It will be compensated below
      endif 	 
   endif 
   if chr eq 'G' then begin
      control_mouse = 0
      dev_kw  = 0
      data_kw = 1
      j = j_tau<dim_data[3]-1
   endif

   ; Toggle between variables in the model atmosphere.
   if chr eq "V" and show_monochromatic eq 0 and show_aux_ima eq 0 then begin
      var = var + 1
      if var gt 3 then var = 0      
      print, '-> Changing displayed variable to ', var_text[var]	   
      case strupcase(var) of
        0: begin
                show_var='T'
                rgb= colors_iris2model('t_cool_hot')
             end
        1: begin
                show_var='VLOS'
                rgb = colors_iris2model('v_bgr')
             end
        2: begin
                show_var='VTURB'
		loadct, 7, /silent
		tvlct, r, g, b, /get
		rgb[*, 0] = r
		rgb[*, 1] = g
		rgb[*, 2] = b
             end
        3: begin
                var=3
                show_var='NE'
                lims_ima=minmax(iris_model.model[*,*,*,3])
                rgb = colors_iris2model('rainbow')
              end
      endcase
      lims_ima=[lim_inf[var], lim_sup[var]]
      tvlct, rgb[*,0], rgb[*,1], rgb[*,2]
      if keyword_set(color_index) then loadct, color_index, /silent 
      lim_inf[var] = lims_ima[0]
      lim_sup[var] = lims_ima[1]
   endif 

   ; Changing the lower limit of the image displayed
   if chr eq 'H' then print, '-> Turning DOWN the LOWER limit of displayed image.'
   if chr eq 'J' then print, '-> Turning UP the LOWER limit of displayed image.'
   ; Changing the upper limit of the image displayed
   if chr eq 'L' then print, '-> Turning UP the UPPER limit of displayed image.'
   if chr eq 'K' then print, '-> Turning DOWN the UPPER limit of displayed image.'
   ; Resetting the limit values to the nominal values
   if chr eq 'T' then print, '-> Resetting the lower and upper limts of the displayed image.'
   if show_monochromatic eq 0 then begin
      if chr eq 'H' then lim_inf[var] = (lim_inf[var]-abs(lim_inf[var])*.02) ;<lim_sup
      if chr eq 'J' then lim_inf[var] = (lim_inf[var]+abs(lim_inf[var])*.02)<lim_sup[var]
      if chr eq 'L' then lim_sup[var] = (lim_sup[var]+abs(lim_sup[var])*.02) ;<lim_sup[var]
      if chr eq 'K' then lim_sup[var] = (lim_sup[var]-abs(lim_sup[var])*.02)>lim_inf[var]
      if chr eq 'T' then begin
	 lim_inf[var] = lims_ima[0]
         lim_sup[var] = lims_ima[1]
      endif	 
      if lim_inf[var] eq lim_sup[var] then lim_inf[var] = (lim_inf[var]-lim_inf[var]*.03)
      if keyword_set(asymmetric_vlos) eq 0 and var eq 1 then begin
	      if chr eq 'H' or chr eq 'J' then lim_sup[var] = -lim_inf[var]>0
	      if chr eq 'L' or chr eq 'K' then lim_inf[var] = -lim_sup[var]<0
      endif 	      
   endif else begin 
      if chr eq 'H' then lim_inf_obs = (lim_inf_obs-abs(lim_inf_obs)*.02)>0 ;<lim_sup
      if chr eq 'J' then lim_inf_obs = (lim_inf_obs+abs(lim_inf_obs)*.02)<lim_sup_obs
      if chr eq 'L' then lim_sup_obs = (lim_sup_obs+abs(lim_sup_obs)*.02) ;<lim_sup_obs
      if chr eq 'K' then lim_sup_obs = (lim_sup_obs-abs(lim_sup_obs)*.02)>lim_inf_obs
      if chr eq 'T' then begin
	 lim_inf_obs = 0.01
         lim_sup_obs = 1.
      endif	 
   endelse
   
   ; Activating the profile+fit window
   if chr eq 'F' and level_info ge 1 then begin
      show_fit = ~show_fit
      if keyword_set(show_fit) then begin
	 window,2, xs=xsp, ys=ysp, xpos=xpos2, ypos=ypos2,  title='IRIS2 Fit'
         print,"-> Showing the profile+fit window."
      endif else begin
         print,"-> Hiding the profile+fit window."
         wdelete, 2
      endelse
   endif    
   ;;; End of Command-line control options	

   ; if keyword_set(aux_ima) eq 0 and show_aux_ima eq 0 then begin
   if show_aux_ima eq 0 then begin
      if keyword_set(show_monochromatic) eq 0 then begin	   
         ima2show = model[*,*,j,var]<lim_sup[var]>lim_inf[var]
	 ima2show[0,0] = lim_sup[var]
	 ima2show[0,1] = lim_inf[var]
         tit_cb = var_text[var]
         lim_sup_cb = lim_sup[var]
         lim_inf_cb = lim_inf[var]
         if var eq 3 then ima2show =alog10(model[*,*,j,var])<lim_sup[var]>lim_inf[var]
         format_cb = '(F6.2)'
      endif else begin 
         ima2show = data_cal[*,*,j_wl]*1e6>lim_inf_obs<lim_sup_obs
         tit_cb = 'I [x 1e6 erg cm^-2 s^-1 sr^-1 Hz^-1]'
         lim_sup_cb = lim_sup_obs
         lim_inf_cb = lim_inf_obs
	 ima2show[0,0] = lim_sup_obs
         ima2show[0,1] = lim_inf_obs
         format_cb = '(F6.2)'
      endelse
	 ;print, ima2show[0,0], ima2show[0,1]
   end else begin
      ima2show = aux_ima
      lim_inf_cb = min(aux_ima)
      lim_sup_cb = max(aux_ima)
      format_cb = '(F0.1)'
      tit_cb = 'aux_ima'
      if keyword_set(show_chi2) then tit_cb = 'chi2'
      if keyword_set(show_index) then tit_cb = 'RP_index_db'
      if keyword_set(show_mu) then tit_cb = 'mu' 
   endelse

   ; Zoom manual box 
   if chr eq 'I' then begin
      if count_box eq 0 then active_box = ~active_box	   
      if active_box eq 1 then begin
	 count_box+=1
	 if count_box eq 1 then begin
	    xb_1 = x_crs 
	    yb_1 = y_crs 
	    dim_ima2show = dim
	    print, ' -> First corner of the zoom-in box selected in: ', xb_1, yb_1
	 endif     
	 if count_box eq 2 then begin
	    xb_2 = x_crs 
	    yb_2 = y_crs 
	    print, ' -> Second corner of the zoom-in box selected in: ', xb_2, yb_2
	    nxb = abs(xb_2 - xb_1)/zoom[0] 
	    nyb = abs(yb_2 - yb_1)/zoom[1] 
	    nxb = abs(xb_2 - xb_1)
	    nyb = abs(yb_2 - yb_1)
	    nxb = ceil(nxb)
	    nyb = ceil(nyb)
	    dim_before_box = dim_ima2show
	    x_crs_before_box = xb_1<xb_2
	    y_crs_before_box = yb_1<yb_2
	    x_before_box = fix(x_crs_before_box/zoom[0])
	    y_before_box = fix(y_crs_before_box/zoom[1])
	    x_before_box = fix(x_crs_before_box)
	    y_before_box = fix(y_crs_before_box)
	    zoom_before_box = zoom
            ;dim = [1,nxb+1, nyb+1]
	    ;stop
	    print, 'Zoom-in box: ', x_before_box,(x_before_box+nxb+1)<dim[1]-1,y_before_box,(y_before_box+nyb+1)<dim[2]-1
            aux = ima2show[x_before_box:(x_before_box+nxb+1)<dim[1]-1,y_before_box:(y_before_box+nyb+1)<dim[2]-1]
	    ;stop
	    ;ima2show[x_before_box+2,y_before_box+2] = ima2show[0,0]
	    ;ima2show[x_before_box+1,y_before_box+1] = ima2show[0,1]
            dim_ima2show = size(aux)
            x_crs = 0 ;(x_crs - xb) < dim[1]
            y_crs = 0 ;(y_crs - yb) < dim[2]
	    zoom = zoom*5.
	 endif     
      endif else begin 	 
     	dim_ima2show = dim
	zoom = zoom_before_box
	;x_crs = x_crs_before_box
	;y_crs = y_crs_before_box
	x_before_box = 0
	y_before_box = 0
	x = x_before_box
	y = y_before_box
	nxb = dim[1]
	nyb = dim[2]
      endelse
      wdelete, 1
      window,1, xs=dim_ima2show[1]*zoom[0], ys=dim_ima2show[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
   endif 

   ; Zoom-out to the whole FoV (keeping the previous zoom/arcsec factor)
   if chr eq 'O' then begin
      dim_ima2show = dim
      x_before_box = 0
      y_before_box = 0
      nxb = dim[1]
      nyb = dim[2]
      zoom=zoom_before_box
      if count_box ge 1 then active_box = ~active_box
      count_box = 0
      wdelete, 1
      window,1, xs=dim_ima2show[1]*zoom[0], ys=dim_ima2show[2]*zoom[1]*colorbar_area, xpos=xpos1, ypos=ypos1, title=title0
   endif

   ; Re-defining ima2show
   ima2show = ima2show[x_before_box:(x_before_box+nxb)<dim[1]-1,y_before_box:(y_before_box+nyb)<dim[2]-1]

   ; Applying the zoom to ima2show     
   if keyword_set(zoom) then ima2show = congrid(ima2show, dim_ima2show[1]*zoom[0], dim_ima2show[2]*zoom[1])

   ; Cross-hair locator
   ima2show[*,(y_scr)<(dim_ima2show[2]*zoom[1]-1)>0]=max(ima2show)
   ima2show[(x_scr)<(dim_ima2show[1]*zoom[0]-1)>0,*]=max(ima2show)

   ; Feeding the thresholds for the proper image display
   ima2show[0,0] = lim_inf_cb
   ima2show[0,1] = lim_sup_cb

   ; Displays/Shows ima2show 
   wset, 1 & tvscl, ima2show 

   ; Erasing/cleaning  the colorbar properly, either because of an update in the limits or because a resize of the window 
   if chr eq 'H' or chr eq 'J' or chr eq 'L' or chr eq 'K' or chr eq 'O' or chr eq 'V' or chr eq 'M' or chr eq 'C' then begin
      if chr eq 'C' then print, ' -> Cleaning IRIS2 Physical Variable Information window'	   
      wset, 3
      eraser = intarr(!d.x_size, !d.y_size/2) ;+1.
      ;eraser[0,0] = 0
      erase 
   endif    

   if lim_inf_cb eq lim_sup_cb then lim_inf_cb = lim_sup_cb*0.99
   wset, 3 & COLORBAR, POSITION=[0.1, 0.65, 0.90, 0.80], MAXRANGE=lim_sup_cb, MINRANGE=lim_inf_cb, title=tit_cb, format=format_cb
   ;print, control_mouse, data_kw, dev_kw
   ;wset, control_mouse  & cursor, x_crs,y_crs, 0, data=data_kw, dev=dev_kw
   ; Reading the coordinates in pixels
   wset, control_mouse  & cursor, x_scr,y_scr, 0, data=0, dev=1

   ; Transforming the coordinates in pixels to the data scale because of the zoom/arcsec factor
   x_crs = floor(x_scr/float(zoom[0]))
   y_crs = floor(y_scr/float(zoom[1]))
   x_crs_ant = x_crs
   y_crs_ant = y_crs
   ; Taking into account the origin of the zoom-in box 
   if control_mouse eq 1 then begin
      if active_box eq 1 then begin
           x = (x_crs+x_before_box)<(dim[1]-1)>0 
           y = (y_crs+y_before_box)<(dim[2]-1)>0 
      endif else begin  
	   x = x_crs<(dim[1]-1)>0
	   y = y_crs<(dim[2]-1)>0
      endelse 	   
   endif

   ; Plotting the model atmosphere  
   cond_inside_data = where(model[x,y,*,3] eq 0)
   
   ;if ( x lt dim_data[1]-1 and y lt dim_data[2]-1 and x ge 0 and y ge 0 and  cond_inside_data[0] eq -1) then begin
   if ( x lt dim[1] and y lt dim[2] and x ge 0 and y ge 0 and  cond_inside_data[0] eq -1) then begin
      wset, 0
      yv_real = [model(x,y,j_tau,var)]
      var_txt_show = var_text_inplot[var]
      wset, 0 
      !p.multi = [0,2,2,0]
      data_unc_show = unc[x,y,j_tau,var] ;/var_factor[var]
      if var eq 3 then begin
	 yv_real = alog10(yv_real)
         data_unc_show = alog10(data_unc_show)
         data_unc_show = abs(yv_real - data_unc_show)
      endif	 
      for i_var=0,3 do begin
	  data_model = model[x,y,*,i_var] ; /var_factor[i_var]    
	  data_unc = unc[x,y,*,i_var] ;/var_factor[i_var]
	  data_unc_minus = data_model-data_unc
	  data_unc_plus = data_model+data_unc
	  ;if i_var eq 4 then begin
	   ; 	  data_model = reform(alog10(data_model))
	   ;	  w_inf = where(finite(data_model, /inf), cuan)
	   ;	  if cuan ne 0 then data_model[w_inf] = 0.
	   ;	  w_nan = where(finite(data_model, /nan), cuan)
	   ;	  if cuan ne 0 then data_model[w_nan] = 0.
	   ;	  data_unc_minus = reform(alog10(data_unc_minus))
	   ;	  w_inf = where(finite(data_unc_minus, /inf), cuan)
	   ;	  if cuan ne 0 then data_unc_minus[w_inf] = 0.
	   ;	  w_nan = where(finite(data_unc_minus, /nan), cuan)
	   ;	  if cuan ne 0 then data_unc_minus[w_nan] = 0.
           ;	  data_unc_plus = reform(alog10(data_unc_plus))
	   ;	  w_inf = where(finite(data_unc_plus, /inf), cuan)
	   ;	  if cuan ne 0 then data_unc_plus[w_inf] = 0.
	   ;	  w_nan = where(finite(data_unc_plus, /nan), cuan)
	   ;	  if cuan ne 0 then data_unc_plus[w_inf] = 0.
          ;endif		  
	  ylog = 0
	  if i_var eq 3 then ylog=1
	  ; plot,  ltau, iris_model.model[x,y,*,i_var], psym=0, subtitle=var_text[i_var], /data
          ; errplot, ltau, iris_model.model[x,y,*,i_var]-iris_model.unc[x,y,*,i_var], iris_model.model[x,y,*,i_var]+iris_model.unc[x,y,*,i_var]
	  plot,  ltau, data_model, psym=0, title=var_text[i_var], /data, /yno, charsize=size_font_plot, xtitle='!3log(!4'+string(115B)+'!3)', ylog=ylog
          mierrplot, ltau, data_unc_minus, data_unc_plus
	  miver, ltau[j_tau], linestyle=1, thick=7, color=11*16, ylog=ylog
	  ;plots, [ltau[j],min(iris_model.model[x,y,*,i_var])], [ltau[j],max(iris_model.model[x,y,*,i_var])], /data
	  ;plots, [ltau[j],ltau[j]], [min(iris_model.model[x,y,*,i_var]), max(iris_model.model[x,y,*,i_var])], /data, linestyle=2, thick=3 
      endfor 
      wset, 0 
      string_pos = '[X,Y] = ['+strcompress(x)+','+strcompress(y)+']'
      xyouts, 0.5, 0.53, string_pos, /norm, align=0.5, size=size_font_plot
      format = '(a, a, a, F-0.2, a, F-0.2)'
      if var eq 0 then begin
	      yv_real = yv_real*1e3
	      data_unc_show =  data_unc_show*1e3
              format = '(a, a, a, I0, a, I0)'
      endif
      string_ltau = string('At !3log(!4'+string(115B)+'!3) = ', ltau[j_tau], format='(a, F4.1)') 
      string_show = string(string_ltau+', ', var_txt_show, '=!3', yv_real, string("261B)+'!3', data_unc_show, format=format)
      ; print"
      ; if var eq 3 then begin
      	      ;string_show = string(string_ltau+', ', var_txt_show, '=!3 10^', yv_real, string("261B)+'!3 10^', data_unc_show, format=format)
      ; print"
      ;endif	      
      wset, 0
      xyouts, 0.5, 0.5, string_show, /norm, align=0.5, size=size_font_plot
      wset, 0
      xyouts, 0.5, 0.47, 'chi2 = '+strmid(strcompress(chi2[x,y]), 0,4), /norm, align=0.5, size=size_font_plot
      !p.multi = 0
      if keyword_set(show_fit) then begin
	 wset, 2     
         plot, wl_obs, iris2_inv[x,y,*]*1e6, title='Observed (dashed) & IRIS2 Inverted Representative Profiles (thick)', xtitle ='Wavelength ['+string("305B)+']', ytitle = "Intensity [x 1e6 erg cm^-2 s^-1 sr^-1 Hz^-1]", charsize=size_font_plot	      
	 ; print, "
         oplot, wl_obs, iris_cal[x,y,*]*1e6, line=2
         xyouts, 0.5, 0.85, 'chi2 = '+strmid(strcompress(chi2[x,y]), 0,4), /norm, align=0.5, size=size_font_plot
	 miver, mgII_k_pos_k3, line=1
         miver, mgII_h_pos_h3, line=1
         miver, mgII_uv_triplet_pos23, line=1
	 if keyword_set(show_monochromatic) then miver, wl_obs[j_wl], linestyle=1, thick=7, color=11*16
      endif	 
      ;string_pos = '[X,Y] = ['+strcompress(x)+','+strcompress(y)+']'
      wset, 3 
      ;stop
      wset, 3 & tvscl, eraser
      xyouts, 0.5, 0.25, string_pos, /norm, align=0.5, size=size_font_plot
      wset, 3
   endif

   ; Shows values in window 1 (IRIS2 3D Model Inspector)
   ;!p.multi=[0,1,0,0] 
   ;wset, 3 & plot, [0], [0], pos=[0.,1,0,1],/norm, noclip=0
   ;wset, 3
   if show_aux_ima eq 1 or show_monochromatic eq 1  then begin
           if keyword_set(aux_ima) then begin
	      yv_real = aux_ima[x,y] 
              var_txt_show = 'Value = '
	      if keyword_set(show_chi2) then var_txt_show = 'chi2 = '
              if keyword_set(show_index) then var_txt_show = 'RP_index_db = '
              if keyword_set(show_mu) then var_txt_show = 'mu = ' 
	      string_show = string(var_txt_show, yv_real, format='(a, f0.1)')
	      if keyword_set(show_chi2) then string_show_aux_ima = string(var_txt_show, yv_real, format='(a, f0.1)')
	      if keyword_set(show_index) then string_show_aux_ima = string(var_txt_show, yv_real, format='(a, I8.1)')
	      if keyword_set(show_mu) then  string_show_aux_ima = string(var_txt_show, yv_real, format='(a, F4.2)')
           endif	      
	   if show_monochromatic eq 1 then begin
	      yv_real = data_cal[x,y,j_wl]*1e6 
	      string_show_aux_ima = string('I [R.U.] = ', yv_real, format='(a, f0.2)')
           endif   
	   wset, 3
	   xyouts, 0.5, 0.1, string_show_aux_ima, /norm, align=0.5, charsize=size_font_plot
	   wset, 3
   endif else begin  
	   wset, 3
           xyouts, 0.5, 0.1, string_show, /norm, align=0.5, charsize=size_font_plot, /noclip
	   wset, 3
           ;!p.multi = 0
   endelse 
  
   ;;; Returns the current position if NONE position was stored
   ;;; Returns the stored positions if some were selected by pressing 'S'
   ;;; The returned structure depends on level_info
   if chr eq 'S' or chr eq 'Q' then begin
	  if chr eq 'S' then print, '-> Storing position '+string_pos+' and its corresponding values in the model, uncertainty, and other variables.'     
	  if level_info eq 0  then begin     
             point = {x_display:x, y_display:y, $ 
                   x_in_raster:x mod dim_data[1], y_in_raster:y, $
                   model_xy:reform(model[x, y,*,*]), uncertainty_xy:reform(unc[x, y,*, *]), $
		   ltau:ltau, $
		   chi2_xy:chi2[x,y], $
		   mu_obs:iris_model.mu, $
		   raster_index_xy:selected_rasters[fix(x / dim_data[1])], $ 
		   raster_filename_xy:selected_filenames[(fix(x / dim_data[1]))], $ 
		   all_rasters_filenames:all_rasters_filename, $
	           selected_rasters:selected_rasters}
          endif
	  if level_info eq 1  then begin     
             point = {x_display:x, y_display:y, $ 
                   x_in_raster:x mod dim_data[1], y_in_raster:y, $
                   model_xy:reform(model[x, y,*,*]), uncertainty_xy:reform(unc[x, y,*, *]), $
		   ltau:ltau, $
	           obs_xy:reform(iris_cal[x, y,*]), iris2_inv_xy:reform(iris2_inv[x, y,*]), $
		   wl:wl_obs, $
		   chi2_xy:chi2[x,y], $
		   mu_obs:iris_model.mu, $
		   raster_index_xy:selected_rasters[fix(x / dim_data[1])], $ 
		   raster_filename_xy:selected_filenames[(fix(x / dim_data[1]))], $ 
		   all_rasters_filenames:all_rasters_filename, $
	           selected_rasters:selected_rasters}
          endif
	  if level_info eq 2 then begin     
             point = {x_display:x, y_display:y, $ 
                   x_in_raster:x mod dim_data[1], y_in_raster:y, $
                   model_xy:reform(model[x, y,*,*]), uncertainty_xy:reform(unc[x, y,*, *]), $
		   ltau:ltau, $
	           obs_xy:reform(iris_cal[x, y,*]), iris2_inv_xy:reform(iris2_inv[x, y,*]), $
		   wl:wl_obs, $
		   chi2_xy:chi2[x,y], $
		   index_db_xy:map_pos[x,y], mu_obs:iris_model.mu, mu_db_xy:map_mu[x,y], $
		   raster_index_xy:selected_rasters[fix(x / dim_data[1])], $ 
		   raster_filename_xy:selected_filenames[(fix(x / dim_data[1]))], $ 
		   all_rasters_filenames:all_rasters_filename, $
	           selected_rasters:selected_rasters}
          endif 
          if list_points.Length eq 0 then begin
	          list_points = LIST(point) 
          endif else begin 
	      if chr eq 'S' then list_points.Add,point
          endelse 	      
       endif 
endwhile

while !d.window ne -1 do wdelete, !d.window

;;; Returns the current position if NONE was selected. 
;;; Returns the selected position(s) If some position(s) was(were) selected
;;; by pressing 'S'
return, list_points.toarray()

end

