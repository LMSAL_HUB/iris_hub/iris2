function IRIS_MP_SINECHISQ, x, params

;+
;
; Chi-squared calculator to be used with MPFIT to find the best-fit
; sine wave.
;
;-

amplitude	= params[0]
frequency	= params[1]
phase		= params[2]
offset		= params[3]

yout = offset + amplitude * SIN( (frequency * x) + phase )

; Polynomial underneath the sine wave
if N_ELEMENTS(params) gt 4 then for i = 4, N_ELEMENTS(params)-1 do begin
	yout = yout + params[i] * x^(i-3.)
endfor

RETURN, yout

end