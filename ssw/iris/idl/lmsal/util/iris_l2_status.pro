function iris_l2_status,obsid, timeline=timeline, drms=drms, nrt=nrt
;
;+
;   Name: iris_l2_status
;
;   Purpose: L1->L2-WWW->HCR pipeline status for input OBSID
;
;   Input Paramters:
;      obsid - Level2 path -or- YYYYMMDD_HHMMSS_<obsid>
;
;   Output:
;      returns structure with status flags & info summarizing current pipeline status
;
;   Calling Sequence:
;      IDL> status=iris_l2_status(obsid) ; although "usually" called via iris_l2_time2status(t0,t1...)
;
;   History:
;      10-dec-2013 - S.L.Freeland - crucial step in full automation of IRIS L1->L2->WWW->HCR pipeline
;      11->18 dec-2013 - evolve/feature addtion
;      18-dec-2013 - add .L2_INPROGRESS and this initial doc-header above
;-

if file_exist(obsid) then begin ; fully qualified path supplied 
   obspath=obsid
   obspath=strmid(obspath,0,strlen(obspath)-(str_lastpos(obspath,'/') eq strlen(obspath)-1))
   obs=ssw_strsplit(obspath,'/',/tail) 
endif else begin
   obs=obsid
   nrt=ssw_deltat(file2time(obs),reltime(/now),/days) lt 3 ; match iris_time2files default dT
   parent=concat_dir('$IRIS_DATA','level2') + (['','_nrt'])(keyword_set(nrt))
   obspath=concat_dir(ssw_time2paths(file2time(obs),parent=parent),obs)
endelse

obs=str_replace(obs,'/','')

l2comp_path=str_replace(obspath,'/level2/','/level2_compressed/')

; deterministic error name checks
errlogs=concat_dir(obspath,['iris_l2_'+obs+'.errorlog','iris_l2.errlog','iris_l2.errorlog']) ; historical list
sserr=where(file_exist(errlogs),elcnt)
l2_errlog=last_nelem(errlogs)
if elcnt gt 0 then l2_errlog=errlogs[sserr[0]]

l2log =concat_dir(obspath,'iris_l2_'+obs+'.log')
wwwdir=concat_dir(obspath,'www')

files=findfile(obspath)
sssji=where(strmatch(files,'*_SJI_*',/fold),nsji)
ssrast=where(strmatch(files,'*_raster_*',/fold),nrast)

wwwfiles=findfile(wwwdir)
ssind=where(strmatch(wwwfiles,'*index.html*',/fold),nind)
ssvoe=where(strmatch(wwwfiles,'VOE*.xml',/fold),nvoe)
ssmp4=where(strmatch(wwwfiles,'*.mov*',/fold),nmp4)
voefile=''
if nvoe gt 0 then voefile=concat_dir(wwwdir,wwwfiles[ssvoe[0]])

retval={date_obs:file2time(obs,out='ccsds'),obsid:obs,obspath:obspath,obspath_exist:file_exist(obspath),$
        nrast:nrast,nsji:nsji,first_sji:'',first_rast:'', $
        l2log:l2log,l2log_exist:file_exist(l2log),l2_errlog:l2_errlog,l2_errlog_exist:file_exist(l2_errlog), $
        l1p5_version:0., l1p5_history:'', $
        l2_version:'', l2_runtime:'', l2_inprogress:0, $
        l2_obsstart:'', l2_obsend:'', l2_nexpected:0l, l2_nreceived:0l,l2_error_trace:'', drms_nl1:0l, $
        l12l2_pipeline_version:1.0, l12l2_pipeline_info:'', $
        nwww:n_elements(wwwfiles)*file_exist(wwwdir),nind:nind,voefile:voefile,nvoe:nvoe,voe_runtime:'', nmp4:nmp4,$
        l2comp_path:l2comp_path, l2comp_path_exist:file_exist(l2comp_path), l2comp_ncomp:0}

pipeline_info=concat_dir(retval.obspath,'l12l2_pipeline.info')
if file_exist(pipeline_info) then begin 
   pipeinfo=rd_tfile(pipeline_info)
   retval.l12l2_pipeline_version=str2number(ssw_strsplit(pipeinfo[0],'version:',/tail))
   retval.l1p5_version=str2number(pipeinfo[1])
   retval.l12l2_pipeline_info=arr2str(pipeinfo[2:*])
endif
   
if nsji gt 0 then begin
   retval.first_sji=files[sssji[0]]   ; maybe future data from this
   if retval.l1p5_version eq 0. then begin
      mreadfits,concat_dir(retval.obspath,retval.first_sji),l1p5
      retval.l1p5_version=get_history(l1p5[0],caller='iris_prep',/version)
   endif
endif
if nrast gt 0 then begin
   retval.first_rast=files[ssrast[0]] ; ditto
   if retval.l1p5_version eq 0. then begin
      mreadfits,concat_dir(retval.obspath,retval.first_rast),l1p5
      retval.l1p5_version=get_history(l1p5[0],caller='iris_prep',/version)
   endif
endif

if retval.nvoe eq 1 then begin 
   xmldat=rd_Tfile(retval.voefile)
   ssd=(where(strmatch(xmldat,'*<Date>*</Date>*',/fold),dcnt))(0)
   if dcnt gt 0 then retval.voe_runtime=strextract(strupcase(xmldat[ssd]),'<DATE>','</DATE>') else $
      box_message,'Problem deriving VOE runtime??'
endif

if retval.l2_errlog_exist then begin ; add error trace (comma delimited list)
   box_message,'l2 error log'
   retval.l2_error_trace=arr2str(rd_tfile(retval.l2_errlog))
endif

if retval.l2log_exist then begin ; add some L2 log info
   spawn,['tail','-500',retval.l2log],l2dat,/noshell
   ssver=where(strpos(l2dat,'Version:') ne -1,vcnt)
   if vcnt eq 0 then begin 
      l2dat=rd_tfile(retval.l2log)
      ssver=where(strpos(l2dat,'Version:') ne -1,vcnt)
   endif
   if vcnt eq 0 then begin 
      box_message,'No L2 Version# , so bailing on L2 Log info
   endif else begin
      retval.l2_version=strcompress(ssw_strsplit(l2dat[ssver],'Version:',/tail),/remove)
      pats='*'+str2arr('Date*:,rec*files,exp*files,obs*start,obs*end')+'*'
      tags=str2arr('l2_runtime,l2_nreceived,l2_nexpected,l2_obsstart,l2_obsend')
      for p=0,n_elements(pats)-1 do begin 
        ssp=where(strmatch(l2dat,pats[p],/fold),pcnt)
        if pcnt eq 1 then retval.(tag_index(retval,tags[p]))=ssw_strsplit(l2dat[ssp],': ',/tail)


      endfor
   endelse
endif

retval.l2_inprogress=(retval.l12l2_pipeline_version gt 1.0) and $
                     total([retval.l2log_exist,retval.l2_errlog_exist]) eq 0  

if retval.l2comp_path_exist then begin 
   gz=file_search(retval.l2comp_path,'*.gz')
   ss=where(strpos(gz,'.gz') ne -1,ncomp)
   retval.l2comp_ncomp=ncomp
endif


if retval.l2_obsstart eq '' and obs ne '' then begin 
   retval.l2_obsstart=file2time(obs)
   if data_chk(timeline,/struct) then tl=timeline[0] else $
      tl=iris_time2timeline(file2time(obs),file2time(obs)) 
   if data_chk(tl,/struct) then begin 
      retval.l2_obsstart=tl.date_obs
      retval.l2_obsend=tl.date_end
   end else  box_message,'Cannot derive obs start/stop
endif
if keyword_set(drms) and retval.l2_obsstart ne '' then begin ; get current L1 file count for this obs
   obs=strmid(retval.obsid,16,100) 
   l1=iris_time2files(retval.l2_obsstart,retval.l2_obsend,obsid=obs,level=1,/jsoc,drms)
   retval.drms_nl1=n_elements(l1) * (l1[0] ne '')
endif



return,retval
end

