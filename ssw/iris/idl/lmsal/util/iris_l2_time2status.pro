function iris_l2_time2status,t0,t1, timeline=timeline, drms=drms, nrt=nrt
;
;+
;   Name: iris_l2_time2status
;
;   Purpose: return info about IRIS L1-L2->WWW->HCR pipeline status for user (app) time range
;
;   Input Parameters:
;      t0,t1 - canonical time range
;
;   Keyword Parameters:
;      timeline (switch) - if set, drive via timeline (default is existing L2 diretories)
;      drms (switch) - passed->iris_l2_status - if set, update the .DRMS_NL1 with current JSOC access
;      nrt (switch) - if set, apply to /level2_nrt/ tree (default=/level2/
;
;   Calling Seqeunce:
;      l2status=iris_l2_time2status(t0,t1 [,/timeline] [,/drms] [,/nrt] )
;
;   Calling Context:
;      l2status=iris_l2_time2status(t0,t1 , /timeline,/drms)
;      l12l2=iris_status2need(l2status,/l12l2) ; obs which are "ready" for L1->L2 
;      cover=iris_status2need(l2stats,/coverage) ; "ready" and lacking www/coverage
;
;   Method:
;      setup calls/loop over time range -> iris_l2_status(obs[i]) ; that function does details
;
;   History:
;      10-dec-2013 - S.L.Freeland - major step in full automation of IRIS pipelines
;      18-dec-2013 - S.L.Freeland - fixed /TIMELINE stuff (probably main usage in auto/cron)
;       9-jan-2014 - S.L.Freeland - required explicit /NRT (removed somewhat arbitry default for "recent" data
;-

retval=-1

nrt=keyword_set(nrt)
if keyword_set(timeline) then begin 
   tl=iris_time2timeline(t0,t1)
   obs=time2file(tl.date_obs,/sec) + '_' + string(tl.obsid,format='(I10.10)')
   ; nrt=nrt or (ssw_deltat(t0,reltime(/now)) lt 4) ; slf, 9-jan-2014 - removed time based default - use /NRT if that's what is desired
   l2top=concat_dir(get_logenv('IRIS_DATA'),'level2' + (['','_nrt'])(nrt) )
   ;obs=concat_dir(ssw_time2paths(tl.date_obs,parent=l2top),obs)
endif else begin 
  obs=iris_time2files(t0,t1,level=2,/obsdir, nrt=nrt)
endelse

if obs[0] ne '' then begin 
   retval=iris_l2_status(obs[0],nrt=nrt, drms=drms)
   for o=1,n_elements(obs)-1 do retval=[temporary(retval),iris_l2_status(obs[o],nrt=nrt,drms=drms)]
endif else box_message,'No L2 for this time range

return,retval
end

