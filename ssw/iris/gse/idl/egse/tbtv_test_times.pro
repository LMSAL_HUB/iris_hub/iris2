;
;
;
sttim = '15-Oct 6:00'
trans = [  8,   8,   8,   8,   8,   8,   8,   8]
dwell = [  4,  12,  12,   4,   4,   4,   4,   4]
extra = [  0,   0,   0,   0,   0,   0,   0,   0]
label = ['c5','h5','c6','h6','c7','h7','c8','h8', 'Dn']
;
v1 = trans+dwell+extra
v2 = v1
for i=1,n_elements(v2)-1 do v2(i) = v2(i) + v2(i-1)
v2 = [0,v2]
;
dattim = anytim2ints(sttim, off=v2*60.*60)
prstr, label + '   ' + fmt_tim(dattim)
;
end
