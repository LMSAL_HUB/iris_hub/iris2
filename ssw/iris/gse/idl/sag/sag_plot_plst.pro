pro sag_plot_plst, plst_fil, sttim, entim
;
;
;sag_plot_plst, '/sxi01/dbase/tb_tv/plot_tb_set1.plst', '25-apr 3:00','25-apr 5:00
;
;
if (not file_exist(plst_fil)) then return
;
mat = rd_ulin_col(plst_fil, /nohead, nocomment='#')
mnem = reform(mat(0,*))
panel = reform(mat(1,*))
tits = reform(mat(2,*))
;
info = sag_get_mnem(sttim, entim, mnem, /nostring)
if (data_type(info) ne 8) then return
all_mnem = info.(0).mnem
all_code = info.(0).code
for i=1,n_elements(tag_names(info))-1 do begin
    all_mnem = [all_mnem, info.(i).mnem]
    all_code = [all_code, info.(i).code]
end
;
upanel = panel(uniq(panel, sort(panel)))
np = n_elements(upanel)
!p.multi = [0,1,np]
timerange = [anytim2ints(sttim), anytim2ints(entim)]
print,fmt_tim(timerange)
;
savep = !p
!p.charsize = 2
for i=0,np-1 do begin
    ss1 = where(panel eq upanel(i), nss1)
    ss2 = where_arr(strlowcase(all_mnem), strlowcase(mnem(ss1)))
    codes = all_code(ss2)
    sag_hkplot, info, code=codes, nover=(i ne np-1), timerange=timerange, $
		/label, /color
end
!p=savep
;
end