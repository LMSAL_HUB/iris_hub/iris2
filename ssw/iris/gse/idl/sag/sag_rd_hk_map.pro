pro sag_rd_hk_map, hkmap2, hkmapfil=hkmapfil
;
;
;
common sag_rd_hk_map_blk1, last_hkmapfil, hkmap
;
if (n_elements(last_hkmapfil) eq 0) then last_hkmapfil = ''
if (n_elements(hkmapfil) eq 0) then hkmapfil = getenv('SAG_HK_MAP_FILE')
if (keyword_set(rdmapfil)) then delvarx, hkmap
if (last_hkmapfil ne hkmapfil) or (n_elements(hkmap) eq 0) then begin
    if (not file_exist(hkmapfil)) then  begin 
       hkmapfil=str_replace(hkmapfil,'$SAG_TELEM_INFO',get_logenv('$SAG_TELEM_INFO'))
       if not file_exist(hkmapfil) then stop, 'SAG_RD_HK: No $SAG_HK_MAP_FILE: ' + hkmapfil
    endif
    print, 'Reading: ' + hkmapfil
    hkmap0 = rd_ulin_col(hkmapfil, /nohead, nocomment='#')
    ;
    n = n_elements(hkmap0(0,*))
    out0 = {apid: 0L, prefix: '', filext: '', dirs: '', size: 0L, def_cad:0.0, iqual:0L, index_cmd: ''}
    hkmap = replicate(out0, n)
    hkmap.apid		= long(reform(hkmap0(0,*)))
    hkmap.prefix	= reform(hkmap0(1,*))
    hkmap.filext	= reform(hkmap0(2,*))
    hkmap.dirs		= reform(hkmap0(3,*))
    hkmap.size		= long(reform(hkmap0(4,*)))
    hkmap.def_cad	= float(reform(hkmap0(5,*)))
    hkmap.iqual		= long(reform(hkmap0(6,*)))
    hkmap.index_cmd	= reform(hkmap0(7,*))
end
last_hkmapfil = hkmapfil
;
if (n_params(0) ge 1) then hkmap2 = hkmap	;pass out as parameter
end
