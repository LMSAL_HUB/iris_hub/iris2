
pro ssw_stereo_fov, file0=file0, file1=file1, index0=index0, data0=data0, index1=index1, $
  data1=data1, map0=map0, map1=map1, smap0=smap0, smap1=smap1, $
  read_out=read_out, carr_out=carr_out, use_curs=use_curs, use_box=use_box, angle=angle, $
  grid=grid, no_grid=no_grid, init=init, fixed_size=fixed_size, wnum_arr=wnum_arr, $
  xsiz_win=xsiz_win, no_plot=no_plot, help=help

;+
; NAME:
;	SSW_STEREO_FOV
;
; PURPOSE:
;	Extract and align corresponding FOVs from a matched pair of STEREO/SECCHI images
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;	IDL> ssw_stereo_fov, index0=index_a, data0=data_a, index1=index_b, data1=data_b, smap0=smap_a, smap1=smap_b
;
; INPUTS:
;	No required input parameters.
;
; OPTIONAL INPUT PARAMETERS:
;	x0, y0, nx, and ny give the initial location (x0, y0) and
;	size (nx, ny) of the box if the keyword INIT is set.  Otherwise, the
;	box is initially drawn in the center of the screen.
;
; KEYWORD PARAMETERS:
;	INIT:  If this keyword is set, x0, y0, nx, and ny contain the initial
;	parameters for the box.
;
;	FIXED_SIZE:  If this keyword is set, nx and ny contain the initial
;	size of the box.  This size may not be changed by the user.
;
;	HELP:  If this keyword is set, print a short message describing
;	operation of the cursor.
;
; OUTPUTS:
;	x0:  X value of lower left corner of box.
;	y0:  Y value of lower left corner of box.
;	nx:  width of box in pixels.
;	ny:  height of box in pixels.
;
;	The box is also constrained to lie entirely within the window.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	A box is drawn in the currently active window.  It is erased
;	on exit.
;
; RESTRICTIONS:
;	Works only with window system drivers.
;
; PROCEDURE:
;	The graphics function is set to 6 for eXclusive OR.  This
;	allows the box to be drawn and erased without disturbing the
;	contents of the window.
;
;	Operation is as follows:
;	Left mouse button:   Move the box by dragging.
;	Middle mouse button: Resize the box by dragging.  The corner
;		nearest the initial mouse position is moved.
;	Right mouse button:  Exit this procedure, returning the
;			     current box parameters.
;
; MODIFICATION HISTORY:
;	2008-04-16 (GLS) - Starting from the current IDL library version of BOX_CURSOR.PRO
;
; TODO:
;	- Possibly need to have two sun_data, r0_sol_radii (and other) variables, for map0 and map1
;-

if not exist(map0) then begin
  if not exist(index0) then begin
    if not exist(file0) then begin
      print,'No file0 passed.  Returning.'
      return
    endif else begin
      mreadfits, file0, index0, data0
      map0 = mk_secchi_map(index0, data0)
    endelse 
  endif else begin
    if not exist(data0) then begin
      print,'No data0 passed.  Returning.'
      return
    endif else begin
      map0 = mk_secchi_map(index0, data0)    
    endelse
  endelse
endif

if not exist(map1) then begin
  if not exist(index1) then begin
    if not exist(file1) then begin
      print,'No file1 passed.  Returning.'
      return
    endif else begin
      mreadfits, file1, index1, data1
      map1 = mk_secchi_map(index1, data1)
    endelse 
  endif else begin
    if not exist(data1) then begin
      print,'No data1 passed.  Returning.'
      return
    endif else begin
      map1 = mk_secchi_map(index1, data1)    
    endelse
  endelse
endif

if not exist(xsiz_win) then xsiz_win = 768
if ( (not exist(grid)) and (not keyword_set(no_grid)) ) then grid=15
rsun_au = (1.38d6/2.0) / 149.60d6

if exist(wnum_arr) then begin
  wnum_map0 = wnum_arr[0]
  wnum_map1 = wnum_arr[1]
  wnum_map0_read_out = wnum_arr[2]
  wnum_map1_read_out = wnum_arr[3]
endif

wdef, wnum_map0, xsiz_win, /ur
plot_map, map0, grid=grid
sys0 = get_sys()
device, get_window=win_coords
wdef, wnum_map1, xsiz_win, xpos=win_coords(0)-xsiz_win, ypos=win_coords(1)
plot_map, map1, grid=grid
sys1 = get_sys()

if keyword_set(read_out) ne 0 then begin
  wset, wnum_map0
  device, get_window=win_coords
  wdef, wnum_map0_read_out, xpos=win_coords(0), ypos=win_coords(1)-20, 256, 32
  wset, wnum_map1
  device, get_window=win_coords
  wdef, wnum_map1_read_out, xpos=win_coords(0), ypos=win_coords(1)-20, 256, 32
endif

; Switch back to map0 window
wset, wnum_map0
put_sys, sys0

sun_data = get_sun(map0.time,dist=dist,sd=sd)
r0_sol_radii = ((sd/map0.rsun)*dist) / rsun_au
clon_earth_view = tim2carr(map0.time)





ON_ERROR, 2
DEVICE, GET_GRAPHICS = old, SET_GRAPHICS = 6  ;Set xor
col = !D.N_COLORS -1
corner = 0

if KEYWORD_SET(help) then begin
  print, "Drag Left button to move box."
  print, "Drag Middle button near a corner to resize box."
  print, "Right button when done."
endif

if KEYWORD_SET(init) eq 0 then begin	; Supply default values for box:
  if KEYWORD_SET(fixed_size) eq 0 then begin
    nx = (!x.crange[1]-!x.crange[0])/8	; No fixed size.
    ny = (!y.crange[1]-!y.crange[0])/8
  endif else begin
    if (N_PARAMS() LT 4) THEN MESSAGE,'Incorrect number of arguments.'
  endelse
  x0 = !x.crange[0] + (!x.crange[1]-!x.crange[0])/2 - nx/2
  y0 = !y.crange[0] + (!y.crange[1]-!y.crange[0])/2 - ny/2
endif else begin
  if (N_PARAMS() LT 4) THEN MESSAGE,'Incorrect number of arguments.'
endelse

button = 0
middleButton = 0
old_button = 0
oldMiddleButton = 0

    while(1) do begin
        if (nx lt 0) then begin
            x0 = x0 + nx
            nx = -nx
	    case corner of
		0: corner = 1
		1: corner = 0
		2: corner = 3
		3: corner = 2
	    endcase
; reset the starting drag point...
            mx0 = x & my0 = y
            x00 = x0 & y00 = y0
            nx0 = nx & ny0 = ny
        endif
        if (ny lt 0) then begin
            y0 = y0 + ny
            ny = -ny
	    case corner of
		0: corner = 3
		3: corner = 0
		1: corner = 2
		2: corner = 1
	    endcase
; reset the starting drag point...
            mx0 = x & my0 = y
            x00 = x0 & y00 = y0
            nx0 = nx & ny0 = ny
        endif

        x0 = x0 > !x.crange[0]
        y0 = y0 > !y.crange[0]
        x0 = x0 < (!x.crange[1]-1 - nx) ;Never outside window
        y0 = y0 < (!y.crange[1]-1 - ny)

; Calculate coordinates of box vertices for drawing:

;        x0y0d = convert_coord(x0, y0, /device, /to_data)
;        x0d = x0y0d[0]
;        y0d = x0y0d[1]
        px = [x0-nx/2, x0+nx/2, x0+nx/2, x0-nx/2, x0-nx/2]
        py = [y0-ny/2, y0-ny/2, y0+ny/2, y0+ny/2, y0-ny/2]
;       pxyd = convert_coord(px, py, /device, /to_data)
;        pxd = reform(pxyd[0,*])
;        pyd = reform(pxyd[1,*])

        plots, px, py, col=col, /data, thick=1, lines=0		; Draw the box
;        plots, pxd, pyd, col=col, /data, thick=1, lines=0	; Draw the box
        empty	; Decwindow bug





; Calculate all coordinates:

; map0 view heliographic:
  xcen_map0 = x0
  ycen_map0 = y0
;  xcen_map0 = x0d
;  ycen_map0 = y0d
; map0 Cartesian coordinates:
  yc_map0 = xcen_map0
  zc_map0 = ycen_map0
; map0 normalized (rsun=1) Cartesian corrdinates:
  ycn_map0 = yc_map0/map0.rsun
  zcn_map0 = zc_map0/map0.rsun
  temp_map0 = ycn_map0*ycn_map0 + zcn_map0*zcn_map0
  xcn_map0 = sqrt(1 - temp_map0)
; map0 view spherical coordinates using local b0, p angles:
  rtp_map0 = c2s([xcn_map0,ycn_map0,zcn_map0], b0=-map0.b0, roll=-map0.roll_angle)
; map0 view lat and lon, and clon:
  lat_map0 = rtp_map0[1]
  lon_map0 = rtp_map0[2]
  clon_map0 = (clon_earth_view[0] + map0.l0 + lon_map0) mod 360

; map1 view lat and lon, and clon:
  lat_map1 = lat_map0
  lon_map1 = lon_map0 + map0.l0 - map1.l0
  clon_map1 = (clon_earth_view[0] + map1.l0 + lon_map1) mod 360
; map1 view heliographic:
;  xy_map1 = (hel2arcmin(lat_map1, lon_map1, date=map1.time))*60d0
;  xy_map1 = (hel2arcmin(lat_map1, lon_map1, rsun=map1.rsun, b0=-map1.b0, roll=-map1.roll_angle))*60d0
  xyz_map1 = s2c([map1.rsun,lat_map1,lon_map1], b0=-map1.b0, roll=-map1.roll_angle)
  xcen_map1 = xyz_map1[1]
  ycen_map1 = xyz_map1[2]
;  xcen_map1 = xy_map1[0]
;  ycen_map1 = xy_map1[1]
; map1 Cartesian coordinates:
  yc_map1 = xcen_map1
  zc_map1 = ycen_map1
; map1 normalized (rsun=1) Cartesian corrdinates:
  ycn_map1 = yc_map1/map1.rsun
  zcn_map1 = zc_map1/map1.rsun
  temp_map1 = ycn_map1*ycn_map1 + zcn_map1*zcn_map1
; map1 coordinates of box vertices for drawing:
  nx_map1 = nx*(map1.rsun/map0.rsun)
  ny_map1 = ny*(map1.rsun/map0.rsun)
;  nx_map1 = (pxd[1]-pxd[0])*(map1.rsun/map0.rsun)
;  ny_map1 = (pyd[2]-pyd[1])*(map1.rsun/map0.rsun)
  px_map1 = [xcen_map1-nx_map1/2, xcen_map1+nx_map1/2, xcen_map1+nx_map1/2, xcen_map1-nx_map1/2, xcen_map1-nx_map1/2]
  py_map1 = [ycen_map1-ny_map1/2, ycen_map1-ny_map1/2, ycen_map1+ny_map1/2, ycen_map1+ny_map1/2, ycen_map1-ny_map1/2]

; Earth view lat and lon:
  lat_cen_earth = lat_map0
  lon_cen_earth = lon_map0 + map0.l0
; Earth view heliographic:
  xy_earth = (hel2arcmin(lat_cen_earth, lon_cen_earth, date=map0.time))*60d0
  xcen_earth = xy_earth[0]
  ycen_earth = xy_earth[1]

; Generate readout text, if requested:
  if keyword_set(read_out) ne 0 then begin
    if (keyword_set(angle)) then begin
      lat_text_map1 = 'N/S: ' + string(xcen_map1, format='(f6.0)')
      lon_text_map1 = 'E/W: ' + string(ycen_map1, format='(f6.0)')
    endif else begin
      if temp_map1 gt 1 then begin
        lat_text_map1 = "You're off the Sun.                   "
        lon_text_map1 = ""
      endif else begin
         if (keyword_set(carr_out)) then begin
          lat_text_map1 = ' LAT: ' + string(lat_map1, format='$(f5.1)')
          lon_text_map1 = 'CLON: ' + string(clon_map1, format='$(f6.1)')
        endif else begin
          lat_text_map1 = 'LAT: ' + string(lat_map1, format='$(f5.1)')
          lon_text_map1 = 'LON: ' + string(lon_map1, format='$(f5.1)')
        endelse
      end
    endelse
  endif

; Switch to map1 window and draw map1 box:
  wset, wnum_map1
  put_sys, sys1
  plots, px_map1, py_map1, col=col, /data, thick=1, lines=0
  empty	; Decwindow bug
; Switch to map1 readout window and write text, if requested:
  if keyword_set(read_out) ne 0 then begin
    wset, wnum_map1_read_out
    xyouts,  10, 10, lat_text_map1, /dev, charsize=1.5, charthick=1.5
    xyouts, 138, 10, lon_text_map1, /dev, charsize=1.5, charthick=1.5
  endif
; Switch back to map0 window
  wset, wnum_map0
  put_sys, sys0





        old_button = button
		oldMiddleButton = middleButton
        cursor, x, y, 2, /data	; Wait for a button
;        cursor, x, y, 2, /device	; Wait for a button
        button = !MOUSE.BUTTON
		middleButton = (button eq 2) or (button eq 3) or (button eq 5)

        if ((button ne 0) and (old_button eq 0)) then begin
            mx0 = x		;For dragging, mouse locn...
            my0 = y
            x00 = x0            ;Orig start of ll corner
            y00 = y0
        endif

        if (button eq 1) then begin ;Drag entire box?
            x0 = x00 + x - mx0
            y0 = y00 + y - my0
        endif

        ;;New size?
        if (middleButton and (NOT KEYWORD_SET(fixed_size))) then begin
            if (NOT oldMiddleButton) then begin ;Find closest corner
                mind = 1e6
                for i=0,3 do begin
                    d = float(px[i]-x)^2 + float(py[i]-y)^2
                    if (d lt mind) then begin
                        mind = d
                        corner = i
                    endif
                endfor
                nx0 = nx	;Save sizes.
                ny0 = ny
            endif
            dx = x - mx0 & dy = y - my0	;Distance dragged...
            case corner of
                0: begin
                    x0 = x00 + dx & y0 = y00 + dy
                    nx = nx0 -dx & ny = ny0 - dy
                endcase
                1: begin
                    y0 = y00 + dy
                    nx = nx0 + dx & ny = ny0 - dy
                endcase
                2: begin
                    nx = nx0 + dx & ny = ny0 + dy
                endcase
                3: begin
                    x0 = x00 + dx
                    nx = nx0 -  dx & ny = ny0 + dy
                endcase
            endcase
        endif

	x0y0d = convert_coord(x0, y0, /device, /to_data)
	x0d = x0y0d[0]
	y0d = x0y0d[1]

        plots, px, py, col=col, /data, thick=1, lines=0		; Erase previous box
;        plots, pxd, pyd, col=col, /data, thick=1, lines=0	; Erase previous box
        empty	; Decwindow bug





; Calculate all coordinates:

; map0 view heliographic:
  xcen_map0 = x0d
  ycen_map0 = y0d
; map0 Cartesian coordinates:
  yc_map0 = xcen_map0
  zc_map0 = ycen_map0
; map0 normalized (rsun=1) Cartesian corrdinates:
  ycn_map0 = yc_map0/map0.rsun
  zcn_map0 = zc_map0/map0.rsun
  temp_map0 = ycn_map0*ycn_map0 + zcn_map0*zcn_map0
  xcn_map0 = sqrt(1 - temp_map0)
; map0 view spherical coordinates using local b0, p angles:
  rtp0_map0 = c2s([xcn_map0,ycn_map0,zcn_map0], b0=-map0.b0, roll=-map0.roll_angle)
; map0 view lat and lon, and clon:
  lat_map0 = rtp0_map0[1]
  lon_map0 = rtp0_map0[2]
  clon_map0 = (clon_earth_view[0] + map0.l0 + lon_map0) mod 360

; map1 view lat and lon, and clon:
  lat_map1 = lat_map0
  lon_map1 = lon_map0 + map0.l0 - map1.l0
  clon_map1 = (clon_earth_view[0] + map1.l0 + lon_map1) mod 360
; map1 view heliographic:
;  xy_map1 = (hel2arcmin(lat_map1, lon_map1, date=map1.time))*60d0
;  xy_map1 = (hel2arcmin(lat_map1, lon_map1, rsun=map1.rsun, b0=-map1.b0, roll=-map1.roll_angle))*60d0
  xyz_map1 = s2c([map1.rsun,lat_map1,lon_map1], b0=-map1.b0, roll=-map1.roll_angle)
  xcen_map1 = xyz_map1[1]
  ycen_map1 = xyz_map1[2]
;  xcen_map1 = xy_map1[0]
;  ycen_map1 = xy_map1[1]

; Earth view lat and lon:
  lat_cen_earth = lat_map0
  lon_cen_earth = lon_map0 + map0.l0
; Earth view heliographic:
  xy_earth = (hel2arcmin(lat_cen_earth, lon_cen_earth, date=map0.time))*60d0
  xcen_earth = xy_earth[0]
  ycen_earth = xy_earth[1]

; Switch to map1 window and erase last map1 box:
  wset, wnum_map1
  put_sys, sys1
  plots, px_map1, py_map1, col=col, /data, thick=1, lines=0
  empty	; Decwindow bug
; Switch to map1 readout window and erase last map1 text:
  if keyword_set(read_out) ne 0 then begin
    wset, wnum_map1_read_out
    xyouts,  10, 10, lat_text_map1, /dev, color=0, charsize=1.5, charthick=1.5
    xyouts, 138, 10, lon_text_map1, /dev, color=0, charsize=1.5, charthick=1.5
  endif
; Switch back to map0 window
  wset, wnum_map0
  put_sys, sys0





        if (button eq 4 AND (NOT oldMiddleButton)) then begin ;Quitting?
            DEVICE, SET_GRAPHICS=old





  sub_map, map0, smap0, xrange=[px[0],px[1]], yrange=[py[1],py[2]]
  sub_map, map1, smap1, xrange=[px_map1[0],px_map1[1]], yrange=[py_map1[1],py_map1[2]]

  if not keyword_set(no_plot) then begin
    wset, wnum_map0 & plot_map, smap0
    wset, wnum_map1 & plot_map, smap1
  endif

  smap0 = add_tag(smap0, map0.data[*,0], 'naxis1')
  smap0 = add_tag(smap0, map0.data[0,*], 'naxis2')
  smap0 = add_tag(smap0, smap0.naxis1, 'xfov')
  smap0 = add_tag(smap0, smap0.naxis2, 'yfov')
  smap0 = add_tag(smap0, 0, 'crval1')
  smap0 = add_tag(smap0, 0, 'crval2')
  smap0 = add_tag(smap0, xcen_earth, 'xcen_earth')
  smap0 = add_tag(smap0, ycen_earth, 'ycen_earth')
  smap0 = add_tag(smap0, index0.obsrvtry, 'obsrvtry')
  smap0 = add_tag(smap0, index0.obsrvtry, 'telescop')
  smap0 = add_tag(smap0, index0.instrume, 'instrume')
  smap0 = add_tag(smap0, index0.wavelnth, 'wavelnth')

  smap1 = add_tag(smap1, map1.data[*,0], 'naxis1')
  smap1 = add_tag(smap1, map1.data[0,*], 'naxis2')
  smap1 = add_tag(smap1, smap1.naxis1, 'xfov')
  smap1 = add_tag(smap1, smap1.naxis2, 'yfov')
  smap1 = add_tag(smap1, 0, 'crval1')
  smap1 = add_tag(smap1, 0, 'crval2')
  smap1 = add_tag(smap1, xcen_earth, 'xcen_earth')
  smap1 = add_tag(smap1, ycen_earth, 'ycen_earth')
  smap1 = add_tag(smap1, index1.obsrvtry, 'obsrvtry')
  smap1 = add_tag(smap1, index1.obsrvtry, 'telescop')
  smap1 = add_tag(smap1, index1.instrume, 'instrume')
  smap1 = add_tag(smap1, index1.wavelnth, 'wavelnth')





            return
        endif

    endwhile





end
