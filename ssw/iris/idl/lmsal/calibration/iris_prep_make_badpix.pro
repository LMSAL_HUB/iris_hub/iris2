function IRIS_PREP_MAKE_BADPIX, flat_index, ttag, $
	thresh = thresh, write = write, $
	fix = fix, spec = spec, path = path, static = static

;+
;
; Read in the flatfield index and make a bad pixel mask for each entry.
; This is mostly used for SJIs; there it marks bad pixels whenever the flatfield
; drops below a certain threshold. There are also a few hard-coded bad pixels
; for the FUV.
;
; KEYWORDS:
;	thresh	-	the threshold value in the flatfield; defaults to 0.4
;	/write	-	if set, then a genx file of bad pixel masks is written out
;	/static	-	if set, then the "static" bad pixel map is updated (i.e.
;				the img_path dependent list is copied to the new fields 
;				in the flat_index)
;
;-

; Set up some defaults
if not KEYWORD_SET(thresh) then thresh = 0.5
sswdata = CONCAT_DIR('$SSW_IRIS','data')
if N_ELEMENTS(ttag) eq 0 then ttag = TIME2FILE(/sec, RELTIME(/now))

if N_TAGS(flat_index) eq 0 then begin
	; Read the latest flat_index file
	flatinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*flat.genx'))
	flatind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(flatinds), 0, 15)))
	latest = WHERE(flatind_tai eq MAX(flatind_tai))
	RESTGEN, file = flatinds[latest], flat_index
endif

; Load the old bad pixel map for reference
if KEYWORD_SET(static) then begin
	sswdata = CONCAT_DIR('$SSW_IRIS','data')
        ;now this needs to read geny also.  
	badinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*badpix.geny'))
	badind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(badinds), 0, 15)))
	latest = WHERE(badind_tai eq MAX(badind_tai))
	;RESTGEN, file = badinds[latest], str = badpix_str
        ; RPT I don't understand why 1-element STR array vs str but oh well...
        filenamearr = badinds[latest]
        filename = filenamearr(0)
        RESTGENX, result, file=filename
        badpix_str = result
endif

; Loop through the index fields and make a structure tag for each
nflat = N_ELEMENTS(flat_index)
for i = 0, nflat - 1 do begin
	imgpath = STRMID(flat_index[i].img_path, 0, 3)
	case imgpath of
		'SJI'	:	begin
			if KEYWORD_SET(static) then begin
				pathind = (WHERE(flat_index.img_path eq flat_index[i].img_path))[0]
				badpix = badpix_str.(pathind)
			endif else begin
				MREADFITS, CONCAT_DIR(sswdata, flat_index[i].filename), hdr, dat
				badpix = LONG64(WHERE(dat lt thresh))
				if KEYWORD_SET(fix) then case 1 of
					flat_index[i].img_path eq 'SJI_2796' : badpix = IRIS_PREP_FIX_BADPIX_2796(badpix)
					flat_index[i].img_path eq 'SJI_2832' : badpix = IRIS_PREP_FIX_BADPIX_2796(badpix)
					flat_index[i].img_path eq 'SJI_1330' : badpix = IRIS_PREP_FIX_BADPIX_1330(badpix)				
				endcase
			endelse
		end
		'FUV'	:	begin
			badpix = [3125941ll, 2699108ll, 2678390ll]
		end
		else	:	begin
			badpix = [0ll]
		end
	endcase
	; Make a new structure tag for this recnum
	tagname = 'F' + STRTRIM(flat_index[i].recnum, 2)
	if i eq 0 then begin
		result = CREATE_STRUCT(tagname, badpix)
	endif else begin
		result = CREATE_STRUCT(result, tagname, badpix)
	endelse
endfor

if KEYWORD_SET(write) then begin
	if N_ELEMENTS(path) eq 0 then path = CONCAT_DIR('$SSW_IRIS','data')
	fname = CONCAT_DIR(path, ttag + '_badpix.geny')
	;SAVEGENX, file = fname, str = result
        SAVEGENX, result, file=fname
endif

RETURN, result

end
