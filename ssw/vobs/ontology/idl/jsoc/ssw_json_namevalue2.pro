function ssw_json_namevalue, jsonin
;
;+
;   Kludge for "special case" (kewords:name:value)
;   I'll make this more graceful later...
;
json=strcompress(arr2str(jsonin),/remove)

nss=stregex(json,'name:|name :')
vss=stregex(json,'values:|values :')
bjson=byte(json)
while (nss gt 0 and vss gt 0) do begin 
   stx=string(bjson(nss:vss+6))
   stxc=strcompress(stx,/remove)
   new=strextract(stxc,':"','",')+':'
   new=strpad(new,strlen(stx))
   bjson(nss)=byte(new)
   json=string(bjson)
nss=stregex(json,'name:|name :')
vss=stregex(json,'values:|values :')

endwhile

; Following "patches" need some cleanup but I'm moving to data request tests..- 
json=ssw_patt_replace(json,'"keywords":[{','{')
json=ssw_patt_replace(json,'},{',',')
json=ssw_patt_replace(json,']}]',']')
json=ssw_patt_replace(json,'[]','[""]')
json=ssw_patt_replace(json,'":',':')
json=ssw_patt_replace(json,'{{','{')
json=ssw_patt_replace(json,',,',',')
json=ssw_patt_replace(json,'],},{',']')
json=ssw_patt_replace(json,'],}]',']')
json=ssw_patt_replace(json,'{keywords:[{','{')

return,json
end
