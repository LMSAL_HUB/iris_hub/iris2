;+
; Project     : VSO
;
; Name        : SOCK_SEARCH
;
; Purpose     : Search for files or directories at a URL 
;
; Category    : utility system sockets
;
; Syntax      : IDL> sock_search,url,results
;
; Inputs      : URL = remote URL directory to search
;
; Outputs     : RESULTS = remote file or directory names
;
; Keywords    : ERR = error string
;               COUNT = # of results
;               DIRECTORY = search for directories
;               DATES = file/directory timestamps
;
; History     : 20-Dec-2018, Zarro (ADNET)
;               21-Mar-2019, Zarro (ADNET)
;               - added DATES
;-

pro sock_search,url,results,_ref_extra=extra,err=err,count=count,directory=directory,dates=dates

results='' & count=0 & dates=''
err=''

if ~is_url(url,/scheme) then begin
 err='Missing or invalid input URL.'
 mprint,err
 return
endif

if is_ftp(url) then begin
 sock_dir_ftp,url,results,_extra=extra,err=err,count=count
 if (n_params() eq 1) then print,results
 return
endif

sock_list,url,dirs,_extra=extra,err=err

;-- parse out file or directory names

directory=keyword_set(directory)
if is_blank(err) then begin
 spec='[^\/\?\:\=\;\,\\]+'
 delim=''
 if directory then delim='\/'
 sdate='> *([0-9]{4}-[0-9]{2}-[0-9]{2} *[0-9]{2}:[0-9]{2}) *<'

 chk1=stregex(dirs,'<a href="('+spec+')'+delim+'">'+spec+delim+'</a>.+'+sdate+'.+',/ext,/sub)
 chk1=strtrim(chk1,2)
 names=comdim2(chk1[1,*])
 dates=comdim2(chk1[2,*]) 
 chk2=where(names ne '' and dates ne '',count)
 if count gt 0 then begin
  results=url+'/'+names[chk2]
  dates=dates[chk2]
 endif else begin
  results=''
  dates=''
 endelse
endif

if count eq 1 then begin
 results=results[0] & dates=dates[0]
endif
if is_string(err) then mprint,err
if (n_params() eq 1) then print,results
return & end
