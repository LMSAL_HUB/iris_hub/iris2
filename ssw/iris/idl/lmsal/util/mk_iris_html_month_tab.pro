
pro mk_iris_html_month_tab, tref, only_day_ref=only_day_ref, $
  parent_arr=parent_arr, relpath_arr=relpath_arr, $
  relpath_archive=relpath_archive, suffix_arr=suffix_arr, $
  col_descrip_arr=col_descrip_arr, outfil_current=outfil_current, outdir_current=outdir_current, $
  strtab_arr=strtab_arr, htmltab=htmltab, $
  quiet=quiet

;+
; Notes:
;   Locations and names of relevant planning files:
;     TDT:
;       /archive/iris/transfer/moc2ops/daily_planning-out-archive/20130703
;         IRIS_science_timeline_20130703.V01.txt
;         T13184TIMFIL.06.html
;         T13184TIMFIL.06
;         iris_20130703.05.gif
;
; TODO:
;   Figure out:
;     FULL_PATH_ARCHIVE
;     ARCH_FILE
;     ARCH_FILE_ARR
;-

if not exist(quiet) then quiet = 1

; Previously relevant directories:
;   '/archive/iris/transfer/moc2ops/daily_planning-out'
;   '/archive/iris/transfer/moc2ops/daily_planning-out-archive'
;   '/sanhome/slater/public_html/missions/iris/timeline_test'
;   '/sanhome/slater/public_html/missions/iris/timeline'
;   '/viz2/media/iris_timelines'
;   '/irisa/ops/timeline'

dir_top_page = ['/irisa/ops/timeline/']

if not exist(source_parent_arr) then source_parent_arr = $
  ['/irisa/ops/timeline/iris_tim_archive']
ndir_source = n_elements(source_parent_arr)

if not exist(archive_parent_arr) then archive_parent_arr = $
  concat_dir(dir_top_page, 'archive')
ndir_archive = n_elements(archive_parent_arr)

if not exist(archive_parent_url) then archive_parent_url = $
  'iris_tim_archive'

if not exist(filnam_html) then filnam_html = 'index.html'

if not exist(relpath_archive_dir) then relpath_monthly_html = './'
;if not exist(relpath_archive_dir) then relpath_monthly_html = 'http://sdowww.lmsal.com/sdomedia/iris_timelines'
;if not exist(relpath_archive_dir) then relpath_monthly_html = 'http://iris.lmsal.com/timeline'

if not exist(filtype_arr) then filtype_arr = ['TLI','TIM','SCI','GIF']
n_filtype = n_elements(filtype_arr)
if not exist(col_descrip_arr) then col_descrip_arr = ['TLI','TIM','SCI','GIF']
if not exist(filnam_grep_arr) then $
  grep_arr = ['IRIS_table_load_inst_b03*', $
              'iris_tim_*.V??', $
	      'IRIS_science_timeline*', $
	      'iris_*.gif']
if not exist(version_pos_start) then version_pos_start = [5, 1, 4, 4]

; Define month and year strings for month specified by TREF parameter:
tref_ext = anytim(tref, /ext)
month_ref = tref_ext(5)
year_ref = tref_ext(6)

month_names      = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
days_in_months   = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
if ((year_ref mod 4) eq 0) then $
  days_in_months = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
ndays = days_in_months(month_ref-1)

; Make table of links to all file types, for all days of specified month:

; Create pre-filled string array for html table:
strtab_arr = strarr(n_filtype+1,ndays)
strtime_arr = string(indgen(ndays)+1,format='$(i2.2)') + $
	       '-' + month_names(month_ref-1) + '-' + strtrim(year_ref,2)
strtab_arr(0,*) = strtime_arr

; ********************************************************************************
; For each date and each file type run through all source dirs looking for a match
; ********************************************************************************

for j=0,ndays-1 do begin        ; Loop through days of month

  day0 = j+1
  t_date_flat = time2file(tref_ext, /date)
  t_day = anytim(file2time(t_date_flat),/ccsds)

  daily_source_dir = string(year_ref, format='$(i4.4)') + '/' + $
  	             string(month_ref, format='$(i2.2)') + '/' + $
	             string(day0, format='$(i2.2)')

  daily_archive_dir = daily_source_dir

  daily_archive_url = concat_dir(archive_parent_url, daily_archive_dir)

; Construct full path to archive dir:
  archive_path0 = concat_dir(archive_parent_arr[0], daily_archive_dir)

  for k=0, n_filtype-1 do begin    ; Loop through file types 





;;; First look for files of this date and this file type in appropriate archive dir,
;;; and make default link to that file:
;;
;;; Construct full path to archive dir:
;;    archive_path0 = concat_dir(archive_parent_arr[0], daily_archive_dir)
;;
;;    if file_exist(archive_path0) then begin
;;      files = file_list(archive_path0, grep_arr[k], $
;;                        files=filenames, quiet=quiet)
;;
;;      if files[0] ne '' then begin
;;        filetimes = anytim(file2time(files), /ccsds)
;;        ss_match_date = where(filetimes eq t_day, n_match_date)
;;
;;	ver_nums = strmid(files(ss_match_date), version_pos_start[k], 2, /rev)
;;	max_ver_num = max(ver_nums, ss_max)
;;	    ss_link = ss_match_date[ss_max]
;;
;;        file_linked = filenames(ss_link)
;;
;;        strtab_arr(k+1,j) = '<a HREF=' + concat_dir(daily_archive_url, file_linked) + '><b>' + $
;;			    col_descrip_arr(k) + '</b></a>'
;;
;;      endif
;;    endif





    for i=0, ndir_source-1 do begin    ; Loop through source directories

; Construct full path to source dir:
      source_path0 = concat_dir(source_parent_arr[i], daily_source_dir)

; Look for files of this date and this file type in this source directory:

      files = file_list(source_path0, grep_arr[k], $
                        files=filenames, quiet=quiet)

      if files[0] ne '' then begin
        filetimes = anytim(file2time(files), /ccsds)
        ss_match_date = where(filetimes eq t_day, n_match_date)

	ver_nums = strmid(files(ss_match_date), version_pos_start[k], 2, /rev)
	max_ver_num = max(ver_nums, ss_max)
	    ss_link = ss_match_date[ss_max]

        file_linked = filenames(ss_link)

        strtab_arr(k+1,j) = '<a HREF=' + concat_dir(daily_archive_url, file_linked) + '><b>' + $
			    col_descrip_arr(k) + '</b></a>'

      endif

    endfor

  endfor

endfor

ss_status = intarr(ndays)
for j=0,ndays-1 do begin
  ss_filled = where(strtab_arr(1:*,j) ne '',n_filled)
  if n_filled gt 0 then ss_status(j) = 1
endfor
ss_good = where(ss_status eq 1, n_good)
if n_good gt 0 then strtab_arr = strtab_arr(*,ss_good) else $
  strtab_arr = strtab_arr(*,0)

;ss_good = where(ss_status eq 1, n_good)
;if n_good gt 0 then strtab_arr = strtab_arr(*,0:max(ss_good)) else $
;  strtab_arr = strtab_arr(*,0)

if size(strtab_arr, /n_dim) gt 1 then strtab_arr = reverse(strtab_arr,2)
strtab_arr = [['Date',col_descrip_arr],[strtab_arr]]
htmltab1 = strtab2html(strtab_arr, /row0header)

; Make table of links to other months:

tarr = timegrid('01-jun-2013', anytim(addtime(!stime,delta_min=31.*24.*60.), /yoh), month=1)
nmonths = n_elements(tarr)
tarr_ext = anytim(tarr, /ext)
month_arr = string(reform(tarr_ext(5,*)),format='$(i2.2)')
year_arr = strtrim(reform(tarr_ext(6,*)),2)
strdate_arr = month_names(month_arr-1) + '-' + strtrim(year_arr,2)
arch_file_arr = 'archive_month_' + year_arr + '_' + month_arr + '.html'

strtab_arr_2 = reverse('<a HREF=' + concat_dir(relpath_monthly_html, transpose(arch_file_arr)) + '><b>' + $
	     transpose(strdate_arr) + '</b></a>',2)
htmltab2 = strtab2html(strtab_arr_2)

; Make table of links to monthly files:

strdate_arr = month_names(month_arr-1) + '-' + strtrim(year_arr,2)
monthly_file_arr = 'monthly_file_' + year_arr + '_' + month_arr + '.tim'
strtab_arr_3 = reverse('<a HREF=' + concat_dir(relpath_monthly_html, $
		       transpose(monthly_file_arr)) + '><b>' + replicate('tim',nmonths) + $
		       '</b></a>',2)
htmltab3 = strtab2html(strtab_arr_3)

; Make composite HTML table:

htmltab_composite =			"<table border='0'>"
htmltab_composite = [htmltab_composite,	"<tr><td valign='top' style='font-family:courier'>"]
htmltab_composite = [htmltab_composite,	htmltab1]
htmltab_composite = [htmltab_composite,	"</td>"]
htmltab_composite = [htmltab_composite,	"<td>&nbsp;<td>"]
htmltab_composite = [htmltab_composite,	"<td valign='top'>"]
htmltab_composite = [htmltab_composite,	htmltab2]
htmltab_composite = [htmltab_composite,	"</td>"]
htmltab_composite = [htmltab_composite,	"<td>&nbsp;<td>"]
htmltab_composite = [htmltab_composite,	"<td valign='top'>"]
htmltab_composite = [htmltab_composite,	htmltab3]
htmltab_composite = [htmltab_composite,	"</td>"]
htmltab_composite = [htmltab_composite,	"</tr>"]
htmltab_composite = [htmltab_composite,	"</table>"]

; Now construct composite web page: 

out =  ['<html><head><title>IRIS Science Planning Files</title></head><body>', $
	'<h1>IRIS Science Planning Files</h1>', $
        '<!--Generated by mk_sot_plans_html.pro-->', $
        '<br><a HREF=http://iris.lmsal.com><b>IRIS Home Page</b></a>', + $
        '<br><a HREF=http://iris.lmsal.com/health-safety><b>IRIS Health and Safety Page</b></a>', + $
        '<br><br><a HREF=' + concat_dir(relpath_monthly_html, 'IRIS_gif_explained.pdf') + '><b><font color="9900cc">IRIS timeline GIF explained (somewhat)</font></b></a>', $
        '<br><br>', htmltab_composite, $
        '<br>File Types:', + $
        '<br><br>TLI:&nbsp&nbsp&nbsp&nbspIRIS_table_load_inst_b03_fast_FSWnnnnn_yyyymmdd_Vnn.proc', + $
        '<br>TIM:&nbsp&nbsp&nbsp&nbspiris_tim_yyyymmdd.nn', + $
        '<br>SCI:&nbsp&nbsp&nbsp&nbspIRIS_science_timeline_yyyymmdd.Vnn.txt']

out = [out, '</body></html>']
arch_file = 'archive_month_' + strtrim(year_ref,2) + '_' + string(month_ref,format='$(i2.2)') + '.html'
for i=0, ndir_archive-1 do $
  prstr, out, file=concat_dir(dir_top_page[i], arch_file)

tnow = !stime
tnow_ext = anytim(tnow, /ext)
month_now = tnow_ext(5)
year_now = tnow_ext(6)

; TODO: Verify that top_dir_page and archive_parent_arr have some number of elements
if ((month_ref eq month_now) and (year_ref eq year_now)) then begin
  for i=0, ndir_archive-1 do $
    spawn,'cp -p ' + concat_dir(dir_top_page[i], arch_file) + ' ' + $
                     concat_dir(dir_top_page[i], filnam_html)
endif

end
