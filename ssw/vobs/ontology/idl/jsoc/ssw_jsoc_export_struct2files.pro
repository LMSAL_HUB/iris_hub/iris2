function ssw_jsoc_export_struct2files, export_struct, $
   local=local, urls=urls, jsoc_parent=jsoc_parent
;
;+
;   Name: ssw_jsoc_export_struct2files
;
;   Input Parameters:
;      export_struct - output from jsoc export (ssw_jsoc(/export) )
;
;   Output:
;      function returns implied file -or- url list for JSOC/SUMS access 
;
;   Keyword Paramters:
;      local - (switch) Generall only@jsoc, return local/nfs SUMS names
;      urls  - (swithc) return jsoc urls (the default)
; 

retval=''
if n_elements(jsoc_parent) eq 0 then jsoc_parent='http://jsoc.stanford.edu'

if not required_tags(export_struct,'data,requestid') then begin 
   box_message,'Expect a completed jsoc export structure, including .DATA'
   return,retval
endif


data=gt_tagval(export_struct,/data) 
fnames=gt_tagval(data,/filename)
fpaths=strtrim(strcompress(str_replace(fnames,'\',' '),/remove),2)
fpaths=(['','/'])(strmid(fpaths,0,1) ne '/') + fpaths

dirs=gt_tagval(export_struct,/dir)
if dirs(0) ne '' then fpaths=str_replace(dirs,'\','')+fpaths

case 1 of 
   keyword_set(local): retval=fpaths
   else: retval=jsoc_parent+fpaths
endcase

return,retval
end


 
