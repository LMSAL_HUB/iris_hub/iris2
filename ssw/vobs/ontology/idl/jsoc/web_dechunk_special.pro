function web_dechunk_special, webstuff
;+
;   Name: web_dechunk_special
;
;   Purpose: special http 1.1 dechunker w/contraints
;
;   Input Parmeters:
;      webstuff - verbatim output from web server, assume chuncked
;
;   Output Parameters:
;      unchunked version (trimmmed/compressed...)
;
;   Restrictions:
;      special case, for <something [chunk#] </something>
;      pending extension of generic web_dechunk
;      Makes some unsubstantiated assumptions about chunking based on
;      empiracal/ad-hoc - I could read the standard, but...
;
;   History:
;      29-apr-2011 - S.L.Freeland - panic mode web_dechunk.pro swap in
;
;   Side Effects - 
;      returned data have indentations etc removed; easy to 
;      apply this alg. and return preserved spacing, but not now.
;
;+

retval=strtrim(webstuff,2)
sso=where(strpos(retval,'<') eq 0 and strlastchar(retval) ne '>',ccnt)

if ccnt gt 0 then begin 
   retval(sso)=retval(sso)+retval(sso+2) ; glue lines
   retval(sso+1)='' ; assume chunk size
   retval(sso+2)='' ; assume end line, now glued to start line
   retval=strarrcompress(retval) ; get rid of now-null lines
endif

return,retval
end
