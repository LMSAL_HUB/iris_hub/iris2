;+
pro iri_disp_fits_head_bdp, ff, code, hc=hc, outfil=outfil, tlmfil=tlmfil
;	Codes
;		0: basic image info
;		1: sequencer
;		2: gt, iss
;		3: mechanisms, cfg delays
;		4: AEC
;
;-
tfo=bin_date(systime())
filt=string(tfo(0), '.', tfo(1), '.', tfo(2), '*/*.fits', format="(I4,A1,I02,A1,I02,A8)")
if (n_elements(ff) eq 0) then ff=Dialog_Pickfile(path=getenv('IRI_FITS_BASE_DIR'), filter=filt, title='Pick fits files to read',/multiple, /must_exist)
if (n_elements(code) eq 0) then code=0
mreadfits, ff, head

out=['IRI_DISP_FITS_HEAD Ver 0.02 Program Run: ' + !stime, $
        '#']

case code of
	0: begin
		list = ['fsn', 'imgfpt', 'instrume', 'tapcode', 'exptime', 'iimgshce', 'int_time', 'img_type', 'compid', 'cropid', 'lutid', 'naxis1', 'naxis2']
		tit0 = '#    FSN  ExpStart                Inst      TAP   Exposure   CmdExp   Int_Time     ImgType  CompID  CropID  LookID    Naxis1    Naxis2'
		format=str2arr('I8,A-22,A-8,I2,F10.6,I6,F10.6,A8,I6,I6,I6,I8,I8')
	end		
	1: begin
		list = ['isqfsn', 'imgfpt', 'iifrmtyp', 'iiobslid', 'iicrsid', 'iifrmlid', 'isqcamid', 'isqtap', 'isqisysn', 'isqbitid', 'isqcmpid']
		tit0 = '#  SqFSN  ExpStart               FrmType   OBSID   CRSID   FRMID SqCamID   SqTAP SqISysN SqBitID  SqCompID'
		format=str2arr('I8,A-22,I6,I6,I6,I6,I6,I6,I6,I6,I8')
	end		
	2: begin
		list = ['fsn', 'imgfpt', 'iissloop', 'iisspzta', 'iisspztb', 'iisspztc', 'irtpzta', 'irtpztb', 'irtpztc', 'iwbpzta', 'iwbpztb', 'iwbpztc', 'igtpoffx', 'igtpoffy', 'igtpsvx', 'igtpsvy']
		tit0 = '#    FSN  ExpStart               ISSLoop ISSPZTA ISSPZTB ISSPZTC  RTPZTA  RTPZTB  RTPZTC  WBPZTA  WBPZTB  WBPZTC GTPOffX GTPoffY  GTPSvX  GTPSvY'
		format=str2arr('I8,A-22,A6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6')
	end		
	3: begin
		list = ['fsn', 'imgfpt', 'ifmcpos', 'ifmpos', 'ifwctgt', 'ifwpos', 'ifwenc', 'iwm1ctgt', 'iwm1cpos', 'iwm1enc', 'iwm2ctgt', 'iwm2cpos', 'iwm2enc', 'iimgcfd1', 'iimgcfd2', 'iimgcfd3', 'iimgcfd4']
		tit0 = '#    FSN  ExpStart                 FMCmd   FMPos   FWCmd   FWPos   FWEnc  WM1Cmd   WM1Pos WM1Enc  WM2Cmd  WM2Pos  WM2Enc  Delay1  Delay2  Delay3  Delay4'
		format=str2arr('I8,A-22,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6,I6')
	end
	4: begin
		list = ['fsn', 'isppktim', 'iiobslid', 'iifrmlid', 'img_type', 'instrume', 'iimgshce', 'iaectid', 'iaecflag', 'iaecflfl', 'iaecevfl', 'iaecenam', 'iaecevlt', 'iaecltim', 'iihis1', 'iihis2', 'iihis3', 'iihis4', 'iihis5']		;'iled1hws', 'iled2hws', 'iled3hws', 'iaecenam', 'iaecevlt', 'iaecltim']
		tit0 = '#    FSN  ISP Pkt Time            ObsList FrmList ImgTyp  Inst      CmdExp    AECTabID  AECFlag FlrFlag EvtFlag EvtMask EvtLapse  AECLapse  HistBin1      HistBin2      HistBin3      HistBin4      HistBin5'
		format=str2arr('I8,A-22,I-6,I-6,A-6,A-8,I-8,I-8,A-6,A-6,A-6,Z-6.4,I-8,I-8,I-12,I-12,I-12,I-12,I-12')
	end		

	5: begin
		list = ['fsn','imgfpt','img_type','instrume','isqoltdx','isqoltnx','isqfltdx','isqfltnx','iiobslid','iifrmlid','iicrsID','iifuvfdb','iinuvfdb','iisjifdb','iisspzta','iisspztb','iisspztc','igtpsvx','igtpsvy','ifmpos','ifwpos','lutid','compid','int_time','iimgshce']		;'iled1hws', 'iled2hws', 'iled3hws', 'iaecenam', 'iaecevlt', 'iaecltim']
		tit0 = '#    FSN  ExpStart                ImgTyp  Inst  OLdx OLnx FLdx FLnx ObsList FrmList CRSID  FUVFDB  NUVFDB  SJIFDB  PZTA      PZTB      PZTC      Sunx    Suny    FMPos   FWPos   LUTID   CompID  IntTim CmdExp'
		format=str2arr('I8,A-22,A-6,A-6,I-3,I-3,I-3,I-3,I-6,I-6,I-6,I-6,I-6,I-6,I-8,I-8,I-8,I-6,I-6,I-6,I-6,I-6,I-6,F5.2,I-6')
	end		

endcase
out=[out, tit0, get_infox(head,list,format=format)]
prstr, out, /nomore, hc=hc, file=outfil, land=land
if (keyword_set(tlmfil)) then tlmfil=get_infox(head,'tlmdsnam')

end
