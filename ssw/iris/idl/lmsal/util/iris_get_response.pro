function iris_get_response,tt, response_file=response_file, $
         pre_launch=pre_launch, version=version, full=full, angstrom=angstrom
;
;+
;   Name: iris_get_response
;
;   Purpose: return IRIS effective area / response structure
;
;   Input Paramters:
;      time : time (or time vector) in anytim compatible format
;             default: current time, which may not be what you want!
;             time is ignored for versions 002 and 001
;
;   Output:
;      function returns effective area structure for the given time(s)
;                       (or vector of structures if times is a vector)
;
;   Keyword Parameters:
;      response_file - optional user supplied
;                    - default is latest effective area file
;                      in $SSW_IRIS distribution
;      version : version number of effective area file to be used
;                - default: latest version
;      /pre_launch : equivalent to version='002'
;       Note: /pre_launch overrides version which overrides response_file
;      /full : returns the full effective area structure including some
;              cryptic coefficients
;      /a : lambda units in Angstrom - default: nm
;
;   Calling Sequence:
;      IDL> iresp=iris_get_response(time)
;      
;   Notes:
;   - Starting with effective area version 003, this routine calculates
;     the time dependent effective area using coefficients.
;   - The time dependent calibration comes from a combination of throughput
;     trending and cross-calibration with SORCE/SOLSTICE
;   - Version 003 is very preliminary, the time dependence may not be
;     that accurate due to various simplifications and assumptions made
;   - Output structure:
;      IDL> help,iresp,/str
;      DATE_OBS        STRING    ''
;      LAMBDA          FLOAT     Array[3601]
;      AREA_SG         FLOAT     Array[3601, 2]
;      NAME_SG         STRING    Array[2]
;      DN2PHOT_SG      FLOAT     Array[2]
;      AREA_SJI        FLOAT     Array[3601, 4]
;      NAME_SJI        STRING    Array[4]
;      DN2PHOT_SJI     FLOAT     Array[4]
;      COMMENT         STRING    ''
;      VERSION         STRING    '003'
;      VERSION_DATE    STRING    '20150331'
;
;      /full returns additional tags, /pre_launch doesn't have dn2phot tags
;
;   - Calls fit_iris_xput
;
;   History:
;      13-Feb-2013 - J.P.Wuelser  - effective area calibration
;                    S.L.Freeland - simple $SSW wrapper ala aia_get_response
;      31-Mar-2015 - J.P.Wuelser  - time varying effective area
;       2-Mar-2016 - J.P.Wuelser  - updated SJI effective area curve
;                                   spline for NUV SG response (was linear)
;
;- 

r=-1
prefix='sra' ; default is effective area 
respdir=get_logenv('$IRIS_RESPONSE') ; default is in $SSW_IRIS distribution
if respdir eq '' then respdir=concat_dir('$SSW_IRIS','response')

; logic to get older version files
if keyword_set(pre_launch) then version='002'
if n_elements(version) eq 1 then case fix(version) of
   1 : response_file = concat_dir(respdir,'iris_sra_20130211.geny')
   2 : response_file = concat_dir(respdir,'iris_sra_20130715.geny')
   3 : response_file = concat_dir(respdir,'iris_sra_c_20150331.geny')
   4 : response_file = concat_dir(respdir,'iris_sra_c_20161022.geny')
endcase
if n_elements(response_file) eq 0 then begin
   response_file=file_search(respdir,'iris_' + prefix+'*')
   response_file=last_nelem(response_file) ; future versions?
endif
if file_exist(response_file) then restgenx,file=response_file(0),r $
else begin
   box_message,'response file > ' + response_file(0) + ' not found...'
   return,r
endelse
 
if fix(r.version) le 2 then begin
  if keyword_set(angstrom) then r.lambda=r.lambda*10.
  return,r
endif

; time dependent effective area
; 1. output structure
if n_elements(tt) eq 0 then get_utc,tt
ntt = n_elements(tt)
if keyword_set(full) then o1=r else begin
  o1 = {date_obs:   '', $
       lambda:      r.lambda, $
       area_sg:     r.area_sg, $
       name_sg:     r.name_sg, $
       dn2phot_sg:  r.dn2phot_sg, $
       area_sji:    r.area_sji, $
       name_sji:    r.name_sji, $
       dn2phot_sji: r.dn2phot_sji, $
       comment:     r.comment, $
       version:     r.version, $
       version_date:r.version_date}
endelse
o1.area_sg  = 0.0
o1.area_sji = 0.0
o = replicate(o1,ntt)
o.date_obs = anytim(tt,/ccsds)

; 2. FUV SG effective areas
; Rough SG spectral ranges.  Setting eff.area to 0 outside of these
lamran = [[133.1,135.9],[138.8,140.8]]
sz = size(r.coeffs_fuv)
; time dependent response for sz[3]=3 wavelengths
rr = fltarr(ntt,sz[3])
for j=0,sz[3]-1 do rr[*,j]=fit_iris_xput(tt,r.c_f_time,r.coeffs_fuv[*,*,j])
; interpolate onto lambda grid, separately for each of the two FUV CCDs
for j=0,1 do begin
  w = where(r.lambda ge lamran[0,j] and r.lambda le lamran[1,j])
  for k=0L,ntt-1 do o[k].area_sg[w,0] $
       =interpol(reform(rr[k,j:j+1]),r.c_f_lambda[j:j+1],r.lambda[w])
endfor

; 3. NUV SG effective areas
; Rough SG spectral ranges.  Setting eff.area to 0 outside of these
lamran = [278.2,283.5]
sz = size(r.coeffs_nuv)
; time dependent response for sz[3] wavelengths
rr = fltarr(ntt,sz[3])
for j=0,sz[3]-1 do rr[*,j]=fit_iris_xput(tt,r.c_n_time,r.coeffs_nuv[*,*,j])
; interpolate onto lambda grid
w = where(r.lambda ge lamran[0] and r.lambda le lamran[1])
if fix(r.version) le 3 then begin
  for k=0L,ntt-1 do o[k].area_sg[w,1] $
       =interpol(reform(rr[k,*]),r.c_n_lambda,r.lambda[w])
endif else begin
  for k=0L,ntt-1 do o[k].area_sg[w,1] $
       =interpol(reform(rr[k,*]),r.c_n_lambda,r.lambda[w],/spline)
endelse

; 4. SJI effective areas
if fix(r.version) le 3 then begin
 sz = size(r.coeffs_sji)
 for j=0,sz[3]-1 do begin
  ; calculate pre-launch area from the individual elements
  pl_a = r.geom_area
  for k=0,n_elements(r.index_el_sji[*,j])-1 do $
      pl_a=pl_a*r.elements[r.index_el_sji[k,j]].trans
  ; time dependent response
  rr = fit_iris_xput(tt,r.c_s_time[*,*,j],r.coeffs_sji[*,*,j])
  ; time dependent profiles
  for k=0L,ntt-1 do o[k].area_sji[*,j]=pl_a*rr[k]
 endfor
endif else begin

 for nuv=0,1 do begin
   ; calculate baseline SJI area curves
   asji = r.geom_area
   for k=0,n_elements(r.index_el_sji[*,nuv*2])-1 do $
     asji=asji*r.elements[reform(r.index_el_sji[k,nuv*2:nuv*2+1])].trans

   ; apply time dependent profile shape adjustment to FUV SJI
   if ~nuv then begin
     ; FUV: apply FUV SG "slant", then normalize so that a weighted (2.4:1)
     ;      sum at C II and Si IV gives constant response
     wei = [2.4,1.0]   ; typical solar ratio CII : SiIV
     wav = r.c_f_lambda
     nwv = n_elements(wav)
     wav = [wav[0],(wav[nwv-2]*2.0+wav[nwv-1])/3.0]       ; 2 wvlngts in nm
     ; calculate baseline SG area for scaling purposes
     asg = r.geom_area
     for k=0,n_elements(r.index_el_sg[*,nuv])-1 do $
       asg=asg*r.elements[r.index_el_sg[k,nuv]].trans
     ; SG and SJI areas at wav
     asg2 = interpol(asg,r.lambda,wav)
     asj2 = fltarr(2,2)
     for j=0,1 do asj2[*,j]=interpol(asji[*,j],r.lambda,wav)
     ; calculate the normalized slant function scal, apply to asji
     for k=0L,ntt-1 do begin
       ; best-estimate slant, i.e., eff.area @ wav / baseline SG @ wav
       sca2 = interpol(o[k].area_sg[*,0],o[k].lambda,wav) / asg2
       ; normalize slant so that total(wei*asj2*sca2)/total(wei*asj2)=1
       for j=0,1 do begin
         sca2n = sca2 * total(wei*asj2[*,j])/total(wei*asj2[*,j]*sca2)
         scaln = interpol(sca2n,wav,r.lambda) > 0.0
         o[k].area_sji[*,j] = asji[*,j]*scaln
       endfor
     endfor
   endif else begin
     ; NUV: essentially same calculation as r.version=3
     for k=0L,ntt-1 do o[k].area_sji[*,2:3]=asji
   endelse
 endfor
 for j=0,3 do begin
 ; SJI specific time dependency
   rr = fit_iris_xput(tt,r.c_s_time[*,*,j],r.coeffs_sji[*,*,j])
   for k=0L,ntt-1 do o[k].area_sji[*,j]=o[k].area_sji[*,j]*rr[k]
 endfor
endelse

if keyword_set(angstrom) then o.lambda=o.lambda*10.
return, o

end
