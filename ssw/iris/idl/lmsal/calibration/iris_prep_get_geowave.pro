function IRIS_PREP_GET_GEOWAVE, header, $
	pstop = pstop

;+
;
; INPUT:
;	header	-	FITS header structure for the image whose correction parameters 
;				are desired. Only the img_path and t_obs fields are used
;
; RETURNS:
;	result	-	a structure giving the geometric/wavelength calibration parameters
;				to be passed to IRIS_GEOWAVE_CORRECT
;
; KEYWORDS:
;	pstop	-	standard debugging switch
;
;-

common IRIS_GEOWAVE_CB, geowave_dat

if N_ELEMENTS(geowave_dat) eq 0 then IRIS_PREP_READ_GEOWAVE

;+++++++++++++++++++++++++++++++++++++++++
; Currently handles array inputs by 
; calling itself recursively; not very 
; efficient, but we'll sort that out later
;-----------------------------------------
numhead = N_ELEMENTS(header)
if numhead gt 1 then begin
	result0 = IRIS_PREP_GET_GEOWAVE(header[0])
	result = REPLICATE(result0, numhead)
	for i = 1, numhead - 1 do result[i] = IRIS_PREP_GET_GEOWAVE(header[i])
	RETURN, result
endif

;+++++++++++++++++++++++++++++++++++++++++++++++++++
; geowave_dat is now a structure array of geowave 
; correction table entries. Now we need to figure out 
; which element in the array to use
;---------------------------------------------------

; Filter the database to rule out any entries that have been superseded by a 
; higher version with the same img_path and t_start
numdat = N_ELEMENTS(geowave_dat)
start_tais = ANYTIM2TAI(geowave_dat.t_start)
start_code = STRING(start_tais, form='(i10)') + '_' + geowave_dat.img_path
tsort = SORT(start_code)
uniqstarts = UNIQ(start_code[tsort])
numuniq = N_ELEMENTS(uniqstarts)
uniqarr = REPLICATE(geowave_dat[0], numuniq)
for i = 0, numuniq - 1 do begin
	this_start_code = start_code[tsort[uniqstarts[i]]]
	entries_at_thist = WHERE(start_code eq this_start_code, numthist)
	latest = WHERE(geowave_dat[entries_at_thist].version eq MAX(geowave_dat[entries_at_thist].version), numlatest)
	if numlatest gt 1 then begin
		PRINT, 'IRIS_PREP_GET_GEOWAVE: Multiple entries with latest version!'
		STOP
	endif
	uniqarr[i] = geowave_dat[entries_at_thist[latest]]
endfor

; Now, identify entries with the right IMG_PATH
imgpath = uniqarr.img_path
headerpath = header.img_path
case headerpath of
	'FUV' :	imgpath = STRMID(imgpath, 0, 3)
	'NUV'	:	
	else	:	headerpath = 'ID'
endcase

pathindex = WHERE(imgpath eq headerpath, numpath)
if numpath lt 1 then begin
	PRINT, 'IRIS_PREP_GET_GEOWAVE: No matching imgpath!'
	PRINT, 'Using identity transform...'
	pathindex = WHERE(imgpath eq 'ID', numpath)
endif
patharr = uniqarr[pathindex]

; Then identify entries where t_start and t_stop span the image's T_OBS
start_tais = ANYTIM2TAI(patharr.t_start)
stop_tais = ANYTIM2TAI(patharr.t_stop)
taiobs = ANYTIM2TAI(header.t_obs)
index = WHERE(start_tais le taiobs and stop_tais ge taiobs, numin)
if headerpath eq 'FUV' then begin
	fuvs = WHERE(patharr[index].img_path eq 'FUVS', numshort)
	fuvl = WHERE(patharr[index].img_path eq 'FUVL', numlong)
	if (numlong ne 1) or (numshort ne 1) then begin
		BOX_MESSAGE, ['IRIS_PREP_GET_GEOWAVE: Did not find unique', $
			'entries for FUVS and FUVL; returning...']
		RETURN, 0
	endif
	result = {fuvs : patharr[fuvs], fuvl : patharr[fuvl]}
endif else begin
	if numin ne 1 then begin
		BOX_MESSAGE, ['IRIS_PREP_GET_GEOWAVE: Multiple matching records found', $
			'Using first match...']
		index = index[0]
	endif
	result = patharr[index]
endelse

if KEYWORD_SET(pstop) then STOP

RETURN, result

end
