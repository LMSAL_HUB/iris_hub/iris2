function ssw_hcr_make_query,t0,t1,_extra=_extra, $
   initialize=initialize, allow_all=allow_all, $
   no_defsel=no_defsel, select=select, all_select=all_select, $
   help=help, debug=debug, search_valid=search_valid, $
   day_window=day_window, hour_window=hour_window, minute_window=minute_window, $
   eventid=eventid, events3=events3, new_api=new_api, extra=extra, $
   correlated_instruments=correlated_instruments, $
   required_corr=required_corr, optional_corr=optional_corr, $
   bypass_limit=bypass_limit, planned=planned
   
;
;+
;   Name: ssw_hcr_make_query
;
;   Purpose: generate valid HEK-HCR query string from user/ssw options
;
;   Input Parameters:
;      t0,t1 - time range (required)
;      select - optional list of desired OUTPUT params to include
;      all_select - enable all available OUTPUT params (see restrictions)
;      {day,hour,minute}_window - optionally expand time range by this window
;      initialize (switch) - if set, refresh the cache from data files
;      help (switch) - if set, show search & select params and exit
;      evendid - special case - if supplied, construct eventid search query and exit
;      correlated_instruments - optional list of instruments for correlated searches; comma delimited list
;      required_corr - synonym for correlated_instruments; comma delimited list
;      optional_corr - optional correlated inst; comma delimited list 
;      planned (switch) - if set, apply to as-planned HCR
;
;   Keyword Parameters:
;      _extra - keyword inherit -> query
;
;   Calling Examples:
;      IDL> qstring=ssw_hcr_make_query(t0,t1,instrument='trace')
;      ISL> query=ssw_hcr_query(qstring) ; use above -> HCR search
;      IDL> qstring=ssw_hcr_make_query(/help) ; show search&select params
;
;   History:
;      Circa early March 2009 - S.L.Freeland - prototype 
;      26-aug-2009 - S.L.Freeland - populate/document
;      10-sep-2009 - S.L.Freeland - force parameter typing (case sensitive!)sensitive
;      28-sep-2009 - S.L.Freeland - eventid keyword & function
;       6-may-2010 - S.L.Freeland - suite/routine name rationalization
;                                   This replaces ssw_hpkb_hcr_make_query.pro
;       4-apr-2011 - S.L.Freeland - cgi url change
;                                   sot.lmsal.com/sot-data -> www.lmsal.com/hek/hcr
;       7-jul-2014 - S.L.Freeland - add /EVENTS3 or implicit -> support new api
;      25-nov-2014 - S.L.Freeland - call to ssw_hcr_newapi_expand
;      27-jun-2017 - S.L.Freeland - add /PLANNED keyword & function (prototype)
;
;   Restrictions:
;      currently inhibiting purpose & descriptions (deal with '"' and "'"s)
;-
common ssw_hpkb_hcr_make_query_blk,asel,aquery
common ssw_hpkb_hcr_make_query_blk2, last_api

debug=keyword_set(debug)
hcrcgi='http://sot.lmsal.com/sot-data'
hcrcgi='http://www.lmsal.com/hek/hcr'

events3=keyword_set(events3) or keyword_set(new_api) ; synonym switches
if keyword_set(required_corr) then correlated_instruments=required_corr ; synonym
corr=data_chk(correlated_instruments,/string) ;

ontdata=get_logenv('SSW_ONTOLOGY_DATA')
if not file_exist(ontdata) then ontdata=concat_dir('$SSW_ONTOLOGY','data')

qfile=concat_dir(ontdata,'ssw_hpkb_hcrparam_query.dat') + (['','_events3'])(events3) 
sfile=concat_dir(ontdata,'ssw_hpkb_hcrparam_select.dat')


if total(file_exist([qfile,sfile])) ne 2 then begin 
   box_message,'problem with configuration...'   
   return,''
endif  

if n_elements(last_api) eq 0 then last_api=events3 
init=n_elements(asel) eq 0 or keyword_set(initialize) or last_api ne events3

if init then begin 
   asel=rd_tfile(sfile)
   aquery=rd_tfile(qfile)
endif

; if event id is supplied, construct that search query and exit
if data_chk(eventid,/string) then begin 
   retval=hcrcgi+'?cmd=view-event&event-id='+url_encode(eventid(0))
   return,retval ; !!!! EARLY EXIT
endif


if keyword_set(help) then begin 
   sparams=[aquery,replicate('-',n_elements(asel)-n_elements(aquery))]
   retval=strjustify(asel)+' '+sparams
   box_message,[strjustify(['Select Params','-------------',asel]) + '  ' + $
                ['Search Params','-----------',sparams]]
   return,retval  ; !!!! Early Exit
endif

defsel=['cadences','jopId','instrument','eventId', $
        'uModes','starttime','stoptime','target','telescope','wavelengths', $
        'xCen','YCen','xFov','yFov']
case 1 of 
   events3 or corr: sel=''  ; ignore select for new_api
   keyword_set(select): begin  ; user supplied at least one...
      if keyword_set(no_defsel) then sel=str2arr(select) else $
         sel=[defsel,str2arr(select)]  ; default append user to default set
   endcase
   keyword_set(all_select): sel=asel ; full list
   else: sel=defsel ; default subset 
endcase

; temporarily, remove 'purpose' 
sel=strlowcase(sel)
if not keyword_set(allow_all) then sel=sel(rem_elem(sel,['purpose','descriptions']))


if n_elements(t0) eq 0 then begin 
   box_message,'Need at least a time/time range
   return,''
endif  
 
case 1 of 
   n_elements(t1) gt 0: tx=[t0,t1]
   else: tx=t1
endcase

time_window,tx,time0,time1,$
   day_window=day_window, hour_window=hour_window, minute_window=minute_window
time0=anytim(time0,/ccsds,/trunc)
time1=anytim(time1,/ccsds,/trunc)

_extra=add_tag(_extra,time0,'starttime') ; lets call these 2 required for now
_extra=add_tag(_extra,time1,'stoptime')

if get_logenv('expand_check') ne '' then stop,'_extra before ssw_hcr_newapi_expand'
expquery=ssw_hcr_newapi_expand(_extra=_extra,rtags=rtags)  ; expand per new-api (events3 for now)
if get_logenv('expand_check') ne '' then stop,'_extra after ssw_hcr_newapi_expand'
events3=events3 or expquery ne ''  
if n_elements(rtags) gt 0 then _extra=rem_tag(_extra,rtags)
if get_logenv('check_newapi') then stop,'_extra,rtags'

search=''
tn=tag_names(_extra)

selstr=''
serstr=''

for i=0,n_tags(_extra)-1 do begin
   selss=where(tn(i) eq strupcase(asel), selcnt)
   qss=where(tn(i) eq strupcase(aquery),qcnt)
   searchp=''
   if qcnt gt 0 then $  
        serstr=temporary(serstr)+ '&' + aquery(qss(0))+'=' + $
           url_encode(strtrim(_extra.(i),2)) else $
              box_message,'Don"t know search param:'+tn(i)
endfor
if n_elements(ser) eq 0 then ser=['startTime','stopTime']

for i=0,n_elements(sel)-1 do begin
   sss=where(strupcase(sel(i)) eq strupcase(asel),scnt)
   if scnt gt 0 then selstr= selstr + $ 
    (['','&select='+asel(sss(0))])(strpos(selstr,'&select='+asel(sss(0))) eq -1)
endfor

if expquery ne '' then selstr=''

scmd=(['submit-',''])(events3) + 'search-events'+ (['2','3'])(events3) ; old v. new paraadigm/api
if corr then scmd='search-events-corr'
if keyword_set(planned) then scmd='view-planned-events'
searchcmd=hcrcgi+'?cmd='+scmd+ (['','&'])(strmid(serstr,0,1) ne '&') + serstr+selstr+(['','&'])(expquery ne '') + expquery  ; fully qualified query

if events3 or corr then begin 
   if strmatch(searchcmd,'*fillnulls*',/fold) eq 0 then searchcmd=searchcmd+'&fillNulls=true' ; pad for downline json_parse use
   optcorr=''
   if keyword_set(optional_corr) then optcorr= $
                                     arr2str('&optionalcorr='+strupcase(str2arr(optional_corr)),'')    
   if corr then searchcmd=searchcmd+ arr2str('&requiredcorr='+strupcase(str2arr(correlated_instruments)),'') + optcorr
   searchcmd=searchcmd+'&outputformat=json'+(['','&bypasslimit=true'])(keyword_set(bypass_limit))
endif

retval=searchcmd

if debug then stop,retval
return,retval
end
