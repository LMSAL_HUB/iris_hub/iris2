function aia_jsoc_badpix2struct, epoch, wave_str=wave_str, dseries=dseries, $
   save_ssw=save_ssw, files_only=files_only, no_gen=no_gen
;+
;   Name: aia_jsoc_badpix2struct
;
;   Purpose: aia bad pixel data jsoc -> structure
;
;   Input Parameters:
;      epoch - optional date (as of now, only single per mission
;
;   Keyword Parameters:
;      wave_str - optional list of wave_str (default=all)
;      files_only - if set, transfer & return nXfilenames 
;
;   Method: ssw_jsoc dialog
;
;   Restrictions/side effects:
;      generally assumed to run on server to populate ssw distribution
;      then, aia_jsoc_getcaldata will use that in-situ output
;      requires jsoc2 privs as of today
;-

if n_elements(dseries) eq 0 then dseries='aia.bad_pixels'

index=ssw_jsoc(ds=dseries,/rs_list,/jsoc2)
exp=ssw_jsoc(/export,ds=dseries,/jsoc2)
urls=ssw_jsoc_export_struct2files(exp)

case 1 of
   keyword_set(save_ssw): outdir=concat_dir('$SSW_AIA','calibration')
   file_exist(outdir):  ; user supplied
   else: outdir=get_temp_dir()
endcase

outname='aia_badpix_'+time2file(index.t_obs)+'_'+ strlowcase(index.wave_str)+'.fits'

outfiles=concat_dir(outdir,outname)
for i=0,n_elements(outname)-1 do begin 
   box_message,'getting> ' + outname(i)
   ssw_file_delete,outfiles(i)
   sock_copy,urls(i),outname(i),out_dir=outdir
endfor

nf=n_elements(outfiles)
fexist=where(file_exist(outfiles),nexist)
if nexist ne nf then stop,'Mismatch want:got'

retval={date_obs:index(0).t_obs,data_series:dseries}
if not keyword_set(files_only) then begin ; build output structure
   for i=0,nf-1 do  begin
      retval=add_tag(retval,mrdfits(outfiles(i),/silent),'w_'+index(i).wave_str)
   endfor
endif

if keyword_set(save_ssw) then $
   write_genxcat,retval,prefix='aia_badpix_',topdir=outdir
return,retval
end







