function IRIS_PREP_GET_VERSIONS, iindex

;+
;
; Looks up version information to set some header keywords in the case where
; iris_prep is being run without data
;
;-

oindex = iindex

; IPRPDVER
common IRIS_DARK_CB, darkstr
if N_TAGS(darkstr) eq 0 then IRIS_PREP_READ_DARKS
oindex.iprpdver = darkstr.version

; IPRPFVER
dummy = IRIS_PREP_FLAT(1, oindex[0], frecnum = frecnum, /nodata)
oindex.iprpfver = frecnum

; IPRPBVER
if oindex[0].img_path eq 'FUV' then begin
	dummy = IRIS_PREP_GET_BACK(oindex[0], bver)
	oindex.iprpbver = bver[0]
endif	;	otherwise, leave it at 0

; IPRPPVER
dummy = IRIS_PREP_GET_BADPIX(oindex[0], badpix_recnum = badpix_recnum)
oindex.iprppver = badpix_recnum

RETURN, oindex

end
