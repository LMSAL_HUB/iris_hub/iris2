;
;
;HISTORY:
;	Written ?? by M.Morrison
;V1.01	 7-Apr-00 (MDM) - Corrected extract sizes mis programmed 
;			  (and only recently the flight software was
;			  doing partials)
;V1.02	26-Mar-01 (MDM) - Removed hardwired lfft dir
;
if (keyword_set(qskip)) then goto, skip
set_plot,'x
;
if (n_elements(date) eq 0) then date = ut_time()
nimg = 12
infil = sxi_stol2files(date, 'lfft_partials', nimg, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
if (n_elements(infil) ne nimg) then stop, 'Not enough images'

mreadfits, infil, head, img
pr_stats, img, 3, img_info
;
skip:
break_file, infil, dsk_log, dir, filnam, ext
list = filnam+ext+'  ' + get_infox(head, 'EGSE_TIM, naxis1, naxis2')
;
xsiz = [intarr(4)+64,   intarr(4)+160,  intarr(4)+256]
ysiz = [intarr(4)+64,   intarr(4)+128,   intarr(4)+64]
amin = fltarr(nimg) +  100
amax = fltarr(nimg) + 1500
stat = strarr(nimg) + '  '
for i=0,nimg-1 do begin
    if (head(i).naxis1 ne xsiz(i)) then stat(i) = stat(i) + 'X_ERR_SIZ '
    if (head(i).naxis2 ne ysiz(i)) then stat(i) = stat(i) + 'Y_ERR_SIZ '
    if (img_info(i).avg lt amin(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_LOW '
    if (img_info(i).avg gt amax(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_HI  '
end
ss = where(strmid(stat,2) eq '', nss)
if (nss ne 0) then stat(ss) = stat(ss) + 'OK'
;
imgstr = get_infox(img_info, 'min,max,avg,dev')
out = ['LFFT_PARTIALS  Ver 1.02  Program run: ' + !stime, $
	'On machine: ' + get_host(), $
	' ', $
	' File/Image             EGSE Time               NX        NY      Min      Max      Avg      Dev  Stat', $
	;19991117_052554.fit  17-NOV-99  05:25:54       528       588     0.00  4095.00  1069.61   245.03  OK
	' ', $
	list + '  ' + imgstr + stat]

prstr, out
outfil = getenv('SXI_LFFT_DIR') + '/tmp/lfft_partials_' + filnam(0) + '.txt
prstr, out, fil=outfil
sprint, outfil, /land
;
end
