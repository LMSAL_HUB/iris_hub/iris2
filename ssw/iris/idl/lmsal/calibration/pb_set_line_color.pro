pro PB_SET_LINE_COLOR, colornames, list=list,count=count, undo=undo, shades=shades, neg=neg
;+
;
; Custom modificiation of SSW routine SET_LINE_COLOR to make a color table that looks better to
; me.
;
; MODIFIED: PB, 1/27/04
; MODIFIED: PB, 4/20/04     Made Transparent shading colors at 100 and up
; MODIFIED: PB  11/30/04 	Return colornames as output parameter
; MODIFIED:	PB	6/15/05		Added /shades keyword (making transparent colors optional)
;-

colornames = ['white', 'black', 'red', 'orange', 'yellow', 'green', 'blue', 'purple', 'magenta', 'turquoise', 'brown']

count=N_ELEMENTS(colornames)
if KEYWORD_SET(list) then begin
    for i=0, N_ELEMENTS(colornames)-1 do PRINT, FORMAT='(i2, 3x, a)', i, colornames(i)
    PRINT, '11-255	grayscale'
    RETURN
endif

for i = 0, N_ELEMENTS(colornames)-1 do LINECOLOR, i, colornames(i)
LINECOLOR, 255, 'black'

; The following lines were added as a klugy fix once I realized that I didn't like the standard colors
; defined by the SSW routine. They define the color at a lower level, so these lines are harder to understand
; but work more directly.

TVLCT, r, g, b, /get

ora = 3
r[ora] = 240
g[ora] = 160
b[ora] = 0

yel = 4
r[yel] = 240
g[yel] = 220
b[yel] = 0

pur = 7
r[pur] = 210
g[pur] = 0
b[pur] = 240

gre = 5
r[gre] = 0
g[gre] = 230
b[gre] = 0

mag = 8
r[mag] = 230
g[mag] = 20
b[mag] = 170

tur = 9
r[tur] = 0
g[tur] = 210
b[tur] = 230

if KEYWORD_SET(shades) then begin
	red_shade = 102
	r[red_shade] = 255
	g[red_shade] = 185
	b[red_shade] = 185

	blue_shade = 106
	r[blue_shade] = 185
	g[blue_shade] = 185
	b[blue_shade] = 255

	purple_shade = 107
	r[purple_shade] = 225
	g[purple_shade] = 165
	b[purple_shade] = 255
endif

TVLCT, r, g, b

;
; end the hacking modifications of the color tables
;
if KEYWORD_SET(undo) then LOADCT, 0
if KEYWORD_SET(neg) then begin
	LINECOLOR, 0, 'black'
	LINECOLOR, 255, 'white'
endif

RETURN

end
