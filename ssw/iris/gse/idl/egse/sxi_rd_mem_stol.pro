pro sxi_rd_mem_stol_s1, lin, addr, data
;
;
;
arr = str2arr(lin, delim=' ')
case arr(0) of
    '/SXI_CCML3': items = ['AHI', 'ALO', 'D1HI', 'D1LO', 'D2HI', 'D2LO', 'D3HI', 'D3LO'] 
    '/SXI_CCML1': items = ['AHI', 'ALO', 'DHI', 'DLO']
    else: stop
end
;
n = n_elements(items)
iout = intarr(n)
for i=0,n-1 do begin
    len = strlen(items(i))
    ss = where(strmid(arr, 0, len) eq items(i), nss)
    if (nss eq 0) then stop, 'Missing argument'
    v = strmid(arr(ss(0)), len+1, 99)
    tmp = 0L
    if (strmid(v, 0, 2) eq '0x') then reads, v, tmp, format='(2x, z)' $
			else reads, v, tmp
    iout(i) = tmp
end
;
addr = iout(0)*'10000'xL + iout(1)
data = byte(iout(2:*), 0, (n-2)*2)
end
;-----------------------------------------------------------
pro sxi_rd_mem_stol, infil, st_addr, array
;+
;NAME:
;	sxi_rd_mem_stol
;PURPOSE:
;	Read an EGSE STOL file and parse into an array
;INPUT:
;	infil	- The input file to ready
;OUTPUT:
;	st_addr - The start address (byte)
;	array	- byte array of data loaded
;HISTORY:
;	Written 26-Feb-01 by M.Morrison
;
if (n_elements(infil) eq 0) then infil = '/sxisw/cfl/egse_v4.x/test/src/loads/20001128a_fdet_load.src'
;
mat = rd_tfile(infil, nocomment='#')
mat = strcompress(mat)
;
ss3 = where(strmid(mat, 0, 10) eq '/SXI_CCML3', nss3)
ss1 = where(strmid(mat, 0, 10) eq '/SXI_CCML1', nss1)
nlong = nss3*3 + nss1
array = bytarr(nlong*4)
;
st_addr = -1
for i=0,n_elements(mat)-1 do begin
    lin = mat(i)
    if (strmid(lin, 0, 9) eq '/SXI_CCML') then begin
	sxi_rd_mem_stol_s1, lin, addr, data
	if (st_addr eq -1) then begin
	    st_addr = addr	;initialized
	end else begin
	    if (addr ne exp_addr) then begin
		print, 'Expected address: ', exp_addr, format='(a, "0x", z8.8)'
		print, 'Received address: ', addr, format='(a, "0x", z8.8)'
		stop
	    end
	end
	array(addr - st_addr) = data
	exp_addr = addr + n_elements(data)	;next expected address
    end
end
;
end