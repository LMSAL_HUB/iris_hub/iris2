function IRIS_PREP_INTERPOLATE_NEWTFUNC, incoords

;+
;
; Takes in a set of coordinates [x,y] and returns a new set [x',y'] showing
; where X and Y would map to in an IRIS image that has been warped using
; IRIS_PREP_GEOWAVE_CORRECT. The parameters for the warping are assumed to
; be stored in a common block.
;
; Used by NEWTON to invert the operation of INTERPOLATE. Is that really the
; best way to do this?
;
;-

; the warping parameters are stored in a common block
common IRIS_GEOWAVE_ROI_CB, params, target_x, target_y
param_tags = TAG_NAMES(params)
dummy = WHERE(param_tags eq 'FUVS', isfuv)
if isfuv then param_tags = TAG_NAMES(params.FUVS)
mapind = WHERE(param_tags eq 'DMAPX', ismap)

xin = incoords[0]
yin = incoords[1]

isfuvl = 0
if isfuv then begin
	if target_x gt 2072 then begin
		dmapx = params.fuvl.dmapx
		dmapy = params.fuvl.dmapy
		isfuvl = 1
		xin = xin - 2072
	endif else begin
		dmapx = params.fuvs.dmapx
		dmapy = params.fuvs.dmapy
	endelse
endif else begin
	dmapx = params.dmapx
	dmapy = params.dmapy
endelse

xout = dmapx[xin, yin] - target_x
yout = dmapy[xin, yin] - target_y

if isfuvl then xout = xout + 2072

RETURN, [xout, yout]

end
