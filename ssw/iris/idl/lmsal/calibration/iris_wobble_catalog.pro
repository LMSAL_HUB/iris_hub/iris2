pro IRIS_WOBBLE_CATALOG, $
	result, $
	save = save, pick = pick, load = load, add = add, opath = opath, $
	plot = plot, pstop = pstop

;+
;
; KEYWORDS:
;	opath	-	String specifying where to load/save the catalogs from; defaults 
;				to ~/iris/wobble
;	/save	-	If set, then save the result as a genx file in opath
;	/load	-	Defaults to 1; must set explicitly to 0 if you don't want to 
;				load a previously-saved wobble catalog
;	/pick	-	If set, then open a DIALOG_PICKFILE window; by default, just uses
;				the latest genx file in the opath
;	/add	-	Load a catalog, and search for any more recent runs to append
;	/pstop	-	Stop in routine for debugging 
;
;-

t0 = '1-jul-2013'
;t1 = STRMID(RELTIME(days=-1), 0, 10)
t1 = RELTIME(days=-1.5)
if N_ELEMENTS(load) eq 0 then load = 1

if not KEYWORD_SET(opath) then opath = '~/iris/wobble/'
if KEYWORD_SET(add) then begin
	load = 1
	save = 1
endif

if KEYWORD_SET(load) then begin
	; Load a previously-saved wobble catalog
	if KEYWORD_SET(pick) then begin
		wobfile = DIALOG_PICKFILE(path = opath, filter = '*wobcat.genx')
	endif else begin
		files = FILE_SEARCH(CONCAT_DIR(opath, '*wobcat.genx'))
		filetai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(files), 0, 15)))
		latest = WHERE(filetai eq MAX(filetai))
		wobfile = files[latest[-1]]
	endelse
	RESTGEN, file = wobfile, result
	if KEYWORD_SET(add) then begin
		t0 = result[-1].tstop
		oresult = result
	endif
endif

if N_ELEMENTS(result) eq 0 or KEYWORD_SET(add) then begin
	; Generate from housekeeping
	obses = IRIS_TIME2TIMELINE(t0, t1)
	if N_TAGS(obses) eq 0 then begin
		PRINT, 'IRIS_WOBBLE_CATALOG: No OBS found in new interval...'
		RETURN
	endif
	iwob = WHERE(obses.obsid eq 4202100004 or obses.obsid eq 4202100002, nwob)
	if nwob eq 0 then begin
		PRINT, 'IRIS_WOBBLE_CATALOG: No new wobble OBS found...'
		RETURN
	endif

	wobroll = IRIS_TIME2HK(obses[iwob], /mag)
	notes = STRARR(nwob)

	wobstr = CREATE_STRUCT('tstart', '', 'repeats', 0l, 'tstop', '', 'obsid', 0ll, $
		'roll', 0., 'pitch', 0., 'yaw', 0., 'notes', '')
	
	result = REPLICATE(wobstr, nwob)
	for i = 0, nwob-1 do begin
		result[i].tstart	= obses[iwob[i]].date_obs
		result[i].tstop	= obses[iwob[i]].date_end
		result[i].repeats	= obses[iwob[i]].repeats
		result[i].obsid	= obses[iwob[i]].obsid
		result[i].roll		= wobroll[i].a_eulerbr_z
		rr					= wobroll[i].a_eulerbr_z * !pi / 180.
		yy					= wobroll[i].a_eulerbr_y * 3600.
		xx					= wobroll[i].a_eulerbr_x * 3600.
		result[i].yaw		= yy * SIN(rr) - xx * COS(rr) + 90
		result[i].pitch		= xx * SIN(rr) + yy * COS(rr) - 90
		result[i].notes	= notes[i]
	endfor
endif

if KEYWORD_SET(add) then begin
	result = [oresult, result]
endif

nwob = N_ELEMENTS(result)
PRINT, '', TAG_NAMES(result), form = '(a5,a25,a10,a25,a12,3a8,a10)'
for i = 0, nwob - 1 do PRINT, i, result[i], form = '(i5,a25,i10,a25,i12,f8.1,2i8,a10)'

if KEYWORD_SET(save) then begin
	wobfile = TIME2FILE(/sec, RELTIME(/now)) + '_wobcat.genx'
	SAVEGEN, file = CONCAT_DIR(opath, wobfile), result
endif

if KEYWORD_SET(plot) then begin
	period = 86400d * 365.25		;	Only approximate
	perihelion = '5-jan-2014'		;	Only approximate
	peritai = ANYTIM2TAI(perihelion)
	daylist = FINDGEN(365) * 86400d  + peritai
	reltemp = COS(daylist/period * 2 * !pi)
	WDEF, 10, 900, 1200
	!p.multi = [0, 1, 5]
	rolls = [0,90,-90,45,-45]
	cols = [2, 5, 6, 7, 8]
	for i = 0, N_ELEMENTS(rolls)-1 do begin
		thisroll = WHERE(ROUND(result.roll) eq rolls[i], numthis)
		tphase = peritai + ANYTIM2TAI(result[thisroll].tstart) mod period
		UTPLOT, TAI2UTC(daylist), reltemp, chars = 2, /xstyle, $
			xtitle = 'Time of year', xticklen = 0.05, $
			title = 'Wobble at roll ' + STRTRIM(rolls[i], 2)
		OUTPLOT, TAI2UTC(tphase), psym = 4, COS(tphase / period * 2 * !pi), $
			color = cols[i], symsize = 2
	endfor
endif

if KEYWORD_SET(pstop) then STOP

end