;------------------------------------------------------------------
pro parse_cmd_def, command, cmd, qdisp=qdisp











end
;cmd = {name: '', group: '', nfrm: 0, nkey: 0, nfil: 0, $	;command structure
;	critical: '', descr: '', date_mod: '', $
;	fil: replicate(tc_fil0, nmax_fil), $
;	key: replicate(tc_key0, nmax_key)}
;
;
;<irisCmd>
;<I_CMD_MNEM descr="" critical="no" guarded="no" date_mod="">
;	<Arg1 required="yes" range="0..10" enum="E_I_CMD_ENUM1" descr=""/>
;	<ArgN required="" range="" enum="" descr=""/>
;</I_CMD_MNEM>
;<E_I_CMD_ENUM val1="string1" val2="string2" ... valn="stringn"/>
;</irisCmd>
;------------------------------------------------------------------

pro parse_ylg_def, ylog, ylg, qdisp=qdisp

;---	ylog is an IDLffXMLDOMNode object

;---	create NodeList object containing dsc names
ylg_names=ylog->GetChildNodes()

;---	number of nodes in NodeList
n_parents=ylg_names->GetLength()

;---	Count total entries
n_entries=0
n_names=0
for i=0, n_parents-1 do begin
	;---	qq is a Node object
	qq=ylg_names->Item(i)
	if (qq->GetNodeType() eq 1) then n_names=n_names+1
endfor

if qdisp then print,'Found '+string(n_names)+' YLG entries'

ylg=replicate(ylg,n_names)
counter=0
for i=0,n_parents-1 do begin
	;---	name is an Element object or a TextNode object 
	name=ylg_names->Item(i)
	;---	only process Elements
	if (name->GetNodeType() eq 1) then begin
		if qdisp then print,name->GetNodeName()
		namename=name->GetNodeName()
		ylg(counter).name=namename
		ylg(counter).descr=name->GetAttribute('descr')
		ylg(counter).date_mod=name->GetAttribute('date_mod')
		ylg(counter).acoeff=name->GetAttribute('acoeff')
		ylg(counter).bcoeff=name->GetAttribute('bcoeff')
		ylg(counter).ccoeff=name->GetAttribute('ccoeff')
		ylg(counter).dcoeff=name->GetAttribute('dcoeff')
		ylg(counter).ecoeff=name->GetAttribute('ecoeff')
		ylg(counter).fcoeff=name->GetAttribute('fcoeff')
		ylg(counter).gcoeff=name->GetAttribute('gcoeff')
		counter=counter+1
	endif		
endfor
end
;ylg = {name: '', descr: '', acoeff:0.0d, bcoeff:0.0d, ccoeff:0.0d, $
;	dcoeff:0.0d, ecoeff:0.0d, fcoeff:0.0d, gcoeff:0.0d, date_mod: ''}
;<irisYlg>
;<Y_I_HTR1_DB_HI descr="" date_mod="" acoeff="0.00102629107211664" bcoeff="2.39593818364401E-4" ccoeff="1.55062023263385E-7" dcoeff="422000.0" ecoeff="819.0" fcoeff="25.0" gcoeff="273.15"/>
;...
;</irisYlg>

;------------------------------------------------------------------

pro parse_alg_def, analog, alg, qdisp=qdisp

;---	analog is an IDLffXMLDOMNode object

;---	create NodeList object containing dsc names
alg_names=analog->GetChildNodes()

;---	number of nodes in NodeList
n_parents=alg_names->GetLength()

;---	Count total entries
n_entries=0
n_names=0
for i=0, n_parents-1 do begin
	;---	qq is a Node object
	qq=alg_names->Item(i)
	if (qq->GetNodeType() eq 1) then n_names=n_names+1
endfor

if qdisp then print,'Found '+string(n_names)+' ALG entries'

alg=replicate(alg,n_names)
counter=0
for i=0,n_parents-1 do begin
	;---	name is an Element object or a TextNode object 
	name=alg_names->Item(i)
	;---	only process Elements
	if (name->GetNodeType() eq 1) then begin
		if qdisp then print,name->GetNodeName()
		namename=name->GetNodeName()
		alg(counter).name=namename
		alg(counter).descr=name->GetAttribute('descr')
		alg(counter).date_mod=name->GetAttribute('date_mod')
		alg(counter).order=6
		alg(counter).coeff(0)=name->GetAttribute('coef0')
		alg(counter).coeff(1)=name->GetAttribute('coef1')
		alg(counter).coeff(2)=name->GetAttribute('coef2')
		alg(counter).coeff(3)=name->GetAttribute('coef3')
		alg(counter).coeff(4)=name->GetAttribute('coef4')
		alg(counter).coeff(5)=name->GetAttribute('coef5')
		counter=counter+1
	endif		
endfor
end
;alg = {name: '', order: 0, coeff: dblarr(6), $
;	date_mod: '', descr: ''}		
;<irisAlg>
;<A_I_FC_CENTER_POSITION descr="" date_mod="" coef0="0" coef1="0.001221001"  coef2="0"  coef3="0"  coef4="0"  coef5="0"/>
;...
;</irisAlg>

;------------------------------------------------------------------

pro parse_dsc_def, discrete, dsc, qdisp=qdisp

;---	discrete is an IDLffXMLDOMNode object

;---	create NodeList object containing dsc names
dsc_names=discrete->GetChildNodes()

;---	number of nodes in NodeList
n_parents=dsc_names->GetLength()

;---	Count total entries
n_entries=0
n_names=0
for i=0, n_parents-1 do begin
	;---	qq is a Node object
	qq=dsc_names->Item(i)
	if (qq->GetNodeType() eq 1) then begin
		n_names=n_names+1
		;---	pp is a NodeList object
		pp=qq->GetChildNodes()
		n_children=pp->GetLength()
		for j=0,n_children-1 do begin
			rr=pp->Item(j)
			if (rr->GetNodeType() eq 1) then n_entries=n_entries+1
		endfor
	endif
endfor
if qdisp then print,'Found '+string(n_entries)+' DSC entries in '+string(n_names)+' names'

dsc=replicate(dsc,n_entries)
counter=0
for i=0,n_parents-1 do begin
	;---	name is an Element object or a TextNode object 
	name=dsc_names->Item(i)
	;---	only process Elements
	if (name->GetNodeType() eq 1) then begin
		if qdisp then print,name->GetNodeName()
		namename=name->GetNodeName()
		;---	entries is a NodeList object
		entries=name->GetChildNodes()
		n_children=entries->GetLength()
		for j=0,n_children-1 do begin
			;---	entry is an Element object or a TextNode object
			entry=entries->Item(j)
			;---	only process Elements
			if (entry->GetNodeType() eq 1) then begin
				if qdisp then print,'	'+entry->GetNodeName()
				dsc(counter).name=namename
				dsc(counter).descr=entry->GetAttribute('descr')
				dsc(counter).low=entry->GetAttribute('min')
				dsc(counter).hi=entry->GetAttribute('max')
				dsc(counter).str=entry->GetNodeName()
				dsc(counter).date_mod=entry->GetAttribute('date_mod')
				counter=counter+1
			endif
		endfor
	endif		
endfor
end
;dsc = {name: '', descr: '', $		;string table description lookup
;	low: 0L, hi: 0L, str: '', date_mod: ''}
;<D_I_SUR_CMD_FC>
;         <NOP min="0" max="0"/>

;------------------------------------------------------------------

pro parse_tlm_pkt, telemetry, tlm, qdisp=qdisp

;---	telemetry is an IDLffXMLDOMNode object

;---	create NodeList object containing tlm packets
telem_pkts=telemetry->GetChildNodes()

;---	number of nodes in NodeList
n_parents=telem_pkts->GetLength()

;---	Count total mnemonics
n_mnem=0
n_pkts=0
for i=0, n_parents-1 do begin
	;---	qq is a Node object
	qq=telem_pkts->Item(i)
	if (qq->GetNodeType() eq 1) then begin
		n_pkts=n_pkts+1
		;---	pp is a NodeList object
		pp=qq->GetChildNodes()
		n_children=pp->GetLength()
		for j=0,n_children-1 do begin
			rr=pp->Item(j)
			if (rr->GetNodeType() eq 1) then n_mnem=n_mnem+1
		endfor
	endif
endfor
if qdisp then print,'Found '+string(n_mnem)+' mnemonics in '+string(n_pkts)+' packets'

tlm=replicate(tlm,n_mnem)
counter=0
for i=0,n_parents-1 do begin
	;---	pkt is an Element object or a TextNode object 
	pkt=telem_pkts->Item(i)
	;---	only process Elements
	if (pkt->GetNodeType() eq 1) then begin
		if qdisp then print,pkt->GetNodeName()
		aid=pkt->GetAttribute('applicationId')
		sr=pkt->GetAttribute('source')
		;---	mnems is a NodeList object
		mnems=pkt->GetChildNodes()
		n_children=mnems->GetLength()
		for j=0,n_children-1 do begin
			;---	mnem is an Element object or a TextNode object
			mnem=mnems->Item(j)
			;---	only process Elements
			if (mnem->GetNodeType() eq 1) then begin
				if qdisp then print,'	'+mnem->GetNodeName()
				tlm(counter).name=strlowcase(mnem->GetNodeName())
				if aid eq 56 then tlm(counter).name='isp'+strmid(strlowcase(mnem->GetNodeName()),1)
				tlm(counter).source=sr
				tlm(counter).group=pkt->GetNodeName()
				tlm(counter).unit=pkt->GetAttribute('unit')
				ct=mnem->GetAttribute('conv_type')
				tlm(counter).conv_code=ct
				if (ct eq 'dsc') then tlm(counter).conv_code='D'
				if (ct eq 'alg') then tlm(counter).conv_code='A'
				if (ct eq 'ylg') then tlm(counter).conv_code='Y'
				tlm(counter).conv=mnem->GetAttribute('conv')
				tlm(counter).descr=mnem->GetAttribute('descr')
				tlm(counter).date_mod=mnem->GetAttribute('date_mod')
				tlm(counter).type=mnem->GetAttribute('encoding')

				tlm(counter).cond=''
				tlm(counter).apid=aid
				tlm(counter).nbit=mnem->GetAttribute('lengthInBits')
				tlm(counter).stbyte=mnem->GetAttribute('byteOffset')-14
				tlm(counter).stbit=mnem->GetAttribute('bitOffset')

				;;;---	Any byte swapping done is relative to the byte order of type UL1
				;;;---	32-bit longs
				if ((mnem->GetAttribute('byteOrder') eq '0123') and (mnem->GetAttribute('encoding') eq 'unsigned')) then tlm(counter).type='UL1'

				;;;---	16-bit ints
				if ((mnem->GetAttribute('byteOrder') eq '01') and (mnem->GetAttribute('encoding') eq 'unsigned')) then tlm(counter).type='UI1'

				;;;---	Bytes
				if ((mnem->GetAttribute('byteOrder') eq '0') and (mnem->GetAttribute('encoding') eq 'unsigned') and (fix(mnem->GetAttribute('lengthInBits')) le 8)) then tlm(counter).type='UB'

				;;;---	Floats, doubles, and long64s
				if ((mnem->GetAttribute('encoding') eq 'float') and (fix(mnem->GetAttribute('lengthInBits')) eq 32)) then tlm(counter).type='SCF'
				if ((mnem->GetAttribute('encoding') eq 'float') and (fix(mnem->GetAttribute('lengthInBits')) eq 64)) then tlm(counter).type='DBL'
				if ((mnem->GetAttribute('encoding') eq 'unsigned') and (fix(mnem->GetAttribute('lengthInBits')) eq 64)) then tlm(counter).type='U64'

				;;;---	Byte-swapped for the HW packet
				;;;--	32-bit longs, short byteswap 0123 -> 1032
				if ((mnem->GetAttribute('byteOrder') eq '3210') and (mnem->GetAttribute('encoding') eq 'unsigned')) then tlm(counter).type='USCHW'

				;;;---	16-bit ints, short byteswap 01 -> 10
				if ((mnem->GetAttribute('byteOrder') eq '10') and (mnem->GetAttribute('encoding') eq 'unsigned')) then tlm(counter).type='UI2'

				;;;--- signed require special treatment since the container must match the final size of the data
				;;;--- signed ints from 1 to 8 bytes
				if (mnem->GetAttribute('encoding') eq 'signed') then begin
					len=fix(mnem->GetAttribute('lengthInBits'))
					bitoff=fix(mnem->GetAttribute('bitOffset'))
					case 1 of
						(len eq 16):	begin
									tlm(counter).type='SI1'
									if ((mnem->GetAttribute('byteOrder') eq '3210') or (mnem->GetAttribute('byteOrder') eq '10')) then tlm(counter).type='SI2'
								end
						(len eq 32):	begin
									tlm(counter).type='SL1'
									if (mnem->GetAttribute('byteOrder') eq '3210') then tlm(counter).type='SSCHW'
								end
						(len le 8):	tlm(counter).type='SB'
						(len eq 64):	tlm(counter).type='S64'
						else: tlm(counter).type='SL1'
					endcase
					if (bitoff ne 0) then begin
						corr=bitoff/8
						tlm(counter).stbyte=mnem->GetAttribute('byteOffset')-14+corr
						tlm(counter).stbit=tlm(counter).stbit-(corr*8)
					endif	
				endif					

;				if ((mnem->GetAttribute('encoding') eq 'signed') and (fix(mnem->GetAttribute('lengthInBits')) eq 64)) then tlm(counter).type='S64'

				counter=counter+1
			endif
		endfor
	endif		
endfor
end
;tlm = {name: '', source: '', group: '', $
;	unit: '', conv_code: '', conv: '',  $
;	descr: '', date_mod: '', $
;	type: '', stbit: 0L, nbit: 0L, $	;in SRC
;	cond: '', $
;	apid: 0, stbyte: 0L}
;<I_SUR_HK_33 applicationId="" source="">
;<I_SUR_EEPROM_STATUS offsetFrom="startOfPacket" encoding="unsigned" lengthInBits="32" byteOffset="40" bitOffset="0" dsc=""/>
;----------------------------------------------------------------------------------

pro parse_cmd_pkt













end
;tc_fil0 = {ifrm: 0, label: '', stbit: 0, length: 0, value: 0L, descr: '', date_mod: ''}
;tc_key0 = {ifrm: 0, label: '', stbit: 0, length: 0, low: 0L, hi: 0L, default: 0L, descr: '', date_mod: ''}
;cmd = {name: '', group: '', nfrm: 0, nkey: 0, nfil: 0, $        ;command structure
;        critical: '', descr: '', date_mod: '', $
;        fil: replicate(tc_fil0, nmax_fil), $
;        key: replicate(tc_key0, nmax_key)}


;----------------------------------------------------------------------------------
pro sag_iri_rd_itos_db, tlm2, alg2, ylg2, dsc2, cmd2, cond2, infil=infil0, qdisp=qdisp

common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd, cond

if not keyword_set(qdisp) then qdisp=0
if qdisp then print,'Here we go'

tlm = {name: '', source: '', group: '', $
	unit: '', conv_code: '', conv: '',  $
	descr: '', date_mod: '', $
	type: '', stbit: 0L, nbit: 0L, $	;in SRC
	cond: '', $
	apid: 0, stbyte: 0L}

alg = {name: '', order: 0, coeff: dblarr(6), $
	date_mod: '', descr: ''}		;analog coefficient conversion table

ylg = {name: '', descr: '', acoeff:0.0d, bcoeff:0.0d, ccoeff:0.0d, $
	dcoeff:0.0d, ecoeff:0.0d, fcoeff:0.0d, gcoeff:0.0d, date_mod: ''}		;logarithmic conversion

dsc = {name: '', descr: '', $		;discrete conversion
	low: 0L, hi: 0L, str: '', date_mod: ''}


;infil='/irisgse/etc/itosDbXML/iris_idl_db.xml'
infil=getenv('IRI_EGSE_DB_FILE')

;---	read xml db file into IDLffXMLDOMDocument object instance called xmldoc
xmldoc=obj_new('IDLffXMLDOMDocument', filename=infil)		;, VALIDATION_MODE=2, /EXCLUDE_IGNORABLE_WHITESPACE,SCHEMA_CHECKING=2)

;---	create Node object (there should only be 1 element called irisTlmPkts)
telemetry=(xmldoc->GetElementsByTagName('irisTlmPkts'))->Item(0)

if obj_valid(telemetry) then begin
	if qdisp then print, 'Processing telemetry...'
	parse_tlm_pkt, telemetry, tlm, qdisp=qdisp
	tlm2=tlm
endif
obj_destroy,telemetry

;---	create Node object (there should only be 1 element called irisDsc)
discrete=(xmldoc->GetElementsByTagName('irisDsc'))->Item(0)

if obj_valid(discrete) then  begin
	if qdisp then print, 'Processing DSC...'
	parse_dsc_def, discrete, dsc, qdisp=qdisp
	dsc2=dsc
endif
obj_destroy,discrete

;---	create Node object (there should only be 1 element called irisAlg)
analog=(xmldoc->GetElementsByTagName('irisAlg'))->Item(0)

if obj_valid(analog) then  begin
	if qdisp then print, 'Processing ALG...'
	parse_alg_def, analog, alg, qdisp=qdisp
	alg2=alg
endif
obj_destroy,analog

;---	create Node object (there should only be 1 element called irisYlg)
ylog=(xmldoc->GetElementsByTagName('irisYlg'))->Item(0)

if obj_valid(ylog) then  begin
	if qdisp then print, 'Processing YLG...'
	parse_ylg_def, ylog, ylg, qdisp=qdisp
	ylg2=ylg
endif
obj_destroy,ylog

if (n_params(0) ge 1) then tlm2 = tlm	;pass out common block as a param
if (n_params(0) ge 2) then alg2 = alg
if (n_params(0) ge 3) then ylg2 = ylg	; added 21-Nov-07, GAL
if (n_params(0) ge 4) then dsc2 = dsc
if (n_params(0) ge 5) then cmd2 = cmd
if (n_params(0) ge 6) then cond2 = cond

end
