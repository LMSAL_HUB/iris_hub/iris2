function ssw_jsoc_ds2struct_dbase, ds, save=save,version=version
;
;+
;   Name: ssw_jsoc_ds2struct_dbase
;
;   Purpose: jsoc DS name  -> structure via lookup dbase; optionally /SAVE
;
;   Input Paramters:
;      ds - data series name(s)
;
;   Output:
;      function returns sswidl-struct(DS-Name)
;
;   Keyword Parameters:
;      save - if set -AND- priviledged, generate the data base
;      version - if set, include IDL V# in dbase name (/SAVE=generation only)
;
;   Calling Sequence:
;      IDL> struct=ssw_jsoc_ds2struct(dsname)
;      IDL> dummy=ssw_jsoc_ds2struct(/save) ; restricted
;
;   History:
;      19-mar-2012 - S.L.Freeland
;      
;
;
priv=get_user() eq 'freeland' ; sorry, just me for now...
save=keyword_set(save)
defds=['hmi.Mharp_720s_nrt',$
        'hmi.MHarp_720s', $  ; added 9-jul-2012
	'su_turmon.Mharpv7_720s',$
	'aia.lev1_nrt2',$
	'aia_test.lev1p5',$
	'aia.lev1',$
	'hmi.lev1_nrt',$
	'hmi.V_45s_nrt',$
	'hmi.lev1',$
	'hmi.V_45s',$
	'hmi_test.M_45s_nrt',$
	'aia.lev1_euv_12s',$
	'aia.lev1_uv_24s',$
	'aia.lev1_vis_1h']

sswtop=get_logenv('SSW')
lmsaltop='/archive/ssw' ; kludge for nodes
if file_exist(lmsaltop) then sswtop=lmsaltop

ontdat=get_logenv('$SSW_ONTOLOGY_DATA')
if not file_exist(ontdat) then begin 
   box_message,'deriving path from $SSW'
   ontdat=concat_dir(sswtop,'vobs/ontology/data')
endif

dbroot='jsoc_series_structures'
version=keyword_set(version)
vname=dbroot + '_'+str_replace(!version.release,'.','')

case 1 of
   save and version: dbname=vname 
   save: dbname=dbroot
   file_exist(concat_dir(ontdat,vname+'.geny')): dbname=vname
   else: dbname=dbroot
endcase



dbfile=concat_dir(ontdat,dbname+'.geny')

box_message,'dbfile>> ' + dbfile

retval=-1 ; as in life, assume abject failure
case 1 of 
   save and priv: begin ; generate dbaser
      for i=0,n_elements(defds)-1 do begin 
         box_message,defds(i)
         serstr=ssw_jsoc(/series_struct,ds=defds(i),/jsoc2)
         tname=id_esc(defds(i)) ; escape series name
         dsstruct=ssw_jsoc_keywords2struct(serstr) ; 
         retval=add_tag(retval,dsstruct,tname)
      endfor
      if not file_exist(dbfile) then savegenx,retval,file=dbfile
      restgenx,file=dbfile,dbold
      ;if str_diff(retval,dbold) then begin 
         box_message,' updating dbase
         savegenx,retval,file=dbfile,/over
      ;endif else box_message,'No changes, not updting dbase
   endcase
   save: begin 
      box_message,'You are not authorized to use /SAVE..., returning'
   endcase
   data_chk(ds,/string): begin
      restgenx,file=dbfile,dbase
      dsnames=strlowcase(id_unesc(tag_names(dbase)))
      dsin=strlowcase(ds)
      ss=where_arr(dsnames,dsin,sscnt)
      case sscnt of
         0: box_message,'No DS name match in dbase
         1: retval=dbase.(ss)
         else: begin
            for i=0,sscnt-1 do retval=add_tag(retval,dbase.(ss(i)),id_esc(dsnames(ss(i))))
         endcase
      endcase
   endcase
   else: begin 
      box_message,'You need to supply data series name(s)'
   endcase
endcase




return,retval
end
