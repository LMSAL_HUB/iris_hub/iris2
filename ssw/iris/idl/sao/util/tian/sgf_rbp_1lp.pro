;             NAME : sgf_rbp_1lp
; 
;          PURPOSE : Perform single Gaussian fit and derive RBp profile (a modified version of the RB asymmetry analysis originally developped by 
;                    De Pontieu et al. 2009, ApJ, 701, L1; see the definition in Section 2 of Tian et al. 2011, ApJ, 738, 18) for a line profile
;
; CALLING SEQUENCE : sgf_rbp_1lp,wvl,lp,ee
; 
;           INPUTS : lp - the line profile
;                    
;                    wvl - wavelength vector
;                    
;                    ee - error vector        
; 
;         KEYWORDS : wave0 - rest wavelength, use the SGF centroid if not set 
;         
;                    lineid - name of the line
;
;                    posi - position of the line profile plot
;                    
;                    xtitle - lable of x-cooridinate of the line profile plot and RB profile plot
;                    
;                    xtitleRB -  lable of y-cooridinate of the RB profile plot
;                    
;                    ytitle -  lable of y-cooridinate of the line profile plot
; 
;                    para_sgf -  output of SGF line parameters: peak intensity, Doppler shift, line width, background intensity
;          
;                    chisq_sgf - output of the chisq value for SGF
;                    
;                    para_rb - output of RBp parameters: relative intensity, velocity, and width
;                    
;                    rb_array - output of RBp profiles [15]
;                    
;                    r_array - relative red wing emission as a fuction of velocity [15]
;                    
;                    b_array - relative blue wing emission as a fuction of velocity [15]
;
;          HISTORY : Written by Hui Tian at CfA, April 4, 2013
;
;; **********************************************************************

pro sgf_rbp_1lp,wvl,lp,ee,$
    lineid=lineid,wave0=wave0,posi=posi,xtitlelp=xtitlelp,xtitleRB=xtitleRB,ytitle=ytitle,$
    para_sgf=para_sgf,chisq_sgf=chisq_sgf,rb_array=rb_array,r_array=r_array,b_array=b_array,para_rb=para_rb
    

!p.font=-1
loadct,0  & tvlct,255L,0L,0L,2L  & tvlct,0L,255L,0L,3L   & tvlct,0L,0L,255L,4L
window,1,xs=900,ys=700, retain = 2

if not(keyword_set(xtitlelp)) then xtitlelp = 'Wavelength (!N!6!sA!r!u!9 %!6 !n!N!3)'
if not(keyword_set(xtitleRB)) then xtitleRB = 'Velocity (km/s)'
if not(keyword_set(ytitle)) then ytitle =  'Counts';'Radiance (erg !Ns!E-1!Ncm!E-2!Nsr!E-1 !N!6!sA!r!u!9 %!6!n!U-1!N!3)'
if not(keyword_set(posi)) then posi=[0.07,0.07,0.60,0.95]
if not(keyword_set(lineid)) then lineid='iris'


dlambda=mean(deriv(wvl)) ;spectral pixel size
nw=n_elements(wvl)

if not(keyword_set(wave0)) then begin
  res = mpfitpeak(wvl,lp, a, nterms = 4, /double, /positive) 
  wave0= a[1] ;set the rest wavelength by assuming 0 shift of the line profile
endif


para_sgf=fltarr(4)

steps = findgen(15)*10. ; R-B Steps in km/s
rb_array=fltarr(15)
r_array=fltarr(15)
b_array=fltarr(15)
para_rb=fltarr(3)

;single Gaussian fit
x=wvl
y=lp
err_ave=ee>0.001<(y*0.9) ;make sure that the error value is larger than 0 and smaller than the data value
plot_io,x,y,title='SGF to '+lineid+' line profile',position=posi,xtitle=xtitlelp,ytitle=ytitle,psym=4,$;/noerase,$
      xrange=[min(x)-dlambda/2,max(x)+dlambda/2],xstyle=1,yrange=[min(y)*0.95>0.1,max(y)*1.5],ystyle=1,charsize=1.5
errplot,x,y-err_ave,y+err_ave,width=0.005,thick=1
bpp = where(y gt 0., bppc)  &  ee = err_ave
if bppc le 4 then begin ;do not do fit
  a=fltarr(4)-999. & chisq=-999. & i_rba2=-999. & v_rba2=-999. & w_rba2=-999. & rba2=fltarr(15)-999.
  goto,para_array
endif
fitsg = mpfitpeak(x[bpp],y[bpp], a, nterms = 4, /double, /positive)   ;fitsg=a[3]+a[0]*exp(-0.5*(x-a[1])^2./(a[2]^2))
chisq = (1./(bppc - 4)) * total(((y[bpp] - fitsg[bpp])/err_ave[bpp])^2)
oplot,x,spline(x[bpp],fitsg,x),color=3L
xyouts,x[1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'i:  '+strmid(strtrim(string(a[0]),2),0,6),charsize=1.5,color=3L
xyouts,x[1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.82),'v:  '+strmid(strtrim(string((a[1]-wave0)/wave0*3e5),2),0,4),charsize=1.5,color=3L
xyouts,x[1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.72),'w:  '+strmid(strtrim(string(a[2]/wave0*3e5*sqrt(2)),2),0,4),charsize=1.5,color=3L


;RBp profile and RBp parameters
ymax=max(smooth(interpol(y,nw*100,/spline),3),sub_m)  &  xvec=interpol(x,nw*100,/spline)
x_rel = interpol(x,nw*10,/spline)-xvec[sub_m]
rbstr = gen_rb_profile(x_rel/wave0*2.999e5, interpol(y,nw*10,/spline), steps, 20.)
rba2= (rbstr.red - rbstr.blue)/ymax  &  rba2=smooth(rba2,3)
i_rba2=min(interpol(rba2,n_elements(steps)*10,/spline),sub_m)  
v_rba2=(sub_m/10.+1)*10 ;use the minimum of the R-B profile to determine the velocity
sub_e=where(interpol(rba2,n_elements(steps)*10,/spline) le i_rba2/2.72,sub_n) 
if sub_e[0] eq -1 then w_rba2=-999. else w_rba2=(sub_e[sub_n-1]-sub_e[0])/2  ;exponential width
plot,steps+10,rba2,title='RBp profile',position=posi+[0.61,0,0.38,0],/noerase,$
    yrange=[-0.4,0.2],ystyle=1,xtitle=xtitleRB,ytitle='RB Asymmetry (percentage)',charsize=1.5,/nodata
;plot,steps+10,rba2,title='',position=posi+[0.6,0,0.38,0],/noerase,$
;    yrange=[-0.4,0.2],ystyle=1,xtitle=xtitleRB,ytitle='RB Asymmetry (percentage)',charsize=1.5,/nodata
oplot,steps+10,rba2,color=2L
xyouts,steps[1],(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'i:  '+strmid(strtrim(string(i_rba2),2),0,4),charsize=1.5,color=2L
xyouts,steps[1],(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.82),'v:  '+strmid(strtrim(string(v_rba2),2),0,4),charsize=1.5,color=2L
xyouts,steps[1],(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.72),'w:  '+strmid(strtrim(string(w_rba2),2),0,4),charsize=1.5,color=2L


para_array:
para_sgf[0]=a[0] ;peak intensity
para_sgf[1]=(a[1]-wave0)/wave0*3e5 ;Doppler shift
para_sgf[2]=a[2]/wave0*3e5*sqrt(2) ;exponential width
para_sgf[3]=a[3] ;background
chisq_sgf=chisq
para_rb[0]=i_rba2
para_rb[1]=v_rba2
para_rb[2]=w_rba2
rb_array=rba2
if chisq ne -999. then begin
r_array=rbstr.red/ymax
b_array=rbstr.blue/ymax
endif else begin
r_array=0
b_array=0
endelse


end
