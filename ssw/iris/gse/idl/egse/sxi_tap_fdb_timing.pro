pro sxi_tap_fdb_timing, sttim, entim, struct=struct, out=out
;
;
;
mnem = ['SXI_SHUT_EXP_DUR', 'SXI_DL_IMG_Q_LEN']
if (n_elements(info) eq 0) then begin
    sxi_cmd_hist, sttim, entim, cmd_out, cmd_hist
    info = sag_get_mnem(sttim, entim, mnem)
end
;
struct0 = {day: 0, time: 0L, $
	expdur: 0.0, del2dl: 0.0, dl_dur: 0.0}
;
out = ['SXI_TAP_FDB_TIMING  Ver 1.0  Program Run: ' + !stime, $
	'Times searched: ' + fmt_tim(sttim) + ' to ' + fmt_tim(entim), $
	' ', $
	'     Date / Time       ExpDur       DelSec2D/L     D/L Dur', $
	' ']
	;28-OCT-99  02:33:07   0.00100000      8.00000      56.0000

;
ss = where(strupcase(cmd_hist.tc) eq 'SXI_TAP_FDB', nss)
if (nss ne 0) then struct = replicate(struct0, nss)
for i=0,nss-1 do begin
    dattim = cmd_hist(ss(i))
    ;
    x = int2secarr(info.hk.daytime, dattim)
    ss2 = where( (x ge 0) and (x lt 120), nss2)
    v = info.hk.value(ss2,1)
    ss3 = where(v eq 1, nss3)   ;downlink started
    exp_dur = info.hk.value(ss2(0), 0) / 1000.
    trans_v = -999.
    dl_dur = -999.
    if (nss3 ne 0) then begin   ;found some transitions
        trans_v = ss3(0)*4.
        exp_dur = info.hk.value(ss2(0) + ss3(0), 0) / 1000.
	;
	ss4 = where(v(ss3(0):*) eq 0, nss4)
	dl_dur = ss4(0)*4.
        ;
        out0 = fmt_tim( dattim ) + string(exp_dur, trans_v, dl_dur)
    end else begin
        out0 = fmt_tim( dattim ) + string(exp_dur) + ' No sign of downlink started'
    end
    ;
    struct(i).day = dattim.day
    struct(i).time = dattim.time
    struct(i).expdur = exp_dur
    struct(i).del2dl = trans_v
    struct(i).dl_dur = dl_dur
    out = [out, out0]
end
;
prstr, out, /nomore
;
end