pro ssw_js2mp4, paths, prelim=prelim, update_index=update_index, no_update_index=no_update_index,orig=orig, $
   write_video=write_video
;
;+
;   Name: ssw_js2mp4
;
;   Purpose: javascript -> mp4 movies; optionally replace 'AnIS' w/mp4
;
;   Input Parameters:
;      paths - one or more paths, assumed already run 'special_movie[2].pro' but mp4 missing or failed
;
;   History:
;      circa 2014 - S.L.Freeland - post processing loop for OS/Archi without intrinsic mp4/QT capabilities
;       6-jan-2015 - S.L.Freeland - add /WRITE_VIDEO keyword & function - extends to non-Macs with +IDL 8.2 (write_video)

pchk=where(1-file_exist(paths),necnt)
if necnt gt 0 then begin 
   box_message,'One or more of your input PATHS does not exist, bailing...'
   return
endif

index=concat_dir(paths,'index.html')
update_index=1-keyword_set(no_update_index)
mpscratch=concat_dir(paths,'mp4_scratch')
mk_dir,mpscratch
temp=curdir()
write_video=keyword_set(write_video) ; new paradigm

for o=0,n_elements(paths)-1 do begin 
   js=file_search(paths[o],'*j.html')
   rootname=ssw_strsplit(ssw_strsplit(js,'/',/tail),'_j.html',/head)
   indexdat=rd_tfile(index[o])
   file_append,index[o]+'_backup',/new , indexdat; make backup
   nmov=n_elements(js)
   if nmov eq 0 then begin 
      box_message,'No JavaScript movies found, bailing..'
      return
   endif
   mpnames=str_replace(js,'_j.html','.mov')
   for j=0,nmov-1 do begin 
      jsdat=rd_tfile(js[j])
      ssg=where(strmatch(jsdat,'*urls*.gif*'),gcnt)
      if gcnt gt 0 then begin 
      gifn=strextract(jsdat[ssg],'"')
      gifs=concat_dir(paths[o],strextract(jsdat[ssg],'"'))
      if write_video then begin ; extension 6-jan-2016 to use IDL WRITE_VIDEO (via image2movie2.pro) 
         cd,paths[0]
         movie_dir=curdir()
         if n_elements(movie_name) eq 0 then movie_name=ssw_strsplit(movie_dir,'/',/tail)
         image2movie2,gifs,/verbatim,/mp4,/write_video,movie_dir=curdir(),movie_name=movie_name
   
      endif else begin ; else, heritage qt_tools (limited to Macs-only which have qt_tools)

         pngs=concat_dir(mpscratch[0],str_replace(gifn,'.gif','.png'))
         files=file_search(mpscratch,'')
         ssw_file_delete,files
         ng=n_elements(gifs)
         for g=0,ng-1 do begin 
            read_gif,gifs[g],img,rr,gg,bb
            write_png,pngs[g],img
         endfor
         cd,paths[o]
         box_message,'Working on: >> ' + mpnames[j]
         image2movie_qt_tools,mpscratch[0],ssw_strsplit(str_replace(mpnames[j],'.mov',''),'/',/tail)
      endelse ; end of 
      if file_exist(mpnames[j]) or file_exist(str_replace(mpnames[j],'.mov','.mp4')) then begin
         box_message,'.mov generated ok...'
         files=file_search(mpscratch,'')
         ssw_file_delete,files
         if update_index then begin 
            box_message,'update_index '+ rootname[j]
            ssanis=where(strpos(indexdat,rootname[j]+'_a.html') ne -1,acnt)
            ssmp4 =where(strpos(indexdat,rootname[j]+'_m.mp4')  ne -1,mp4cnt)
            case 1 of 
               acnt eq 1: begin
                  astring=strextract(indexdat[ssanis],'_a.html">Java(AniS)','</B>',/inc)
                  mpstring='.mov">MP4 ' + strtrim(file_size(mpnames[j],/auto,/string),2) + '</B>'
                  indexdat[ssanis]=str_replace(indexdat[ssanis],astring,mpstring)
               endcase
               mp4cnt eq 1: begin 
                  mp4=str_replace(mpnames[j],'.mov','.mp4')
                  if file_exist(mp4) and ~file_exist(mpnames[j]) then spawn,['cp',mp4,mpnames[j]],/noshell
                  astring=strextract(indexdat[ssmp4],'_m.mp4"','</B>',/inc)
                  mpstring='.mov">MP4 ' + strtrim(file_size(mpnames[j],/auto,/string),2) + '</B>'
                  indexdat[ssmp4]=str_replace(indexdat[ssmp4],astring,mpstring)
               endcase
               else: box_message,'No ANIS or mp4 found current index..'
            endcase
         endif
      endif else begin 
        box_message,'Problem with .mov generation'
        box_message,mpscratch
      endelse
      if get_logenv('check_mp4') ne '' then stop,'pwd,mpnames[j]
      endif else box_message,'No gifs?? ' + paths[o]
   endfor

   if update_index then begin 
      if get_logenv('check_index') ne '' then stop,'index,indexdat
      file_append,index[0],indexdat,/new
   endif

     


endfor

cd,temp

return
end



