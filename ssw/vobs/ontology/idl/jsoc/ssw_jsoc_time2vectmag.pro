pro ssw_jsoc_time2vectmag, t0, t1, index, data, $
   debug=debug, outdir=outdir, progress=progress, ds=ds, $
   field=field, azimuth=azimuth, inclination=inclination, $
   fillingfactor=fillingfactor, intensity=intensity, blos=blos
;
;+
;   Name: ssw_jsoc_time2vectmag
;
;   Purpose: user timerange -> jsoc vector magnetogram
;
;   Input Parameters:
;      t0,t1 - user time frame
;
;   Output Parameters:
;      index [,data] - metadata (structure vector)
;                      data - if present, implies jsoc->local copy 
;
;   Keyword Parameters:
;      outdir - parent output directory (ignored if only 3 params)
;      debug - if set, debug mode (will pause in ssw_jsoc calls...)
;      progress - if set, show file transfer progress (via sock_copy)
;      ds - optional data series name
;      /field,/azimuth,/inclination,/fillingfactor,/intensity,/blos -
;      Optional desired components (jsoc "segments") to include (default=all)
;
;   Restrictions:
;      This version is prototype to test jsoc/api - times are ignored and
;      only the most recent set of components is returned
;   
;   History:
;      7-Nov-2009 - S.L.Freeland - place holder; eventually the majority
;                   of this logic will be subsumed by the general purpose
;                   ssw_time2jsoc;
;     11-nov-2009 - S.L.Freeland - add component keywords/option
;-
;       

debug=keyword_set(debug)

if n_elements(ds) eq 0 then ds='su_yang.hmi_vector' ; default/proto

serstr=ssw_jsoc(/series_struct,ds=ds,  debug=debug)

idx=ssw_jsoc(/RS_LIST,serstr=serstr,DS=serstr.interval.firstrecord, debug=debug)

nseg=n_elements(serstr.segments)
index=replicate(idx,nseg)

ftype=gt_tagval(serstr.segments,/name)
cunit=gt_tagval(serstr.segments,/units)

index=add_tag(index,ftype,'FTYPE')
index=rep_tag_value(index,cunit,'cunit1')

comps=str2arr('field,azimuth,inclination,fillingfactor,intensity,blos')
nc=n_elements(comps)
needss=intarr(nc)-1
for c=0,n_elements(comps)-1 do begin 
   estat=execute('kw=keyword_set('+comps(c)+')')
   needss(c)=needss(c) + (kw*(c+1))
endfor
ss=where(needss ne -1, ncnt)
if ncnt eq 0 then needss=indgen(nc) else begin
   needss = needss(ss) ; default=all 
   index=index(needss)
endelse
nout=n_elements(needss)

if n_params() gt 3 then begin  ; export request
   if n_elements(outdir) eq 0 then outdir=curdir()
   topurl='http://jsoc.stanford.edu'
   exp=ssw_jsoc(/export,serstr=serstr,ds=serstr.interval.lastrecord,$
       method='url_quick', debug=debug)
   files=(exp.data.filename)(needss)
   files=str_replace(files,'\','')
   urls=concat_dir(topurl,files)
   locfiles=concat_dir(outdir,files)
   break_url,urls,ip,paths,fnames
   outdirs=concat_dir(outdir,paths)
   for i=0,nout-1 do begin ; sock_copy OUT_DIR assumes scalar?
      mk_dir,outdirs(i)
      sock_copy,urls(i),out_dir=outdirs(i), progress=progress
   endfor
   if total(file_exist(locfiles)) eq nout then begin
       mreadfits,locfiles,dummy,data,outsize=outsize
       index=join_struct(dummy,temporary(index))
   endif else begin
      box_message,'not all files found...
   endelse
endif

return
end

