function ssw_service_watchdog, status, _extra=_extra, init=init, post_heartbeat=post_heartbeat, $
   test_fail=test_fail, message=message
;
;+
;   Name: ssw_service_watchdog
;
;   Purpose: check function of SSW services 
;
;   Input Paramters:
;      NONE
;
;   Output:
;      Function returns boolean: 1=>All Services OK , 0=>At least one failure (see STATUS output param) 
;
;   Output Parameters:
;      status - structure vect with detailed status {service_name:<name>,status:<status}, one per service
;
;   Keyword Parameters:
;      post_heartbeat - if set, POST an entry -> heartbeat cgi -> monitor
;
;   Calling Sequence:
;      IDL> ok=ssw_service_watchdog([STATUS] [,/POST])
;
;   History:
;     13-jan-2010 - S.L.Freeland
;
;-
; 

dbdir=concat_dir('$SSW_ONTOLOGY','data')

serdbase=concat_dir(concat_dir('$SSW_ONTOLOGY','data'), $
   'ssw_service_watchdog.geny')


init=keyword_set(init) or ~file_exist(serdbase)

if init then begin ; define expected response dbase
   serfile=concat_dir('$SSW','site/idl/ontology/ssw_service_watchdog.dat')
   scalls=rd_tfile(serfile)
   scalls=strarrcompress(scalls)
   nserv=n_elements(scalls)
   sstr={cgicall:'',response:'',nlines:0}
   dbase=replicate(sstr,nserv)
   dbase.cgicall=scalls
   for i=0,nserv-1 do begin 
      sock_list,scalls(i),resp
      resp=strlowcase(resp)
      dbase(i).nlines=n_elements(resp)
      ss0=where(strpos(resp,'<html>') ne -1,scnt)
      sse=where(strpos(resp,'</html>') ne -1,ecnt)
      dbase(i).response=arr2str(resp(ss0:sse),'')
   endfor
   savegenx,file=serdbase,dbase,/over
endif  

status={service:'',status:0}
restgenx,file=serdbase,dbase
ncall=n_elements(dbase)
status=replicate(status,ncall)
status.service=strextract(dbase.cgicall,'/cgi-ssw/','?')
ncall=n_elements(dbase)

for i=0, ncall-1 do begin 
   sock_list,dbase(i).cgicall,resp
   resp=strlowcase(resp)
   ss0=where(strpos(resp,'<html>') ne -1,scnt)
   sse=where(strpos(resp,'</html>') ne -1,ecnt)
   respx='' ; assume failure
   if scnt gt 0 and ecnt gt 0 then respx=arr2str(resp(ss0:sse),'') 
   status(i).status = (respx eq dbase(i).response) or $
      n_elements(resp) gt 100 
help,respx
endfor

postit=keyword_set(post_heartbeat)

retval=total(status.status) eq ncall 
if postit then begin 
   cgi='http://www.lmsal.com/cgi-ssw/ssw_service_hpkb_status.sh?'
   failed=where(1-status.status,fcnt)
   if keyword_set(test_fail) then begin
      failed=test_fail ; 
      fcnt=n_elements(test_fail)
   endif
   fmess=''
   if n_elements(message) eq 0 then umess='' else umess='['+message(0)+']<br>'
   if fcnt gt 0 then fmess='<font color=orange>'+ $
      arr2str(status(failed).service,'<br>')+'</font>'
   query=['PROCESS_ORIG=SSW Services - SF', $
          'STATUS='+(['Failed','Running'])(fcnt eq 0), $
          'SUBJECT=SSW Web Services Status', $
          'MESSAGE='+ umess + (['All OK',fmess])(fmess ne ''), $
          'USERID=freeland@lmsal.com', $
          'BACKUP=Slater', $
          'TIME_DATE='+anytim(reltime(/now),/ccsds)]
   qq=str_replace(url_encode(query),'%3D','=')
   sock_list,cgi+qq,out
endif
 

return,retval
end


