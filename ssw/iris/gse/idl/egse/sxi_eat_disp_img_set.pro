pro sxi_eat_disp_img_set, infil, bin_down
;+
;NAME:
;	SXI_EAT_DISP_IMG_SET
;PURPOSE:
;	Display a set of images in a mosaic.
;HISTORY:
;	Written 1-Oct-99 by M.Morrison
;-
;
mreadfits, infil, head, img, outsiz = [528/bin_down, 588/bin_down]
;
break_file, infil, dsk_log, dir, filnam, ext
xy_raster, filnam, img
;
end
