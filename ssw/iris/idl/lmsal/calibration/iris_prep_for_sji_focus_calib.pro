pro iris_prep_for_sji_focus_calib, files, ffout, outdir=outdir, verbose=verbose

;+
;   Name: iris_prep_for_sji_focus_calib
;
;   Purpose: Read a set of Level 0 or 1 SJI fits files and make cropped, corrected arrays
;            in new local fits files.
;
;   Input Parameters:
;      files - list of one or more IRIS fits files to make cropped, corrected arrays.
;
;   Output Parameters:
;      index - meta data / structure vector associated with IRISFDILES
;      data - optional DATA
;
;   Keyword Parameters:
;       outdir  - optional, determines the base directory for the data products.  The default base directory is: "./"
;       verbose - enable print statements
;
;   Method:
;
;   Restrictions:
;
;   History:
;      05-feb-2016 - T. Tarbell -  Prepared for solar soft by Phil Shirts (renamed/header, etc.)
;      05-feb-2016 - P. Shirts  - small mods to support inclusion in automated calls of 
;
;-
;

if keyword_set(verbose) then verbose=1 else verbose=0
if keyword_set(outdir) then outdir = outdir+'/' else outdir='./'
ffout = ''
waves = ['SJI_1330','SJI_1400','SJI_2796','SJI_2832','SJI_5000W']
wlabel = ['_1330','_1400','_2796','_2832','_5000W']

; Clean x just before slit at bottom & top of CCD
sxbot = [512,506,480,485,481]
sxtop = [508,500,485,490,485]

; Number of pixels (+1) to interpolate over to hide slit
nslit = 12

; Crop limits to get real image data
nxlow =  [  20,  20,  16,  16,  16]
nxhigh = [1019,1019,1015,1015,1015]
nylow =  [  20,  20,  20,  20,  20]
nyhigh = [1075,1075,1075,1075,1075]
nuvxlow = 983

; Final image sizes
nx = 1000
ny = 1056
nf = n_elements(files)
;stop, 'stopped in iris_prep_for_sji_focus_calib: see why read_iris call fails. it says it cannot files any files.'
read_iris,files,in,/silent,/nodata
wave = in.img_path
nout = 0

; Loop over the 5 SJI channels
for kk=0,4 do begin
  ww = where(wave eq waves(kk),count)
  if (count ge 1) then begin
    sxdelta = 1./float(ny)*(sxtop(kk)-sxbot(kk))
    nxlo = nxlow(kk)
    nxhi = nxhigh(kk)
    nylo = nylow(kk)
    nyhi = nyhigh(kk)
    read_iris,files(ww),in1,im1,/silent
    iris_prep,in1,im1,inp,imp,/nowarp
    for ii=0,count-1 do begin
      i = ww(ii)
;     Crop it to 1000 x 1056 array of good data
      imc = reform(float(imp(nxlo:nxhi,nylo:nyhi,ii)),nx,ny)
      if (kk ge 2) then for j=0,ny-1 do imc(nuvxlow+1:*,j) = imc(nuvxlow,j)
      inp(ii).naxis1 = nx
      inp(ii).naxis2 = ny
;     Get rid of black particles
;     Maybe try a linear 3:2:1 filter here instead of big medians; and average the vertical & horizontals                                               
;     Note this blows up if a particle is within 3 px of an edge
;     print, 'BEFORE tv, bytscl(sqrt(imc > 0.),0,400)'
;     removing -- don't see a need for this right now.         tv,bytscl(sqrt(imc > 0.),0,400)
;     print, 'AFTER tv, bytscl(sqrt(imc > 0.),0,400)'
      imc1 = imc
      imc2 = reverse(imc,1)
      imc3 = reverse(imc,2)
      imc4 = reverse(imc2,2)         
      wb = where(imc lt 0.5,countb)
      wwb = where(wb gt 2 or wb mod nx gt 2,countwwb)
      if (countwwb ge 1) then begin
        wb = wb(wwb)
        for j=0,countwwb-1 do $ 
           imc1(wb(j))  = median([imc1(wb(j)-3:wb(j)-1),imc1(wb(j)-(indgen(3)+1)*nx)])
        end
 ;      print,countb,countwwb
 ;      tv,bytscl(sqrt(imc1 > 0.),0,400)
        wb  = where(imc2 lt 0.5,countb)
        wwb = where(wb gt 2 or wb mod nx gt 2,countwwb)
        if (countwwb ge 1) then begin
          wb = wb(wwb)
          for j=0,countwwb-1 do $ 
             imc2(wb(j))  = median([imc2(wb(j)-3:wb(j)-1),imc2(wb(j)-(indgen(3)+1)*nx)])
        end
 ;      print,countb,countwwb
 ;      tv,bytscl(sqrt(imc2 > 0.),0,400)
        imc2 = reverse(imc2,1)
        wb = where(imc3 lt 0.5,countb)
        wwb = where(wb gt 2 or wb mod nx gt 2,countwwb)
        if (countwwb ge 1) then begin
          wb = wb(wwb)
          for j=0,countwwb-1 do $ 
            imc3(wb(j))  = median([imc3(wb(j)-3:wb(j)-1),imc3(wb(j)-(indgen(3)+1)*nx)])
        end
;       print,countb,countwwb
;       tv,bytscl(sqrt(imc3 > 0.),0,400)
        imc3 = reverse(imc3,2)
        wb = where(imc4 lt 0.5,countb)
        wwb = where(wb gt 2 or wb mod nx gt 2,countwwb)
        if (countwwb ge 1) then begin
          wb = wb(wwb)
          for j=0,countwwb-1 do $ 
            imc4(wb(j))  = median([imc4(wb(j)-3:wb(j)-1),imc4(wb(j)-(indgen(3)+1)*nx)])
        end
;       print,countb,countwwb
;       tv,bytscl(sqrt(imc3 > 0.),0,400)
        imc4 = reverse(reverse(imc4,2),1)
        imc = 0.25*(imc1+imc2+imc3+imc4)  
        imc = round(imc > 0.)

;       Interpolate across slit
        for jj=0,ny-1 do begin
          sxlo = round(sxbot(kk)+float(jj)*sxdelta)
          for j=1,nslit-1 do imc(sxlo+j,jj) = float(nslit-j)/nslit*imc(sxlo,jj)+float(j)/nslit*imc(sxlo+nslit,jj)
        endfor

;       Write out flat fits files
        hdr = struct2fitshead(inp(ii))
        sxaddpar,hdr,'BITPIX',32,before='NAXIS'
        break_file,files(i),dl,dd,fil,ext
        outf = outdir+fil+wlabel(kk)+ext
        writefits,outf,imc,hdr

        if (verbose eq 1) then begin
          print,'Saved ',waves(kk),nx,' x ',ny,' image in ',outf,form='(A,A,I6,A,I5,A,A)'
        endif

        nout = nout+1
        if (nout eq 1) then ffout = [outf] else ffout = [ffout,outf]
      endfor
    end
  endfor

  if (verbose eq 1) then begin
    print, nout,' SJI images processed'
  endif
  ;set_plot, oldDevice

  return
end
