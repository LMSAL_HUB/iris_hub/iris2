function iris_hcr2files,hcr,sji=sji,raster=raster,mp4=mp4,javascript=javascript,thumnail=thumnail,fits=fits, $
   wave=wave, urls=urls, context=context, aia=aia, hinode=hinode, sdo=sdo, count=count, debug=debug
;
;+ 
;   Name: iris_hcr2files
;
;   Purpose: return filenames associated with input HCR record; user defined types
;
;   Input Parameter:
;      hcr - one HCR record
;
;   Keyword Parameters:
;      fits - if set, return level2 FITS names ; default are graphics
;      sji -  return SJI files (graphics or /FITS)
;      raster - return RASTER files (graphics or L2 fits if /FITS set)
;      javascript - return javascript .html files
;      mp4 - return mp4 files
;      thumbnail=thumbnail - return thumnails
;      context - return context graphics
;      urls - if set, return urls, not file paths
;      wave - if set, sub-select on WAVE
;
;   History:
;      circa 1-feb-2017 - S.L.Freeland
;      21-apr-2017 - tighten up WAVE pattern a bit (don't match accidental obs/times  _1600, _1330,...)
;+
;

count=0 ; assume failure
if not required_tags(hcr,'obspath,full_obsid,url') then begin
   box_message,'requires an IRIS HCR record, bailing...'
   return,''
endif

hcrx=hcr[0] ; only one for now
if n_elements(hcr) gt 1 then box_message,'Warning: only applying to first hcr input'

debug=keyword_set(debug)
www=keyword_set(mp4) or keyword_set(thumbnail) or keyword_set(context) or keyword_set(javascript)
hinode=keyword_set(hinode)
aia=keyword_set(aia) or keyword_set(sdo)
hstring=(['','hinode/'])(hinode)
astring=(['','aia/'])(aia)
spath=hcrx.obspath+'/'+(['','www'])(www) + hstring + astring
files=findfile(spath)
spath=spath[0]
allfiles=concat_dir(spath,files)

sjis=str_replace(strtrim(str2arr(hcrx.umodes),2),'_fits','.fits')

fits=keyword_set(fits)
raster=keyword_set(raster)
sji=keyword_set(sji)
thumb=keyword_set(thumbnail)

retval=''
case 1 of 
   raster and fits: begin    
      ssr=where(strmatch(allfiles,'*raster*fits'))
      retval=allfiles[ssr]
   endcase
   aia or hinode: begin 
      sso=where(strmatch(allfiles,'*.fits'),ocnt)
      if ocnt gt 0 then retval=allfiles[sso] 
      if debug then stop,'allfiles,ocnt
   endcase
   keyword_set(sji) and fits: retval=sjis
   keyword_set(javascript): begin 
      ssjs=where(strmatch(allfiles,'*j.html'))
      retval=allfiles[ssjs]
   endcase
   keyword_set(context): begin 
      pat='*context_*'+(['full','thumb'])(thumb)+'.gif'
      ssc=where(strmatch(allfiles,pat))
      retval=allfiles[ssc]
   endcase
   else: begin 
      box_message,'returning SJI .fits
      retval=sjis
   endcase
endcase

if keyword_set(urls) then retval=concat_dir('http://www.lmsal.com/solarsoft/',retval)

if retval[0] ne ''  and n_elements(wave) ne 0 then begin 
   swave=strtrim(wave,2) 
   swave=strtrim((['_',''])(strpos(swave,'_') ne -1)+swave,2)
   if debug then stop,'swave'
   ss=where(strpos(retval,swave+'.')  ne -1 or strpos(retval,swave+'_') ne -1 ,sscnt)
   if sscnt eq 0 then begin 
      box_message,'no matches for you input wave'
      retval=''
   endif else retval=retval[ss]
endif

count=n_elements(retval) * (retval[0] ne '')

return,retval
end

