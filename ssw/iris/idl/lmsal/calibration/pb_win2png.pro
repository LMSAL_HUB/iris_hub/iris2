pro PB_WIN2PNG, filename, $
	_extra = extra

;+
;
; Takes the contents of an IDL plot window and writes them to a .JPG file
;
;-

if N_ELEMENTS(filename) eq 0 then filename = 'idl.png'

case !d.name of
	'Z'	:	begin
		image = TVRD()
		TVLCT, r, g, b, /get
		WRITE_PNG, filename, image, r, g, b, _extra = extra
	end
	'X'	:	begin
		image = TVRD(/true)
		WRITE_PNG, filename, image, _extra = extra
	end
	else:	begin
		PRINT, 'PB_WIN2PNG : Only works on X or Z plot device!'
	end
endcase

end