;+
;	15-Oct-2012	DSS
;
;	Set GSE_PKT_FILE_DIR to point to desired archive on the raid disks
;
;	Inputs:
;		ws	int or string, the source workstation number; currently supports 2, 3, 5, 6, 7, 8, 16, 23, or 'local'
;
;	Outputs:
;		none
;
pro iri_data_path, ws
;-
if not keyword_set(ws) then ws=''
if not keyword_set(ws) then read, ws, prompt='Enter the workstation number that originated the data: '
ws=strcompress(ws,/remove)
b1='/net/iris-raid1.lmms.lmco.com/c/itos'
b2='/net/iris-raid2.lmms.lmco.com/c/itos'

case ws of
	'2': loc=b2+'2'
	'3': loc=b2+'3'
	'5': loc=b2+'5'
	'6': loc=b2+'6'
	'7': loc=b1+'7'
	'8': loc=b1+'8'
	'16': loc=b1+'16'
	'23': loc=b1+'23'
	'local': loc=''
	else: begin
		print, 'Unknown workstation provided: '+strcompress(ws,/rem)
		print, 'No settings applied.'
		return
	end
endcase

setenv,'GSE_PKT_FILE_DIR='+loc+'/sswpkts/'
print, 'GSE_PKT_FILE_DIR='+getenv('GSE_PKT_FILE_DIR')

end
