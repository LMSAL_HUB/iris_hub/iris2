@./iris_flux_radcal

function get_info_irisl2, file_name, onlyfactor=onlyfactor, factor_and_data=factor_and_data, mgii_only=mgii_only, dir=dir, nosave=nosave
       
       dir_sav = './'
       if keyword_set(dir) then dir_sav = dir

       rastfile  = file_name
       d = iris_load(rastfile)
       iwindx = d->getwindx(2796.20)
       ;if iwindx ne -1 then begin
       lambda_file =  d->getlam(iwindx)
       read_iris_l2, rastfile, index, data, wave = 'Mg II k 2796'
       index = index[0]
       ;iresp = iris_get_response(index.date_obs, version='003')
       factor_radcal_lambda = 1
       factor_radcal_lambda_hz = 1
       dim_lambda = size(lambda)
       date =  anytim2cal(index.date_obs , form=1)
       ;print, j_cuan, date, index.xcen, index.ycen,  cos((heliocentric(conv_a2h([index.xcen, index.ycen], date), date))*!dpi/180.)
       pos_xy = [index.xcen, index.ycen]
       lonlat = conv_a2h([index.xcen, index.ycen], date)
       mu = cos((heliocentric(lonlat, date))*!dpi/180.)
       factor = iris_flux_radcal(t0, t1, local=file_name, units=2, onlyfactor=onlyfactor, factor_and_data=factor_and_data, $
                mgii_only=mgii_only)
     
       pos_name = strpos(file_name, 'iris_l2_')
       name_sav = dir_sav+'/info_'+strrep(strmid(file_name, pos_name, pos_name+100), '.fits', '.sav')
       if keyword_set(factor_and_data) then name_sav = dir_sav+'/data_info_'+strrep(strmid(file_name, pos_name, pos_name+100), '.fits', '.sav')

       spcscl = index.cdelt1
       spxscl = index.cdelt2
       spyscl = index.cdelt3


       
       DATE_OBS = index.DATE_OBS
       DATE_END = index.DATE_END
       POS_X  = index.CRVAL3
       POS_Y  = index.CRVAL2
       STEPT_AV = index.STEPT_AV

       pos_ref = strpos(file_name, 'iris_l2')
       date_in_filename = strmid(file_name, pos_ref+8,15)
       iris_obs_code    = strmid(file_name, pos_ref+24,10)
       raster_info      = strmid(file_name, pos_ref+48,5)

       dim_data = size(data, /dim)    
       extent_arcsec_arcsec = [0, dim_data[1]*SPXSCL, 0, dim_data[0]*SPYSCL]
       extent_px_px = [0, dim_data[1]*1., 0, dim_data[0]*1.]
       extent_px_arcsec  = [0, dim_data[1]*1., 0, dim_data[0]*SPYSCL]
       extent_time_arcsec = [0, dim_data[1]*STEPT_AV, 0, dim_data[0]*SPYSCL]
       extent_time_px     = [0, dim_data[1]*STEPT_AV, 0, dim_data[0]]

       min_extent = fltarr(5)
       list_extent = [[extent_arcsec_arcsec], [extent_px_px],$
                      [extent_px_arcsec],[extent_time_arcsec], [extent_time_px]]
       list_extent_coords = list(['X [arcsec]','Y [arcsec]'], ['X [px]','Y [px]'], $
                             ['X [px]','Y [arcsec]'],['time [s]','Y [arcsec]'], $
                         ['time [s]','Y [px]'])

       for j =0, 4 do min_extent[j] = abs(list_extent[3,j]- list_extent[1,j])		  
       aux_min = min(min_extent, where_min)
       extent_opt = list_extent[where_min]
       extent_opt_coords = list_extent_coords[where_min]

       info = {file_name:file_name, index:index, lambda_file:lambda_file, date:date, pos_xy:pos_xy, $
		lonlat:lonlat,mu:mu, spcscl:spcscl, spxscl:spxscl, spyscl:spyscl, $
	        date_in_filename:date_in_filename, $
		iris_obs_code:iris_obs_code,raster_info:raster_info, $
		extent_arcsec_arcsec:extent_arcsec_arcsec,$
		extent_px_px:extent_px_px, extent_px_arcsec:extent_px_arcsec, $
		extent_time_arcsec:extent_time_arcsec,extent_time_px:extent_time_px, $
		extent_opt:extent_opt,extent_opt_coords:extent_opt_coords, $
		list_extent_coords:list_extent_coords,factor:factor} 
       ; save, file_name, index, factor, lambda_file, date, pos_xy, lonlat, mu, spcscl, spxscl, spyscl, $
	;    date_in_filename, iris_obs_code, raster_info, $
	;   extent_arcsec_arcsec, extent_px_px, extent_px_arcsec, extent_time_arcsec, extent_time_px, $
	;   extent_opt, extent_opt_coords, list_extent_coords, file = name_sav 
 
       if keyword_set(nosave) eq 0 then save, info, file=name_sav 
     
       return, info
       stop
      
end
