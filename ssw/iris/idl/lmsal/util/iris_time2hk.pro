function iris_time2hk, t0, t1, quiet=quiet, force_remote = force_remote, $
	temperatures=temperatures, magnetometer=magnetometer, description=descriptions, $
	position=position, pointing=pointing, temps=temps, append=append, refresh=refresh
	
;;
;   Name: iris_time2hk
;
;   Purpose: time -> Paul Boerners www web summaries
;
;   Input Parameters"
;   t0 - time(s) or start time of range - may be iris 'index' structure vector
;   t1 - end time of range
;
;   Output:
;      function returns desired hk structures - all withing time range -or- matching times t0
;      if t0 is structure vector and /APPEND set, return is 'index' with matching hk appended
;
;   Keyword Parameters:
;      temps (switch) - if set , return temperatures
;      pointing (switch) - if set , return pointing/quats (including IRIS positions)
;      magnetometer (switch) - same as /pointing
;      position (switch) - subset of mag , time+x/y/z only
;      only_tags - optional subset of tags (faster, assumes user knows explicit tag name)
;      append (swith) - if set -and- t0 is structure vector ("index"), then append houskeeping 
;      refresh (switch) - initialize cache (e.g. common block)
;      force_remote (switch) -	if set, then always use http files. If not, tries to read the data from NFS first
;
;   Calling Examples:
;      IDL> temps=iris_time2hk('15-aug-2013 12:00','17-aug-2013 06:00',/temps) ; temperatures for time range
;      IDL> mag=iris_time2hk('15-aug-2013 12:00','17-aug-2013 06:00',/mag) ; magnetometer (& IRIS position) for time range
;      IDL> addhk=iris_time2hk(index,/append,/temps) ; input iris index, output includes appended housekeeping params
;
;   History:
;      21-aug-2013 - S.L.Freeland - generalization of iris_time2magnetometer
;       4-sep-2013 - S.L.Freeland - create_struct -> create_struct_temp (i/o thread safe version for parallel execution)
;      13-sep-2013 - S.L.Freeland - add cache of most recent url/contents (significant speedup for consecutive 'index' in loops)
;      19-dec-2013 - P. Boerner - add /pointing and /position options, shift URL to irisa
;      23-mar-2015 - P. Boerner - check for local files first
;      30-jan-2017 - R. Timmons - for temperature use, check for
;                    /irisa/data/prep/aux/temp_interpolated.  This
;                    folder has gaps from housekeeping dropouts filled
;                    with adjacent orbits (first suggested by Steve
;                    Saar).  Only tested locally.  
;      31-mar-2017 - J-P. Wuelser - made message for above conditional on loud
;
;   Restrictions:
;      assumes row 1 of .txt files are the tag/param names
;      default data types are double Except tag/param names with DATE or TIME - those are string
;-

common iris_time2hk_blk,last_url0, last_list0, last_template0

loud=1-keyword_set(quiet)

refresh=keyword_set(refresh) or n_elements(last_url0) eq 0

if refresh then begin ; reset cache
   last_url0=''
   delvarx,last_list0
   delvarx,last_template0
endif

case n_params() of
   0: begin 
         t0=reltime(days=-1,/day_only)
         t1=reltime(days=1,/day_only)
   endcase
   1: begin 
         if data_chk(t0,/struct) then begin ; optional 'index' input logic
            itimes=gt_tagval(t0,/date_obs,missing=gt_tagval(t0,/t_obs,missing=''))
            if itimes[0] eq '' then begin 
               box_message,'Unrecognized structure input, bailing....' ; Unstructure Exit on unrecognized structure
               return,-1
            endif
            time_window,itimes,t0x,t1,days=[-1,0] 
         endif else t1=reltime(t0,/days,/day_only)
   endcase
   else: ; user supplied t0/t1
endcase

if n_elements(t0x) eq 0 then t0x=reltime(t0,days=-1) ; lookback one day 

hktop='/irisa/data/prep/aux/'
remote=1-file_exist(hktop) or keyword_set(force_remote)
hktop=([hktop,'http://www.lmsal.com/solarsoft/irisa/data/prep/aux/'])(remote) 

if loud then box_message, 'USING NEW INTERPOLATED TEMPERATURE DIR'

case 1 of 
   keyword_set(position): subdir='pointing'
   keyword_set(pointing): subdir='pointing'
   keyword_set(magnetometer): subdir='pointing'
   keyword_set(temperatures) or keyword_set(temps): subdir='temp_interpolated'
   else: subdir='temp' ; default
endcase

parent=hktop+subdir
urls=ssw_time2filelist(t0x,t1,parent=parent,/pflat,ext='.txt',/quiet)

if urls[0] eq '' then begin
   box_message,'No urls within your start range..
   return,-1
endif


if urls[0] eq last_url0 then begin
   ulist=last_list0
   template=last_template0
   tags=tag_names(template) 
endif else begin ; new url, refresh cache 
   if loud then box_message,urls[0]
   if remote then begin
	   sock_list,urls[0],ulist
   endif else begin
   		ulist = RD_TFILE(urls[0])
   endelse
   last_url0=urls[0]
   last_list0=ulist
   ; create template struture 
   tags=str2arr(strtrim(strcompress(ulist[0]),2),' ')
   ttypes=replicate('D',n_elements(tags)) ; defaut=double
   dd=where(strpos(tags,'DATE') ne -1 or strpos(tags,'TIME') ne -1)
   ttypes[dd]='A'
   create_struct_temp,template,'',tags,ttypes
   last_template0=template
endelse


for u=1,n_elements(urls)-1 do begin 
   if loud then box_message,urls[u]
   if remote then begin
	   sock_list,urls[u],ulistu
   endif else begin
		ulistu = RD_TFILE(urls[u])
   endelse
   ulist=[temporary(ulist),ulistu]
endfor

ulist=ulist[where(strpos(ulist,tags[0]) eq -1)]
retval=replicate(template,n_elements(ulist))

cols=str2cols(ulist,' ',/trim)
for t=0,n_tags(retval)-1 do begin 
   retval.(t)=reform(cols[t,*])
endfor

if n_elements(itimes) gt 0 then begin 
   sst=tim2dset(anytim(retval.date_obs,/int),anytim(itimes,/int),delta=dts)
   retval=retval[sst]
   if keyword_set(append) then retval=join_struct(t0,retval) 
endif else begin 
   sst=where(anytim(retval.date_obs) ge anytim(t0) and anytim(retval.date_obs) le anytim(t1),sscnt)
   if sscnt eq 0 then begin 
      box_message,'No records in your time range
      retval=-1
   endif else begin 
     retval=retval[sst]
   endelse
endelse

return,retval

end



