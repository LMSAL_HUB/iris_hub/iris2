;===============================================================================
;===============================================================================
;===============================================================================
;
; README
;
; This file contains a ton of subroutines. The main, user-level routine is
; named "iris_fd_compare_files", and it is the very last routine in this file.
; To jump to the header documentation for "iris_fd_compare_files", search on
; the phrase "foobar".
;
;===============================================================================
;===============================================================================
;===============================================================================

FUNCTION iris_fd_sad, minuend, subtrahend

;============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sad - Smallest Angle Difference
;
;
; CATEGORY:
;
;   Utility
;
;
; PURPOSE / DESCRIPTION
; 
;   Return difference of two angles with result in the range (-180,180].
;
;
; CALLING SEQUENCE:
;
;   iris_fd_sad, minuend, subtrahend
;
;
; INPUTS:
;
;   MINUEND - [Mandatory] (number scalar)
;             The angle from which the subtrahend will be subtracted. 
;             May have any positive or negative value. Units = degrees.
;
;   SUBTRAHEND - [Mandatory] (number scalar)
;                The angle which will be subtracted from the minuend. 
;                May have any positive or negative value. Units = degrees.
;
; OUTPUTS:
;
;   Returned - (double-float scalar)
;              The function returns the angle difference of the inputs,
;              such that:
;              IRIS_FD_SAD(MINUEND,SUBTRAHEND) = MINUEND - SUBTRAHEND
;              The useful functionality is that the returned value is
;              rotated into the range (-180,180].
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
;
;
;-
;============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_SAD'

  ;; Check that the correct number of parameters are supplied or stop.
  if (n_params() NE 2) then begin
    print, prognam+': Incorrect number of parameters. Stopping.'
    stop
  endif

  ;; Check that inputs are scalars.
  if ((n_elements(minuend) NE 1) OR (n_elements(subtrahend) NE 1)) then begin
    print, prognam+': Inputs must be scalars. Stopping.'
    stop
  endif

  ;; Check that inputs are numbers.
  number_types = [1L,2,3,4,5,12,13,14,15]
  dtype = size(minuend, /type)
  ss = where(number_types EQ dtype, count)
  if (count NE 1) then begin
    print, prognam+': MINUEND must be a number. Stopping.'
    stop
  endif
  dtype = size(subtrahend, /type)
  ss = where(number_types EQ dtype, count)
  if (count NE 1) then begin
    print, prognam+': SUBTRAHEND must be a number. Stopping.'
    stop
  endif


;; =============================================
;; Main section

  diff = double(minuend - subtrahend) + 180d

  while (diff LT 0d) do diff += 360d

  diff = (diff mod 360d) - 180d


;; =============================================
;; Finish.

  return, diff

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_data2device, $
      x_in  =x_data,     $
      y_in  =y_data,     $
      x_out =x_dev,      $
      y_out =y_dev

;============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_data2device 
;
;
; CATEGORY:
;
;   Utility
;
;
; PURPOSE / DESCRIPTION
;
;   Convert data coordinates to device coordinates.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_data2device, [x_in=x_data, x_out=x_dev], [y_in=y_data, y_out=y_dev]
;
;
; INPUTS:
;
;   X_IN     - [Optional] (number scalar)
;              An x-value in the data coordinate system to be converted to an
;              x-value in the device coordinate system (X_OUT).
;
;   Y_IN     - [Optional] (number scalar)
;              A y-value in the data coordinate system to be converted to a
;              y-value in the device coordinate system (Y_OUT).
;
; OUTPUTS:
;
;   X_OUT    - [Optional] (number scalar)
;              An x-value in the device coordinate system that corresponds to
;              X_IN in the data coordinate system.
;
;   Y_OUT    - [Optional] (number scalar)
;              A y-value in the device coordinate system that corresponds to
;              Y_IN in the data coordinate system.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-10' ;--- (M.Weber) Written.
;
;
;-
;============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_DATA2DEVICE'


;; =============================================
;; Main.

  if (arg_present(x_data) EQ 1) $
    then x_dev = !D.X_SIZE * (!X.S[0] + (!X.S[1]*float(x_data))) $
    else x_dev = 0

  if (arg_present(y_data) EQ 1) $
    then y_dev = !D.Y_SIZE * (!Y.S[0] + (!Y.S[1]*float(y_data))) $
    else y_dev = 0


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

FUNCTION iris_fd_make_idstring, $
           fdf_file,            $
           fdt_file


;============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_idstring - Make ID String
;
;
; CATEGORY:
;
;   Utility
;
;
; PURPOSE / DESCRIPTION
; 
;   Return string of the form 'type_FDFyyyymmdd_FDTyyyymmdd'.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_make_idstring, fdf_file, fdt_file
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file produced by the MOC. May be a pathname.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file produced by the LM Flight Dynamics Team. 
;              May be a pathname.
;
; OUTPUTS:
;
;   Returned - (string scalar)
;              The function returns a string of the form:
;              'type_FDFyyyymmdd_FDTyyyymmdd'
;              where <type> is one of the file types (e.g., aaz, eclipse, etc.),
;              and the FDF and FDT datecodes are extracted from the 
;              respective filenames.
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-06' ;--- (M.Weber) Written.
;
;
;-
;============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MAKE_IDSTRING'


;; =============================================
;; Main.

  fdf_pos1 = strpos(fdf_file, 'FDF_IRIS', /reverse_search)+9
  fdf_pos2 = strpos(fdf_file, '_', fdf_pos1)
  output = strmid(fdf_file, fdf_pos1, fdf_pos2-fdf_pos1) + '_FDF' + $
           strmid(fdf_file, fdf_pos2+1, 8)
  if (strmid(output,0,4) EQ 'view') then begin
    fdf_pos2 = strpos(fdf_file, '_', fdf_pos2+1)
    output = strmid(fdf_file, fdf_pos1, fdf_pos2-fdf_pos1) + '_FDF' + $
             strmid(fdf_file, fdf_pos2+1, 8)
  endif
  fdt_pos1 = strpos(fdt_file, 'IRIS', /reverse_search)+5
  fdt_pos1 = strpos(fdt_file, '_', fdt_pos1+1)
  substr = '_FDT' + strmid(fdt_file, fdt_pos1+1, 8) 
  if ((strmid(substr,4,4) EQ '10de') OR (strmid(substr,4,4) EQ '5deg')) then begin
    fdt_pos1 = strpos(fdt_file, '_', fdt_pos1+1)
    substr = '_FDT' + strmid(fdt_file, fdt_pos1+1, 8)
  endif
  output = output + substr


;; =============================================
;; Finish.

  return, output

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_aaz,        $
      fdf_file,               $
      fdt_file,               $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      fdf_parsed =fdf_parsed, $
      fdt_parsed =fdt_parsed

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_aaz
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of AAZ event lists to determine unique matches.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'aaz', are read in as IDL 
;   structure arrays. Certain fields are compared for "close enough" matching
;   to assign unique pairs, stored in MATCHES. For matched pairs, the 
;   differences ("deltas") on the data fields are calculated and stored. 
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct 
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_match_aaz, fdf_file, fdt_file, matches, fdf_unmatched, $
;     fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "aaz", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "aaz", produced by the LM Flight 
;              Dynamics Team.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "AAZ_MATCH_TYPE", array of 
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "AAZ_MATCH_TYPE", array of 
;              size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for 
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "AAZ_MATCH_TYPE", array of 
;              size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for 
;              more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", array of 
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented. 
;              The data has been "parsed" from the ASCII file into the IDL 
;              environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a 
;              line) from FDT_FILE, and all events in the file are represented. 
;              The data has been "parsed" from the ASCII file into the IDL 
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-28' ;--- (M.Weber) Written.
progver = 'v2013-Mar-29' ;--- (M.Weber) Had neglected to update FDT_INDICES
;                             when no more FDT entries after multiple candidates.
progver = 'v2013-Mar-30' ;--- (M.Weber) Fixed bug when FDTs run out before FDFs.
progver = 'v2013-Apr-11' ;--- (M.Weber) Fixed bug with LAST_FDF.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-May-06' ;--- (M.Weber) Modified tolerances according to BdP's
;                             feedback.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MATCH_AAZ'

  ;; Set tolerances on the fields.
  tol_direction  =  0b   ;; must match exactly ('Decreasing' or 'Increasing')
  tol_time       = 30L   ;; [sec]
  tol_angle      =  0.1d ;; [deg]

  match_template = {aaz_match_type,                  $
                    file_type       :         'aaz', $
                    fdf_filename    :      fdf_file, $
                    fdt_filename    :      fdt_file, $
                    fdf_index       :           -1L, $
                    fdt_index       :           -1L, $
                    fdf_direction   :            '', $
                    fdt_direction   :            '', $
                    delta_direction :            0b, $
                    tol_direction   : tol_direction, $
                    fdf_time        :            '', $
                    fdt_time        :            '', $
                    delta_time      :            0L, $
                    tol_time        :      tol_time, $
                    fdf_angle       :            0d, $
                    fdt_angle       :            0d, $
                    delta_angle     :            0d, $
                    tol_angle       :     tol_angle   }


;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    matches = -1L
    fdf_unmatched = -1L
    fdt_unmatched = -1L
    return
  endif


;; =============================================
;; Main loop to find matches.

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_parsed)
  fdt_indices = lindgen(n_elements(fdt_parsed))

  ;; Prepare simple time arrays to work with.
  fdf_tai = anytim2tai(fdf_parsed.aaz_time)
  fdt_tai = anytim2tai(fdt_parsed.aaz_time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  matches = match_template
  fdf_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    last_fdf = i_fdf

    ss = where( (abs(fdt_tai-fdf_tai[i_fdf]) LE tol_time) AND $
                (fdt_parsed.aaz_direction EQ fdf_parsed[i_fdf].aaz_direction), $
                count)

    case count of

      0: begin
        unmatched_line = match_template
        unmatched_line.fdf_index = i_fdf
        unmatched_line.fdf_direction = fdf_parsed[i_fdf].aaz_direction
        unmatched_line.fdf_time = fdf_parsed[i_fdf].aaz_time
        unmatched_line.fdf_angle = fdf_parsed[i_fdf].aaz_angle
        fdf_unmatched = [fdf_unmatched, unmatched_line]
      end

      1: begin
        ss = ss[0]
        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_direction = fdf_parsed[i_fdf].aaz_direction
        matched_line.fdt_direction = fdt_parsed[ss].aaz_direction
        matched_line.delta_direction = matched_line.fdt_direction NE $
                                       matched_line.fdf_direction
        matched_line.fdf_time = fdf_parsed[i_fdf].aaz_time
        matched_line.fdt_time = fdt_parsed[ss].aaz_time
        matched_line.delta_time = fdt_tai[ss] - fdf_tai[i_fdf]
        matched_line.fdf_angle = fdf_parsed[i_fdf].aaz_angle
        matched_line.fdt_angle = fdt_parsed[ss].aaz_angle
        matched_line.delta_angle = matched_line.fdt_angle - $
                                   matched_line.fdf_angle
        matches = [matches, matched_line]

        if (n_elements(fdt_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_tai,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_tai = fdt_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

      else: begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdt_tai[ss] EQ min(fdt_tai[ss])))[0]
        ss = ss[ss_earliest]

        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_direction = fdf_parsed[i_fdf].aaz_direction
        matched_line.fdt_direction = fdt_parsed[ss].aaz_direction
        matched_line.delta_direction = matched_line.fdt_direction EQ $
                                       matched_line.fdf_direction
        matched_line.fdf_time = fdf_parsed[i_fdf].aaz_time
        matched_line.fdt_time = fdt_parsed[ss].aaz_time
        matched_line.delta_time = fdt_tai[ss] - fdf_tai[i_fdf]
        matched_line.fdf_angle = fdf_parsed[i_fdf].aaz_angle
        matched_line.fdt_angle = fdt_parsed[ss].aaz_angle
        matched_line.delta_angle = matched_line.fdt_angle - $
                                   matched_line.fdf_angle
        matches = [matches, matched_line]

        if (n_elements(fdt_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_tai,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_tai = fdt_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

    endcase

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF lines.

  if (last_fdf LT N_fdf-1) then begin
    for i_fdf = (last_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index = fdf_indices[i_fdf]
      unmatched_line.fdf_direction = fdf_parsed[i_fdf].aaz_direction
      unmatched_line.fdf_time = fdf_parsed[i_fdf].aaz_time
      unmatched_line.fdf_angle = fdf_parsed[i_fdf].aaz_angle
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif

  ;; Do clean-up on the seed entry for fdf_unmatched.
  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse


;; =============================================
;; Deal with the leftover unmatched FDT lines.

  fdt_unmatched = match_template

  if (fdt_tai[0] NE -1) then begin
    N_fdt = n_elements(fdt_tai)
    for i_fdt = 0L,(N_fdt-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index = fdt_indices[i_fdt]
      unmatched_line.fdt_direction = fdt_parsed[i_fdt].aaz_direction
      unmatched_line.fdt_time = fdt_parsed[i_fdt].aaz_time
      unmatched_line.fdt_angle = fdt_parsed[i_fdt].aaz_angle
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor

    ;; And do clean-up on the seed entry.
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; ...And clean up on MATCHES.

  if (n_elements(matches) GE 2) then begin
    matches = matches[1:*]
  endif else begin
    matches = -1
  endelse


;; =============================================
;; Finish.

  ;; We used up FDT_STC already, so re-get it if requested by keyword.
  if arg_present(fdt_parsed) then fdt_parsed = iris_fdf2struct(fdt_file)

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_eclipse,    $
      fdf_file,               $
      fdt_file,               $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      fdf_parsed =fdf_parsed, $
      fdt_parsed =fdt_parsed

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_eclipse
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of ECLIPSE event lists to determine unique matches.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'eclipse', are read in as 
;   IDL structure arrays. Certain fields are compared for "close enough" 
;   matching to assign unique pairs, stored in MATCHES. For matched pairs, the
;   differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_match_eclipse, fdf_file, fdt_file, matches, fdf_unmatched, $
;     fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "eclipse", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "eclipse", produced by the LM 
;              Flight Dynamics Team.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array 
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array 
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Mar-30' ;--- (M.Weber) Found some FDT_TAIs that had to be
;                             turned into FDT_ST_TAIs. Fixed bug when FDTs
;                             run out before FDFs.
progver = 'v2013-Apr-11' ;--- (M.Weber) Fixed bug with LAST_FDF.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-May-06' ;--- (M.Weber) Modified tolerances according to BdP's
;                             feedback.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MATCH_ECLIPSE'

  ;; Set tolerances on the fields.
  tol_st_time  = 60d   ;; [sec]
  tol_en_time  = 60d   ;; [sec]
  tol_duration = 90d   ;; [sec]
  tol_st_pass  =  0L   ;; [sec]
  tol_en_pass  =  0L   ;; [sec]
  tol_c_cond   =  0b   ;; must match exactly ('Penumbra' or 'Umbra')
  tol_w_cond   =  0b   ;; must match exactly ('Penumbra' or 'Umbra')
  tol_tot_dur  = 90d   ;; [sec]

  match_template = {eclipse_match_type,            $
                    file_type      :    'eclipse', $
                    fdf_filename   :     fdf_file, $
                    fdt_filename   :     fdt_file, $
                    fdf_index      :          -1L, $
                    fdt_index      :          -1L, $
                    fdf_st_time    :           '', $
                    fdt_st_time    :           '', $
                    delta_st_time  :           0d, $
                    tol_st_time    :  tol_st_time, $
                    fdf_en_time    :           '', $
                    fdt_en_time    :           '', $
                    delta_en_time  :           0d, $
                    tol_en_time    :  tol_en_time, $
                    fdf_duration   :           0d, $ 
                    fdt_duration   :           0d, $ 
                    delta_duration :           0d, $ 
                    tol_duration   : tol_duration, $
                    fdf_st_pass    :           0L, $ 
                    fdt_st_pass    :           0L, $ 
                    delta_st_pass  :           0L, $ 
                    tol_st_pass    :  tol_st_pass, $
                    fdf_en_pass    :           0L, $ 
                    fdt_en_pass    :           0L, $ 
                    delta_en_pass  :           0L, $ 
                    tol_en_pass    :  tol_en_pass, $
                    fdf_c_cond     :           '', $
                    fdt_c_cond     :           '', $
                    delta_c_cond   :           0b, $
                    tol_c_cond     :   tol_c_cond, $
                    fdf_w_cond     :           '', $
                    fdt_w_cond     :           '', $
                    delta_w_cond   :           0b, $
                    tol_w_cond     :   tol_w_cond, $
                    fdf_tot_dur    :           0d, $ 
                    fdt_tot_dur    :           0d, $ 
                    delta_tot_dur  :           0d, $ 
                    tol_tot_dur    :  tol_tot_dur   }


;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    matches = -1L
    fdf_unmatched = -1L
    fdt_unmatched = -1L
    return
  endif


;; =============================================
;; Main loop to find matches.

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_parsed)
  fdt_indices = lindgen(n_elements(fdt_parsed))

  ;; Prepare simple time arrays to work with.
  fdf_st_tai = anytim2tai(fdf_parsed.start_time)
  fdt_st_tai = anytim2tai(fdt_parsed.start_time)
  fdf_en_tai = anytim2tai(fdf_parsed.stop_time)
  fdt_en_tai = anytim2tai(fdt_parsed.stop_time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  matches = match_template
  fdf_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    last_fdf = i_fdf

    ss = where( (abs(fdt_st_tai-fdf_st_tai[i_fdf]) LE tol_st_time) AND $
                (abs(fdt_en_tai-fdf_en_tai[i_fdf]) LE tol_en_time), $
                count)

    case count of

      0: begin
        unmatched_line = match_template
        unmatched_line.fdf_index = i_fdf
        unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        unmatched_line.fdf_en_time = fdf_parsed[i_fdf].stop_time
        unmatched_line.fdf_duration = fdf_parsed[i_fdf].duration
        unmatched_line.fdf_st_pass = fdf_parsed[i_fdf].start_pass
        unmatched_line.fdf_en_pass = fdf_parsed[i_fdf].stop_pass
        unmatched_line.fdf_c_cond = fdf_parsed[i_fdf].current_condition
        unmatched_line.fdf_w_cond = fdf_parsed[i_fdf].worst_condition
        unmatched_line.fdf_tot_dur = fdf_parsed[i_fdf].total_duration
        fdf_unmatched = [fdf_unmatched, unmatched_line]
      end

      1: begin
        ss = ss[0]
        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].stop_time
        matched_line.fdt_en_time = fdt_parsed[ss].stop_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_duration = fdf_parsed[i_fdf].duration
        matched_line.fdt_duration = fdt_parsed[ss].duration
        matched_line.delta_duration = matched_line.fdt_duration - $
                                      matched_line.fdf_duration
        matched_line.fdf_st_pass = fdf_parsed[i_fdf].start_pass
        matched_line.fdt_st_pass = fdt_parsed[ss].start_pass
        matched_line.delta_st_pass = matched_line.fdt_st_pass - $
                                     matched_line.fdf_st_pass
        matched_line.fdf_en_pass = fdf_parsed[i_fdf].stop_pass
        matched_line.fdt_en_pass = fdt_parsed[ss].stop_pass
        matched_line.delta_en_pass = matched_line.fdt_en_pass - $
                                     matched_line.fdf_en_pass
        matched_line.fdf_c_cond = fdf_parsed[i_fdf].current_condition
        matched_line.fdt_c_cond = fdt_parsed[ss].current_condition
        matched_line.delta_c_cond = matched_line.fdt_c_cond NE $
                                    matched_line.fdf_c_cond
        matched_line.fdf_w_cond = fdf_parsed[i_fdf].worst_condition
        matched_line.fdt_w_cond = fdt_parsed[ss].worst_condition
        matched_line.delta_w_cond = matched_line.fdt_w_cond NE $
                                    matched_line.fdf_w_cond
        matched_line.fdf_tot_dur = fdf_parsed[i_fdf].total_duration
        matched_line.fdt_tot_dur = fdt_parsed[ss].total_duration
        matched_line.delta_tot_dur = matched_line.fdt_tot_dur - $
                                     matched_line.fdf_tot_dur
        matches = [matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

      else: begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdt_st_tai[ss] EQ min(fdt_st_tai[ss])))[0]
        ss = ss[ss_earliest]

        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].stop_time
        matched_line.fdt_en_time = fdt_parsed[ss].stop_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_duration = fdf_parsed[i_fdf].duration
        matched_line.fdt_duration = fdt_parsed[ss].duration
        matched_line.delta_duration = matched_line.fdt_duration - $
                                      matched_line.fdf_duration
        matched_line.fdf_st_pass = fdf_parsed[i_fdf].start_pass
        matched_line.fdt_st_pass = fdt_parsed[ss].start_pass
        matched_line.delta_st_pass = matched_line.fdt_st_pass - $
                                     matched_line.fdf_st_pass
        matched_line.fdf_en_pass = fdf_parsed[i_fdf].stop_pass
        matched_line.fdt_en_pass = fdt_parsed[ss].stop_pass
        matched_line.delta_en_pass = matched_line.fdt_en_pass - $
                                     matched_line.fdf_en_pass
        matched_line.fdf_c_cond = fdf_parsed[i_fdf].current_condition
        matched_line.fdt_c_cond = fdt_parsed[ss].current_condition
        matched_line.delta_c_cond = matched_line.fdt_c_cond - $
                                    matched_line.fdf_c_cond
        matched_line.fdf_w_cond = fdf_parsed[i_fdf].worst_condition
        matched_line.fdt_w_cond = fdt_parsed[ss].worst_condition
        matched_line.delta_w_cond = matched_line.fdt_w_cond - $
                                    matched_line.fdf_w_cond
        matched_line.fdf_tot_dur = fdf_parsed[i_fdf].total_duration
        matched_line.fdt_tot_dur = fdt_parsed[ss].total_duration
        matched_line.delta_tot_dur = matched_line.fdt_tot_dur - $
                                     matched_line.fdf_tot_dur
        matches = [matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

    endcase

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF lines.

  if (last_fdf LT N_fdf-1) then begin
    for i_fdf = (last_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index = i_fdf
      unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
      unmatched_line.fdf_en_time = fdf_parsed[i_fdf].stop_time
      unmatched_line.fdf_duration = fdf_parsed[i_fdf].duration
      unmatched_line.fdf_st_pass = fdf_parsed[i_fdf].start_pass
      unmatched_line.fdf_en_pass = fdf_parsed[i_fdf].stop_pass
      unmatched_line.fdf_c_cond = fdf_parsed[i_fdf].current_condition
      unmatched_line.fdf_w_cond = fdf_parsed[i_fdf].worst_condition
      unmatched_line.fdf_tot_dur = fdf_parsed[i_fdf].total_duration
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif

  ;; Do clean-up on the seed entry for fdf_unmatched.
  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse


;; =============================================
;; Deal with the leftover unmatched FDT lines.

  fdt_unmatched = match_template

  if (fdt_st_tai[0] NE -1) then begin
    N_fdt = n_elements(fdt_st_tai)
    for i_fdt = 0L,(N_fdt-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index = i_fdt
      unmatched_line.fdt_st_time = fdt_parsed[i_fdt].start_time
      unmatched_line.fdt_en_time = fdt_parsed[i_fdt].stop_time
      unmatched_line.fdt_duration = fdt_parsed[i_fdt].duration
      unmatched_line.fdt_st_pass = fdt_parsed[i_fdt].start_pass
      unmatched_line.fdt_en_pass = fdt_parsed[i_fdt].stop_pass
      unmatched_line.fdt_c_cond = fdt_parsed[i_fdt].current_condition
      unmatched_line.fdt_w_cond = fdt_parsed[i_fdt].worst_condition
      unmatched_line.fdt_tot_dur = fdt_parsed[i_fdt].total_duration
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor

    ;; And do clean-up on the seed entry.
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; ...And clean up on MATCHES.

  if (n_elements(matches) GE 2) then begin
    matches = matches[1:*]
  endif else begin
    matches = -1
  endelse


;; =============================================
;; Finish.

  ;; We used up FDT_STC already, so re-get it if requested by keyword.
  if arg_present(fdt_parsed) then fdt_parsed = iris_fdf2struct(fdt_file)

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_events,      $
      fdf_file,                $
      fdt_file,                $
      matches,                 $
      fdf_unmatched,           $
      fdt_unmatched,           $
      fdf_parsed  =fdf_parsed, $
      fdt_parsed  =fdt_parsed, $
      pass_offset =pass_offset

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_events
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of EVENTS event lists to determine unique matches.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'events', are read in as
;   IDL structure arrays. Certain fields are compared for "close enough"
;   matching to assign unique pairs, stored in MATCHES. For matched pairs, the
;   differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_match_events, fdf_file, fdt_file, matches, fdf_unmatched, $
;     fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "events", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "events", produced by the LM
;              Flight Dynamics Team.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array 
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array 
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;   2) For the angular fields (LON_ASCEN_NODE, RIGHT_ASCEN, and LON_DESCEN_NODE),
;      the DELTA angles are rationalized into (-180,180]. 
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-30' ;--- (M.Weber) Written.
progver = 'v2013-Apr-06' ;--- (M.Weber) Added angle rationalization for
;                             DELTA_RI_ASC, DELTA_LO_ASC, and DELTA_LO_DESC.
progver = 'v2013-Apr-11' ;--- (M.Weber) Fixed bug with LAST_FDF.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified tolerances according to BdP's
;                             feedback.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
progver = 'v2013-Jul-16' ;--- (M.Weber) Made a correction to how FDF counts pass IDs.
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MATCH_EVENTS'

  ;; Set tolerances on the fields.
  tol_pass     =  0b   ;; []
  tol_st_time  = 30d   ;; [sec]
  tol_en_time  = 30d   ;; [sec]
  tol_apogee   =  3d   ;; [km]
  tol_t_apog   = 60d   ;; [sec]
  tol_perigee  =  3d   ;; [km]
  tol_t_perig  = 60d   ;; [sec]
  tol_period   = 30d   ;; [sec]
  tol_ri_asc   =  0.5d ;; [deg]
  tol_lo_asc   =  0.5d ;; [deg]
  tol_lo_desc  =  0.5d ;; [deg]

  match_template = {events_match_type,             $
                    file_type      :     'events', $
                    fdf_filename   :     fdf_file, $
                    fdt_filename   :     fdt_file, $
                    fdf_index      :          -1L, $
                    fdt_index      :          -1L, $
                    fdf_pass       :           0L, $
                    fdt_pass       :           0L, $
                    delta_pass     :           0L, $
                    tol_pass       :     tol_pass, $
                    fdf_st_time    :           '', $
                    fdt_st_time    :           '', $
                    delta_st_time  :           0d, $
                    tol_st_time    :  tol_st_time, $
                    fdf_en_time    :           '', $
                    fdt_en_time    :           '', $
                    delta_en_time  :           0d, $
                    tol_en_time    :  tol_en_time, $
                    fdf_apogee     :           0d, $ 
                    fdt_apogee     :           0d, $ 
                    delta_apogee   :           0d, $ 
                    tol_apogee     :   tol_apogee, $
                    fdf_t_apog     :           '', $
                    fdt_t_apog     :           '', $
                    delta_t_apog   :           0d, $
                    tol_t_apog     :   tol_t_apog, $
                    fdf_perigee    :           0d, $
                    fdt_perigee    :           0d, $
                    delta_perigee  :           0d, $
                    tol_perigee    :  tol_perigee, $
                    fdf_t_perig    :           '', $
                    fdt_t_perig    :           '', $
                    delta_t_perig  :           0d, $
                    tol_t_perig    :  tol_t_perig, $
                    fdf_period     :           0d, $
                    fdt_period     :           0d, $
                    delta_period   :           0d, $
                    tol_period     :   tol_period, $
                    fdf_ri_asc     :           0d, $
                    fdt_ri_asc     :           0d, $
                    delta_ri_asc   :           0d, $
                    tol_ri_asc     :   tol_ri_asc, $
                    fdf_lo_asc     :           0d, $
                    fdt_lo_asc     :           0d, $
                    delta_lo_asc   :           0d, $
                    tol_lo_asc     :   tol_lo_asc, $
                    fdf_lo_desc    :           0d, $
                    fdt_lo_desc    :           0d, $
                    delta_lo_desc  :           0d, $
                    tol_lo_desc    :  tol_lo_desc   }

  default, pass_offset, '0'

;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    sub_matches = -1L
    fdf_unmatched = -1L
    fdt_unmatched = -1L
    return
  endif

  ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
  ;; about how many orbits IRIS has made. BdP directed that the correction be applied
  ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
  fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])


;; =============================================
;; Main loop to find matches.

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_parsed)
  fdt_indices = lindgen(n_elements(fdt_parsed))

  ;; Prepare simple pass and time arrays to work with.
  fdt_pass = fdt_parsed.pass
  fdf_st_tai = anytim2tai(fdf_parsed.start_time)
  fdt_st_tai = anytim2tai(fdt_parsed.start_time)
  fdf_en_tai = anytim2tai(fdf_parsed.end_time)
  fdt_en_tai = anytim2tai(fdt_parsed.end_time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  matches = match_template
  fdf_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    last_fdf = i_fdf 

    ss = where(fdt_pass EQ fdf_parsed[i_fdf].pass, count)

    case count of

      0: begin
        unmatched_line = match_template
        unmatched_line.fdf_index = i_fdf
        unmatched_line.fdf_pass = fdf_parsed[i_fdf].pass
        unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        unmatched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        unmatched_line.fdf_apogee = fdf_parsed[i_fdf].apogee
        unmatched_line.fdf_t_apog = fdf_parsed[i_fdf].apogee_time
        unmatched_line.fdf_perigee = fdf_parsed[i_fdf].perigee
        unmatched_line.fdf_t_perig = fdf_parsed[i_fdf].perigee_time
        unmatched_line.fdf_period = fdf_parsed[i_fdf].period
        unmatched_line.fdf_ri_asc = fdf_parsed[i_fdf].right_ascen
        unmatched_line.fdf_lo_asc = fdf_parsed[i_fdf].lon_ascen_node
        unmatched_line.fdf_lo_desc = fdf_parsed[i_fdf].lon_descen_node
        fdf_unmatched = [fdf_unmatched, unmatched_line]
      end

      1: begin
        ss = ss[0]
        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_pass = fdf_parsed[i_fdf].pass
        matched_line.fdt_pass = fdt_parsed[ss].pass
        matched_line.delta_pass = matched_line.fdt_pass - $
                                  matched_line.fdf_pass
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        matched_line.fdt_en_time = fdt_parsed[ss].end_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_apogee = fdf_parsed[i_fdf].apogee
        matched_line.fdt_apogee = fdt_parsed[ss].apogee
        matched_line.delta_apogee = matched_line.fdt_apogee - $
                                    matched_line.fdf_apogee
        matched_line.fdf_t_apog = fdf_parsed[i_fdf].apogee_time
        matched_line.fdt_t_apog = fdt_parsed[ss].apogee_time
        matched_line.delta_t_apog = anytim2tai(fdt_parsed[ss].apogee_time) - $
                                    anytim2tai(fdf_parsed[i_fdf].apogee_time)
        matched_line.fdf_perigee = fdf_parsed[i_fdf].perigee
        matched_line.fdt_perigee = fdt_parsed[ss].perigee
        matched_line.delta_perigee = matched_line.fdt_perigee - $
                                     matched_line.fdf_perigee
        matched_line.fdf_t_perig = fdf_parsed[i_fdf].perigee_time
        matched_line.fdt_t_perig = fdt_parsed[ss].perigee_time
        matched_line.delta_t_perig = anytim2tai(fdt_parsed[ss].perigee_time) - $
                                     anytim2tai(fdf_parsed[i_fdf].perigee_time)
        matched_line.fdf_period = fdf_parsed[i_fdf].period
        matched_line.fdt_period = fdt_parsed[ss].period
        matched_line.delta_period = matched_line.fdt_period - $
                                    matched_line.fdf_period
        matched_line.fdf_ri_asc = fdf_parsed[i_fdf].right_ascen
        matched_line.fdt_ri_asc = fdt_parsed[ss].right_ascen
        matched_line.delta_ri_asc = iris_fd_sad(matched_line.fdt_ri_asc, $
                                                matched_line.fdf_ri_asc)
        matched_line.fdf_lo_asc = fdf_parsed[i_fdf].lon_ascen_node
        matched_line.fdt_lo_asc = fdt_parsed[ss].lon_ascen_node
        matched_line.delta_lo_asc = iris_fd_sad(matched_line.fdt_lo_asc, $
                                                matched_line.fdf_lo_asc)
        matched_line.fdf_lo_desc = fdf_parsed[i_fdf].lon_descen_node
        matched_line.fdt_lo_desc = fdt_parsed[ss].lon_descen_node
        matched_line.delta_lo_desc = iris_fd_sad(matched_line.fdt_lo_desc, $
                                                 matched_line.fdf_lo_desc)
        matches = [matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_pass = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_pass = fdt_pass[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

      else: begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdt_st_tai[ss] EQ min(fdt_st_tai[ss])))[0]
        ss = ss[ss_earliest]

        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_pass = fdf_parsed[i_fdf].pass
        matched_line.fdt_pass = fdt_parsed[ss].pass
        matched_line.delta_pass = matched_line.fdt_pass - $
                                  matched_line.fdf_pass
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        matched_line.fdt_en_time = fdt_parsed[ss].end_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_apogee = fdf_parsed[i_fdf].apogee
        matched_line.fdt_apogee = fdt_parsed[ss].apogee
        matched_line.delta_apogee = matched_line.fdt_apogee - $
                                    matched_line.fdf_apogee
        matched_line.fdf_t_apog = fdf_parsed[i_fdf].apogee_time
        matched_line.fdt_t_apog = fdt_parsed[ss].apogee_time
        matched_line.delta_t_apog = anytim2tai(fdt_parsed[ss].apogee_time) - $
                                    anytim2tai(fdf_parsed[i_fdf].apogee_time)
        matched_line.fdf_perigee = fdf_parsed[i_fdf].perigee
        matched_line.fdt_perigee = fdt_parsed[ss].perigee
        matched_line.delta_perigee = matched_line.fdt_perigee - $
                                     matched_line.fdf_perigee
        matched_line.fdf_t_perig = fdf_parsed[i_fdf].perigee_time
        matched_line.fdt_t_perig = fdt_parsed[ss].perigee_time
        matched_line.delta_t_perig = anytim2tai(fdt_parsed[ss].perigee_time) - $
                                     anytim2tai(fdf_parsed[i_fdf].perigee_time)
        matched_line.fdf_period = fdf_parsed[i_fdf].period
        matched_line.fdt_period = fdt_parsed[ss].period
        matched_line.delta_period = matched_line.fdt_period - $
                                    matched_line.fdf_period
        matched_line.fdf_ri_asc = fdf_parsed[i_fdf].right_ascen
        matched_line.fdt_ri_asc = fdt_parsed[ss].right_ascen
        matched_line.delta_ri_asc = iris_fd_sad(matched_line.fdt_ri_asc, $
                                                matched_line.fdf_ri_asc)
        matched_line.fdf_lo_asc = fdf_parsed[i_fdf].lon_ascen_node
        matched_line.fdt_lo_asc = fdt_parsed[ss].lon_ascen_node
        matched_line.delta_lo_asc = iris_fd_sad(matched_line.fdt_lo_asc, $
                                                matched_line.fdf_lo_asc)
        matched_line.fdf_lo_desc = fdf_parsed[i_fdf].lon_descen_node
        matched_line.fdt_lo_desc = fdt_parsed[ss].lon_descen_node
        matched_line.delta_lo_desc = iris_fd_sad(matched_line.fdt_lo_desc, $
                                                 matched_line.fdf_lo_desc)
        matches = [matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_pass = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_pass = fdt_pass[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

    endcase

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF lines.

  if (last_fdf LT N_fdf-1) then begin
    for i_fdf = (last_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index = i_fdf
      unmatched_line.fdf_pass = fdf_parsed[i_fdf].pass
      unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
      unmatched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
      unmatched_line.fdf_apogee = fdf_parsed[i_fdf].apogee
      unmatched_line.fdf_t_apog = fdf_parsed[i_fdf].apogee_time
      unmatched_line.fdf_perigee = fdf_parsed[i_fdf].perigee
      unmatched_line.fdf_t_perig = fdf_parsed[i_fdf].perigee_time
      unmatched_line.fdf_period = fdf_parsed[i_fdf].period
      unmatched_line.fdf_ri_asc = fdf_parsed[i_fdf].right_ascen
      unmatched_line.fdf_lo_asc = fdf_parsed[i_fdf].lon_ascen_node
      unmatched_line.fdf_lo_desc = fdf_parsed[i_fdf].lon_descen_node
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif

  ;; Do clean-up on the seed entry for fdf_unmatched.
  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse


;; =============================================
;; Deal with the leftover unmatched FDT lines.

  fdt_unmatched = match_template

  if (fdt_st_tai[0] NE -1) then begin
    N_fdt = n_elements(fdt_st_tai)
    for i_fdt = 0L,(N_fdt-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index = i_fdt
      unmatched_line.fdt_pass = fdt_parsed[i_fdt].pass
      unmatched_line.fdt_st_time = fdt_parsed[i_fdt].start_time
      unmatched_line.fdt_en_time = fdt_parsed[i_fdt].end_time
      unmatched_line.fdt_apogee = fdt_parsed[i_fdt].apogee
      unmatched_line.fdt_t_apog = fdt_parsed[i_fdt].apogee_time
      unmatched_line.fdt_perigee = fdt_parsed[i_fdt].perigee
      unmatched_line.fdt_t_perig = fdt_parsed[i_fdt].perigee_time
      unmatched_line.fdt_period = fdt_parsed[i_fdt].period
      unmatched_line.fdt_ri_asc = fdt_parsed[i_fdt].right_ascen
      unmatched_line.fdt_lo_asc = fdt_parsed[i_fdt].lon_ascen_node
      unmatched_line.fdt_lo_desc = fdt_parsed[i_fdt].lon_descen_node
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor

    ;; And do clean-up on the seed entry.
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; ...And clean up on MATCHES.

  if (n_elements(matches) GE 2) then begin
    matches = matches[1:*]
  endif else begin
    matches = -1
  endelse


;; =============================================
;; Finish.

  ;; We used up FDT_STC already, so re-get it if requested by keyword.
  if arg_present(fdt_parsed) then fdt_parsed = iris_fdf2struct(fdt_file)

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_ground,     $
      fdf_file,               $
      fdt_file,               $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      fdf_parsed =fdf_parsed, $
      fdt_parsed =fdt_parsed

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_ground
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of GROUND event lists to determine unique matches.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'ground', are read in as
;   IDL structure arrays. Certain fields are compared for "close enough"
;   matching to assign unique pairs, stored in MATCHES. For matched pairs, the
;   differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_match_ground, fdf_file, fdt_file, matches, fdf_unmatched, $
;     fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "ground", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "ground", produced by the LM
;              Flight Dynamics Team.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "GROUND_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "GROUND_MATCH_TYPE", array
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "GROUND_MATCH_TYPE", array
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-May-06' ;--- (M.Weber) Modified tolerances according to BdP's
;                             feedback.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MATCH_GROUND'

  ;; Set tolerances on the fields.
  tol_time = 5d   ;; [sec]
  tol_lat  = 0.5d ;; [deg]
  tol_lon  = 0.5d ;; [deg]
  tol_alt  = 3d   ;; [km]

  match_template = {ground_match_type,        $
                    file_type    :  'ground', $
                    fdf_filename :  fdf_file, $
                    fdt_filename :  fdt_file, $
                    fdf_index    :       -1L, $
                    fdt_index    :       -1L, $
                    fdf_time     :        '', $
                    fdt_time     :        '', $
                    delta_time   :        0d, $
                    tol_time     :  tol_time, $
                    fdf_lat      :        0d, $
                    fdt_lat      :        0d, $
                    delta_lat    :        0d, $
                    tol_lat      :   tol_lat, $
                    fdf_lon      :        0d, $
                    fdt_lon      :        0d, $
                    delta_lon    :        0d, $
                    tol_lon      :   tol_lon, $
                    fdf_alt      :        0d, $ 
                    fdt_alt      :        0d, $ 
                    delta_alt    :        0d, $ 
                    tol_alt      :   tol_alt   }


;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    sub_matches = -1L
    fdf_unmatched = -1L
    fdt_unmatched = -1L
    return
  endif


;; =============================================
;; Main loop to find matches.

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_parsed)
  fdt_indices = lindgen(n_elements(fdt_parsed))

  ;; Prepare simple pass and time arrays to work with.
  fdf_tai = anytim2tai(fdf_parsed.time)
  fdt_tai = anytim2tai(fdt_parsed.time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  matches = match_template
  fdf_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    last_fdf = i_fdf

    ss = where( (abs(fdt_tai-fdf_tai[i_fdf]) LE tol_time), count)

    case count of

      0: begin
        unmatched_line = match_template
        unmatched_line.fdf_index = i_fdf
        unmatched_line.fdf_time = fdf_parsed[i_fdf].time
        unmatched_line.fdf_lat = fdf_parsed[i_fdf].lat
        unmatched_line.fdf_lon = fdf_parsed[i_fdf].lon
        unmatched_line.fdf_alt = fdf_parsed[i_fdf].alt
        fdf_unmatched = [fdf_unmatched, unmatched_line]
      end

      1: begin
        ss = ss[0]
        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_time = fdf_parsed[i_fdf].time
        matched_line.fdt_time = fdt_parsed[ss].time
        matched_line.delta_time = fdt_tai[ss] - fdf_tai[i_fdf]
        matched_line.fdf_lat = fdf_parsed[i_fdf].lat
        matched_line.fdt_lat = fdt_parsed[ss].lat
        matched_line.delta_lat = iris_fd_sad(matched_line.fdt_lat, $
                                             matched_line.fdf_lat)
        matched_line.fdf_lon = fdf_parsed[i_fdf].lon
        matched_line.fdt_lon = fdt_parsed[ss].lon
        matched_line.delta_lon = iris_fd_sad(matched_line.fdt_lon, $
                                             matched_line.fdf_lon)
        matched_line.fdf_alt = fdf_parsed[i_fdf].alt
        matched_line.fdt_alt = fdt_parsed[ss].alt
        matched_line.delta_alt = matched_line.fdt_alt - matched_line.fdf_alt
        matches = [matches, matched_line]

        if (n_elements(fdt_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_tai = fdt_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

      else: begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdt_tai[ss] EQ min(fdt_tai[ss])))[0]
        ss = ss[ss_earliest]

        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_time = fdf_parsed[i_fdf].time
        matched_line.fdt_time = fdt_parsed[ss].time
        matched_line.delta_time = fdt_tai[ss] - fdf_tai[i_fdf]
        matched_line.fdf_lat = fdf_parsed[i_fdf].lat
        matched_line.fdt_lat = fdt_parsed[ss].lat
        matched_line.delta_lat = iris_fd_sad(matched_line.fdt_lat, $
                                             matched_line.fdf_lat)
        matched_line.fdf_lon = fdf_parsed[i_fdf].lon
        matched_line.fdt_lon = fdt_parsed[ss].lon
        matched_line.delta_lon = iris_fd_sad(matched_line.fdt_lon, $
                                             matched_line.fdf_lon)
        matched_line.fdf_alt = fdf_parsed[i_fdf].alt
        matched_line.fdt_alt = fdt_parsed[ss].alt
        matched_line.delta_alt = matched_line.fdt_alt - matched_line.fdf_alt
        matches = [matches, matched_line]

        if (n_elements(fdt_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1 
          fdt_parsed = -1
          fdt_tai = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the 
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_tai = fdt_tai[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

    endcase

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF lines.

  if (last_fdf LT N_fdf-1) then begin
    for i_fdf = (last_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index = i_fdf
      unmatched_line.fdf_time = fdf_parsed[i_fdf].time
      unmatched_line.fdf_lat = fdf_parsed[i_fdf].lat
      unmatched_line.fdf_lon = fdf_parsed[i_fdf].lon
      unmatched_line.fdf_alt = fdf_parsed[i_fdf].alt
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif

  ;; Do clean-up on the seed entry for fdf_unmatched.
  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse


;; =============================================
;; Deal with the leftover unmatched FDT lines.

  fdt_unmatched = match_template

  if (fdt_tai[0] NE -1) then begin
    N_fdt = n_elements(fdt_tai)
    for i_fdt = 0L,(N_fdt-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index = i_fdt
      unmatched_line.fdt_time = fdt_parsed[i_fdt].time
      unmatched_line.fdt_lat = fdt_parsed[i_fdt].lat
      unmatched_line.fdt_lon = fdt_parsed[i_fdt].lon
      unmatched_line.fdt_alt = fdt_parsed[i_fdt].alt
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor

    ;; And do clean-up on the seed entry.
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; ...And clean up on MATCHES.

  if (n_elements(matches) GE 2) then begin
    matches = matches[1:*]
  endif else begin
    matches = -1
  endelse


;; =============================================
;; Finish.

  ;; We used up FDT_STC already, so re-get it if requested by keyword.
  if arg_present(fdt_parsed) then fdt_parsed = iris_fdf2struct(fdt_file)

  return

END


;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_match_radzones, $
      zone_name,                $
      fdf_file,                 $
      fdt_file,                 $
      matches,                  $
      fdf_unmatched,            $
      fdt_unmatched,            $
      fdf_parsed =fdf_parsed,   $
      fdt_parsed =fdt_parsed

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_match_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Compare two files of RADZONES event lists to determine unique matches for
;   a single type of radiation zone specified by keyword.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'radzones', are read in
;   as IDL structure arrays. Certain fields are compared for "close enough"
;   matching to assign unique pairs, stored in SUB_MATCHES. For matched pairs,
;   the differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_sub_match_radzones, zone_name, fdf_file, fdt_file, matches, $
;   IDL>   fdf_unmatched, fdt_unmatched, [fdf_parsed=fdf_parsed], $
;   IDL>   [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   ZONE_NAME -  [Mandatory] (string, scalar)
;              One of the possible radiation zone names for file-type
;              "radzones". (E.g., 'SAA', 'N_HLZ', or 'S_HLZ'.)
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "radzones", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "radzones", produced by the LM
;              Flight Dynamics Team.
; OUTPUTS:
;
;   SUB_MATCHES -  [Mandatory] (structure of type "RADZONES_MATCH_TYPE", array
;              of size [N_matched_events])
;              Each element of SUB_MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "RADZONES_MATCH_TYPE",
;              array of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "RADZONES_MATCH_TYPE",
;              array of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED_IN=FDF_PARSED_IN - [Optional] (structure of type "Anonymous",
;              array of size [N_FDF_events])
;              Each element of FDF_PARSED_IN contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. For a given ZONE_NAME, there are two mid-level
;              structure arrays (INNER and OUTER). FDF_PARSED_IN holds the data
;              for <zone>_INNER. (The two structures are identical for SAA.)
;              See the Manual for more information.
;
;   FDF_PARSED_OUT=FDF_PARSED_OUT - [Optional] (structure of type "Anonymous",
;              array of size [N_FDF_events])
;              Each element of FDF_PARSED_OUT contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. For a given ZONE_NAME, there are two mid-level
;              structure arrays (INNER and OUTER). FDF_PARSED_OUT holds the data
;              for <zone>_OUTER. (The two structures are identical for SAA.)
;              See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;   2) The program will crash if one does not set exactly one of the station
;      switches.
;
;   3) Sometimes the FDF file will only contain Svalbard data for the "SG1S"
;      antenna. It is implied then that the data is identical for the other
;      Svalbard antennae.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-14' ;--- (M.Weber) Split off from <iris_fd_match_radzones>
;                             in a major refactoring.
progver = 'v2013-May-29' ;--- (M.Weber) Added a block to watch out for FDT SAA
;                             passages of zero duration. Also adjusted matching
;                             SAA FDF entry against FDT entry, to use closest
;                             of multiple matches instead of earliest.
;
;
;-
;===============================================================================

;; =============================================
;; Basic setup.

  prognam = 'IRIS_FD_SUB_MATCH_RADZONES'

  file_type = 'radzones'

  ;; Set tolerances on the fields.
  tol_st_time_outer = 60d   ;; [sec]
  tol_st_time_inner = 60d   ;; [sec]
  tol_en_time_inner = 60d   ;; [sec]
  tol_en_time_outer = 60d   ;; [sec]

  matches = -1L
  fdf_unmatched = -1L
  fdt_unmatched = -1L


;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    return
  endif


;; =============================================
;; Isolate the specified zone structures.

  case strup(zone_name) of

    'SAA': begin
             ss1 = where(tag_names(fdf_parsed) EQ 'SAA', count1)
             if (count1 EQ 1) then begin
               fdf_par_inner = fdf_parsed.saa
               fdf_par_outer = fdf_parsed.saa
             endif

             ss2 = where(tag_names(fdt_parsed) EQ 'SAA', count2)
             if (count2 EQ 1) then begin
               fdt_par  = fdt_parsed.saa
             endif

             if ((count1 NE 1) OR (count2 NE 1)) then return

             ;; START_LAG is the expected time between the outer boundary
             ;; start_time and the inner boundary start_time.
             ;; For SAAs, there is only one boundary, so treat as the same.
             start_lag = 0d
             ;; FDT_LAG is the expected time between the FDT inner boundary
             ;; start_time and the FDT inner boundary stop_time.
             ;; For SAAs, there is only one boundary, so jump backwards to
             ;; catch the same row.
             fdt_lag = -900d
    end

    'N_HLZ': begin
             ss1 = where(tag_names(fdf_parsed) EQ 'NORTH_HLZ_INNER', count1)
             if (count1 EQ 1) then fdf_par_inner = fdf_parsed.north_hlz_inner

             ss2 = where(tag_names(fdf_parsed) EQ 'NORTH_HLZ_OUTER', count2)
             if (count2 EQ 1) then fdf_par_outer = fdf_parsed.north_hlz_outer

             ss3 = where(tag_names(fdt_parsed) EQ 'N_HLZ', count3)
             if (count3 EQ 1) then fdt_par = fdt_parsed.n_hlz

             if ((count1 NE 1) OR (count2 NE 1) OR (count3 NE 1)) then return

             start_lag = 420d
             fdt_lag = 660d
    end

    'S_HLZ': begin
             ss1 = where(tag_names(fdf_parsed) EQ 'SOUTH_HLZ_INNER', count1)
             if (count1 EQ 1) then fdf_par_inner = fdf_parsed.south_hlz_inner

             ss2 = where(tag_names(fdf_parsed) EQ 'SOUTH_HLZ_OUTER', count2)
             if (count2 EQ 1) then fdf_par_outer = fdf_parsed.south_hlz_outer

             ss3 = where(tag_names(fdt_parsed) EQ 'S_HLZ', count3)
             if (count3 EQ 1) then fdt_par = fdt_parsed.s_hlz

             if ((count1 NE 1) OR (count2 NE 1) OR (count3 NE 1)) then return

             start_lag = 420d
             fdt_lag = 660d
    end

    else: begin
          ;; ZONE_NAME isn't recognized, so just abort.
          return
    end

  endcase


;; =============================================
;; Define organization for outputs.

  match_template = {radzones_match_type,                     $
                    file_type           :         file_type, $
                    zone                :         zone_name, $
                    fdf_filename        :          fdf_file, $
                    fdt_filename        :          fdt_file, $
                    fdf_index_outer     :               -1L, $
                    fdf_index_inner     :               -1L, $
                    fdt_index1          :               -1L, $
                    fdt_index2          :               -1L, $
                    fdf_st_time_outer   :                '', $
                    fdt_st_time_outer   :                '', $
                    delta_st_time_outer :                0d, $
                    tol_st_time_outer   : tol_st_time_outer, $
                    fdf_st_time_inner   :                '', $
                    fdt_st_time_inner   :                '', $
                    delta_st_time_inner :                0d, $
                    tol_st_time_inner   : tol_st_time_inner, $
                    fdf_en_time_inner   :                '', $
                    fdt_en_time_inner   :                '', $
                    delta_en_time_inner :                0d, $
                    tol_en_time_inner   : tol_en_time_inner, $
                    fdf_en_time_outer   :                '', $
                    fdt_en_time_outer   :                '', $
                    delta_en_time_outer :                0d, $
                    tol_en_time_outer   : tol_en_time_outer   }


;; =============================================
;; Find matches.  (See Note #3.)

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_par_outer)
  fdf_indices = lindgen(n_elements(fdf_par_inner))
  fdt_indices = lindgen(n_elements(fdt_par))

  ;; Prepare simple time arrays to work with.
  fdf_outer_st_tai = anytim2tai(fdf_par_outer.start_time)
  fdf_outer_en_tai = anytim2tai(fdf_par_outer.stop_time)
  fdf_inner_st_tai = anytim2tai(fdf_par_inner.start_time)
  fdf_inner_en_tai = anytim2tai(fdf_par_inner.stop_time)
  fdt_st_tai = anytim2tai(fdt_par.start_time)
  fdt_en_tai = anytim2tai(fdt_par.stop_time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  matches = match_template
  fdf_unmatched = match_template
  fdt_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    current_fdf = i_fdf

    ;; This flag keeps track of whether a match was made on this pass.
    qmatch1 = 0B
    qmatch3 = 0B

    ;; Match FDF_INNER_start to FDF_OUTER_start + START_LAG.
    ss1 = where(abs(fdf_inner_st_tai - fdf_outer_st_tai[i_fdf] - start_lag) $
                LE 600d, count1)

    if (count1 GE 1) then begin
      qmatch1 = 1B
      if (count1 GT 1) then begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdf_inner_st_tai[ss1] EQ $
                             min(fdf_inner_st_tai[ss1])))[0]
        ss1 = ss1[ss_earliest]
      endif else ss1 = ss1[0]

      ;; Match FDT_start to FDF_OUTER_start.
      ss2 = where(abs(fdt_st_tai - fdf_outer_st_tai[i_fdf]) LE 600d, count2)

      if (count2 GT 1) then begin
        ;; If there is more than one match, it might be because there are
        ;; FDT entries with zero duration. We remove those from consideration.
        ss_real = where((fdt_en_tai[ss2]-fdt_st_tai[ss2]) GE 0.1)
        ss2 = ss2[ss_real]
        count2 = n_elements(ss2)
      endif

      if (count2 GE 1) then begin
        if (count2 GT 1) then begin
          ;; If there is more than one match, we take the one closest to the 
          ;; FDF time. 
          st_offset = abs(fdt_st_tai[ss2]-fdf_outer_st_tai[i_fdf])
          ss_closest = (where(st_offset EQ min(st_offset)))[0]
          ss2 = ss2[ss_closest]
        endif else ss2 = ss2[0]

        ;; Match FDT_start to FDT_start + FDT_LAG.
        ;; For SAA, it is the same entry, else it is the next one.
        if (strup(zone_name) EQ 'SAA') then ss3 = ss2 else ss3 = ss2+1

        ;; Check to make sure FDT_INDICES[SS3] exists before declaring a match.
        if (ss3 LT n_elements(fdt_indices)) then begin
          ;; We made it to here if all three matches were made.
          count3 = 1L
          qmatch3 = 1B

          matched_line = match_template
          matched_line.fdf_index_outer = i_fdf
          matched_line.fdf_index_inner = fdf_indices[ss1]
          matched_line.fdt_index1 = fdt_indices[ss2]
          matched_line.fdt_index2 = fdt_indices[ss3]
          matched_line.fdf_st_time_outer = fdf_par_outer[i_fdf].start_time
          matched_line.fdf_st_time_inner = fdf_par_inner[ss1].start_time
          matched_line.fdf_en_time_inner = fdf_par_inner[ss1].stop_time
          matched_line.fdf_en_time_outer = fdf_par_outer[i_fdf].stop_time
          matched_line.fdt_st_time_outer = fdt_par[ss2].start_time
          if (strup(zone_name) EQ 'SAA') then begin
            matched_line.fdt_st_time_inner = fdt_par[ss2].start_time
            matched_line.fdt_en_time_inner = fdt_par[ss3].stop_time
          endif else begin
            matched_line.fdt_st_time_inner = fdt_par[ss2].stop_time
            matched_line.fdt_en_time_inner = fdt_par[ss3].start_time
          endelse
          matched_line.fdt_en_time_outer = fdt_par[ss3].stop_time
          matched_line.delta_st_time_outer =             $
            anytim2tai(matched_line.fdt_st_time_outer) - $
            anytim2tai(matched_line.fdf_st_time_outer)
          matched_line.delta_st_time_inner =             $
            anytim2tai(matched_line.fdt_st_time_inner) - $
            anytim2tai(matched_line.fdf_st_time_inner)
          matched_line.delta_en_time_inner =             $
            anytim2tai(matched_line.fdt_en_time_inner) - $
            anytim2tai(matched_line.fdf_en_time_inner)
          matched_line.delta_en_time_outer =             $
            anytim2tai(matched_line.fdt_en_time_outer) - $
            anytim2tai(matched_line.fdf_en_time_outer)
          matches = [matches, matched_line]

          ;; Remove matched elements from consideration.
          if (n_elements(fdf_indices) EQ 1) then begin
            fdf_indices = -1
          endif else begin
            not_ss1 = rest_mask(fdf_indices,ss1)
            fdf_par_inner = fdf_par_inner[not_ss1]
            fdf_inner_st_tai = fdf_inner_st_tai[not_ss1]
            fdf_inner_en_tai = fdf_inner_en_tai[not_ss1]
            fdf_indices = fdf_indices[not_ss1]
          endelse

          ss_fdt = [ss2,ss3]
          ss_fdt = ss_fdt[uniq(ss_fdt)]
          if (n_elements(fdt_indices) EQ n_elements(ss_fdt)) then begin
            fdt_indices = -1
          endif else begin
            not_ss_fdt = rest_mask(fdt_indices,ss_fdt)
            fdt_par = fdt_par[not_ss_fdt]
            fdt_st_tai = fdt_st_tai[not_ss_fdt]
            fdt_en_tai = fdt_en_tai[not_ss_fdt]
            fdt_indices = fdt_indices[not_ss_fdt]
          endelse

          ;; Abort out of the FOR loop if we run out of elements.
          if ((fdf_indices[0] EQ -1) OR (fdt_indices[0] EQ -1)) $
            then i_fdf = N_fdf+1

        endif   ;; (ss3 LT n_elements(fdt_indices))
      endif   ;; (count2 GE 1)
    endif   ;; (count1 GE 1)

    ;; If a match wasn't made on this pass, then put this FDF_OUTER
    ;; into the unmatched list.
    if (qmatch3 EQ 0B) then begin
      unmatched_line = match_template
      unmatched_line.fdf_index_outer = i_fdf
      unmatched_line.fdf_st_time_outer = fdf_par_outer[i_fdf].start_time
      unmatched_line.fdf_en_time_outer = fdf_par_outer[i_fdf].stop_time

      ;; If at least INNER was matched to OUTER, then add that info, too.
      if (qmatch1 EQ 0B) then begin
        unmatched_line.fdf_index_inner = fdf_indices[ss1]
        unmatched_line.fdf_st_time_inner = fdf_par_inner[ss1].start_time
        unmatched_line.fdf_en_time_inner = fdf_par_inner[ss1].stop_time
      endif

      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endif   ;; (qmatch3 EQ 0B)

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF_OUTER lines.

  if (current_fdf LT N_fdf-1) then begin
    for i_fdf = (current_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index_outer = i_fdf
      unmatched_line.fdf_st_time_outer = fdf_par_outer[i_fdf].start_time
      unmatched_line.fdf_en_time_outer = fdf_par_outer[i_fdf].stop_time

      ;; Keep trying to find INNER matches, at least.
      if (fdf_indices[0] NE -1) then begin
        ss1 = where(abs(fdf_inner_st_tai - fdf_outer_st_tai[i_fdf] - start_lag) $
                    LE 600d, count1)
        if (count1 GE 1) then begin
          if (count1 GT 1) then begin
            ;; If there is more than one match, we take the earliest one.
            ss_earliest = (where(fdf_inner_st_tai[ss1] EQ $
                                 min(fdf_inner_st_tai[ss1])))[0]
            ss1 = ss1[ss_earliest]
          endif else ss1 = ss1[0]
          unmatched_line.fdf_index_inner = fdf_indices[ss1]
          unmatched_line.fdf_st_time_inner = fdf_par_inner[ss1].start_time
          unmatched_line.fdf_en_time_inner = fdf_par_inner[ss1].stop_time
        endif
        ;; Remove matched elements from consideration.
        if (n_elements(fdf_indices) EQ 1) then begin
          fdf_indices = -1
        endif else begin
          not_ss1 = rest_mask(fdf_indices,ss1)
          fdf_par_inner = fdf_par_inner[not_ss1]
          fdf_inner_st_tai = fdf_inner_st_tai[not_ss1]
          fdf_inner_en_tai = fdf_inner_en_tai[not_ss1]
          fdf_indices = fdf_indices[not_ss1]
        endelse

        fdf_unmatched = [fdf_unmatched, unmatched_line]
      endif  ;; (fdf_indices[0] NE -1)
    endfor  ;; (i_fdf loop)
  endif  ;; (Finishing up the remaining FDF_OUTER lines, if any.)


;; =============================================
;; Deal with any leftover unmatched FDF_INNER lines.

  if (fdf_indices[0] NE -1) then begin
    for i_fdf = 0L,(n_elements(fdf_indices)-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index_inner = fdf_indices[i_fdf]
      unmatched_line.fdf_st_time_inner = fdf_par_inner[i_fdf].start_time
      unmatched_line.fdf_en_time_inner = fdf_par_inner[i_fdf].stop_time
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif


;; =============================================
;; Deal with any leftover unmatched FDT lines.

  if (fdt_indices[0] NE -1) then begin
    for i_fdt = 0L,(n_elements(fdt_indices)-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index1 = fdt_indices[i_fdt]
      unmatched_line.fdt_index2 = fdt_indices[i_fdt]
      unmatched_line.fdt_st_time_outer = fdt_par[i_fdt].start_time
      unmatched_line.fdt_st_time_inner = fdt_par[i_fdt].stop_time
      unmatched_line.fdt_en_time_inner = fdt_par[i_fdt].start_time
      unmatched_line.fdt_en_time_outer = fdt_par[i_fdt].stop_time
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor
  endif


;; =============================================
;; Clean up seed entries.

  if (n_elements(matches) GE 2) then begin
    matches = matches[1:*]
  endif else begin
    matches = -1
  endelse

  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse

  if (n_elements(fdt_unmatched) GE 2) then begin
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_radzones,   $
      fdf_file,               $
      fdt_file,               $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      fdf_parsed =fdf_parsed, $
      fdt_parsed =fdt_parsed

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Compare two files of RADZONES event lists to determine unique matches
;   for all of the possible stations/antennae.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'radzones', are read in
;   as IDL structure arrays. Certain fields are compared for "close enough"
;   matching to assign unique pairs, stored in MATCHES. For matched pairs,
;   the differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_match_radzones, fdf_file, fdt_file, matches, fdf_unmatched, $
;   IDL>   fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "radzones", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "radzones", produced by the LM
;              Flight Dynamics Team.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "radzones" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "radzones" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "radzones" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-May-06' ;--- (M.Weber) Modified tolerances according to BdP's
;                             feedback. Fixed a typo: missing "=".
progver = 'v2013-May-14' ;--- (M.Weber) Major refactoring to allow for HLZs
;                             and much other stuff. Split off a lot of the
;                             functionality to <iris_fd_sub_match_radzones>.
;
;-
;===============================================================================

;; =============================================
;; Perform input checks.

  prognam = 'IRIS_FD_MATCH_RADZONES'

  file_type = 'radzones'


;; =============================================
;; Get matches for each station.

  iris_fd_sub_match_radzones, 'SAA', fdf_file, fdt_file, matches_saa, $
    fdf_unmatched_saa, fdt_unmatched_saa

  iris_fd_sub_match_radzones, 'N_HLZ', fdf_file, fdt_file, matches_nhlz, $
    fdf_unmatched_nhlz, fdt_unmatched_nhlz

  iris_fd_sub_match_radzones, 'S_HLZ', fdf_file, fdt_file, matches_shlz, $
    fdf_unmatched_shlz, fdt_unmatched_shlz, fdf_parsed=fdf_parsed, $
    fdt_parsed=fdt_parsed


;; =============================================
;; Create the overarching structures.

  matches = {                             $
             file_type    :    file_type, $
             fdf_filename :     fdf_file, $
             fdt_filename :     fdt_file, $
             saa          :  matches_saa, $
             n_hlz        : matches_nhlz, $
             s_hlz        : matches_shlz   }

  fdf_unmatched = {                                   $
                   file_type    :          file_type, $
                   fdf_filename :           fdf_file, $
                   fdt_filename :           fdt_file, $
                   saa          :  fdf_unmatched_saa, $
                   n_hlz        : fdf_unmatched_nhlz, $
                   s_hlz        : fdf_unmatched_shlz   }

  fdt_unmatched = {                                   $
                   file_type    :          file_type, $
                   fdf_filename :           fdf_file, $
                   fdt_filename :           fdt_file, $
                   saa          :  fdt_unmatched_saa, $
                   n_hlz        : fdt_unmatched_nhlz, $
                   s_hlz        : fdt_unmatched_shlz   }


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_match_view,    $
      fdf_file,                $
      fdt_file,                $
      file_type,               $
      sub_matches,             $
      fdf_unmatched,           $
      fdt_unmatched,           $
      fdf_parsed  =fdf_parsed, $
      fdt_parsed  =fdt_parsed, $
      mgs         =mgs,        $
      waps        =waps,       $
      asfs        =asfs,       $
      sg1s        =sg1s,       $
      sg3x        =sg3x,       $
      sg6x        =sg6x,       $
      sg22x       =sg22x,      $
      pass_offset =pass_offset

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_match_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Compare two files of VIEW_10DEG or VIEW_5DEG event lists to determine unique
;   matches for a single station/antenna specified by keyword.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'view_XXdeg', are read in
;   as IDL structure arrays. Certain fields are compared for "close enough"
;   matching to assign unique pairs, stored in SUB_MATCHES. For matched pairs,
;   the differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_sub_match_view, fdf_file, fdt_file, file_type, sub_matches, $
;     fdf_unmatched, fdt_unmatched, [fdf_parsed=fdf_parsed], $
;     [fdt_parsed=fdt_parsed], [/mgs, /waps, /asfs, /sg1s, /sg3x, /sg6x, /sg22x]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "view_XXdeg", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "view_XXdeg", produced by the LM
;              Flight Dynamics Team.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   /MGS     - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the McMurdo-10m station.
;
;   /WAPS    - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Wallops station.
;
;   /ASFS    - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Alaska-11-3m station.
;
;   /SG1S    - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Svalbard-1S station.
;
;   /SG3X    - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Svalbard-3X station.
;
;   /SG6X    - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Svalbard-6X station.
;
;   /SG22X   - [Optional (but see Note #2)] (keyword switch)
;              Set this switch to perform matches for the Svalbard-22X station.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
; OUTPUTS:
;
;   SUB_MATCHES -  [Mandatory] (structure of type "VIEW_MATCH_TYPE", array
;              of size [N_matched_events])
;              Each element of SUB_MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "VIEW_MATCH_TYPE",
;              array of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "VIEW_MATCH_TYPE",
;              array of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;   2) The program will crash if one does not set exactly one of the station
;      switches.
;
;   3) Sometimes the FDF file will only contain Svalbard data for the "SG1S"
;      antenna. It is implied then that the data is identical for the other
;      Svalbard antennae.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
progver = 'v2013-Jul-16' ;--- (M.Weber) Made a correction to how FDF counts pass IDs.
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform input checks.

  prognam = 'IRIS_FD_SUB_MATCH_VIEW'

  ;; Enforce one, and only one, station switch.
  if (keyword_set(mgs)  + keyword_set(waps) + keyword_set(asfs) $
    + keyword_set(sg1s) + keyword_set(sg3x) + keyword_set(sg6x) $
    + keyword_set(sg22x)) NE 1 then begin
    print, prognam + ': One and only one station switch must be ' + $
           'indicated. Stopping.'
    stop
  endif


;; =============================================
;; Read in two files.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)

  ;; If either FDF_PARSED or FDT_PARSED are empty, then we can go home early.
  if ((size(fdf_parsed,/type) NE 8) OR (size(fdt_parsed,/type) NE 8)) then begin
    sub_matches = -1L
    fdf_unmatched = -1L
    fdt_unmatched = -1L
    return
  endif


;; =============================================
;; Perform setup for specified station.

  sub_matches = -1L
  fdf_unmatched = -1L
  fdt_unmatched = -1L

  fdf_tags = tag_names(fdf_parsed)
  fdt_tags = tag_names(fdt_parsed)

  case 1b of

    keyword_set(mgs): begin
      ss = where((fdf_tags EQ 'MCMURDO_10DEG') OR (fdf_tags EQ 'MGS_10DEG') $
                 OR (fdf_tags EQ 'MGS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find MGS passes in FDF file. Returning.'
        return
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'MGS_10m'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'MGS_10M_10DEG') OR (fdt_tags EQ 'MGS_10DEG') $
                 OR (fdt_tags EQ 'MGS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find MGS passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'MGS_10m'
      endelse
    end

    keyword_set(waps): begin
      ss = where((fdf_tags EQ 'WAPS_10DEG') OR (fdf_tags EQ 'WAPS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find WAPS passes in FDF file. Returning.'
        return
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'WAPS'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'WAPS_10DEG') OR (fdt_tags EQ 'WAPS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find WAPS passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'WAPS'
      endelse
    end

    keyword_set(asfs): begin
      ss = where((fdf_tags EQ 'ASF_11_3M_10DEG') OR (fdf_tags EQ 'ASFS_10DEG') $
                 OR (fdf_tags EQ 'ASFS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find ASFS passes in FDF file. ' + $
               'Returning.'
        return
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'ASFS_11_3m'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'ASF_11_3M_10DEG') OR (fdt_tags EQ 'ASFS_10DEG') $
                 OR (fdt_tags EQ 'ASFS'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find ASFS passes in FDT file. ' + $
               'Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'ASFS_11_3m'
      endelse
    end

    keyword_set(sg1s): begin
      ss = where((fdf_tags EQ 'SG1S_10DEG') OR (fdf_tags EQ 'SG1_10DEG') $
                 OR (fdf_tags EQ 'SG1'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find SG1S passes in FDF file. Returning.'
        return
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'SG1S'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'SG1S_10DEG') OR (fdt_tags EQ 'SG1_10DEG') $
                 OR (fdt_tags EQ 'SG1'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find SG1S passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'SG1S'
      endelse
    end

    keyword_set(sg3x): begin
      ss = where((fdf_tags EQ 'SG3X_10DEG') OR (fdf_tags EQ 'SG3_10DEG') $
                 OR (fdf_tags EQ 'SG3'), count) 
      if (count EQ 0) then begin
        ss = where((fdf_tags EQ 'SG1S_10DEG') OR (fdf_tags EQ 'SG1_10DEG') $
                   OR (fdf_tags EQ 'SG1'), count) 
        if (count EQ 0) then begin
          print, prognam + ': Cannot find SG3X or SG1S passes ' + $
                 'in FDF file. Returning.'
          return
        endif else begin
          fdf_parsed = fdf_parsed.(ss[0])
          fdf_station = 'SG1S'
          ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
          ;; about how many orbits IRIS has made. BdP directed that the correction be applied
          ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
          fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
        endelse
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'SG3X'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'SG3X_10DEG') OR (fdt_tags EQ 'SG3_10DEG') $
                 OR (fdt_tags EQ 'SG3'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find SG3X passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'SG3X'
      endelse
    end

    keyword_set(sg6x): begin
      ss = where((fdf_tags EQ 'SG6X_10DEG') OR (fdf_tags EQ 'SG6_10DEG') $
                 OR (fdf_tags EQ 'SG6'), count)
      if (count EQ 0) then begin
        ss = where((fdf_tags EQ 'SG1S_10DEG') OR (fdf_tags EQ 'SG1_10DEG') $
                   OR (fdf_tags EQ 'SG1'), count)
        if (count EQ 0) then begin
          print, prognam + ': Cannot find SG6X or SG1S passes ' + $
                 'in FDF file. Returning.'
          return
        endif else begin
          fdf_parsed = fdf_parsed.(ss[0])
          fdf_station = 'SG1S'
          ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
          ;; about how many orbits IRIS has made. BdP directed that the correction be applied
          ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
          fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
        endelse
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'SG6X'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'SG6X_10DEG') OR (fdt_tags EQ 'SG6_10DEG') $
                 OR (fdt_tags EQ 'SG6'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find SG6X passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'SG6X'
      endelse
    end

    keyword_set(sg22x): begin
      ss = where((fdf_tags EQ 'SG22X_10DEG') OR (fdf_tags EQ 'SG22_10DEG') $
                 OR (fdf_tags EQ 'SG22'), count)
      if (count EQ 0) then begin
        ss = where((fdf_tags EQ 'SG1S_10DEG') OR (fdf_tags EQ 'SG1_10DEG') $
                   OR (fdf_tags EQ 'SG1'), count)
        if (count EQ 0) then begin
          print, prognam + ': Cannot find SG22X or SG1S passes ' + $
                 'in FDF file. Returning.'
          return
        endif else begin
          fdf_parsed = fdf_parsed.(ss[0])
          fdf_station = 'SG1S'
          ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
          ;; about how many orbits IRIS has made. BdP directed that the correction be applied
          ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
          fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
        endelse
      endif else begin
        fdf_parsed = fdf_parsed.(ss[0])
        fdf_station = 'SG22X'
        ;; This step is a correction for a fundamental disagrement between the FDF and FDT groups
        ;; about how many orbits IRIS has made. BdP directed that the correction be applied
        ;; to the FDF pass number because the FDT number needs to stay consistent with NORAD TLEs.
        fdf_parsed.pass = fdf_parsed.pass + long(pass_offset[0])
      endelse
      ss = where((fdt_tags EQ 'SG22X_10DEG') OR (fdt_tags EQ 'SG22_10DEG') $
                 OR (fdt_tags EQ 'SG22'), count)
      if (count EQ 0) then begin
        print, prognam + ': Cannot find SG22X passes in FDT file. Returning.'
        return
      endif else begin
        fdt_parsed = fdt_parsed.(ss[0])
        fdt_station = 'SG22X'
      endelse
    end

    else: begin
      print, prognam + ': None of the station keywords switches were ' + $
             'called. Returning.'
      return
    end

  endcase

  fdt_parsed_saved = fdt_parsed

  ;; Set tolerances on the fields.
  tol_pass        =  0b   ;; []
  tol_st_time     = 15d   ;; [sec]
  tol_en_time     = 15d   ;; [sec]
  tol_duration    = 30d   ;; [sec]
  tol_max_el      =  0.5d ;; [deg]
  tol_max_el_time = 15d   ;; [sec]

  match_template = {view_match_type,                     $
                    file_type         :       file_type, $
                    fdf_filename      :        fdf_file, $
                    fdt_filename      :        fdt_file, $
                    fdf_station       :     fdf_station, $
                    fdt_station       :     fdt_station, $
                    fdf_index         :             -1L, $
                    fdt_index         :             -1L, $
                    fdf_pass          :              0L, $
                    fdt_pass          :              0L, $
                    delta_pass        :              0L, $
                    tol_pass          :        tol_pass, $
                    fdf_st_time       :              '', $
                    fdt_st_time       :              '', $
                    delta_st_time     :              0d, $
                    tol_st_time       :     tol_st_time, $
                    fdf_en_time       :              '', $
                    fdt_en_time       :              '', $
                    delta_en_time     :              0d, $
                    tol_en_time       :     tol_en_time, $
                    fdf_duration      :              0d, $
                    fdt_duration      :              0d, $
                    delta_duration    :              0d, $
                    tol_duration      :    tol_duration, $
                    fdf_max_el        :              0d, $
                    fdt_max_el        :              0d, $
                    delta_max_el      :              0d, $
                    tol_max_el        :      tol_max_el, $
                    fdf_max_el_time   :              '', $
                    fdt_max_el_time   :              '', $
                    delta_max_el_time :              0d, $
                    tol_max_el_time   : tol_max_el_time   }


;; =============================================
;; Main loop to find matches.

  ;; These are for bookkeeping.
  N_fdf = n_elements(fdf_parsed)
  fdt_indices = lindgen(n_elements(fdt_parsed))

  ;; Prepare simple pass and time arrays to work with.
  fdt_pass = fdt_parsed.pass
  fdf_st_tai = anytim2tai(fdf_parsed.start_time)
  fdt_st_tai = anytim2tai(fdt_parsed.start_time)
  fdf_en_tai = anytim2tai(fdf_parsed.end_time)
  fdt_en_tai = anytim2tai(fdt_parsed.end_time)
  fdf_mel_tai = anytim2tai(fdf_parsed.max_elevation_time)
  fdt_mel_tai = anytim2tai(fdt_parsed.max_elevation_time)

  ;; We seed these variables with an empty first element so that the main loop
  ;; doesn't have to handle initialization.
  ;; At the end, we'll delete these empties.
  sub_matches = match_template
  fdf_unmatched = match_template

  for i_fdf = 0L,(N_fdf-1) do begin

    last_fdf = i_fdf

    ss = where(fdt_pass EQ fdf_parsed[i_fdf].pass, count)

    case count of

      0: begin
        unmatched_line = match_template
        unmatched_line.fdf_index = i_fdf
        unmatched_line.fdf_pass = fdf_parsed[i_fdf].pass
        unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        unmatched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        unmatched_line.fdf_duration = fdf_parsed[i_fdf].duration
        unmatched_line.fdf_max_el = fdf_parsed[i_fdf].max_elevation
        unmatched_line.fdf_max_el_time = fdf_parsed[i_fdf].max_elevation_time
        fdf_unmatched = [fdf_unmatched, unmatched_line]
      end

      1: begin
        ss = ss[0]
        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_pass = fdf_parsed[i_fdf].pass
        matched_line.fdt_pass = fdt_parsed[ss].pass
        matched_line.delta_pass = matched_line.fdt_pass - $
                                  matched_line.fdf_pass
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        matched_line.fdt_en_time = fdt_parsed[ss].end_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_duration = fdf_parsed[i_fdf].duration
        matched_line.fdt_duration = fdt_parsed[ss].duration
        matched_line.delta_duration = matched_line.fdt_duration - $
                                      matched_line.fdf_duration
        matched_line.fdf_max_el = fdf_parsed[i_fdf].max_elevation
        matched_line.fdt_max_el = fdt_parsed[ss].max_elevation
        matched_line.delta_max_el = matched_line.fdt_max_el - $
                                    matched_line.fdf_max_el
        matched_line.fdf_max_el_time = fdf_parsed[i_fdf].max_elevation_time
        matched_line.fdt_max_el_time = fdt_parsed[ss].max_elevation_time
        matched_line.delta_max_el_time = fdt_mel_tai[ss] - fdf_mel_tai[i_fdf]
        sub_matches = [sub_matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_mel_tai = -1
          fdt_pass = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_mel_tai = fdt_mel_tai[not_ss]
          fdt_pass = fdt_pass[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end

      else: begin
        ;; If there is more than one match, we take the earliest one.
        ss_earliest = (where(fdt_st_tai[ss] EQ min(fdt_st_tai[ss])))[0]
        ss = ss[ss_earliest]

        matched_line = match_template
        matched_line.fdf_index = i_fdf
        matched_line.fdt_index = fdt_indices[ss]
        matched_line.fdf_pass = fdf_parsed[i_fdf].pass
        matched_line.fdt_pass = fdt_parsed[ss].pass
        matched_line.delta_pass = matched_line.fdt_pass - $
                                  matched_line.fdf_pass
        matched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
        matched_line.fdt_st_time = fdt_parsed[ss].start_time
        matched_line.delta_st_time = fdt_st_tai[ss] - fdf_st_tai[i_fdf]
        matched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
        matched_line.fdt_en_time = fdt_parsed[ss].end_time
        matched_line.delta_en_time = fdt_en_tai[ss] - fdf_en_tai[i_fdf]
        matched_line.fdf_duration = fdf_parsed[i_fdf].duration
        matched_line.fdt_duration = fdt_parsed[ss].duration
        matched_line.delta_duration = matched_line.fdt_duration - $
                                      matched_line.fdf_duration
        matched_line.fdf_max_el = fdf_parsed[i_fdf].max_elevation
        matched_line.fdt_max_el = fdt_parsed[ss].max_elevation
        matched_line.delta_max_el = matched_line.fdt_max_el - $
                                    matched_line.fdf_max_el
        matched_line.fdf_max_el_time = fdf_parsed[i_fdf].max_elevation_time
        matched_line.fdt_max_el_time = fdf_parsed[ss].max_elevation_time
        matched_line.delta_max_el_time = fdt_mel_tai[ss] - fdf_mel_tai[i_fdf]
        sub_matches = [sub_matches, matched_line]

        if (n_elements(fdt_st_tai) EQ 1) then begin
          ;; This branch is entered when we have run out of FDT lines to match to.
          ;; So we cause the loop to terminate early.
          i_fdf = N_fdf + 1
          fdt_parsed = -1
          fdt_st_tai = -1
          fdt_en_tai = -1
          fdt_mel_tai = -1
          fdt_pass = -1
          fdt_indices = -1
        endif else begin
          ;; Since we have matched to an FDT line, we remove it from the
          ;; list so it cannot be matched to again.
          ;; This bit changes SS to be all the lines we want to keep.
          not_ss = rest_mask(fdt_indices,ss)
          fdt_parsed = fdt_parsed[not_ss]
          fdt_st_tai = fdt_st_tai[not_ss]
          fdt_en_tai = fdt_en_tai[not_ss]
          fdt_mel_tai = fdt_mel_tai[not_ss]
          fdt_pass = fdt_pass[not_ss]
          fdt_indices = fdt_indices[not_ss]
        endelse
      end
    endcase

  endfor  ;; i_fdf loop


;; =============================================
;; Deal with any leftover unmatched FDF lines.

  if (last_fdf LT N_fdf-1) then begin
    for i_fdf = (last_fdf+1),(N_fdf-1) do begin
      unmatched_line = match_template
      unmatched_line.fdf_index = i_fdf
      unmatched_line.fdf_pass = fdf_parsed[i_fdf].pass
      unmatched_line.fdf_st_time = fdf_parsed[i_fdf].start_time
      unmatched_line.fdf_en_time = fdf_parsed[i_fdf].end_time
      unmatched_line.fdf_duration = fdf_parsed[i_fdf].duration
      unmatched_line.fdf_max_el = fdf_parsed[i_fdf].max_elevation
      unmatched_line.fdf_max_el_time = fdf_parsed[i_fdf].max_elevation_time
      fdf_unmatched = [fdf_unmatched, unmatched_line]
    endfor
  endif

  ;; Do clean-up on the seed entry for fdf_unmatched.
  if (n_elements(fdf_unmatched) GE 2) then begin
    fdf_unmatched = fdf_unmatched[1:*]
  endif else begin
    fdf_unmatched = -1
  endelse


;; =============================================
;; Deal with the leftover unmatched FDT lines.

  fdt_unmatched = match_template

  if (fdt_st_tai[0] NE -1) then begin
    N_fdt = n_elements(fdt_st_tai)
    for i_fdt = 0L,(N_fdt-1) do begin
      unmatched_line = match_template
      unmatched_line.fdt_index = i_fdt
      unmatched_line.fdt_pass = fdt_parsed[i_fdt].pass
      unmatched_line.fdt_st_time = fdt_parsed[i_fdt].start_time
      unmatched_line.fdt_en_time = fdt_parsed[i_fdt].end_time
      unmatched_line.fdt_duration = fdt_parsed[i_fdt].duration
      unmatched_line.fdt_max_el = fdt_parsed[i_fdt].max_elevation
      unmatched_line.fdt_max_el_time = fdt_parsed[i_fdt].max_elevation_time
      fdt_unmatched = [fdt_unmatched, unmatched_line]
    endfor

    ;; And do clean-up on the seed entry.
    fdt_unmatched = fdt_unmatched[1:*]
  endif else begin
    fdt_unmatched = -1
  endelse


;; =============================================
;; ...And clean up on SUB_MATCHES.

  if (n_elements(sub_matches) GE 2) then begin
    sub_matches = sub_matches[1:*]
  endif else begin
    sub_matches = -1
  endelse


;; =============================================
;; Finish.

  ;; We used up FDT_STC already, so re-get it if requested by keyword.
  if arg_present(fdt_parsed) then fdt_parsed = fdt_parsed_saved

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_match_view,        $
      fdf_file,                $
      fdt_file,                $
      file_type,               $
      matches,                 $
      fdf_unmatched,           $
      fdt_unmatched,           $
      fdf_parsed  =fdf_parsed, $
      fdt_parsed  =fdt_parsed, $
      pass_offset =pass_offset

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_match_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Compare two files of VIEW_xxDEG event lists to determine unique matches
;   for all of the possible stations/antennae.
;
;   Two named files (FDF_FILE and FDT_FILE), of type 'view_10deg' or 'view_5deg',
;   are read in as IDL structure arrays. Certain fields are compared for "close
;   enough" matching to assign unique pairs, stored in MATCHES. For matched pairs,
;   the differences ("deltas") on the data fields are calculated and stored.
;   Leftover entries are stored in FDF_UNMATCHED and FDT_UNMATCHED. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_match_view, fdf_file, fdt_file, file_type, matches, fdf_unmatched, $
;     fdt_unmatched, [fdf_parsed=fdf_parsed], [fdt_parsed=fdt_parsed]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "view_10deg" or "view_5deg",
;              produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "view_10deg" or "view_5deg",
;              produced by the LM Flight Dynamics Team.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
; OUTPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "view_xxdeg" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   FDF_PARSED=FDF_PARSED - [Optional] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED=FDT_PARSED - [Optional] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-11' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform input checks.

  prognam = 'IRIS_FD_MATCH_VIEW'


;; =============================================
;; Get matches for each station.

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_mgs, $
    fdf_unmatched_mgs, fdt_unmatched_mgs, /mgs, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_waps, $
    fdf_unmatched_waps, fdt_unmatched_waps, /waps, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_asfs, $
    fdf_unmatched_asfs, fdt_unmatched_asfs, /asfs, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_sg1s, $
    fdf_unmatched_sg1s, fdt_unmatched_sg1s, /sg1s, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_sg3x, $
    fdf_unmatched_sg3x, fdt_unmatched_sg3x, /sg3x, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_sg6x, $
    fdf_unmatched_sg6x, fdt_unmatched_sg6x, /sg6x, pass_offset=pass_offset

  iris_fd_sub_match_view, fdf_file, fdt_file, file_type, matches_sg22x, $
    fdf_unmatched_sg22x, fdt_unmatched_sg22x, /sg22x, pass_offset=pass_offset


;; =============================================
;; Create the overarching structures.

  matches = {                              $
             file_type    :     file_type, $
             fdf_filename :      fdf_file, $
             fdt_filename :      fdt_file, $
             mgs          :   matches_mgs, $
             waps         :  matches_waps, $
             asfs         :  matches_asfs, $
             sg1s         :  matches_sg1s, $
             sg3x         :  matches_sg3x, $
             sg6x         :  matches_sg6x, $
             sg22x        : matches_sg22x   }

  fdf_unmatched = {                                    $
                   file_type    :           file_type, $
                   fdf_filename :            fdf_file, $
                   fdt_filename :            fdt_file, $
                   mgs          :   fdf_unmatched_mgs, $
                   waps         :  fdf_unmatched_waps, $
                   asfs         :  fdf_unmatched_asfs, $
                   sg1s         :  fdf_unmatched_sg1s, $
                   sg3x         :  fdf_unmatched_sg3x, $
                   sg6x         :  fdf_unmatched_sg6x, $
                   sg22x        : fdf_unmatched_sg22x   }

  fdt_unmatched = {                                    $
                   file_type    :           file_type, $
                   fdf_filename :            fdf_file, $
                   fdt_filename :            fdt_file, $
                   mgs          :   fdt_unmatched_mgs, $
                   waps         :  fdt_unmatched_waps, $
                   asfs         :  fdt_unmatched_asfs, $
                   sg1s         :  fdt_unmatched_sg1s, $
                   sg3x         :  fdt_unmatched_sg3x, $
                   sg6x         :  fdt_unmatched_sg6x, $
                   sg22x        : fdt_unmatched_sg22x   }


;; =============================================
;; Reload the "unparsed" structures, in case they weren't
;; caught by the subroutines.

  fdf_parsed = iris_fdf2struct(fdf_file)
  fdt_parsed = iris_fdf2struct(fdt_file)


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

FUNCTION iris_fd_padded_hgram,   $
           data,                 $
           binsize   =binsize,   $
           locations =locations, $
           nodata    =nodata

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_plot
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Return data's histogram, created such that the bins are centered on
;   the origin and there is one bin of zero padding at either end.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;    hist = iris_fd_padded_hgram(data, binsize=binsize, locations=locations, $
;                                [/nodata])
;
; INPUTS:
;
;   DATA    -  [Mandatory] (number, array of size [N_matched_events])
;              The values for which the histogram will be created.
;
;   BINSIZE -  [Mandatory] (number, scalar)
;              This is the binsize to use for the histogram.
;
;   /NODATA -  [Optional] (Boolean switch)
;              This switch forces the code to ignore DATA and create a 
;              three-bin histogram with values of zero. This is useful for
;              creating the plots anyway when there is no good data.
;
; OUTPUTS:
;
;   Returns -  [Mandatory] (integer, array of size [N_bins])
;              Histogram values for DATA.
;
;   LOCATIONS=LOCATIONS -  [Optional] (number, array of size [N_bins])
;              The starting locations for each bin.
;
; 
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-10' ;--- (M.Weber) Written.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PADDED_HGRAM'


;; =============================================
;; Pad the data at places that will center the bins on the origin.

  ;; Don't want to overwrite data as an output.
  if (keyword_set(nodata) EQ 1B) then data2 = 0.0 else data2 = data

  ;; HIGH_PADDING needs a tiny push because <histogram> is twitchy with
  ;; boundaries.
  low_padding  = (floor(min(data2)/binsize-1.5)+0.50000)*binsize
  high_padding = (floor(max(data2)/binsize+0.5)+0.50001)*binsize

  padded_data = [low_padding, high_padding, data2]


;; =============================================
;; Calculate histogram and remove padded data elements.

  hist = histogram(padded_data, binsize=binsize, locations=locations)

  if (keyword_set(nodata) EQ 1B) then begin
    ;; This case is useful because LOCATIONS was created.
    hist[*] = 0
  endif else begin
    nh = n_elements(hist)
    hist[0] = hist[0] - 1
    hist[nh-1] = hist[nh-1] - 1
  endelse


;; =============================================
;; Finish.

  return, hist

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_make_plot,    $
      data,               $
      indices,            $
      tol,                $
      binsize,            $
      units,              $
      idstring,           $
      data_name,          $
      type_name,          $
      last_win,           $
      station  =station,  $
      zone     =zone,     $
      nodevice =nodevice, $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_plot
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Given a bunch of specific info and data, create a histogram plot with
;   notes on the data statistics, and draw to an IDL graphics window or a
;   PostScript file.
;
;   The subroutine is called separately for each DELTA_xxx data field across
;   all of the matched events. Vertical dotted lines indicate the tolerance
;   range for that data, as contained in MATCHES.TOL_xxx. The upper left corner
;   of the plot prints the mean / minimum / maximum / standard deviation of the
;   distribution, as well as the total number of events. The histogram bins and
;   the plot are centered on the origin. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_make_plot, data, indices, tol, binsize, units, idstring, $
;     data_name, type_name, last_win, [station=station], [zone=zone], $
;     [/nodevice], [/pipeline]
;
;
; INPUTS:
;
;   DATA    -  [Mandatory] (number, array of size [N_matched_events])
;              These are values for MATCHES[*].DELTA_xxx, where xxx is one of
;              the data fields. These are the values that will be plotted
;              in histogram mode.
;
;   INDICES -  [Mandatory] (integer, array of size [N_usuable_matched_events])
;              Some of the data values from the matches may be corrupted.
;              DATA[INDICES] should be the subset of DATA which is usable.
;
;   TOL     -  [Mandatory] (number, scalar)
;              This is MATCHES[0].TOL_xxx, where xxx is one of the data fields.
;              I.e., this is the listed tolerance limit for this data field.
;
;   BINSIZE -  [Mandatory] (number, scalar)
;              This is the binsize to use for the histogram.
;
;   UNITS   -  [Mandatory] (string, scalar)
;              Units for DATA.
;
;   IDSTRING -  [Mandatory] (string, scalar)
;              This is a substring that is useful for naming the plotfile.
;              It is an output of the subroutine <iris_fd_make_idstring>.
;
;   DATA_NAME -  [Mandatory] (string, scalar)
;              The actual name xxx of the data field in DELTA_xxx, as a string.
;              For labeling.
;
;   TYPE_NAME -  [Mandatory] (string, scalar)
;              The type of FD file that the data came from. E.g., 'AAZ',
;              'eclipse', 'events', etc.
;
;   LAST_WIN -  [Mandatory] (number, scalar)
;              This is the window ID of the last IDL graphics window. The
;              plot will be created in window LAST_WIN+1, and LAST_WIN will
;              be incremented.
;
;   STATION=STATION -  [Optional] (string, scalar)
;              One of the possible station names for file-types "view_10deg"
;              and "view_5deg". For labeling.
;
;   ZONE=ZONE -  [Optional] (string, scalar)
;              One of the possible radiation zone names for file-type
;              "radzones".
;
;   /NODEVICE - [Optional] (Boolean switch)
;              This switch forces the code to **NOT** open and close an
;              IDL graphics window (pipeline=0) or PostScript device
;              (pipeline=1). This is useful for sending multiple plots
;              to a single window or file.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plot will be written to the PostScript device, instead of
;              to an IDL graphics window.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-10' ;--- (M.Weber) Written.
progver = 'v2013-Jul-15' ;--- (M.Weber) Written. Added "N_matches > 1" when
;                             indices is empty. Error to calculate lindgen(0).
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MAKE_PLOT'

  qpipe = keyword_set(pipeline)
  qnodev = keyword_set(nodevice)

  N_matches = n_elements(data)
  if ((N_matches EQ 1) AND (data[0] EQ -9999)) then N_matches = 0L

  if (indices[0] EQ -1L) then begin
    nodata = 1
    not_indices = lindgen(N_matches > 1)
    N_matches = 0L
  endif else begin
    nodata = 0
    not_indices = rest_mask(data, indices)
    data = data[indices]
  endelse


;; =============================================
;; Create histogram.

  ;; This subroutine calculates the histogram, pads the ends with zeros
  ;; so that the PSYM=10 plot looks better, and centers the bins on the origin
  ;; for symmetry. The /NODATA keyword gives back some zeros to make an empty
  ;; plot with.
  hist = iris_fd_padded_hgram(data, binsize=binsize, locations=loc, $
                              nodata=nodata)


;; =============================================
;; Prepare for plotting.

  ;; The "x-range" of the plot window should be as wide as possible to show
  ;; all of the histogram and the tolerance limits.
  Nh = n_elements(hist)
  plot_min = min([loc[1], -1d*tol])
  plot_max = max([loc[Nh-1],  tol])

  ;; Then we nudge the limits a little wider so that the interesting bits don't
  ;; run right off the edge of the plot boundaries.
  plot_min = plot_min * 1.1
  plot_max = plot_max * 1.1

  ;; And nudge again if the plot range is less than ~3 bins wide.
  if (3.0*binsize GE (plot_max-plot_min)) then begin
    plot_min = plot_min * 2.0
    plot_max = plot_max * 2.0
  endif

  ;; Prepare plotting device.
  if (qnodev EQ 1) then begin              ;; If /nodevice EQ True...
    if qpipe then begin
      chsiz1 = 1
      chsiz2 = 1
      chsiz3 = 4
      pscl = 20.
    endif else begin
      chsiz1 = 3
      chsiz2 = 2
      chsiz3 = 5
      pscl = 1.
    endelse
  endif else begin                         ;; Else if /nodevice EQ False...
    if qpipe then begin
      set_plot, 'ps'
      device, file='iris_fd_plotfile_' + idstring + '_' + data_name + '.ps'
      chsiz1 = 1
      chsiz2 = 1
      chsiz3 = 3
      pscl = 20
    endif else begin
      last_win = last_win + 1
      wdef, last_win, 800, 600
      chsiz1 = 2
      chsiz2 = 2
      chsiz3 = 4
      pscl = 1.
    endelse
  endelse


;; =============================================
;; Plot.

  ;; There is a small shift to the abscissa to make the bins center correctly.
  tit1 = 'Dist. of DELTA_'+data_name+'s for '+type_name
  if (keyword_set(station) EQ 1B) then tit1 = '[' + station + '] ' + tit1
  if (keyword_set(zone) EQ 1B) then tit1 = '[' + zone + '] ' + tit1
  plot, loc+(0.5*binsize), hist, psym=10, $
        xr=[plot_min, plot_max], /xstyle, $
        xtit='DELTA_'+data_name+' (FDT - FDF) [' + units + ']', $
        ytit='Num. usable matches', tit=tit1, charsize=chsiz1

  ;; Draw dotted lines to mark the tolerance window.
  if (tol EQ 0) then tol = 0.5
  plots, [0,0]-tol, !y.crange, linesty=1
  plots, [0,0]+tol, !y.crange, linesty=1

  ;; Add text for the data stats.
  text1 = 'mean/min/ max/ stdev/num'
  num_data = total(hist, /integer)
  if (num_data EQ 0) then begin             ;; If no data...
    text2 = ' -- / --/  --/ -- / --'
  endif else begin
    if (num_data EQ 1) then begin           ;; Else if only one datum...
      mean_data = data
      stdev_data = 0.0
    endif else begin                        ;; Else if multiple data...
      stdev_data = stdev(data, mean_data)
    endelse
    min_data = min(data)
    max_data = max(data)
    text2 = string(mean_data,  format='(F6.2)') + '/' + $
            string(min_data,   format='(F6.2)') + '/' + $
            string(max_data,   format='(F6.2)') + '/' + $
            string(stdev_data, format='(F6.2)') + '/' + $
            strcompress(num_data, /rem)
  endelse
  x_data = !x.crange[0]
  y_data = !y.crange[1]
  iris_fd_data2device, x_in=x_data, y_in=y_data, x_out=x0, y_out=y1
  xyouts, x0+20, y1-30*pscl, text1, /device, charsize=chsiz2
  xyouts, x0+20, y1-60*pscl, text2, /device, charsize=chsiz2

  ;; Indicate the number of input data that had problems.
  ;; "partials" indicates whether the initial pass was a partial overlap with the
  ;; file's time range.
  ;; "errors" indicates the number of problematic passes that weren't the
  ;; first pass.
  text3 = 'matches/partials/errs/flags'
  if (num_data EQ N_matches) then begin
    N_partials = 0L
  endif else begin
    if (not_indices[0] EQ 0L) then N_partials = 1 else N_partials = 0
  endelse
  N_errors = N_matches - num_data - N_partials
  ss = where(abs(data) GT tol, N_flags)
  text4 = strcompress(N_matches,  /rem) + '/' + $
          strcompress(N_partials, /rem) + '/' + $
          strcompress(N_errors,   /rem) + '/' + $
          strcompress(N_flags,    /rem)
  x_data = !x.crange[1]
  iris_fd_data2device, x_in=x_data, x_out=x1
  xyouts, x1-20, y1-30*pscl, text3, /device, align=1, charsize=chsiz2
  xyouts, x1-20, y1-60*pscl, text4, /device, align=1, charsize=chsiz2

  if (num_data EQ 0) then begin
    text5 = 'NO DATA'
    x_data = mean(!x.crange)
    y_data = mean(!y.crange)
    iris_fd_data2device, x_in=x_data, y_in=y_data, x_out=xmid, y_out=ymid
    xyouts, xmid, ymid, text5, /device, align=0.5, charsize=chsiz3
  endif


;; =============================================
;; Close and finish.

  ;; We are finished generating this plot.
  if (qnodev EQ 0) then begin              ;; If /nodevice EQ False...
    if qpipe then begin
      device, /close
      set_plot, 'x'
    endif
  endif

  return

END






;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_aaz,  $
      fdf_file,        $
      fdt_file,        $
      matches,         $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_aaz
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "aaz", plot the histograms
;   of errors ("deltas") for each data field.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the 
;   matched events. Vertical dotted lines indicate the tolerance range for that 
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot 
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_aaz, fdf_file, fdt_file, matches, [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "aaz", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "aaz", produced by the LM Flight
;              Dynamics Team.
;
;   MATCHES -  [Mandatory] (structure of type "AAZ_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_AAZ'

  ;; Check for whether input is of correct type and how many matches there are.
  qmatches = 0
  if (size(matches, /type) EQ 8) then begin
    if (tag_names(matches, /st) EQ 'AAZ_MATCH_TYPE') then qmatches = 1
  endif 

  type_name = 'AAZ'

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(fdf_file, fdt_file)
  endif

  ;; For windows...
  last_win = -1

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Plot the distribution for DELTA_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_time)
    tol_data = matches[0].tol_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_time NE error_tstring) AND $
               (matches.fdt_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 20d  ;; [sec]
  data_units = 'sec'
  data_name = 'TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_ANGLE.

  ;; Two blocks to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_angle)
    tol_data = matches[0].tol_angle
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_angle NE error_number) AND $
               (matches.fdt_angle NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.04d  ;; [deg]
  data_units = 'deg'
  data_name = 'ANGLE'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_eclipse, $
      fdf_file,           $
      fdt_file,           $
      matches,            $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_eclipse
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "eclipse", plot the histograms
;   of errors ("deltas") for each data field.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the
;   matched events. Vertical dotted lines indicate the tolerance range for that
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_eclipse, fdf_file, fdt_file, matches, [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "eclipse", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "eclipse", produced by the LM Flight
;              Dynamics Team.
;
;   MATCHES -  [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_ECLIPSE'

  ;; Check for whether input is of correct type and how many matches there are.
  qmatches = 0
  if (size(matches, /type) EQ 8) then begin
    if (tag_names(matches, /st) EQ 'ECLIPSE_MATCH_TYPE') then qmatches = 1
  endif

  type_name = 'ECLIPSE'

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(fdf_file, fdt_file)
  endif

  ;; For windows...
  last_win = -1

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Plot the distribution for DELTA_ST_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_st_time)
    tol_data = matches[0].tol_st_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_st_time NE error_tstring) AND $
               (matches.fdt_st_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 40d  ;; [sec]
  data_units = 'sec'
  data_name = 'ST_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_en_time)
    tol_data = matches[0].tol_en_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_en_time NE error_tstring) AND $
               (matches.fdt_en_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 40d  ;; [sec]
  data_units = 'sec'
  data_name = 'EN_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_DURATION.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_duration)
    tol_data = matches[0].tol_duration
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_duration NE error_number) AND $
               (matches.fdt_duration NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 20d  ;; [sec]
  data_units = 'sec'
  data_name = 'DURATION'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_ST_PASS.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_st_pass)
    tol_data = matches[0].tol_st_pass
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_st_pass NE error_number) AND $
               (matches.fdt_st_pass NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 1d  ;; [number]
  data_units = 'n/a'
  data_name = 'ST_PASS'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_PASS.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_en_pass)
    tol_data = matches[0].tol_en_pass
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_en_pass NE error_number) AND $
               (matches.fdt_en_pass NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 1d  ;; [number]
  data_units = 'n/a'
  data_name = 'EN_PASS'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_C_COND.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_c_cond)
    tol_data = matches[0].tol_c_cond
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ;; There is no error code for text strings.
    ss = lindgen(n_elements(data))
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 1d  ;; [Boolean]
  data_units = 'T/F: mismatched?'
  data_name = 'C_COND'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_W_COND.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_w_cond)
    tol_data = matches[0].tol_w_cond
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ;; There is no error code for text strings.
    ss = lindgen(n_elements(data))
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 1d  ;; [Boolean]
  data_units = 'T/F: mismatched?'
  data_name = 'W_COND'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_TOT_DUR.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_tot_dur)
    tol_data = matches[0].tol_tot_dur
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_tot_dur NE error_number) AND $
               (matches.fdt_tot_dur NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 20d  ;; [sec]
  data_units = 'sec'
  data_name = 'TOT_DUR'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_events, $
      fdf_file,          $
      fdt_file,          $
      matches,           $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_events
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "events", plot the histograms
;   of errors ("deltas") for each data field.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the
;   matched events. Vertical dotted lines indicate the tolerance range for that
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_events, fdf_file, fdt_file, matches, [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "events", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "events", produced by the LM Flight
;              Dynamics Team.
;
;   MATCHES -  [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-05' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_EVENTS'

  ;; Check for whether input is of correct type and how many matches there are.
  qmatches = 0
  if (size(matches, /type) EQ 8) then begin
    if (tag_names(matches, /st) EQ 'EVENTS_MATCH_TYPE') then qmatches = 1
  endif

  type_name = 'EVENTS'

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(fdf_file, fdt_file)
  endif

  ;; For windows...
  last_win = -1

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Plot the distribution for DELTA_PASS.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_pass)
    tol_data = matches[0].tol_pass
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_pass NE error_number) AND $
               (matches.fdt_pass NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 1d  ;; [number]
  data_units = 'n/a'
  data_name = 'PASS'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_ST_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_st_time)
    tol_data = matches[0].tol_st_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_st_time NE error_tstring) AND $
               (matches.fdt_st_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'ST_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_en_time)
    tol_data = matches[0].tol_en_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_en_time NE error_tstring) AND $
               (matches.fdt_en_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'EN_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_APOGEE.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_apogee)
    tol_data = matches[0].tol_apogee
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_apogee NE error_number) AND $
               (matches.fdt_apogee NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 2d  ;; [km]
  data_units = 'km'
  data_name = 'APOGEE'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_T_APOG.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_t_apog)
    tol_data = matches[0].tol_t_apog
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_t_apog NE error_tstring) AND $
               (matches.fdt_t_apog NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'T_APOG'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_PERIGEE.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_perigee)
    tol_data = matches[0].tol_perigee
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_perigee NE error_number) AND $
               (matches.fdt_perigee NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 2d  ;; [km]
  data_units = 'km'
  data_name = 'PERIGEE'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_T_PERIG.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_t_perig)
    tol_data = matches[0].tol_t_perig
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_t_perig NE error_tstring) AND $
               (matches.fdt_t_perig NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'T_PERIG'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_PERIOD.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_period)
    tol_data = matches[0].tol_period
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_period NE error_number) AND $
               (matches.fdt_period NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'PERIGEE'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_RI_ASC.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_ri_asc)
    tol_data = matches[0].tol_ri_asc
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_ri_asc NE error_number) AND $
               (matches.fdt_ri_asc NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'RI_ASC'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_LO_ASC.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_lo_asc)
    tol_data = matches[0].tol_lo_asc
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_lo_asc NE error_number) AND $
               (matches.fdt_lo_asc NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'LO_ASC'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_LO_DESC.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_lo_desc)
    tol_data = matches[0].tol_lo_desc
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_lo_desc NE error_number) AND $
               (matches.fdt_lo_desc NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'LO_DESC'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_ground, $
      fdf_file,          $
      fdt_file,          $
      matches,           $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_ground
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "ground", plot the histograms
;   of errors ("deltas") for each data field.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the
;   matched events. Vertical dotted lines indicate the tolerance range for that
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_ground, fdf_file, fdt_file, matches, [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "ground", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "ground", produced by the LM Flight
;              Dynamics Team.
;
;   MATCHES -  [Mandatory] (structure of type "GROUND_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_GROUND'

  ;; Check for whether input is of correct type and how many matches there are.
  qmatches = 0
  if (size(matches, /type) EQ 8) then begin
    if (tag_names(matches, /st) EQ 'GROUND_MATCH_TYPE') then qmatches = 1
  endif

  type_name = 'GROUND'

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(fdf_file, fdt_file)
  endif

  ;; For windows...
  last_win = -1

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Plot the distribution for DELTA_TIME.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_time)
    tol_data = matches[0].tol_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_time NE error_tstring) AND $
               (matches.fdt_time NE error_tstring)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 2d  ;; [sec]
  data_units = 'sec'
  data_name = 'TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_LAT.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_lat)
    tol_data = matches[0].tol_lat
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_lat NE error_number) AND $
               (matches.fdt_lat NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'LAT'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_LON.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_lon)
    tol_data = matches[0].tol_lon
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_lon NE error_number) AND $
               (matches.fdt_lon NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'LON'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_ALT.

  ;; One block to generalize to the term "data":
  if (qmatches EQ 1) then begin
    data = double(matches.delta_alt)
    tol_data = matches[0].tol_alt
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((matches.fdf_alt NE error_number) AND $
               (matches.fdt_alt NE error_number)      )
  endif else begin
    data = 0
    tol_data = 0
    ss = -1L
  endelse
  binsize_data = 2d  ;; [km]
  data_units = 'km'
  data_name = 'ALT'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, type_name, last_win, pipeline=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_plot_radzones, $
      matches,                 $
      zone_name,               $
      last_win,                $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_plot_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs of only one zone, of type "radzones",
;   plot the histograms of errors ("deltas") for a single data field as
;   specified by keyword.
;
;   For a single data field, DELTA_xxx is plotted as a histogram across all of
;   the matched events. Vertical dotted lines indicate the tolerance range for
;   that data, as contained in SUB_MATCHES.TOL_xxx. The upper left corner of the
;   plot prints the mean / minimum / maximum / standard deviation of the
;   distribution, as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_sub_plot_radzones, sub_matches, zone_name, last_win, [/pipeline]
;
;
; INPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "radzones" is a structure with tags
;              for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   ZONE_NAME -  [Mandatory] (string, scalar)
;              One of the possible radiation zone names for file-type
;              "radzones". (E.g., 'SAA', 'N_HLZ', or 'S_HLZ'.)
;
;   LAST_WIN - [Mandatory] (integer, scalar)
;              The most recent window ID that was plotted to.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   none
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) The program will crash if one does not set exactly one of the data field
;      switches.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Split off from <iris_fd_plot_radzones>
;                             in a major refactoring.
progver = 'v2013-Jul-15' ;--- (M.Weber) Changed "if qpipe" to "if (qpipe AND
;                             q_matches)" because sub_matches was used inside
;                             the IF block.
progver = 'v2013-Jul-15' ;--- (M.Weber) Undid previous fix. Replaced fix by
;                             getting filenames from MATCHES instead of from
;                             SUB_MATCHES.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_SUB_PLOT_RADZONES'

  file_type = 'radzones'

  ;; Check for and extract the correct substructure.
  ss = where(tag_names(matches) EQ strup(zone_name), count)
  if (count EQ 1) then sub_matches = matches.(ss[0]) else sub_matches = -1

  ;; Check for whether input is of correct type and how many matches there are.
  ;; Presume guilt until proven innocent.
  q_matches = 0b
  if (size(sub_matches, /type) EQ 8) then begin
    if (tag_names(sub_matches, /st) EQ 'RADZONES_MATCH_TYPE') then begin
      q_matches = 1b
    endif
  endif

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(matches[0].fdf_filename, $
                                     matches[0].fdt_filename   )
    set_plot, 'ps'
    device, file='iris_fd_plotfile_' + idstring + '_' + zone_name + '.ps'
    device, xsize=6, ysize=4.5, /inches
  endif else begin
    last_win = last_win + 1
    wdef, last_win, 800*2.5, 600*2
    !p.multi = [0,2,2]
  endelse


;; =============================================
;; Plot the distribution for DELTA_ST_TIME_OUTER.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_st_time_outer)
    tol_data = sub_matches[0].tol_st_time_outer
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_st_time_outer NE error_tstring) AND $
               (sub_matches.fdt_st_time_outer NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'ST_TIME_OUTER'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=zone_name, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_ST_TIME_INNER.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_st_time_inner)
    tol_data = sub_matches[0].tol_st_time_inner
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_st_time_inner NE error_tstring) AND $
               (sub_matches.fdt_st_time_inner NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'ST_TIME_INNER'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=zone_name, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_TIME_INNER.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_en_time_inner)
    tol_data = sub_matches[0].tol_en_time_inner
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_en_time_inner NE error_tstring) AND $
               (sub_matches.fdt_en_time_inner NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'EN_TIME_INNER'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=zone_name, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_TIME_OUTER.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_en_time_outer)
    tol_data = sub_matches[0].tol_en_time_outer
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_en_time_outer NE error_tstring) AND $
               (sub_matches.fdt_en_time_outer NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'EN_TIME_OUTER'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=zone_name, /nodevice, pipeline=pipeline


;; =============================================
;; Close the device.

  if qpipe then begin
    device, /close
    set_plot, 'x'
  endif else begin
    !p.multi = 0
  endelse


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_radzones, $
      matches,         $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "radzones", plot the histograms
;   of errors ("deltas") for each data field, for all of the possible zones.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the
;   matched events. Vertical dotted lines indicate the tolerance range for that
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_radzones, matches, [/pipeline]
;
;
; INPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "radzones" is a structure with tags
;              for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of
;              to IDL graphics windows.
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring to allow for HLZs
;                             and much other stuff. Split off a lot of the
;                             functionality to <iris_fd_sub_plot_radzones>.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_RADZONES'

  ;; For windows...
  last_win = -1


;; =============================================
;; Make lots of plots.

  ;; Simply extract each middle layer (zones) in turn and send to the
  ;; sub-plotter.
  iris_fd_sub_plot_radzones, matches, 'SAA',   last_win, pipe=pipeline
  iris_fd_sub_plot_radzones, matches, 'N_HLZ', last_win, pipe=pipeline
  iris_fd_sub_plot_radzones, matches, 'S_HLZ', last_win, pipe=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_plot_view,    $
      sub_matches,            $
      file_type,              $
      station,                $
      last_win,               $
      pipeline =pipeline,     $
      fdf_file =fdf_filename, $
      fdt_file =fdt_filename

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_plot_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs of only one station/antenna, of type
;   "view_10deg" or "view_5deg", plot the histograms of errors ("deltas") for a
;   single data field as specified by keyword.
;
;   For a single data field, DELTA_xxx is plotted as a histogram across all of
;   the matched events. Vertical dotted lines indicate the tolerance range for
;   that data, as contained in SUB_MATCHES.TOL_xxx. The upper left corner of the
;   plot prints the mean / minimum / maximum / standard deviation of the
;   distribution, as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_sub_plot_view, sub_matches, file_type, station, last_win, [/pipeline]
;
;
; INPUTS:
;
;   SUB_MATCHES -  [Mandatory] (structure of type "VIEW_MATCH_TYPE",
;              array of size [N_matched_events])
;              Each element of SUB_MATCHES contains data about a unique matched
;              pair of FDF and FDT events for a single station/antenna. See the
;              Manual for more information.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   STATION -  [Mandatory] (string, scalar)
;              The station name as a string, to be included in the plot title.
;              Here is the recommended list: {MGS, WAPS, ASF, SG1S, SG3X, SG6X,
;              SG22X}.
;
;   LAST_WIN - [Mandatory] (integer, scalar)
;              The most recent window ID that was plotted to.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   none
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) The program will crash if one does not set exactly one of the data field
;      switches.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-10' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Changed STATION from
;                             a keyword to a mandatory parameter. Removed
;                             NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
progver = 'v2013-Jul-15' ;--- (M.Weber) Changed "if qpipe" to "if (qpipe AND
;                             q_matches)" because sub_matches was used inside
;                             the IF block.
progver = 'v2013-Jul-15' ;--- (M.Weber) Undid previous fix. Replaced fix by
;                             getting filenames from MATCHES instead of from
;                             SUB_MATCHES.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_SUB_PLOT_VIEW'

  ;; Check for whether input is of correct type and how many matches there are.
  ;; Presume guilt until proven innocent.
  q_matches = 0b
  if (size(sub_matches, /type) EQ 8) then begin
    if (tag_names(sub_matches, /st) EQ 'VIEW_MATCH_TYPE') then begin
      q_matches = 1b
    endif
  endif

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d

  qpipe = keyword_set(pipeline)
  if qpipe then begin
    idstring = iris_fd_make_idstring(fdf_filename, fdt_filename)
    set_plot, 'ps'
    device, file='iris_fd_plotfile_' + idstring + '_' + station + '.ps'
    device, xsize=6, ysize=4.5, /inches
  endif else begin
    last_win = last_win + 1
    wdef, last_win, 800*3, 600*2
    !p.multi = [0,3,2]
  endelse


;; =============================================
;; Plot the distribution for DELTA_ST_TIME.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_st_time)
    tol_data = sub_matches[0].tol_st_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_st_time NE error_tstring) AND $
               (sub_matches.fdt_st_time NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'ST_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=station, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_EN_TIME.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_en_time)
    tol_data = sub_matches[0].tol_en_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_en_time NE error_tstring) AND $
               (sub_matches.fdt_en_time NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'EN_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=station, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_DURATION.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_duration)
    tol_data = sub_matches[0].tol_duration
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_duration NE error_number) AND $
               (sub_matches.fdt_duration NE error_number)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'DURATION'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=station, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_MAX_EL.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_max_el)
    tol_data = sub_matches[0].tol_max_el
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_max_el NE error_number) AND $
               (sub_matches.fdt_max_el NE error_number)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 0.2d  ;; [deg]
  data_units = 'deg'
  data_name = 'MAX_EL'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=station, /nodevice, pipeline=pipeline


;; =============================================
;; Plot the distribution for DELTA_MAX_EL_TIME.

  if (q_matches EQ 1B) then begin
    data = double(sub_matches.delta_max_el_time)
    tol_data = sub_matches[0].tol_max_el_time
    ;; Identify cases with valid values. However, the subroutine
    ;; we're about to call will deal with the results.
    ss = where((sub_matches.fdf_max_el_time NE error_tstring) AND $
               (sub_matches.fdt_max_el_time NE error_tstring)      )
  endif else begin
    data = -9999
    tol_data = 1
    ss = -1L
  endelse
  binsize_data = 10d  ;; [sec]
  data_units = 'sec'
  data_name = 'MAX_EL_TIME'

  iris_fd_make_plot, data, ss, tol_data, binsize_data, data_units, idstring, $
    data_name, file_type, station=station, /nodevice, pipeline=pipeline


;; =============================================
;; Close the device.

  if qpipe then begin
    device, /close
    set_plot, 'x'
  endif else begin
    !p.multi = 0
  endelse


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_plot_view, $
      matches,         $
      file_type,       $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_plot_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "view_10deg" or "view_5deg",
;   plot the histograms of errors ("deltas") for each data field, for all of the
;   possible stations/antennae.
;
;   Each data field's DELTA_xxx is plotted as a histogram across all of the
;   matched events. Vertical dotted lines indicate the tolerance range for that
;   data, as contained in MATCHES.TOL_xxx. The upper left corner of the plot
;   prints the mean / minimum / maximum / standard deviation of the distribution,
;   as well as the total number of events. This is a subroutine of
;   IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_plot_view, matches, file_type, [/pipeline]
;
;
; INPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "view_XXdeg" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of
;              to IDL graphics windows.
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-10' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the
;                             X device.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_PLOT_VIEW'

  ;; For windows...
  last_win = -1


;; =============================================
;; Make lots of plots.

  ;; Simply extract each middle layer (stations) in turn and send to the
  ;; sub-plotter.
  iris_fd_sub_plot_view, matches.mgs,   file_type, 'MGS',   last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.waps,  file_type, 'WAPS',  last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.asfs,  file_type, 'ASFS',  last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.sg1s,  file_type, 'SG1S',  last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.sg3x,  file_type, 'SG3X',  last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.sg6x,  file_type, 'SG6X',  last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename
  iris_fd_sub_plot_view, matches.sg22x, file_type, 'SG22X', last_win, $
       pipe=pipeline, fdf_file=matches.fdf_filename, fdt_file=matches.fdt_filename


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_make_rpt_hdr1, $
      fdf_file,            $
      fdt_file,            $
      file_type,           $
      varfile,             $
      report_arr

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_rpt_hdr1 - (MAKE RePorT HeaDeR section 1)
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Add the top-level section of the comparison report to a string array.
;
;   The top-level section (header 1) is only written once, even for multipart
;   file-types (e.g., "radzones" and "view_XXdeg"). This subroutine only
;   generates the content strings from the inputs and adds them to the
;   report array. This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is
;   not intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "aaz", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "aaz", produced by the LM Flight
;              Dynamics Team.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals one of the following:
;              {'aaz', 'eclipse', 'events', 'ground, 'radzones', 'view_10deg',
;              'view_5deg'}.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;
; OUTPUTS:
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. This
;              subroutine adds new content by appending more lines to
;              REPORT_ARR. A different subroutine handles the writing of the
;              report to the screen and ASCII file.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Written.
;
;
;-
;===============================================================================


;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MAKE_RPT_HDR1'


;; =============================================
;; Generate the report header content as a string array.

  ;; We seed the array with a blank line for two reasons:
  ;; 1) So we can just add lines by concatenation.
  ;; 2) So there is a blank line appended to the report file
  ;;    before the content starts.
  report_arr = ''

  report_arr = [report_arr, $
    '===========================================================']
  report_arr = [report_arr, $
    'REPORT ON COMPARING FDT VERSUS FDF OF THIS TYPE: ' + file_type]

  report_arr = [report_arr, 'FDF filename: ' + fdf_file]
  report_arr = [report_arr, 'FDT filename: ' + fdt_file]
  report_arr = [report_arr, 'Variables saved filename: ' + varfile]


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_make_rpt_hdr2, $
      file_type,           $
      fdf_parsed,          $
      fdt_parsed,          $
      matches,             $
      fdf_unmatched,       $
      fdt_unmatched,       $
      parsed_timename,     $
      unmatched_timename,  $
      report_arr

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_rpt_hdr2 - (MAKE RePorT HeaDeR section 2)
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Add the secondary header of the comparison report to a string array.
;
;   The secondary header (header 2) reports some basic information about the
;   number of FDF/FDT entries and the number of matches and "unmatches". For the
;   simple file-types, this information only appears once. For the multipart
;   file-types (e.g., "radzones" and "view_XXdeg"), the secondary header is
;   repeated for every zone or station, since a different number of matches
;   potentially may be found for each. This subroutine only generates the
;   content strings from the inputs and adds them to the report array. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_make_rpt_hdr2, file_type, fdf_parsed, fdt_parsed, matches, $
;   IDL>   fdf_unmatched, fdt_unmatched, parsed_timename, unmatched_timename, $
;   IDL>   report_arr
;
;
; INPUTS:
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals one of the following:
;              {'aaz', 'eclipse', 'events', 'ground, 'radzones', 'view_10deg',
;              'view_5deg'}.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous",
;              array of size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. For the multipart files, FDF_PARSED must correspond
;              to the structure for a single radiation zone or station/antenna.
;              See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous",
;              array of size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. For the multipart files, FDT_PARSED must correspond
;              to the structure for a single radiation zone or station/antenna.
;              See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "xxx_MATCH_TYPE", where "xxx" is
;              one of the allowed file-types, array of size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "xxx_MATCH_TYPE", where "xxx"
;              is one of the allowed file-types, 
;              array of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "xxx_MATCH_TYPE", where "xxx"
;              is one of the allowed file-types,
;              array of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   PARSED_TIMENAME - [Mandatory] (string, scalar)
;              This string should be the tag name from FDF_PARSED for the
;              time field that is used to indicate the overall time of the
;              event. Where there are multiple times, it is usually the first one.
;
;   UNMATCHED_TIMENAME - [Mandatory] (string, scalar)
;              This string should be the tag name from FDF_UNMATCHED for the
;              time field that is used to indicate the overall time of the
;              event. Where there are multiple times, it is usually the first one.
;
;
; OUTPUTS:
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. This
;              subroutine adds new content by appending more lines to
;              REPORT_ARR. A different subroutine handles the writing of the
;              report to the screen and ASCII file.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Written.
;
;
;-
;===============================================================================


;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MAKE_RPT_HDR2'

  allowable_match_types = ['AAZ_MATCH_TYPE', 'ECLIPSE_MATCH_TYPE', $
    'EVENTS_MATCH_TYPE', 'GROUND_MATCH_TYPE', 'RADZONES_MATCH_TYPE', $
    'VIEW_MATCH_TYPE']


;; =============================================
;; Establish how many events there are in each variable.

  ;; Before we proceed, we need to check which variables are empty.
  ;; Assume guilt (zeros) until proven innocent (has legitimate members).
  N_fdf = 0L     ;; Number FDF parsed
  N_fdt = 0L     ;; Number FDT parsed
  N_mats = 0L    ;; Number MATcheS
  N_funm = 0L    ;; Number fdF UNMatched
  N_tunm = 0L    ;; Number fdT UNMatched

  if (size(fdf_parsed, /type) EQ 8) then begin
    N_fdf = n_elements(fdf_parsed)
    ss = where(tag_names(fdf_parsed) EQ strup(parsed_timename), count)
    if (count EQ 1) then begin
      fdf_times = fdf_parsed.(ss[0])
    endif else begin
      if (qpipe EQ 0) then print, prognam + $
        ": FDF_PARSED doesn't have " + strup(parsed_timename) + ". Stopping."
      stop
    endelse
  endif

  if (size(fdt_parsed, /type) EQ 8) then begin
    N_fdt = n_elements(fdt_parsed)
    ss = where(tag_names(fdt_parsed) EQ strup(parsed_timename), count)
    if (count EQ 1) then begin
      fdt_times = fdt_parsed.(ss[0])
    endif else begin
      if (qpipe EQ 0) then print, prognam + $
        ": FDT_PARSED doesn't have " + strup(parsed_timename) + ". Stopping."
      stop
    endelse
  endif

  ;; Is this a structure?
  if (size(matches, /type) EQ 8) then begin
    ;; Does it have one of the allowed names?
    ss = where(allowable_match_types EQ tag_names(matches, /st), count)
    if (count EQ 1) then begin
      ;; Is the name consistent with FILE_TYPE?
      if (strmid(strup(file_type),0,3) EQ $
          strmid(tag_names(matches, /st),0,3)) then begin
        N_mats = n_elements(matches)
      endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": MATCHES structure name doesn't match FILE_TYPE. Stopping."
        stop
      endelse
    endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": MATCHES has unrecognized structure name. Stopping."
      stop
    endelse
  endif

  ;; Is this a structure?
  if (size(fdf_unmatched, /type) EQ 8) then begin
    ;; Does it have one of the allowed names?
    ss = where(allowable_match_types EQ tag_names(fdf_unmatched, /st), count)
    if (count EQ 1) then begin
      ;; Is the name consistent with FILE_TYPE?
      if (strmid(strup(file_type),0,3) EQ $
          strmid(tag_names(fdf_unmatched, /st),0,3)) then begin
        N_funm = n_elements(fdf_unmatched)
      endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": FDF_UNMATCHED structure name doesn't match FILE_TYPE. Stopping."
        stop
      endelse
    endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": FDF_UNMATCHED has unrecognized structure name. Stopping."
      stop
    endelse
  endif
  ;; Is this a structure?
  if (size(fdt_unmatched, /type) EQ 8) then begin
    ;; Does it have one of the allowed names?
    ss = where(allowable_match_types EQ tag_names(fdt_unmatched, /st), count)
    if (count EQ 1) then begin
      ;; Is the name consistent with FILE_TYPE?
      if (strmid(strup(file_type),0,3) EQ $
          strmid(tag_names(fdt_unmatched, /st),0,3)) then begin
        N_tunm = n_elements(fdt_unmatched)
      endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": FDT_UNMATCHED structure name doesn't match FILE_TYPE. Stopping."
        stop
      endelse
    endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": FDT_UNMATCHED has unrecognized structure name. Stopping."
      stop
    endelse
  endif


;; =============================================
;; Generate the report header content to the string array.

  report_arr = [report_arr, $
    'Number of FDF/FDT entries: ' + strcompress(N_fdf,/rem) + ' / ' + $
    strcompress(N_fdt,/rem)]

  if (N_fdf EQ 0) then begin
    report_arr = [report_arr, 'FDF range of start times: NO FDF DATA.']
  endif else begin
    report_arr = [report_arr, $
      'FDF range of start times: ' + fdf_times[0] + ' / ' + fdf_times[N_fdf-1]]
  endelse

  if (N_fdt EQ 0) then begin
    report_arr = [report_arr, 'FDT range of start times: NO FDT DATA.']
  endif else begin
    report_arr = [report_arr, $
      'FDT range of start times: ' + fdt_times[0]+ ' / ' + fdt_times[N_fdt-1]]
  endelse

  report_arr = [report_arr, $
    'Number of matches / FDF unmatched / FDT unmatched: ' + $
    strcompress(N_mats,/rem) + ' / ' + strcompress(N_funm,/rem) + ' / ' + $
    strcompress(N_tunm,/rem)]

  if ((N_fdf GE 1) AND (N_tunm GE 1)) then begin
    fdf_tai = anytim2tai(fdf_times)
    ss1 = (where(tag_names(fdt_unmatched) EQ 'FDT_'+unmatched_timename))[0]
    fdt_tai = anytim2tai(fdt_unmatched.(ss1))
    ss = where((fdt_tai GE fdf_tai[0]) AND $
               (fdt_tai LE fdf_tai[N_fdf-1]), count)
  endif else begin
    count = 0L
  endelse
  report_arr = [report_arr, $
    'Number of unmatched FDT entries inside/outside of FDF time range: ' + $
    strcompress(count,/rem) + ' / ' + strcompress(N_tunm-count,/rem)]


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_make_rpt_var,     $
      matches,                $
      data_varname,           $
      data_units,             $
      report_arr,             $
      qtimestr   =qtimestr,   $
      qnumber    =qnumber,    $
      qstring    =qstring,    $
      used2match =used2match, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_make_rpt_var - (MAKE RePorT VARiable entry)
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Add an entry for a specific data field in the comparison report to a string
;   array.
;
;   For a given data field, three lines are generated which contain content
;   regarding the statistics of the data distribution. This subroutine only
;   generates the content strings from the inputs and adds them to the report
;   array. This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not
;   intended for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_make_rpt_var, matches, data_varname, data_units, report_arr, $
;   IDL>   [/qtimestr, /qnumber, /qstring], [/used2match], [/pipeline]
;
;
; INPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "xxx_MATCH_TYPE", where "xxx" is
;              one of the allowed file-types, array of size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   DATA_VARNAME - [Mandatory] (string, scalar)
;              This is the name for one of the data fields in MATCHES. The
;              statistics for this field's DELTA_xxx distribution are what
;              will be reported by the subroutine. For example, if MATCHES has
;              the grouped tags FDF_TIME, FDT_TIME, DELTA_TIME, and TOL_TIME, 
;              then 'TIME' is the name of that data field.
;
;   DATA_UNITS - [Mandatory] (string, scalar)
;              The units for differences of this data field. For example,
;              if DATA_VARNAME='TIME', then the units will be 'sec'.
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far, but
;              will usually be empty before the top-level header is addded. This
;              subroutine adds new content by appending more lines to
;              REPORT_ARR. A different subroutine handles the writing of the
;              report to the screen and ASCII file.
;
;   /QTIMESTR - ("timestring")
;   /QNUMBER  - ("number")
;   /QSTRING  - ("string")
;             - [Mandatory] (Boolean switch)
;              One and only one of these switches must be indicated. This
;              input identifies what type of data the specified variable is.
;
;   /USED2MATCH  - [Mandatory] (Boolean switch)
;              This switch indicates whether the specified variable was one of
;              the fields used to identify matches of events.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The subroutine's error messages are only printed to the screen
;              if PIPELINE=0 (i.e., if the switch is not indicated).
;
; OUTPUTS:
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. This
;              subroutine adds new content by appending more lines to
;              REPORT_ARR. A different subroutine handles the writing of the
;              report to the screen and ASCII file.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Written.
;
;
;-
;===============================================================================


;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_MAKE_RPT_VAR'

  qpipe = keyword_set(pipeline) EQ 1B

  delta_varname = 'DELTA_' + strup(data_varname)
  tol_varname   = 'TOL_'   + strup(data_varname)
  fdf_varname   = 'FDF_'   + strup(data_varname)
  fdt_varname   = 'FDT_'   + strup(data_varname)

  allowable_match_types = ['AAZ_MATCH_TYPE', 'ECLIPSE_MATCH_TYPE', $
    'EVENTS_MATCH_TYPE', 'GROUND_MATCH_TYPE', 'RADZONES_MATCH_TYPE', $
    'VIEW_MATCH_TYPE']

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  qtimestr = keyword_set(qtimestr)
  qnumber = keyword_set(qnumber)
  qstring = keyword_set(qstring)
  if ((qtimestr + qnumber + qstring) NE 1) then begin
    if (qpipe EQ 0) then print, prognam + $
      ": Failed to pick exactly one of the Qxxxx keywords. Stopping."
    stop
  endif
  if qtimestr then error_val = anytim(0,/ccsds)
  if qnumber  then error_val = -9999d
  ;; No error_val for QSTRING.


;; =============================================
;; Establish how many events there are in each variable.

  ;; Before we proceed, we need to check if MATCHES is empty.
  ;; Assume guilt (zeros) until proven innocent (has legitimate members).
  N_mats = 0L    ;; Number MATcheS

  ;; Is this a structure?
  if (size(matches, /type) EQ 8) then begin
    ;; Does it have one of the allowed names?
    ss = where(allowable_match_types EQ tag_names(matches, /st), count)
    if (count EQ 1) then begin
      N_mats = n_elements(matches)
      ;; Does it have the variable names we want?
      ss_delta = (where(tag_names(matches) EQ delta_varname, cnt_delta))[0]
      ss_tol = (where(tag_names(matches) EQ tol_varname))[0]
      if (cnt_delta EQ 1) then begin
        data = matches.(ss_delta)
        tol_data = matches[0].(ss_tol)
        tol_data_str = strcompress(string(tol_data, format='(F7.2)'), /rem)
      endif else begin
        if (qpipe EQ 0) then print, prognam + $
          ": MATCHES doesn't have DELTA_" + strup(data_varname) + ". Stopping."
        stop
      endelse
    endif else begin
      if (qpipe EQ 0) then print, prognam + $
        ": MATCHES has unrecognized structure name. Stopping."
      stop
    endelse
  endif

  ;; If no data, then go home early.
  if (N_mats EQ 0) then return


;; =============================================
;; Generate the data content to the report array.

    ;; Assume these exist if we have reached this far.
    if qstring then begin
      N_good = N_mats
      ss_good = lindgen(N_good)
    endif else begin
      ss_fdf = (where(tag_names(matches) EQ fdf_varname, cnt_fdf))[0]
      ss_fdt = (where(tag_names(matches) EQ fdt_varname, cnt_fdt))[0]
      ss_good = where((matches.(ss_fdf) NE error_val) AND $
                      (matches.(ss_fdt) NE error_val), N_good)
    endelse

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      delta_varname + ' [' + data_units + '] N_used/mean/min/max/stdev/tol: ' $
      + strcompress(N_good,/rem) + '/' + mean_data + '/' + min_data + '/' + $
      max_data + '/' + stdev_data + '/' + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      delta_varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; The next line depends upon whether this data field was used to
    ;; identify matches.
    if (keyword_set(used2match) eq 1B) then begin
      report_arr = [report_arr, $
      'This field was used to make matches, so all ' + delta_varname + ' LE ' $
      + tol_varname + '.']

    endif else begin
      ;; Indicate the number of data values that exceed the tolerance.
      if (N_flags EQ 0) then begin
        report_arr = [report_arr, delta_varname + ': PASSED']
      endif else begin
        if (N_flags EQ 1) then begin
          report_arr = [report_arr, delta_varname + $
            ':           ********************* 1 FLAG *********************']
        endif else begin
          report_arr = [report_arr, delta_varname + $
            ':           ********************* ' + strcompress(N_flags,/rem) + $
            ' FLAGS *********************']
        endelse
      endelse

    endelse


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_aaz,       $
      fdf_file,               $
      fdt_file,               $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline


;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_aaz
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "aaz", generate a text report
;   on the comparison and statistics of the data.
;
;   The report is printed to the screen and written to an ASCII text file. See 
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct 
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_report_aaz, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
;     fdf_unmatched, fdt_unmatched, varfile, [reportfile=reportfile], [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "aaz", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "aaz", produced by the LM Flight 
;              Dynamics Team.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "aaz" is a structure with tags for each
;              of the stations/antennae. Each of those tags is an structure
;              array of type "Anonymous" and of size [N_FDF_events]. Each
;              element contains data for an event (i.e., a line) from an FDF
;              file of type "aaz", and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "aaz" is a structure with tags for each
;              of the stations/antennae. Each of those tags is an structure
;              array of type "Anonymous" and of size [N_FDT_events]. Each
;              element contains data for an event (i.e., a line) from an FDT
;              file of type "aaz", and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "AAZ_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "AAZ_MATCH_TYPE", array
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "AAZ_MATCH_TYPE", array
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were / 
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-06' ;--- (M.Weber) Fixed a bug when counting the number
;                             of deltas within the tolerances.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_AAZ'

  file_type = 'aaz'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'


;; =============================================
;; Generate the subsections of the report.

  iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr

  iris_fd_make_rpt_hdr2, file_type, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, 'AAZ_TIME', 'TIME', report_arr

  iris_fd_make_rpt_var, matches, 'TIME', 'sec', report_arr, /qtimestr, $
    /used2match, pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'ANGLE', 'deg', report_arr, /qnumber, $
    pipeline=pipeline


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_eclipse,   $
      fdf_file,               $
      fdt_file,               $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_eclipse
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "eclipse", generate a text report
;   on the comparison and statistics of the data.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_report_eclipse, fdf_file, fdt_file, fdf_parsed, fdt_parsed, $
;     matches, fdf_unmatched, fdt_unmatched, varfile, [reportfile=reportfile], $
;     [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "eclipse", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "eclipse", produced by the LM Flight 
;              Dynamics Team.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "eclipse" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "eclipse", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "eclipse" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "eclipse", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "ECLIPSE_MATCH_TYPE", array
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-06' ;--- (M.Weber) Fixed a bug when counting the number
;                             of deltas within the tolerances.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_ECLIPSE'

  file_type = 'eclipse'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'


;; =============================================
;; Generate the subsections of the report.

  iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr

  iris_fd_make_rpt_hdr2, file_type, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, 'START_TIME', 'ST_TIME', report_arr

  iris_fd_make_rpt_var, matches, 'ST_TIME', 'sec', report_arr, /qtimestr, $
    /used2match, pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'EN_TIME', 'sec', report_arr, /qtimestr, $
    /used2match, pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'DURATION', 'sec', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'ST_PASS', 'n/a', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'EN_PASS', 'n/a', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'C_COND', 'n/a', report_arr, /qstring, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'W_COND', 'n/a', report_arr, /qstring, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'TOT_DUR', 'sec', report_arr, /qnumber, $
    pipeline=pipeline


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_events,    $
      fdf_file,               $
      fdt_file,               $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_events
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "events", generate a text report
;   on the comparison and statistics of the data.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_report_events, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
;     fdf_unmatched, fdt_unmatched, varfile, [reportfile=reportfile], [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "events", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "events", produced by the LM Flight 
;              Dynamics Team.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "events" is a structure with tags for
;              each of the stations/antennae. Each of those tags is a
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "events", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "events" is a structure with tags for
;              each of the stations/antennae. Each of those tags is a
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "events", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "EVENTS_MATCH_TYPE", array
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_EVENTS'

  file_type = 'events'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'


;; =============================================
;; Generate the subsections of the report.

  iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr

  iris_fd_make_rpt_hdr2, file_type, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, 'START_TIME', 'ST_TIME', report_arr

  iris_fd_make_rpt_var, matches, 'PASS', 'n/a', report_arr, /qnumber, $
    /used2match, pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'ST_TIME', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'EN_TIME', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'APOGEE', 'km', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'T_APOG', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'PERIGEE', 'km', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'T_PERIG', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'PERIOD', 'sec', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'RI_ASC', 'deg', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'LO_ASC', 'deg', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'LO_DESC', 'deg', report_arr, /qnumber, $
    pipeline=pipeline


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_ground,    $
      fdf_file,               $
      fdt_file,               $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_ground
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   For a list of matched FDF/FDT pairs, of type "ground", generate a text report
;   on the comparison and statistics of the data.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_report_ground, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
;     fdf_unmatched, fdt_unmatched, varfile, [reportfile=reportfile], [/pipeline]
;
;
; INPUTS:
;
;   MATCHES -  [Mandatory] (structure of type "GROUND_MATCH_TYPE", array of
;              size [N_matched_events])
;              Each element of MATCHES contains data about a unique matched
;              pair of FDF and FDT events. See the Manual for more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "GROUND_MATCH_TYPE", array
;              of size [N_FDF_unmatched_events])
;              Each element of FDF_UNMATCHED contains data about an event from
;              FDF_FILE that wasn't matched to an FDT event. See the Manual for
;              more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "GROUND_MATCH_TYPE", array
;              of size [N_FDT_unmatched_events])
;              Each element of FDT_UNMATCHED contains data about an event from
;              FDT_FILE that wasn't matched to an FDF event. See the Manual for
;              more information.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", array of
;              size [N_FDF_events])
;              Each element of FDF_PARSED contains data for an event (i.e., a
;              line) from FDF_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", array of
;              size [N_FDT_events])
;              Each element of FDT_PARSED contains data for an event (i.e., a
;              line) from FDT_FILE, and all events in the file are represented.
;              The data has been "parsed" from the ASCII file into the IDL
;              environment. See the Manual for more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one 
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_GROUND'

  file_type = 'ground'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'


;; =============================================
;; Generate the subsections of the report.

  iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr

  iris_fd_make_rpt_hdr2, file_type, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, 'TIME', 'TIME', report_arr

  iris_fd_make_rpt_var, matches, 'TIME', 'sec', report_arr, /qtimestr, $
    /used2match, pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'LAT', 'deg', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'LON', 'deg', report_arr, /qnumber, $
    pipeline=pipeline

  iris_fd_make_rpt_var, matches, 'ALT', 'km', report_arr, /qnumber, $
    pipeline=pipeline


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_report_radzones, $
      zone_name,                 $
      fdf_parsed,                $
      fdt_parsed,                $
      matches,                   $
      fdf_unmatched,             $
      fdt_unmatched,             $
      report_arr,                $
      pipeline =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_report_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "radzones", generate a text
;   report on the comparison and statistics of the data for a single radiation
;   zone specified by keyword.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_sub_report_radzones, zone_name, fdf_parsed, fdt_parsed, $
;   IDL>   matches, fdf_unmatched, fdt_unmatched, report_arr, [/pipeline]
;
;
; INPUTS:
;
;   ZONE_NAME -  [Mandatory] (string, scalar)
;              One of the possible radiation zone names for file-type
;              "radzones". (E.g., 'SAA', 'N_HLZ', or 'S_HLZ'.)
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "radzones" is a structure with tags
;              for each of the radiations zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the radiations zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. A
;              different subroutine initializes this variable.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. This
;              subroutine adds new content for a specific radiation zone by
;              appending more lines to REPORT_ARR. A different subroutine
;              handles the writing of the report to the screen and ASCII file.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-May-17' ;--- (M.Weber) Split off from <iris_fd_report_radzones>
;                             in a major refactoring.
;
;
;-
;===============================================================================


;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_SUB_REPORT_RADZONES'

  file_type = 'radzones'


;; =============================================
;; Isolate the specified zone structures.

  case strup(zone_name) of
    'SAA': parsed_zone = 'SAA'
    'N_HLZ': parsed_zone = 'NORTH_HLZ_OUTER'
    'S_HLZ': parsed_zone = 'SOUTH_HLZ_OUTER'
  endcase

  ss = where(strmid(tag_names(fdf_parsed),0,3) EQ strmid(parsed_zone,0,3))
  fdf_par = fdf_parsed.(ss[0])

  ss = where(strmid(tag_names(fdt_parsed),0,3) EQ strmid(zone_name,0,3))
  fdt_par = fdt_parsed.(ss[0])

  ss = where(strmid(tag_names(matches),0,3) EQ strmid(zone_name,0,3))
  mats = matches.(ss[0])

  ss = where(strmid(tag_names(fdf_unmatched),0,3) EQ strmid(zone_name,0,3))
  fdf_unm = fdf_unmatched.(ss[0])

  ss = where(strmid(tag_names(fdt_unmatched),0,3) EQ strmid(zone_name,0,3))
  fdt_unm = fdt_unmatched.(ss[0])


;; =============================================
;; Add a separator and the 2nd header.

  report_arr = [report_arr, $
    '..........................................']
  report_arr = [report_arr, $
    'Subsection for this radiation zone: ' + zone_name]

  iris_fd_make_rpt_hdr2, file_type, fdf_par, fdt_par, mats, $
    fdf_unm, fdt_unm, 'START_TIME', 'ST_TIME_OUTER', report_arr


;; =============================================
;; Add the data fields.

  iris_fd_make_rpt_var, mats, 'ST_TIME_OUTER', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, mats, 'ST_TIME_INNER', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, mats, 'EN_TIME_INNER', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline

  iris_fd_make_rpt_var, mats, 'EN_TIME_OUTER', 'sec', report_arr, /qtimestr, $
    pipeline=pipeline


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_radzones,  $
      fdf_file,               $
      fdt_file,               $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "radzones", generate a text
;   report on the comparison and statistics of the data for all of the
;   possible radiation zones.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   IDL> iris_fd_report_radzones, fdf_file, fdt_file, fdf_parsed, $
;   IDL>   fdt_parsed, matches, fdf_unmatched, fdt_unmatched, varfile, $
;   IDL>   [reportfile=reportfile], [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "radzones", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "radzones", produced by the LM
;              Flight Dynamics Team.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "radzones", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "radzones" is a structure with tags
;              for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "radzones" is a structure with
;              tags for each of the radiation zones. Each of those tags is an
;              structure array of type "RADZONES_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-05' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring to allow for HLZs
;                             and much other stuff. Split off a lot of the
;                             functionality to <iris_fd_sub_report_radzones>.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_RADZONES'

  file_type = 'radzones'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'


;; =============================================
;; Generate the overall header of the report.

  iris_fd_make_rpt_hdr1, fdf_file, fdt_file, file_type, varfile, report_arr


;; =============================================
;; Generate the remainder of the report.

  ;; The second header and data fields are iterated over the various types
  ;; of radiation zones. This is implemented with multiple calls to an
  ;; intermediate subroutine.

  iris_fd_sub_report_radzones, 'SAA', fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, report_arr, pipeline=pipeline

  iris_fd_sub_report_radzones, 'N_HLZ', fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, report_arr, pipeline=pipeline

  iris_fd_sub_report_radzones, 'S_HLZ', fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, report_arr, pipeline=pipeline


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_sub_report_view, $
      fdf_file,              $
      fdt_file,              $
      file_type,             $
      station,               $
      fdf_parsed,            $
      fdt_parsed,            $
      matches,               $
      fdf_unmatched,         $
      fdt_unmatched,         $
      report_arr

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_sub_report_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "view_10deg" or "view_5deg",
;   generate a text report on the comparison and statistics of the data for a
;   single station/antenna specified by keyword.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_sub_report_view, fdf_file, fdt_file, file_type, station, $
;     fdf_parsed, fdt_parsed, matches, fdf_unmatched, fdt_unmatched, report_arr
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "view_10deg" or "view_5deg",
;              produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "view_10deg" or "view_5deg",
;              produced by the LM Flight Dynamics Team.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   STATION -  [Mandatory] (string, scalar)
;              One of the possible station names for file-types "view_10deg"
;              and "view_5deg". E.g., 'MGS', WAPS', etc. For labeling.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "view_XXdeg" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. A
;              different subroutine initializes this variable.
;
; OUTPUTS:
;
;   REPORT_ARR - [Mandatory] (string, array of size [N_report_lines])
;              This string array contains the text of the report, so far. This
;              subroutine adds new content for a specific station/antenna by
;              appending more lines to REPORT_ARR. A different subroutine
;              handles the writing of the report to the screen and ASCII file.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) The program will crash if one does not set exactly one of the station
;      switches.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-11' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc. Removed NOTIND.PRO.
progver = 'v2013-Apr-24' ;--- (M.Weber) Fixed typo: 3rd line of ST_TIME block
;                             referred to EN_TIME.
progver = 'v2013-Apr-26' ;--- (M.Weber) Fixed bug trying to run <stdev> on one
;                             datum.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
;
;
;-
;===============================================================================


;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_SUB_REPORT_VIEW'

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Isolate the specified station data.

  ss = where(strmid(tag_names(fdf_parsed),0,3) EQ strmid(station,0,3))
  fpar = fdf_parsed.(ss[0])

  ss = where(strmid(tag_names(fdt_parsed),0,3) EQ strmid(station,0,3))
  tpar = fdt_parsed.(ss[0])

  ss = where(strmid(tag_names(matches),0,3) EQ strmid(station,0,3))
  mats = matches.(ss[0])

  ss = where(strmid(tag_names(fdf_unmatched),0,3) EQ strmid(station,0,3))
  funm = fdf_unmatched.(ss[0])

  ss = where(strmid(tag_names(fdt_unmatched),0,3) EQ strmid(station,0,3))
  tunm = fdt_unmatched.(ss[0])


;; =============================================
;; Establish how many events there are in each variable.

  ;; Assume guilt (zeros) until proven innocent (has legitimate members).
  N_tpar = 0L    ;; Number fdT PARsed
  N_fpar = 0L    ;; Number fdF PARsed
  N_mats = 0L    ;; Number MATcheS
  N_funm = 0L    ;; Number fdF UNMatched
  N_tunm = 0L    ;; Number fdT UNMatched

  if (size(fpar, /type) EQ 8) then N_fpar = n_elements(fpar)
  if (size(tpar, /type) EQ 8) then N_tpar = n_elements(tpar)

  if (size(mats, /type) EQ 8) then begin
    if (tag_names(mats, /st) EQ 'VIEW_MATCH_TYPE') $
      then N_mats = n_elements(mats)
  endif

  if (size(funm, /type) EQ 8) then begin
    if (tag_names(funm, /st) EQ 'VIEW_MATCH_TYPE') $
      then N_funm = n_elements(funm)
  endif

  if (size(tunm, /type) EQ 8) then begin
    if (tag_names(tunm, /st) EQ 'VIEW_MATCH_TYPE') $
      then N_tunm = n_elements(tunm)
  endif


;; =============================================
;; Write station subreport.

  ;; Station reports get a subsection header.
  if (keyword_set(station) EQ 1B) then begin
    report_arr = [report_arr, $
      '..........................................']
    report_arr = [report_arr, $
      'Subsection for this station/antenna: ' + station]
  endif

  report_arr = [report_arr, $
    'Number of FDF/FDT entries: ' + strcompress(N_fpar,/rem) + ' / ' + $
    strcompress(N_tpar,/rem)]

  if (N_fpar EQ 0) then begin
    report_arr = [report_arr, 'FDF range of start times: NO FDF DATA.']
  endif else begin
    report_arr = [report_arr, $
      'FDF range of start times: ' + fpar[0].start_time + ' / ' + $
      fpar[N_fpar-1].start_time]
  endelse

  if (N_tpar EQ 0) then begin
    report_arr = [report_arr, 'FDT range of start times: NO FDT DATA.']
  endif else begin
    report_arr = [report_arr, $
      'FDF range of start times: ' + tpar[0].start_time + ' / ' + $
      tpar[N_tpar-1].start_time]
  endelse

  report_arr = [report_arr, $
    'Number of matches / FDF unmatched / FDT unmatched: ' + $
    strcompress(N_mats,/rem) + ' / ' + strcompress(N_funm,/rem) + ' / ' + $
    strcompress(N_tunm,/rem)]

  if ((N_fpar GE 1) AND (N_tunm GE 1)) then begin
    fdf_tai = anytim2tai(fpar.start_time)
    fdt_tai = anytim2tai(tunm.fdt_st_time)
    ss = where((fdt_tai GE fdf_tai[0]) AND $
               (fdt_tai LE fdf_tai[N_fpar-1]), count)
  endif else begin
    count = 0L
  endelse
  report_arr = [report_arr, $
    'Number of unmatched FDT entries inside/outside of FDF time range: ' + $
    strcompress(N_tunm-count,/rem) + ' / ' + strcompress(count,/rem)]

  if (N_mats GE 1) then begin

    ;; DELTA_ST_TIME

    ;; Generalize the data for replicability.
    varname = 'DELTA_ST_TIME'
    varunits = 'sec'
    data = mats.delta_st_time
    tol_data = mats[0].tol_st_time
    tol_data_str = strcompress(string(tol_data, format='(F7.2)'), /rem)
    ss_good = where((mats.fdf_st_time NE error_tstring) AND $
                    (mats.fdt_st_time NE error_tstring), N_good)

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      varname + ' [' + varunits + '] mean/min/max/stdev/tol: ' + $
      mean_data + '/' + min_data + '/' + max_data + '/' + stdev_data + '/' $
      + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; Indicate the number of data values that exceed the tolerance.
    if (N_flags EQ 0) then begin
      report_arr = [report_arr, varname + ': PASSED']
    endif else begin
      if (N_flags EQ 1) then begin
        report_arr = [report_arr, $
          varname + ':           ********************* 1 FLAG *********************']
      endif else begin
        report_arr = [report_arr, $
          varname + ':           ********************* ' $
          + strcompress(N_flags,/rem) + ' FLAGS *********************']
      endelse
    endelse


    ;; DELTA_EN_TIME

    ;; Generalize the data for replicability.
    varname = 'DELTA_EN_TIME'
    varunits = 'sec'
    data = mats.delta_en_time
    tol_data = strcompress(string(mats[0].tol_en_time, format='(F7.2)'), /rem)
    ss_good = where((mats.fdf_en_time NE error_tstring) AND $
                    (mats.fdt_en_time NE error_tstring), N_good)

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      varname + ' [' + varunits + '] mean/min/max/stdev/tol: ' + $
      mean_data + '/' + min_data + '/' + max_data + '/' + stdev_data + '/' $
      + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; Indicate the number of data values that exceed the tolerance.
    if (N_flags EQ 0) then begin
      report_arr = [report_arr, varname + ': PASSED']
    endif else begin
      if (N_flags EQ 1) then begin
        report_arr = [report_arr, $
          varname + ':           ********************* 1 FLAG *********************']
      endif else begin
        report_arr = [report_arr, $
          varname + ':           ********************* ' $
          + strcompress(N_flags,/rem) + ' FLAGS *********************']
      endelse
    endelse


    ;; DELTA_DURATION

    ;; Generalize the data for replicability.
    varname = 'DELTA_DURATION'
    varunits = 'sec'
    data = mats.delta_duration
    tol_data = strcompress(string(mats[0].tol_duration, format='(F7.2)'), /rem)
    ss_good = where((mats.fdf_duration NE error_number) AND $
                    (mats.fdt_duration NE error_number), N_good)

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      varname + ' [' + varunits + '] mean/min/max/stdev/tol: ' + $
      mean_data + '/' + min_data + '/' + max_data + '/' + stdev_data + '/' $
      + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; Indicate the number of data values that exceed the tolerance.
    if (N_flags EQ 0) then begin
      report_arr = [report_arr, varname + ': PASSED']
    endif else begin
      if (N_flags EQ 1) then begin
        report_arr = [report_arr, $
          varname + ':           ********************* 1 FLAG *********************']
      endif else begin
        report_arr = [report_arr, $
          varname + ':           ********************* ' $
          + strcompress(N_flags,/rem) + ' FLAGS *********************']
      endelse
    endelse


    ;; DELTA_MAX_EL

    ;; Generalize the data for replicability.
    varname = 'DELTA_MAX_EL'
    varunits = 'deg'
    data = mats.delta_max_el
    tol_data = strcompress(string(mats[0].tol_max_el, format='(F7.2)'), /rem)
    ss_good = where((mats.fdf_max_el NE error_number) AND $
                    (mats.fdt_max_el NE error_number), N_good)

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      varname + ' [' + varunits + '] mean/min/max/stdev/tol: ' + $
      mean_data + '/' + min_data + '/' + max_data + '/' + stdev_data + '/' $
      + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; Indicate the number of data values that exceed the tolerance.
    if (N_flags EQ 0) then begin
      report_arr = [report_arr, varname + ': PASSED']
    endif else begin
      if (N_flags EQ 1) then begin
        report_arr = [report_arr, $
          varname + ':           ********************* 1 FLAG *********************']
      endif else begin
        report_arr = [report_arr, $
          varname + ':           ********************* ' $
          + strcompress(N_flags,/rem) + ' FLAGS *********************']
      endelse
    endelse


    ;; DELTA_MAX_EL_TIME

    ;; Generalize the data for replicability.
    varname = 'DELTA_MAX_EL_TIME'
    varunits = 'deg'
    data = mats.delta_max_el_time
    tol_data = strcompress(string(mats[0].tol_max_el_time, format='(F7.2)'), /rem)
    ss_good = where((mats.fdf_max_el_time NE error_tstring) AND $
                    (mats.fdt_max_el_time NE error_tstring), N_good)

    ;; Various data stats.
    if (N_good EQ 0) then begin       ;; If no good values...
      stdev_data = '--'
      mean_data = '--'
      min_data = '--'
      max_data = '--'
      N_partials = '--'
      N_errors = '--'
      N_flags = 0L
    endif else begin                 ;; Else if good values...
      data = data[ss_good]
      min_data = strcompress(string(min(data), format='(F7.2)'), /rem)
      max_data = strcompress(string(max(data), format='(F7.2)'), /rem)
      if (ss_good[0] EQ 0L) then N_partials = 0 else N_partials = 1
      N_errors = N_mats - N_good - N_partials
      ss_flags = where(abs(data) GT tol_data, N_flags)
      if (N_good EQ 1) then begin        ;; If one good value...
        mean_data = strcompress(string(data, format='(F7.2)'), /rem)
        stdev_data = '--'
      endif else begin                  ;; Else if multiple good values...
        stdev_data = strcompress(string(stdev(data, mean_data), $
                                        format='(F7.2)'), /rem)
        mean_data = strcompress(string(mean_data, format='(F7.2)'), /rem)
      endelse
    endelse
    report_arr = [report_arr, $
      varname + ' [' + varunits + '] mean/min/max/stdev/tol: ' + $
      mean_data + '/' + min_data + '/' + max_data + '/' + stdev_data + '/' $
      + tol_data_str]

    ;; Indicate the number of input data that had problems being parsed.
    ;; "N_partial_passes" indicates whether the initial pass only partially
    ;; overlapped the file's time range.
    ;; "N_value_errors" indicates the number of problematic values that weren't
    ;; in the first pass.
    report_arr = [report_arr, $
      varname + ' N_matches/N_partial_passes/N_value_errors: ' + $
      strcompress(N_mats,/rem) + '/' + strcompress(N_partials,/rem) + '/' + $
      strcompress(N_errors,/rem)]

    ;; Indicate the number of data values that exceed the tolerance.
    if (N_flags EQ 0) then begin
      report_arr = [report_arr, varname + ': PASSED']
    endif else begin
      if (N_flags EQ 1) then begin
        report_arr = [report_arr, $
          varname + ':           ********************* 1 FLAG *********************']
      endif else begin
        report_arr = [report_arr, $
          varname + ':           ********************* ' $
          + strcompress(N_flags,/rem) + ' FLAGS *********************']
      endelse
    endelse

  endif


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_report_view,      $
      fdf_file,               $
      fdt_file,               $
      file_type,              $
      fdf_parsed,             $
      fdt_parsed,             $
      matches,                $
      fdf_unmatched,          $
      fdt_unmatched,          $
      varfile,                $
      reportfile =reportfile, $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_report_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   For a list of matched FDF/FDT pairs, of type "view_10deg" or "view_5deg",
;   generate a text report on the comparison and statistics of the data for all
;   of the possible stations/antennae.
;
;   The report is printed to the screen and written to an ASCII text file. See
;   the Manual for a full description of the report's contents. This is a
;   subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended for direct
;   usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_report_view, fdf_file, fdt_file, file_type, fdf_parsed, fdt_parsed, $
;     matches, fdf_unmatched, fdt_unmatched, varfile, [reportfile=reportfile], $
;     [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "view_10deg" or "view_5deg",
;              produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "view_10deg" or "view_5deg",
;              produced by the LM Flight Dynamics Team.
;
;   FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   FDF_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDF_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDF file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   FDT_PARSED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_PARSED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "Anonymous" and of size [N_FDT_events].
;              Each element contains data for an event (i.e., a line) from
;              an FDT file of type "view_XXdeg", and all events in the file are
;              represented. The data has been "parsed" from the ASCII file into
;              the IDL environment. See the Manual for more information.
;
;   MATCHES -  [Mandatory] (structure of type "Anonymous", scalar)
;              MATCHES for file type "view_XXdeg" is a structure with tags
;              for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_xxDEG_MATCH_TYPE" and of
;              size [N_matched_events]. Each element contains data about a
;              unique matched pair of FDF and FDT events. See the Manual for
;              more information.
;
;   FDF_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDF_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_xxDEG_MATCH_TYPE" and of size
;              [N_FDF_unmatched_events]. Each element contains data about an
;              event from FDF_FILE that wasn't matched to an FDT event. See the
;              Manual for more information.
;
;   FDT_UNMATCHED - [Mandatory] (structure of type "Anonymous", scalar)
;              FDT_UNMATCHED for file type "view_XXdeg" is a structure with
;              tags for each of the stations/antennae. Each of those tags is an
;              structure array of type "VIEW_xxDEG_MATCH_TYPE" and of size
;              [N_FDT_unmatched_events]. Each element contains data about an
;              event from FDT_FILE that wasn't matched to an FDF event. See the
;              Manual for more information.
;
;   VARFILE -  [Mandatory] (string, scalar)
;              Name of the IDL savefile to which the program variables were /
;              will be saved. See the Manual for more information.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The report will only be written to REPORTFILE, and will not
;              be written to the screen.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   none
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-11' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to only write to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_REPORT_VIEW'

  qpipe = keyword_set(pipeline) EQ 1B

  ;; Set a default report filename if one isn't provided.
  if (keyword_set(reportfile) EQ 0b) then $
    reportfile = 'iris_fd_report_file.txt'

  ;; The parser returns specific values when it encounters "bad" data.
  ;; We define the returned values by field type so we can check for them.
  error_tstring = anytim(0,/ccsds)
  error_number = -9999d


;; =============================================
;; Generate the report header content as a string array.

  ;; We seed the array with a blank line for two reasons:
  ;; 1) So we can just add lines by concatenation.
  ;; 2) So there is a blank line appended to the report file
  ;;    before the content starts.
  report_arr = ''

  report_arr = [report_arr, $
    '===========================================================']
  report_arr = [report_arr, $
    'REPORT ON COMPARING FDT VERSUS FDF OF THIS TYPE: ' + matches[0].file_type]

  report_arr = [report_arr, 'FDF filename: ' + matches[0].fdf_filename]
  report_arr = [report_arr, 'FDT filename: ' + matches[0].fdt_filename]
  report_arr = [report_arr, 'Variables saved filename: ' + varfile]


;; =============================================
;; Use a subroutine to generate the report content per station.

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'MGS', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'WAPS', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'ASFS', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'SG1S', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'SG3X', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'SG6X', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr

  iris_fd_sub_report_view, fdf_file, fdt_file, file_type, 'SG22X', $
    fdf_parsed, fdt_parsed, matches,  fdf_unmatched, fdt_unmatched, report_arr


;; =============================================
;; Add a timestamp.

  report_arr = [report_arr, 'Report time: ' + systim()]


;; =============================================
;; Print report to screen.

  ;; If not in "pipeline" mode, then may print to screen.
  if (qpipe EQ 0) then hprint, report_arr


;; =============================================
;; Print report to file.

  if (file_test(reportfile) EQ 1L) then begin
    openu, lun, reportfile, /get_lun, /append
  endif else begin
    openw, lun, reportfile, /get_lun
  endelse

  N_lines = n_elements(report_arr)
  for i_line = 0L,(N_lines-1) do printf, lun, report_arr[i_line]

  close, lun
  free_lun, lun


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_aaz,      $
      fdf_file,               $
      fdt_file,               $
      reportfile =reportfile, $
      varfile    =varfile,    $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_aaz
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of AAZ event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended 
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_aaz, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile], [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "aaz", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "aaz", produced by the LM Flight 
;              Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be 
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value 
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then 
;              the report text will be appended to the file, rather than 
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_AAZ'


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_aaz, fdf_file, fdt_file, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed


;; =============================================
;; Show plots of AAZ deltas.

  iris_fd_plot_aaz, fdf_file, fdt_file, matches, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default 
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_aaz, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, matches, fdf_unmatched, fdt_unmatched, $
    fdf_parsed, fdt_parsed, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_eclipse,  $
      fdf_file,               $
      fdt_file,               $
      reportfile =reportfile, $
      varfile    =varfile,    $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_eclipse
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of ECLIPSE event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_eclipse, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile], [pipeline=pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "eclipse", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "eclipse", produced by the LM Flight 
;              Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Mar-29' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_ECLIPSE'


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_eclipse, fdf_file, fdt_file, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed


;; =============================================
;; Show plots of ECLIPSE deltas.

  iris_fd_plot_eclipse, fdf_file, fdt_file, matches, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default 
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_eclipse, fdf_file, fdt_file, fdf_parsed, fdt_parsed, $
    matches, fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, $
    pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, matches, fdf_unmatched, fdt_unmatched, $
    fdf_parsed, fdt_parsed, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_events,     $
      fdf_file,                 $
      fdt_file,                 $
      reportfile  =reportfile,  $
      varfile     =varfile,     $
      pass_offset =pass_offset, $
      pipeline    =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_events
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of EVENTS event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_events, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile], [pipeline=pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "events", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "events", produced by the LM Flight 
;              Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-05' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_EVENTS'


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_events, fdf_file, fdt_file, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed, $
    pass_offset=pass_offset


;; =============================================
;; Show plots of EVENTS deltas.

  iris_fd_plot_events, fdf_file, fdt_file, matches, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default 
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_events, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, matches, fdf_unmatched, fdt_unmatched, $
    fdf_parsed, fdt_parsed, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_ground,   $
      fdf_file,               $
      fdt_file,               $
      reportfile =reportfile, $
      varfile    =varfile,    $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_ground
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of GROUND event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_ground, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile], [/pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "ground", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "ground", produced by the LM Flight 
;              Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_GROUND'


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_ground, fdf_file, fdt_file, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed


;; =============================================
;; Show plots of GROUND deltas.

  iris_fd_plot_ground, fdf_file, fdt_file, matches, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default 
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_ground, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, matches, fdf_unmatched, fdt_unmatched, $
    fdf_parsed, fdt_parsed, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_radzones, $
      fdf_file,               $
      fdt_file,               $
      reportfile =reportfile, $
      varfile    =varfile,    $
      pipeline   =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_radzones
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of RADZONES event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_radzones, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile]
;
;
; INPUTS:
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "radzones", produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "radzones", produced by the LM 
;              Flight Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-06' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_RADZONES'

  ;; Check that the correct number of parameters are supplied or stop.
  if (n_params() NE 2) then begin
    print, prognam + ': Wrong number of parameters. Returning.'
    return
  endif

  ;; Check that the input files are not lists.
  if ((n_elements(fdf_file) NE 1) OR (n_elements(fdf_file) NE 1)) then begin
    print, prognam + ': Filename inputs cannot be arrays. Returning.'
    return
  endif


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_radzones, fdf_file, fdt_file, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed


;; =============================================
;; Show plots of RADZONES deltas.

  iris_fd_plot_radzones, matches, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default 
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_radzones, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
    fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, $
    pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, matches, fdf_unmatched, fdt_unmatched, $
    fdf_parsed, fdt_parsed, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================

PRO iris_fd_compare_view,       $
      fdf_file,                 $
      fdt_file,                 $
      file_type   =file_type,   $
      reportfile  =reportfile,  $
      varfile     =varfile,     $
      pass_offset =pass_offset, $
      pipeline    =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_view
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
;
;   Compare two files of VIEW_xxDEG event lists to determine unique matches and
;   generate a report and plots.
;
;   This is a subroutine of IRIS_FD_COMPARE_FILES.PRO, and is not intended
;   for direct usage by a user.
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_view, fdf_file, fdt_file, [file_type=file_type], $
;     [reportfile=reportfile], [varfile=varfile]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file, of type "view_10deg" or "view_5deg",
;              produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file, of type "view_10deg" or "view_5deg",
;              produced by the LM Flight Dynamics Team.
;
;   FILE_TYPE=FILE_TYPE - [Mandatory] (string, scalar)
;              String ID for file type, equals either 'view_10deg' or 'view_5deg'.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then
;              the report text will be appended to the file, rather than
;              overwriting it.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of
;              to IDL graphics windows.
;
; OUTPUTS:
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-11' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Modified to rely on <iris_fd_make_idstring>.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
progver = 'v2013-May-17' ;--- (M.Weber) Major refactoring and consolidated the
;                             VIEW_10DEG and VIEW_5DEG versions. 
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_VIEW'


;; =============================================
;; Read input files and identify matches.

  iris_fd_match_view, fdf_file, fdt_file, file_type, matches, fdf_unmatched, $
    fdt_unmatched, fdf_parsed=fdf_parsed, fdt_parsed=fdt_parsed, pass_offset=pass_offset


;; =============================================
;; Show plots of VIEW_xxDEG deltas.

  iris_fd_plot_view, matches, file_type, pipeline=pipeline


;; =============================================
;; Generate report on comparison.

  ;; The VARFILE string is written to the report, so we generate its default
  ;; value at this point instead of in the savefile section.
  varfile = 'iris_fd_varfile_' + iris_fd_make_idstring(fdf_file, fdt_file) + $
            '.sav'

  iris_fd_report_view, fdf_file, fdt_file, file_type, fdf_parsed, fdt_parsed, $
    matches, fdf_unmatched, fdt_unmatched, varfile, reportfile=reportfile, $
    pipeline=pipeline


;; =============================================
;; Save variables to an IDL savefile.

  ;; The REPORTFILE string is saved to the savefile, which is why this
  ;; section comes after the reporting section where the default value
  ;; of REPORTFILE is generated.

  save, file=varfile, fdf_file, fdt_file, fdf_parsed, fdt_parsed, matches, $
        fdf_unmatched, fdt_unmatched, reportfile, varfile


;; =============================================
;; Finish.

  return

END

;===============================================================================
;===============================================================================
;===============================================================================
; foobar

PRO iris_fd_compare_files,      $
      fdf_file,                 $
      fdt_file,                 $
      reportfile  =reportfile,  $
      varfile     =varfile,     $
      pass_offset =pass_offset, $
      pipeline    =pipeline

;===============================================================================
;+
;
; PROJECT:
;
;   IRIS
;
;
; NAME:
;
;   iris_fd_compare_files
;
;
; CATEGORY:
;
;   Flight dynamics
;
;
; PURPOSE / DESCRIPTION
; 
;   Compare two files of Flight Dynamic event lists to determine unique matches
;   and generate a report and plots.
;
;   This routine is the user-level interface to the "iris_fd_" programs. Given 
;   one FDF (GSFC-MOC) file and one FDT (LMSAL) file of the same type, this 
;   suite of software will (a) match elements between the files, (b) generate a 
;   text report on the comparisons and the unmatched elements, (c) print the 
;   report to and the unmatched elements, (c) print the report to the screen and
;   to a file REPORTFILE, (d) plot the variances of the matches, and (e) save 
;   relevant data to an IDL savefile VARFILE. Specifications for the FDx files 
;   are provided in the document "IRIS ICD Between The Mission Operations Center 
;   and the LM Flight Dynamics Team". A fuller description of this "iris_fd_" 
;   suite is provided in the document "IRIS Flight Dynamics Comparison Software 
;   Manual".
;
;
; CALLING SEQUENCE:
;
;   iris_fd_compare_files, fdf_file, fdt_file, [reportfile=reportfile], $
;     [varfile=varfile], [pipeline=pipeline]
;
;
; INPUTS:
;
;   FDF_FILE - [Mandatory] (string, scalar)
;              Name of an FDF file produced by the MOC.
;
;   FDT_FILE - [Mandatory] (string, scalar)
;              Name of an FDT file produced by the LM Flight Dynamics Team.
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of an ASCII text file to which the text report will be 
;              written. If keyword_set(REPORTFILE) EQ 0, then the default value 
;              is "iris_fd_report_file.txt". If REPORTFILE already exists, then 
;              the report text will be appended to the file, rather than 
;              overwriting it.
;
;   PASS_OFFSET=PASS_OFFSET - [Optional] (string, scalar)
;              Integer value which gives the PASS ID offset correction for the 
;              FDF files, to resolve discrepancies with FDT. Defaults to zero.
;
;   /PIPELINE - [Optional] (Boolean switch)
;              This switch forces the code to operate in "pipeline" mode.
;              The plots will be written to PostScript files, instead of 
;              to IDL graphics windows.
;
; OUTPUTS: 
;
;   REPORTFILE=REPORTFILE - [Optional] (string, scalar)
;              Name of the ASCII text file to which the text report was written.
;
;   VARFILE=VARFILE - [Optional] (string, scalar)
;              Name of the IDL savefile to which the program variables were
;              saved. For more information about those variables, see the 
;              document "IRIS Flight Dynamics Comparison Software Manual".
;
;
; COMMON BLOCKS:
;
;   none
;
;
; NOTES:
;
;   1) FDF_FILE and FDT_FILE do not commute in all cases, so they should be
;      provided in the argument list in the indicated order.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       emailed to...
;       this person: mweber
;       at this address: cfa.harvard.edu
;
;
; MODIFICATION HISTORY:
;
progver = 'v2013-Apr-11' ;--- (M.Weber) Written.
progver = 'v2013-Apr-12' ;--- (M.Weber) Added header doc.
progver = 'v2013-May-06' ;--- (M.Weber) Added PIPELINE keyword and functionality
;                             to plot to the PostScript device instead of the 
;                             X device, and to only write the report to file.
progver = 'v2013-May-17' ;--- (M.Weber) Consolidated the VIEW_10DEG and 
;                             VIEW_5DEG calls. 
progver = 'v2013-Jul-22' ;--- (M.Weber) Added the PASS_OFFSET keyword for
;                             correcting discrepancies in PASS IDs between the
;                             FDF and FDT files.
;
;
;-
;===============================================================================

;; =============================================
;; Perform setup and any input checks.

  prognam = 'IRIS_FD_COMPARE_FILES'

  ;; Check that the correct number of parameters are supplied or stop.
  if (n_params() NE 2) then begin
    print, prognam + ': Wrong number of parameters. Returning.'
    return
  endif

  ;; Check that the input files are not lists.
  if ((n_elements(fdf_file) NE 1) OR (n_elements(fdf_file) NE 1)) then begin
    print, prognam + ': Filename inputs cannot be arrays. Returning.'
    return
  endif

  default, pass_offset, '0'


;; =============================================
;; Determine the type of FD file the inputs are.

  fdf_pos1 = strpos(fdf_file, 'IRIS', /reverse_search)+5
  fdf_str = strmid(fdf_file, fdf_pos1, 3)
  if (fdf_str EQ 'vie') then fdf_str = strmid(fdf_file, fdf_pos1+5, 3)

  fdt_pos1 = strpos(fdt_file, 'IRIS', /reverse_search)+5
  fdt_str = strmid(fdt_file, fdt_pos1, 3)
  if (fdt_str EQ 'vie') then fdt_str = strmid(fdt_file, fdt_pos1+5, 3)

  ;; Check that the input files are the same type.
  if (fdt_str NE fdf_str) then begin
    print, prognam + ': Inputs are for conflicting file types. Returning.'
    return
  endif


;; =============================================
;; Route the inputs to the appropriate handling routines.

  case fdf_str of

    'aaz': begin
       iris_fd_compare_aaz, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, pipeline=pipeline
    end

    'ecl': begin
       iris_fd_compare_eclipse, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, pipeline=pipeline
    end

    'eve': begin
       iris_fd_compare_events, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, pass_offset=pass_offset, pipeline=pipeline
    end

    'gro': begin
       iris_fd_compare_ground, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, pipeline=pipeline
    end

    'rad': begin
       iris_fd_compare_radzones, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, pipeline=pipeline
    end

    '10d': begin
       iris_fd_compare_view, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, file_type='view_10deg', pass_offset=pass_offset, $
         pipeline=pipeline
    end

    '5de': begin
       iris_fd_compare_view, fdf_file, fdt_file, reportfile=reportfile, $
         varfile=varfile, file_type='view_5deg', pass_offset=pass_offset, $
         pipeline=pipeline
    end

    else: begin
      print, prognam+": Don't recognize this file type. Returning."
      return
    end

  endcase


;; =============================================
;; Finish.

  return

END
