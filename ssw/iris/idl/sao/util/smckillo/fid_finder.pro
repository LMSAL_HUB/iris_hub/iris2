pro fid_finder, t1, t2, f_1330=f_1330, f_1400=f_1400, f_2796=f_2796, f_2832=f_2832, $
	f_fuv_near=f_fuv_near, f_fuv_far=f_fuv_far, f_nuv=f_nuv, display=display, $
	obsid=obsid, desc=desc, dir=dir

; NAME:   fid_finder.pro
;       
; PURPOSE:  Find the pixel locations of the IRIS fiducial markers in both SJI and spectra
;
; CATEGORY: Image Calibration
;
; CALLING SEQUENCE: fid_finder, t1, t2, /f_1330
;
; INPUT ARGUMENTS:  
;       t1 = Start date and time in string format, e.g. '2013-07-31 11:00' or '2013-08-07T10:03:51'
;		t2 = End date and time in string format, e.g. '2013-07-31 12:40' or '2013-08-07T10:48:20'
;
; INPUT KEYWORD PARAMETER:
;		f_1330 = Set to process SJI 1330A image data
;		f_1400 = Set to process SJI 1400A image data
;		f_2796 = Set to process SJI 2796A image data
;		f_2832 = Set to process SJI 2832A image data
;		f_fuv_near = Set to process near FUV spectra
;					Location fixed at C II line
;		f_fuv_far = Set to process far FUV spectra
;					Location fixed at Si IV line
;		f_nuv = Set to process NUV spectra
;					Location fixed at MG II line
;		display = Set to display context SJI and spectral images
;		obsid = Set to further narrow down selected files by obsid 
;				in string format, e.g. '4202100002'
;		desc = Description to be added to the save filename in string format
;		dir = Set to search for files in the directory supplied
;
; OUTPUTS: Save file that containes the following arrays:
;			d_t = The t_obs date and time
;			x1_arr = Array of bottom fiducial X positions in pixels
;			y1_arr = Array of bottom fiducial Y positions in pixels
;			x2_arr = Array of top fiducial X positions in pixels
;			y2_arr = Array of top fiducial Y positions in pixels
;			dist_arr = Array of calcualted distances between fiducials in pixels
;			s_arr = Array of calculated slope between fiducials in SJI
;			ang_arr = Array of calculated angle seperation of fiducials in SJI
;			chisq_x1_arr = Goodness of fit for X1 slices
;			chisq_x2_arr = Goodness of fit for X2 slices
;			chisq_y1_arr = Goodness of fit for Y1 slices
;			chisq_y2_arr = Goodness of fit for Y2 slices
;
; PROGRAM NOTES: 
;			When measured values are considered bad (the goodness of fit values are large)
;			the value is thrown out by setting it to 0. Keep this in mind when using the 
;			output data.
;
; OTHER PROGRAMS NEEDED:
; 			fid_fitter - Used to fit slices taken to get sub-pixel values for locations
;			iris_search - Used to find the IRIS files between two given times
;
;
; MODIFICATION HISTORY:
;	2013/08/26 Written by Sean McKillop (smckillop@cfa.harvard.edu)

; Search for the wanted files in the given time range
  if keyword_set(dir) then file = iris_search(t1, t2, localdir=dir)
  if not keyword_set(dir) then file = iris_search(t1, t2)
  
  fsize = size(file, /n_elements)
  if fsize eq 1 then begin
     if file eq -1 then return
  endif
  
;file = file_search('/home/smckillo/Desktop/IRIS_FID/SJI_Data/all_data/*.fits')
  
  read_iris, file, index
  
; Seperate out which type of data based on provided keyword
; SJI_FUV_1330
  if keyword_set(f_1330) then begin
     spect = 0  
     if keyword_set(obsid) then	w = where(index.img_path eq 'SJI_1330' and index.isqoltid eq obsid)
     ; w = where(index.img_path eq 'SJI_1330' and (index.isqoltid eq '4204000005' or index.isqoltid eq '4204000003'))
     if not keyword_set(obsid) then w = where(index.img_path eq 'SJI_1330')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2 = bytscl(data, min=0, max=1000)
  endif
  
; SJI_FUV_1400
  if keyword_set(f_1400) then begin
     spect = 0
     if keyword_set(obsid) then w = where(index.img_path eq 'SJI_1400' and index.isqoltid eq obsid)
     ;w = where(index.img_path eq 'SJI_1400' and (index.isqoltid eq '4204000005' or index.isqoltid eq '4204000003'))
     if not keyword_set(obsid) then w = where(index.img_path eq 'SJI_1400')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2 = bytscl(data, min=0, max=1000)
  endif
  
; SJI_FUV_2796
  if keyword_set(f_2796) then begin
     spect = 0
     if keyword_set(obsid) then w = where(index.img_path eq 'SJI_2796' and index.isqoltid eq obsid)
     ; w = where(index.img_path eq 'SJI_2796' and (index.isqoltid eq '4204000005' or index.isqoltid eq '4204000003'))
     if not keyword_set(obsid) then w = where(index.img_path eq 'SJI_2796')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2 = bytscl(data, min=0, max=1000)
  endif
  
; SJI_FUV_2832
  if keyword_set(f_2832) then begin
     spect = 0
     if keyword_set(obsid) then w = where(index.img_path eq 'SJI_2832' and index.isqoltid eq obsid)
     ; w = where(index.img_path eq 'SJI_2832' and (index.isqoltid eq '4204000005' or index.isqoltid eq '4204000003'))
     if not keyword_set(obsid) then w = where(index.img_path eq 'SJI_2832')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2=bytscl(data, min=0, max=4000)
  endif
  
; Find any spectra data in the files
; FUV
  fuv=0
  if keyword_set(f_fuv_near) then fuv=1
  if keyword_set(f_fuv_far) then fuv=1
  if fuv eq 1 then begin
     spect = 1
     if keyword_set(obsid) then w = where(index.img_path eq 'FUV' and index.isqoltid eq obsid)
     if not keyword_set(obsid) then w = where(index.img_path eq 'FUV')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2 = bytscl(data, min=0, max=250)
     data3 = reverse(data2,2)
  endif
  
;NUV
  if keyword_set(f_nuv) then begin
     spect = 1
     if keyword_set(obsid) then w = where(index.img_path eq 'NUV' and index.isqoltid eq obsid)
     if not keyword_set(obsid) then w = where(index.img_path eq 'NUV')
     if w[0] eq -1 then begin
        box_message, 'No files found. Check times and/or obsid values.'
        return
     endif
     file2 = file(w)
     fsize = size(file2, /n_elements)
     read_iris, file2, index, data
; Scale Images
     data2 = bytscl(data, min=0, max=900)
     data3 = reverse(data2,1)
  endif
  
; Create needed arrays to populate
  d_t = strarr(fsize)           ; Date and time
  x1_arr = fltarr(fsize)        ; Lower fiducial x-position
  y1_arr = fltarr(fsize)        ; Lower fiducial y-position
  x2_arr = fltarr(fsize)        ; Upper fiducial x-position
  y2_arr = fltarr(fsize)        ; Upper fiducial y-position
  dist_arr = fltarr(fsize)      ; Calcualted distance between fiducials
  s_arr = fltarr(fsize)         ; Calculated slope between fiducials
  ang_arr = fltarr(fsize)       ; Calculated angle between fiducials
  chisq_x1_arr = fltarr(fsize)  ; Goodness of fit values for lower fiducial x-position
  chisq_x2_arr = fltarr(fsize)  ; Goodness of fit values for lower fiducial y-position
  chisq_y1_arr = fltarr(fsize)  ; Goodness of fit values for upper fiducial x-position
  chisq_y2_arr = fltarr(fsize)  ; Goodness of fit values for upper fiducial y-position
  
  if spect eq 0 then begin      ; Because SJI
     
; Start process
     for i=0, fsize-1 do begin   

; Variable that gets set to one when there is no data to find the bottom fiducial
		ycount = 0 
		
; Variable that gets set to one when there is no data to find the top fiducial	
		ycount2 = 0
		
; Set default starting values of fiducial locations 
        if keyword_set(f_1330) then begin
           x1 = 1533.5
           y1 = 252.0
           x2 = 1535.5
           y2 = 794.0
        endif   
        
        if keyword_set(f_1400) then begin
           x1 = 1541.5
           y1 = 238.0
           x2 = 1543.5
           y2 = 780.0
        endif  
        
        if keyword_set(f_2796) then begin
           x1 = 502.5
           y1 = 235.0
           x2 = 505.5
           y2 = 769.0
        endif
        
        if keyword_set(f_2832) then begin
           x1 = 504.0
           y1 = 234.0
           x2 = 507.5
           y2 = 768.0
        endif
        
; Dummy start values in case they are needed
        xb=x1
        xt=x2
        yb=y1
        yt=y2
        
; Set plot ranges
        xranl1 = x1 - 510
        xranr1 = x1 + 550
        xranl2 = x1 - 70
        xranr2 = x1 + 70
        yranb1 = y1 - 100
        yrant1 = y1 + 100
        yranb2 = y2 - 100
        yrant2 = y2 + 100
        
; Set slice ranges   
        py1l = y1 - 5
        py1r = y1 + 5
        px1l = x1 - 7
        px1r = x1 + 7
        py2l = y2 - 5
        py2r = y2 + 5
        px2l = x2 - 7
        px2r = x2 + 7
        
        if keyword_set(display) then begin      
           loadct, 0, /silent
           window, 0, xsize=900, ysize=900
           plot_image, data2(*,*,i), xrange=[xranl1, xranr1], position=[.05,.05,.98,.98]
           
; Plot slice along slit
           window, 3, xsize=1100, ysize=900
           !P.Multi = [0,2,2]
        endif
; Determine X1:
        tot_data = data2(px1l:px1r, y1+2:y1+8, i)
        gy = total(tot_data, 2)
        
; Adjust the centering pixel a little bit since the coded values may not
; be the best.
        min1 = where(gy eq min(gy))
        if size(min1, /n_elements) ge 2 then begin
           min1 = min1(0)
        endif
        mindiff = min1 - 7
        x1 = x1 + mindiff
        px1l = x1 - 7
        px1r = x1 + 7
        tot_data = data2(px1l:px1r, y1+2:y1+8, i)
        gy = total(tot_data, 2)
        
; Generate x values with the pixel offset since gy loses this info     	
        gx = indgen(15)
        gx = px1l(0) + gx
        
; Perform the fitting
        values = fid_fitter(gx, gy, px1l, /xval)
        
; Set the values returned from the fitting
        yfit = values[0:14]
        coeff = values[15]
        chisq_x1 = values[16]
        
        if keyword_set(display) then begin 
           loadct, 0, /silent
           plot, gx, gy, xtitle='X-Pixel Positions', ytitle='Intensity', title='X-Direction Slice of Bottom Feducial (X1)',$
                 yrange=[min(gy)-100, max(gy)+100], ystyle=1, xstyle=1
           
           oplot, gx, yfit, color=fsc_color('red')
        endif
; Determine if value is good and if not, throw it out
        if chisq_x1 le 50 then x1 = coeff
        if chisq_x1 gt 50 then x1 = 0    
        xd = abs(x1-xb)
        if xd ge 5 then x1 = 0
        
; Determine Y1 based on the better X1 value
        if x1 eq 0 then xc = gx(min1)
        if x1 ne 0 then xc = x1
        gy = data2(xc, py1l:py1r, i)
        
; Perform the same procedure as above
        max1 = where(gy eq max(gy))
        if size(max1, /n_elements) ge 2 then begin
           max1 = max1(0)
        endif
        mindiff = max1 - 5
        y1 = y1 + mindiff
        py1l = y1 - 5
        py1r = y1 + 5
        gy = data2(xc, py1l:py1r, i)
        gy = gy(0:10)
        gx = indgen(11)
        gx = py1l(0) + gx
        tot_size = size(gy, /n_elements)
        tot = total(gy,1)
        ave1 = average(gy)
        ave_m_diff1 = max(gy)-ave1
        if ave_m_diff1 le 3 then begin
           y1 = 0
           chisq_y1 = -9999
        endif else begin
           
           values = fid_fitter(gx, gy, py1l, /yval)
           
           yfit = values[0:10]
           coeff = values[11]
           chisq_y1 = values[12]
           
           if keyword_set(display) then begin 
              loadct, 0, /silent
              plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Bottom Feducial (Y1)',$
                    yrange=[min(gy)-20, max(gy)+20], ystyle=1
              oplot, gx, yfit, color=fsc_color('green')
           endif
           
           if chisq_y1 le 2 then y1 = coeff
           if chisq_y1 gt 2 then y1 = 0
           yd = abs(y1-yb)
           if yd ge 5 then y1 = 0
        endelse
        
; Determine X2:
        tot_data = data2(px2l:px2r, y2+2:y2+8, i)
        gy = total(tot_data, 2)
        min1 = where(gy eq min(gy))
        if size(min1, /n_elements) ge 2 then begin
           min1 = min1(0)
        endif
        mindiff = min1 - 7
        x2 = x2 + mindiff
        px2l = x2 - 7
        px2r = x2 + 7
        tot_data = data2(px2l:px2r, y2+2:y2+8, i)
        gy = total(tot_data, 2)
        gx = indgen(15)
        gx = px2l(0) + gx
        values = fid_fitter(gx, gy, px2l, /xval)
        
        yfit = values[0:14]
        coeff = values[15]
        chisq_x2 = values[16]
        
        if keyword_set(display) then begin 
           loadct, 0, /silent
           plot, gx, gy, xtitle='X-Pixel Positions', ytitle='Intensity', title='X-Direction Slice of Top Feducial (X2)',$
                 yrange=[min(gy)-100, max(gy)+100], ystyle=1, xstyle=1
           oplot, gx, yfit, color=fsc_color('orange')
        endif
        
        if chisq_x2 le 50 then x2 = coeff
        if chisq_x2 gt 50 then x2 = 0
        xd = abs(x2-xt)
        if xd ge 5 then x2 = 0
        
; Determine Y2 based on the better X2 value
        if x2 eq 0 then xc = xt
        if x2 ne 0 then xc = x2
        gy = data2(xc,py2l:py2r, i)
        max1 = where(gy eq max(gy))
        if size(max1, /n_elements) ge 2 then begin
           max1 = max1(0)
        endif
        mindiff = max1 - 5
        y2 = y2 + mindiff
        py2l = y2 - 5
        py2r = y2 + 5
        gy = data2(xc, py2l:py2r, i)
        gy = gy(0:10)
        gx = indgen(11)
        gx = py2l(0) + gx
        tot_size = size(gy, /n_elements)
        tot = total(gy,1)
        ave1 = average(gy)
        ave_m_diff1 = max(gy)-ave1     
        if ave_m_diff1 le 3 then begin
           y2 = 0
           chisq_y2 = -9999
        endif else begin
           
           values = fid_fitter(gx, gy, py2l, /yval)
           
           yfit = values[0:10]
           coeff = values[11]
           chisq_y2 = values[12]
           
           if keyword_set(display) then begin       
              loadct, 0, /silent
              plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Top Feducial (Y2)',$
                    yrange=[min(gy)-20, max(gy)+20], ystyle=1, xstyle=1
              oplot, gx, yfit, color=fsc_color('blue')
           endif
           
           if chisq_y2 le 2 then y2 = coeff
           if chisq_y2 gt 2 then y2 = 0
           yd = abs(y2-yb)
           if yd ge 5 then y2 = 0
           
        endelse
; A tracker on the plots to know what file you are on
        sfsize = trim(string(fsize))
        si = trim(string(i+1))
        itrack = 'File ' + si + ' out of ' + sfsize
        xyouts, 535, 885, itrack, /device
        
; Calculate the distance, slope and angle
        
        if y1 eq 0 then begin
           d=0
           s=0
           ang=0
           print, 'Bottom Feducial location undetermined'
        endif
        if y2 eq 0 then begin
           d=0
           s=0
           ang=0
           print, 'Top Feducial location undetermined'
        endif
        if y1 ne 0 and y2 ne 0 then begin
           d = sqrt((x2-x1)^2 + (y2-y1)^2)
           s = 1/((y2-y1)/(x2-x1))
           ang = 180/!pi*atan(s)
           
           print, 'Distance between feducials is: ', trim(d), ' pixels'
           print, 'Slope of slit is: ', trim(s)
           print, 'Angle away from optimal vertical position: ', trim(ang), ' degrees'
        endif
        
; Fill in arrays with measured and calculated info      
        d_t(i) = index(i).t_obs
        x1_arr(i) = x1
        y1_arr(i) = y1
        x2_arr(i) = x2
        y2_arr(i) = y2
        dist_arr(i) = d
        s_arr(i) = s
        ang_arr(i) = ang
        chisq_x1_arr(i) = chisq_x1
        chisq_x2_arr(i) = chisq_x2      
        chisq_y1_arr(i) = chisq_y1
        chisq_y2_arr(i) = chisq_y2
        
        !P.Multi = [0,0,0]      ; Reset plotting
     endfor
     
; Determine the avearge values to get offsets between types
     y1_g = y1_arr(where(y1_arr gt 0))
     y2_g = y2_arr(where(y2_arr gt 0))
     x1_g = x1_arr(where(x1_arr gt 0))
     x2_g = x2_arr(where(x2_arr gt 0))
     
     y1_ave = average(y1_g)
     y2_ave = average(y2_g)
     x1_ave = average(x1_g)
     x2_ave = average(x2_g)
     
; Print the table of information (currently surpressed)   
;   print, 'Date_Time                    ', 'x1           ', 'y1           ', 'x2           ', $
;          'y2           ', 'Distance  ', 'Slope          ', 'Angle'
;   forprint, d_t, x1_arr, y1_arr, x2_arr, y2_arr, dist_arr, s_arr, ang_arr
     
; Save these values out so they can be read in later and used to
; create plots over large times.
     da_ti = strmid(d_t(0), 0, 19)
     da_ti = str_replace(da_ti, '-', '_')
     da_ti = str_replace(da_ti, ':', '_')
     fil = index(0).img_path
     
     if keyword_set(desc) then begin
        desc = desc
        file_name = da_ti + '_' + fil + '_' + desc + '_Fid_Var.sav'
     endif
     if not keyword_set(desc) then file_name = da_ti + '_' + fil + '_Fid_Var.sav'
     
     save, d_t, x1_arr, y1_arr, x2_arr, y2_arr, dist_arr, s_arr, ang_arr, chisq_x1_arr, chisq_y1_arr, chisq_x2_arr, chisq_y2_arr,filename = file_name
     
  endif
  
;====================================================================================================================================
;--------------------------------------------------------Spectra Fiducials-----------------------------------------------------------
;====================================================================================================================================
; My version to find spectral fiducials since Hui's code doesn't
; work for general cases. 
  
  if spect eq 1 then begin
     
; Display the spectra
     for i=0, fsize-1 do begin
        if keyword_set(display) then begin 
           loadct, 0, /silent
           window, 0, xsize=1600
           plot_image, data3(*,*,i), position=[.05,.05,.98,.98]
           window, 1, xsize=1100, ysize=900
           !P.Multi = [0,1,2]
        endif
; Define the standard locations for the fiducials 
        
        if keyword_set(f_nuv) then begin
           x1 = 531.0
           y1 = 260.0
           x2 = 545.0
           y2 = 800.0
        endif
        
        if keyword_set(f_fuv_near) then begin
; There are differnt possible sizes the spectral dimension could be	
           if index(i).naxis1 lt 1040 then begin
              x1 = 54.0
              y1 = 216.0 
              x2 = 51.0
              y2 = 756.0
           endif
           if index(i).naxis1 gt 1040 and index(i).naxis1 lt 4144 then begin
              x1 = 113.0     
              y1 = 216.0      
              x2 = 106.0      
              y2 = 755.0
           endif
           if index(i).naxis1 gt 2072 then begin
              x1 = 225.0
              y1 = 217.0
              x2 = 208.0
              y2 = 756.0
           endif	
        endif
        
        if keyword_set(f_fuv_far) then begin
; It is assumed that running the far case means that you want to capture data from the 
; the other detector. If the detector isn't there then the program bails.	
           if index(i).naxis1 gt 2072 and index(i).naxis1 le 4144 then begin
              x1 = 3818.0
              y1 = 245.0
              x2 = 3793.0
              y2 = 785.0
           endif
           if index(i).naxis1 lt 2100 then begin
              box_message, 'No far FUV data (not the other detector)'
              return
           endif
        endif
        
; Set dummy values in case they are needed	
        xb = x1
        yb = y1
        xt = x2
        yt = y2
        
; Set slice ranges 
        py1l = y1 - 5
        py1r = y1 + 5
        
        px1l = x1 - 7
        px1r = x1 + 7
        
        py2l = y2 - 5
        py2r = y2 + 5
        
        px2l = x2 - 7
        px2r = x2 + 7
        
; Now that starting positions are defined - take slices along y-axis
; and fit to Guassians just like in the SJIs
        
; Do a quick slice across bright line to get better x-values.
        pxl = px1l - 10
        pxr = px1r + 10
        gy = data3(pxl:pxr, y1+10, i)
        gx = indgen(35)
        gx = pxl(0) + gx
        values = fid_fitter(gx, gy, pxl, /yval) 
        yfit = values[0:34]
        coeff = values[35]
        chisq_x1 = values[36]
        
        if chisq_x1 le 3 then x1 = coeff
        if chisq_x1 gt 3 then x1 = xb
        if x1 lt 0 then x1 = xb
        
; The slicing and fitting is done in the same way as the SJIs      
; Determine y1
        tot_data = data3(x1-1:x1+1, py1l:py1r, i)
        gy = total(tot_data, 1)
        
; Adjust the centering pixel a little bit since the coded values may not
; be the best.
        min1 = where(gy eq min(gy))
        if size(min1, /n_elements) ge 2 then begin
           min1 = min1(0)
        endif
        mindiff = min1 - 5
        y1 = y1 + mindiff
        py1l = y1 - 5
        py1r = y1 + 5
        tot_data = data3(x1-1:x1+1, py1l:py1r, i)
        gy = total(tot_data, 1)
      
; Continue on to perform slice.
        gx = indgen(11)
        gx = py1l(0) + gx
        ave1 = average(gy)
        m1 = max(gy)
        
        if m1 eq 0 then begin
        	ycount = 1
        	plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Bottom Feducial (Y1)',$
                    yrange=[min(gy)-20, m1+20], ystyle=1, xstyle=1, /nodata
        	xyouts, 400, 675, 'No data to find bottom fiducial', charsize=2, /device
        	box_message, 'No data to find bottom fiducial'
        endif	
        
        ave_m_diff1 = m1-ave1
stop
        if ave_m_diff1 le 3 then begin
           y1 = 0
           chisq_y1 = -9999
        endif else begin
           
           values = fid_fitter(gx, gy, py1l, /xval) ; xval since looking at drop in intensity
           
           yfit = values[0:10]
           coeff = values[11]
           chisq_y1 = values[12]
           
           if keyword_set(display) then begin         
              loadct, 0, /silent
              plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Bottom Feducial (Y1)',$
                    yrange=[min(gy)-20, m1+20], ystyle=1, xstyle=1
              oplot, gx, yfit, color=fsc_color('green')
           endif
       
; Save out the Y value if the fitting worked well
           if chisq_y1 le 3 then y1 = coeff
           if chisq_y1 gt 3 then y1 = 0
           yd = abs(y1-yt)
           if yd ge 5 then y1 = 0
        endelse    
        
; Do a quick slice across bright line to get better x-values.
        pxl = px2l - 10
        pxr = px2r + 10
        gy = data3(pxl:pxr, y2+10, i)
        gx = indgen(35)
        gx = pxl(0) + gx
        values = fid_fitter(gx, gy, pxl, /yval) 
        yfit = values[0:34]
        coeff = values[35]
        chisq_x2 = values[36]
        
        if chisq_x2 le 3 then x2 = coeff
        if chisq_x2 gt 3 then x2 = xb
        if x2 lt 0 then x2 = xb
        
; Determine y2
        tot_data = data3(x2-1:x2+1, py2l:py2r, i)
        gy = total(tot_data, 1)
        
; Adjust the centering pixel a little bit since the hard coded values may not
; be the best.
        min1 = where(gy eq min(gy))
        if size(min1, /n_elements) ge 2 then begin
           min1 = min1(0)
        endif
        mindiff = min1 - 5
        y2 = y2 + mindiff
        py2l = y2 - 5
        py2r = y2 + 5
        tot_data = data3(x2-1:x2+1, py2l:py2r, i)
        gy = total(tot_data, 1)
        
; Continue on to perform slice.
        gx = indgen(11)
        gx = py2l(0) + gx
        ave1 = average(gy)
        m1 = max(gy)
        
        if m1 eq 0 then begin
        	ycount2 = 1
        	plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Top Feducial (Y2)',$
                    yrange=[min(gy)-20, m1+20], ystyle=1, xstyle=1, /nodata
        	xyouts, 400, 285, 'No data to find Top fiducial', charsize=2
        	box_message, 'No data to find Top fiducial'
        endif
        
        ave_m_diff1 = m1-ave1
        if ave_m_diff1 le 3 then begin
           y2 = 0
           chisq_y2 = -9999
        endif else begin
           
           values = fid_fitter(gx, gy, py2l, /xval)
           
           yfit = values[0:10]
           coeff = values[11]
           chisq_y2 = values[12]
           
           if keyword_set(display) then begin    	
              loadct, 0, /silent
              plot, gx, gy, xtitle='Y-Pixel Positions', ytitle='Intensity', title='Y-Direction Slice of Top Feducial (Y2)',$
                    yrange=[min(gy)-20, m1+20], ystyle=1, xstyle=1              
              oplot, gx, yfit, color=fsc_color('blue')
           endif
           
; Save the Y value if the fiting works well
           if chisq_y2 le 3 then y2 = coeff
           if chisq_y2 gt 3 then y2 = 0
           yd = abs(y2-yt)
           if yd ge 5 then y2 = 0
        endelse
        !P.Multi = [0,0,0]

; If both the bottom and the top fiducial could not be found in the same image then 
; assume that either there is no data that covers either fiducial or that the hard-
; coded pixel points are incorrect and leave the program. Either of these could be true 
; since the trigger is having the max value of the slice be 0. 
		if ycount eq 1 and ycount2 eq 1 then begin
			box_message, 'Neither fiducial could be found. Data is either not present or the slice locations need to be adjusted'
			return
		endif
        
; Calculate the distance, slope and angle
        if y1 eq 0 then begin
           d=0
           s=0
           ang=0
           print, 'Bottom Feducial location undetermined'
        endif
        
        if y2 eq 0 then begin
           d=0
           s=0
           ang=0
           print, 'Top Feducial location undetermined'
        endif
        
        if y1 ne 0 and y2 ne 0 then begin
           d = sqrt((x2-x1)^2 + (y2-y1)^2)
           s = 1/((y2-y1)/(x2-x1))
           ang = 180/!pi*atan(s)
           
           print, 'Distance between feducials is: ', trim(d), ' pixels'
           print, 'Slope of slit is: ', trim(s)
           print, 'Angle away from optimal vertical position: ', trim(ang), ' degrees'
        endif
        
; Fill in the data      
        d_t(i) = index(i).t_obs
        x1_arr(i) = x1
        y1_arr(i) = y1
        x2_arr(i) = x2
        y2_arr(i) = y2
        dist_arr(i) = d
        s_arr(i) = s
        ang_arr(i) = ang
        chisq_y1_arr(i) = chisq_y1
        chisq_y2_arr(i) = chisq_y2
        
; Save these values out so they can be read in later and used to
; create plots over large times.
        da_ti = strmid(d_t(0), 0, 19)
        da_ti = str_replace(da_ti, '-', '_')
        da_ti = str_replace(da_ti, ':', '_')      
        fil = index(0).img_path
        
        if keyword_set(desc) then begin
           desc = desc
           file_name = da_ti + '_' + fil + '_' + desc + '_Fid_Var.sav'
        endif
        
        if not keyword_set(desc) then begin
           file_name = da_ti + '_' + fil + '_Fid_Var.sav'
        endif  
        
        save, d_t, x1_arr, y1_arr, x2_arr, y2_arr, dist_arr, s_arr, ang_arr, chisq_y1_arr, chisq_y2_arr,filename = file_name
     endfor 
  endif
  stop
end


