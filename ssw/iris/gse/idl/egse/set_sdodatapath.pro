Pro set_sdoDataPath, select=select
;+
; 	Name: set_sdoDataPath
;	$Id: set_sdodatapath.pro 4942 2012-01-13 03:52:02Z sabolish $
;	$Name:  $
;
;	Purpose: set the packet data path for sag_plot_mnem etc.
;
;	Formal Parameter/Keyword:
;	select 	if present will post a menu to user to allow manual selection
;		of the IDL data path. Default is always to use the A-side or -cmd
;		machine.
;
;	History: Created for SDO SOC or JSOC, G.Linford, Jun 18, 2008
;
;	NOTE: set_sdoDataPath, made from set_sdoDataPath to get around an IDL
;		compiler feature or bug depending on your point of view, that
;		does not auto compile UNIX file names which have mix cases!!!
;
;	$Log: set_sdodatapath.pro,v $
;	Revision 1.6  2010/02/18 22:23:13  sdotc
;	added paths to plot copies of TVAC data
;	
;	Revision 1.5  2010/02/03 19:33:36  linford
;	added fm1a-b machines default settings
;	
;	Revision 1.4  2010/02/03 19:24:06  linford
;	add ioc-cmd and moc-cmd for default settings
;	
;	Revision 1.3  2009/05/13 18:51:21  linford
;	Added aiafm1a, 1b to path selection
;	
;	Revision 1.2  2009/05/13 00:43:52  linford
;	Added hmifm1a, 1b to path selection
;	
;	Revision 1.1  2008/08/21 22:00:01  linford
;	replaces set_sdoDataPath
;	
;	Revision 1.3  2008/08/05 16:44:53  linford
;	update for MOC machines, fix idex offset
;	
;	Revision 1.2  2008/08/05 16:42:15  linford
;	update for MOC machines
;	
;	Revision 1.1  2008/06/18 22:15:32  linford
;	checkin simple sdoDataPath setter
;	
;
;-

ahost = get_host();		Get the current host:

;res = strCmp( string1, string2, N);
;res = strSplit( string, 'pattern')

;res = strmid('expression',first_char,len,/rev)

;res = strPos('expression',search_string)   if not available -> -1

;aiaioc-mon -> /net/aiaioc-cmd/disk1/packets

; Data Paths:
dpath1 = '/net/aiaioc-cmd/disk1/log/packets'
dpath2 = '/net/aiaioc-mon/disk1/log/packets'
dpath3 = '/net/hmiioc-cmd/disk1/log/packets'
dpath4 = '/net/hmiioc-mon/disk1/log/packets'
dpath5 = '/net/aiamoc-cmd/disk1/log/packets'
dpath6 = '/net/aiamoc-mon/disk1/log/packets'
dpath7 = '/net/hmimoc-cmd/disk1/log/packets'
dpath8 = '/net/hmimoc-mon/disk1/log/packets'
dpath9 = '/net/hmifm1a/disk2/log/packets'
dpath10= '/net/hmifm1b/disk2/log/packets'
dpath11= '/net/aiafm1a/disk2/log/packets'
dpath12= '/net/aiafm1b/disk2/log/packets'
dpath13= '/net/aiamoc-mon/disk1/archive/aiafm1a/egsesw/log/packets'
dpath14= '/net/hmimoc-mon/disk1/archive/hmifm1a/egsesw/log/packets'
paths = [dpath1, dpath2, dpath3, dpath4, dpath5, dpath6, dpath7, dpath8, dpath9, dpath10, dpath11, dpath12, dpath13, dpath14 ]

if keyword_set(select) then begin
	res = xmenu_sel(paths,/one)
endif else begin
	if ( strMatch(ahost,'aiaioc-mon') ) then begin
		res = 0
	endif
	if ( strMatch(ahost,'aiaioc-cmd') ) then begin
		res = 0
	endif
	if ( strMatch(ahost,'hmiioc-mon') ) then begin
		res = 2
	endif
	if ( strMatch(ahost,'hmiioc-cmd') ) then begin
		res = 2
	endif
	if ( strMatch(ahost,'aiamoc-mon') ) then begin
		res = 4
	endif
	if ( strMatch(ahost,'aiamoc-cmd') ) then begin
		res = 4
	endif
	if ( strMatch(ahost,'hmimoc-mon') ) then begin
		res = 6
	endif
	if ( strMatch(ahost,'hmimoc-cmd') ) then begin
		res = 6
	endif
	if ( strMatch(ahost,'hmifm1b') ) then begin
		res = 8
	endif
	if ( strMatch(ahost,'hmifm1a') ) then begin
		res = 8
	endif	
	if ( strMatch(ahost,'aiafm1b') ) then begin
		res = 10
	endif
	if ( strMatch(ahost,'aiafm1a') ) then begin
		res = 10
	endif	
endelse

set_logenv,'GSE_PKT_FILE_DIR',paths[res]

print,'Setting the IDL Packet Data Path to: ', paths[res]

end
