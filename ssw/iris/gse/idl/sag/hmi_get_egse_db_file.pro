function hmi_get_egse_db_file, prefix
;+
;NAME:
;	hmi_get_egse_db_file
;PURPOSE:
;	Return the filename of the current database file
;SAMPLE CALLING SEQUENCE:
;	infil = hmi_get_egse_db_file(getenv('HMI_EGSE_DB_PREFIX'))
;HISTORY:
;	Written 23-Sep-99 by M.Morrison
;	 7-Jul-00 (MDM) - Moved SXI_EGSE_DB_FILE override from sag_sxi_rd_hk_db.pro
;	 14-Jan-05 (GAL) - Name change.
;-
;
if (n_elements(prefix) eq 0) then prefix = ''
;
ff = file_list('$GSE_DATABASE_DIR', prefix+'*')
infil = ff(n_elements(ff)-1)
;
tmp = getenv('HMI_EGSE_DB_FILE')
if (keyword_set(tmp)) then infil = tmp
;
return, infil
end
