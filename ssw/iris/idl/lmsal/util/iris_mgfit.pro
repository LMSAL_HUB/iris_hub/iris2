function iris_mgfit,x,p
;PURPOSE-generates a vector of a double gaussian + wing line profile
;        described in schmit et al 2015 ApJ
;INPUTS: X-vector of the abscissa
;        P- 9 element vector that contains the gaussian parameters
;P[0]=Background intensity
;P[1]=wing slope
;P[2]=centroid of linear wing profile
;P[3]=amplitude of positive gaussian
;P[4]=centroid of positive gaussian
;P[5]=width^2 of the positive gaussian
;P[6]=fractional amplitude of negative gaussian
;P[7]=centroid of negative gaussian;
;P[8]=width^2 of negative gaussian
;
;OUTPUTS: Vector of same size as X with the line profile

y=p(0)+p(1)*abs(p(2)-x)+p(3)*exp(-(x-p(4))^2/p(5))-p(6)*p(3)*exp(-(x-p(7))^2/p(8))
return,y
end

