pro ssw_ppm2png, indir, outdir
ppmfiles=file_search(indir,'*.ppm')
nfiles=n_elements(ppmfiles)
break_file,ppmfiles,ll,pp,ff
pngnames=ff+'.png'

if n_elements(outdir) eq 0 then outdir=indir
pngfiles=concat_dir(outdir,pngnames)

for i=0,nfiles-1 do begin 
   read_ppm,ppmfiles(i),data
   write_png,pngnames(i),data

endfor


return
end

