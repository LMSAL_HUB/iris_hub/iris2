pro iris_l2_multi_rast_rebin, ii,dd ,oii, odd,  best_pos=best_pos, maxxy=maxxy, nx=nx, stack=stack, confactor=confactor, _extra=_extra, label=label

if not required_tags(ii,'wavepix0,wavepixn,wavenames') then begin 
   box_message,'expect output from read_iris_l2_multi_rast..., bailing'
   return
endif

nx0=data_chk(dd,/nx)
ny0=data_chk(dd,/ny)
ndimen=data_chk(dd,/ndim)

i0=ii[0]

wp0=fix(str2arr(i0.wavepix0))
wp1=fix(str2arr(i0.wavepixn))
nw=n_elements(wp0)

sd0=size(dd)

nr=([1,(sd0)(3)])(ndimen eq 4)

if n_elements(nx) eq 0 then nx=1280
case 1 of 
   keyword_set(confactor): begin 
      confact=confactor
      nxn=nx0*confact
      nyn=ny0*confact
   endcase
   keyword_set(nx): begin 
      nxn=nx
      confact=float(nxn)/nx0
      nyn=ny0*confact
   endcase
   else: stop,'impossible to get here, congrats.
endcase


if ndimen eq 4 then dd=reform(dd,nx0,ny0,nr*sd0[4])

odd=make_array(nxn+nw,nyn,data_chk(dd,/nim),type=data_chk(dd,/type))
npn0=wp0*confact

labelit=keyword_set(label)

wnames=str2arr(i0.wavenames)
wmins= strtrim(str2arr(i0.wavemins),2)
wmaxs= strtrim(str2arr(i0.wavemaxs),2)

for w=0,nw-1 do begin 
   wdat=dd[wp0[w]:wp1[w],*,*]
   cdat=confac(temporary(wdat),confact,_extra=_extra)
   nimg=data_chk(cdat,/nim)
   
   if labelit then begin 
       ;if w eq 0 then cdat=align_label_cube(anytim(ii.date_obs,out='ecs',/trunc),cdat,_extra=_extra)
       ;cdat=align_label_cube(replicate(wmins[w],nimg),cdat,orientation=90,/ul,_extra=_extra,/clobber)
       ;cdat=align_label_cube(replicate(wmaxs[w],nimg),cdat,orientation=90,/ur,_extra=_extra,/clobber)
       cdat=align_label_cube(replicate(wnames[w],nimg),cdat,/uc,_extra=_extra,/clobber)
   endif   
   odd[npn0[w],0,0]=cdat ; insert this wave->
endfor

if ndimen eq 4 then dd=reform(dd,nx0,ny0,nr,sd0[4])

oii=ii


return
end





;
;
