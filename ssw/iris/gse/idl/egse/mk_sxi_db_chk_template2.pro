pro mk_sxi_db_chk_template2, sort_option=sort_option, $
				select=select
;
;
;
sag_sxi_rd_hk_db, tlm, alg, dsc, cmd, /qdebug
;
if (n_elements(sort_option) eq 0) then sort_option = 1
case sort_option of
    1: begin
	ss = sort(cmd.fil(0).value)	;sort by opcode
	end
    2: begin
	ss = sort(cmd.name)		;sort alphabetically
 	end
endcase
;
fmt1 = '(a23,1x, a1,1x, a53, 1x, a7, 1x, a7, 1x, "# ", a34)'
fmtc = '(1x, z2.2,1x,a30)
tit = ['Telemetry Mnemonic', 'ConvTyp', 'Command to issue', 'ValMin', 'ValMax', 'Comments']
dash = replicate('-----------------------------------------------------------------',6)

n = n_elements(ss)
out_tab = strarr(n+2)
out_lst = strarr(n)
out_tab(0) = string(tit, format=fmt1)
out_tab(1) = string(dash, format=fmt1)
for i=0L,n-1 do begin
    cmd0 = cmd(ss(i))
    cmdstr = '/' + cmd0.name
    for j=0,cmd0.nkey-1 do cmdstr = cmdstr + $
	' ' + cmd0.key(j).label + '=' + strtrim(cmd0.key(j).default,2)
    cmdstr = cmdstr + '                                                             '
    ;
    comment = string(cmd0.fil(0).value, $
				cmd0.descr, format=fmtc)
    out0 = string('.', 'R', cmdstr, $
				'.', '.', $
     			        comment, format=fmt1)
    out_tab(i+2) = out0
end

prstr, out_tab
prstr, out_tab, file='$SAG_TELEM_INFO/sxi_db_check_template2.tab'
end

