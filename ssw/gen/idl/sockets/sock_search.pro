;+
; Project     : VSO
;
; Name        : SOCK_SEARCH
;
; Purpose     : Search for files or directories at a URL 
;
; Category    : utility system sockets
;
; Syntax      : IDL> sock_search,url,results
;
; Inputs      : URL = remote URL directory to search
;
; Outputs     : RESULTS = remote file or directory names
;
; Keywords    : ERR = error string
;               COUNT = # of results
;               DIRECTORY = search for directories
;
; History     : 20-Dec-2018, Zarro (ADNET)
;-

pro sock_search,url,results,_ref_extra=extra,err=err,count=count,directory=directory

results='' & count=0
err=''

if ~is_url(url,/scheme) then begin
 err='Missing or invalid input URL.'
 mprint,err
 return
endif

if is_ftp(url) then begin
 sock_dir_ftp,url,results,_extra=extra,err=err,count=count
 if (n_params() eq 1) then print,results
 return
endif

sock_list,url,dirs,_extra=extra,err=err

;-- parse out file or directory names

directory=keyword_set(directory)
if is_blank(err) then begin
 spec='[^\/\?\:\=\;\,\\]+'
 delim=''
 if directory then delim='\/'

 chk1=stregex(dirs,'<a href="('+spec+')'+delim+'">'+spec+delim+'</a>',/ext,/sub)
 chk1=strtrim(chk1,2)
 temp=chk1[1,*]
 chk2=where(temp ne '' and strpos(temp,'.') ne 0,count)
 if count gt 0 then begin
  results=url+'/'+comdim2(chk1[1,chk2]) 
 endif else results=''
endif

if is_string(err) then mprint,err
if (n_params() eq 1) then print,results
return & end
