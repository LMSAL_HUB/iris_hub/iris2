pro IRIS_PREP_TREND_VERSIONS, logdir, typehdr

;+
;
; Write a genx file containing version information for the iris_prep software and
; ancillary databases used to process the data. The most important keywords are
; these:
;
;	IPRPVER (iris_prep version)
;	IPRPGVER (Geowave recnum)
;	IPRPFVER (flat recnum)
;	IPRPPDBV (PDB version)
;
; I thought about having it just write these, but decided there's no reason not to
; write the whole header array in case you decide some other keyword is important 
; some time down the line.
;
;-

SAVEGEN, file = CONCAT_DIR(logdir, 'versions.genx'), typehdr

end
