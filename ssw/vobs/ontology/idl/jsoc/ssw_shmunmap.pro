pro ssw_shmunmap, pattern, menu=menu

;+
;   Name: ssw_shmunmap
;
;   Purpose: unamp shared memory segments; all (default) or via menu
;
;   Input Paramters:
;       pattern - optional pattern to match against SHM segment name
;
;   Keyword Parameters:
;      menu - if set, use menu
;
help,/shared_memory,out=out
sm=where(out ne '',smcnt)
if smcnt eq 0 then begin 
   box_message,'No shared memory segments
   return
endif

if n_elements(pattern) eq 0 then pattern='IDL_SHM*'

ss=where(strmatch(out,pattern,/fold_case),sscnt)
if sscnt eq 0 then begin
   box_message,'No shared memory segment names match pattern>> '+pattern(0)
   return
endif

msegs=out(ss)
cols=str2cols(msegs,/unaligned)
strtab2vect,cols,snames
if keyword_set(menu) then begin
  ss=xmenu_sel(msegs)
  if ss(0) eq -1 then begin 
     box_message,'No selection.. returning'
     return
  endif
  snames=snames(ss)
endif

for i=0,n_elements(snames) -1 do shmunmap,snames(i) ; unmap

end
