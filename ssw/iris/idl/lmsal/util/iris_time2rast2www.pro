pro iris_time2rast2www,t1,t2, no_mp4=no_mp4,no_ingest=no_ingest, _extra=_extra, do_mp4=do_mp4
;
;+
;   Name: iris_time2rast2www
;
;   Purpose: driver for auto HCR IRIS L2 raster -> WWW summmaries (calls iris_l2_rast2www et al)
;
;   History:
;     1-jul-2014 - S.L.Freeland 
;    20-nov-2014 - S.L.Freeland - changed default to NO_MP4 (now done via ssw_ffmpeg via special_movie2)
;    21-jul-2016 - S.L.Freeland - add iris_raster_movies call (-> <obs>/www/irm_raster/... )
;    16-may-2017 - S.L.Freeland - allow 1st param to be an IRIS obsid yyyymmdd_hhmmss_<obsid>
;    04-Feb-2019 - P.G.Shirts   - changed it to call iris_raster_movies3.pro, and use a sanhome staging directory.
;-

domp4=keyword_set(do_mp4)

if n_params() eq 1 then hcr=iris_obs2hcr(t1) else hcr=iris_obs2hcr(t1,t2)
if not data_chk(hcr,/struct) then begin
   box_message,'No HCR records in this time window, so bailing...
   return ; early exit on no HCR, since assumes /www/index.html (SJI) & VOE/xml exists
endif
obspaths=ssw_strsplit(hcr.umodes,'iris_l2',/head)
ss=where(file_exist(obspaths),ecnt)
if ecnt eq 0 then begin 
   box_message,'No obspaths found
   return
endif
obspaths=obspaths[ss]
hcr=hcr[ss]

if get_logenv('check_hcr') ne '' then stop

hcrcal=where(strmatch(hcr.sciobjectives,'*cal*',/fold) or $
strmatch(hcr.sciobjectives,'*throughput*',/fold) or strmatch(hcr.sciobjectives,'*sensitiv*',/fold))

hcrsci=rem_elem(indgen(n_elements(hcr)),hcrcal,scount) ; "science" modes only (exlude cal)
if scount gt 0 then hcr=hcr[hcrsci] else box_message,'Only calibration OBS in this time range?'

for o=0,scount-1 do begin 
   box_message,'Doing> ' + hcr[o].obstitle
   l2stat=iris_l2_status(hcr[o].obspath)
   if l2stat.l2_errlog_exist or l2stat.l2_inprogress then begin 
      box_message,'L2 Error or in-progress - skipping this obs
   endif else begin
      iris_l2_rast2www,obspaths[o],/new_paradigm,_extra=_extra ; generate raster summary graphics/movie in <obspath>/www/raster/<stuff>
      if domp4 then iris_make_mp42,obsin=obspaths[o],/raster ; generate mp4 and replace anis (_a.html) with mp4 (.mov) in .../raster/index.html
      iris_l2_rast2landing,obspaths[o],no_ingest=no_ingest ; add .../raster/ hooks-> /www/index.html and VOE and update HCR entry
      ;original iris_raster_movies,hcr[o].full_obsid,/online,/write_video ; /plot_lambda - lambda plot off 9-aug-2016
      iris_raster_movies3,hcr[o].full_obsid,staging_dir='/sanhome/data_ops/iris_raster_movies_build/',/online  ;will this staging dir definition work?
   endelse
endfor

return
end





