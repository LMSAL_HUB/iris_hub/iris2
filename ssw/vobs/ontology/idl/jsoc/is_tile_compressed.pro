function is_tile_compressed, fitsfiles, count=count, wherex=wherex
;
;+ 
;   Name: is_tile_compressed
;
;   Purpose: boolean - input files tile compressed? - optionally, return where
;
;   Input Parameters:
;      fitsfiles - list of one or more fits files to check
;
;   Keyword Parameters:
;      wherex - if set, return where(compressed) ; -1 if none compressed
;      count - number of compressed
;
;   History:
;       18-oct-2011 - S.L.Freeland - tile compression support utility
;       15-nov-2011 - S.L.Freeland - sped up 
;
;   
;-

if n_elements(fitsfiles) eq 0 then begin 
   box_message,'Need fitsfile(s) vector input...'
   return,-1
endif

nret=n_elements(fitsfiles)
retval=intarr(nret)
for i=0,nret-1 do begin 
   isfits=is_fits(fitsfiles(i),ext)
   retval(i)=ext eq 'BINTABLE'
endfor

count=total(retval)


if keyword_set(wherex) then retval=where(retval eq 1,count)

if n_elements(retal) eq 1 then retval=retval(0)

return,retval
end


