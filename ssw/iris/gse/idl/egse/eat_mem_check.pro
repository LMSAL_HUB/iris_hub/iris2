;+
;NAME:
;	eat_mem_check
;PURPOSE:
;	Perform the EGSE acceptance test memory check portion
;HISTORY:
;	Written 5-Mar-01 by M.Morrison
;-
;
if (n_elements(dattim) eq 0) then datetim = ut_time()
if (n_elements(occurance) eq 0) then occurance = 0
;
input, 'Enter memory dump occurance to check', occurance, occurance
;
sxi_disp_mem_dump, /hc

stol_file = '$GSE_CFL_BASE_DIR/test/src/eat_mem_load1.src'
sxi_mem_compare, stol_file, datetim, occurance
;
end