function iris_time2magnetometer, t0, t1, quiet=quiet, bad_records=bad_records
;;
;   Name: iris_time2magnetometer
;
;   Purpose: time -> Paul Boerners www web summaries
;
;   Input Parameters"
;   t0,t1 - users time range
;
;   Keyword Parameters:
;      bad_records (output) - vector of "bad" records (unexpected # of colums)
;
;   History:
;      Circa 15-aug-2013 - S.L.Freeeland
;      11-jan-2014 - filter out bad mag data records (usually NaN,Infinity,overflow...) 
;-
;

loud=1-keyword_set(quiet)

case n_params() of
   0: begin 
         t0=reltime(days=-1,/day_only)
         t1=reltime(days=1,/day_only)
   endcase
   1: t1=reltime(t0,/days,/day_only)
   else:
endcase

t0x=reltime(t0,days=-1) ; lookback one day 

parent='http://www.lmsal.com/~boerner/iris/magnetometer/'
urls=ssw_time2filelist(t0x,t1,parent=parent,/pflat,ext='.txt',/quiet)

if urls[0] eq '' then begin
   box_message,'No urls within your start range..
   return,-1
endif

if loud then box_message,urls[0]
sock_list,urls[0],ulist

; create template struture 
tags=str2arr(strtrim(strcompress(ulist[0]),2),' ')
ttypes=replicate('D',n_elements(tags)) ; defaut=double
dd=where(strpos(tags,'DATE') ne -1 or strpos(tags,'TIME') ne -1)
ttypes[dd]='A'
create_struct_temp,template,'',tags,ttypes

for u=1,n_elements(urls)-1 do begin 
   if loud then box_message,urls[u]
   sock_list,urls[u],ulistu
   ulist=[temporary(ulist),ulistu]
endfor

ulist=ulist[where(strpos(ulist,tags[0]) eq -1)]

; check for bad table alignment
bulist=total(byte(strcompress(ulist)) eq 32b,1) ; # blanks per line 
ssb=where(bulist ne max(bulist),bcnt)
if bcnt gt 0 then begin
   box_message,'ignoring ' + strtrim(bcnt,2) + ' bad records out of ' +strtrim(n_elements(ulist),2)
   bad_records=ulist[ssb] ; return via keyword
   ulist=ulist(where(bulist eq max(bulist)))
   
end
   


retval=replicate(template,n_elements(ulist))

cols=str2cols(ulist,' ',/trim)
for t=0,n_tags(retval)-1 do begin 
   retval.(t)=reform(cols[t,*])
endfor

sst=where(anytim(retval.date_obs) ge anytim(t0) and anytim(retval.date_obs) le anytim(t1),sscnt)

if sscnt eq 0 then begin 
  box_message,'No records in your time range
  retval=-1
endif else begin 
  retval=retval[sst]
endelse

return,retval

end



