function calc_dist, x, y, mio=mio , vector=vector, blas=blas, batch_size=batch_size, weights=weights


dim_x = size(x, /dim)
dim_y = size(y, /dim)
x = double(x)
y = double(y)
if keyword_set(mio) then begin 
   sum1 = rebin(total(x*x, 1), dim_x[1], dim_y[1])
   sum2 = rebin(transpose(total(y*y, 1)), dim_x[1], dim_y[1])
   sum3 = -2.*(matrix_multiply(x, y, /atrans))
   ;help, sum1, sum2, sum3
   ;stop
   if keyword_set(weights) eq 0 then begin
      out = sqrt(sum1+sum2+sum3)
   endif else begin
      ;weights = rebin(total(weights*weights, 1), dim_x[1], dim_y[1])
      ; out = sqrt((sum1+sum2+sum3)/(weights*weights))
      weights2x = rebin(weights*weights, dim_x[0], dim_x[1])
      sum1 = rebin(total(x*x*weights2x, 1), dim_x[1], dim_y[1])
      weights2y = rebin(weights*weights, dim_y[0], dim_y[1])
      sum2 = rebin(transpose(total(y*y*weights2y, 1)), dim_x[1], dim_y[1])
      sum3 = -2.*(matrix_multiply(x*weights2x, y, /atrans))
      out = sqrt((sum1+sum2+sum3))
   endelse
endif 


if keyword_set(batch_size) then begin

tx2 = (total(x*x, 1))
ty2 = transpose(total(y*y, 1))
sum2 = rebin(ty2, batch_size, dim_y[1])
out = dblarr(dim_x[1], dim_y[1])

for j = 0, ceil(dim_x[1]/batch_size)-1. do begin
    ini = j*batch_size
    fin = min([(j+1)*(batch_size-1), dim_x[1]-1])
    sum1 = rebin(tx2[ini:fin], fin-ini+1, dim_y[1])
    sum3 = -2*(matrix_multiply(x[*,ini:fin], y, /atrans))
    ;help, sum1, sum2, sum3
    print, j
    out[ini:fin,*] = sqrt(sum1+sum2+sum3)
endfor

endif



if keyword_set(vector) then begin
;help, x, y
d = dblarr(dim_y[1], dim_x[1])
for k=0d, dim_y[1]-1d do begin   
    ;for each cluster (randomly chosen profile)
    tmp = (x - (rebin(reform(y[*,k]),dim_x[0],dim_x[1])))
    help, tmp
    if keyword_set(weights) eq 0 then begin
       tmp = tmp*tmp
    endif else begin    
       tmp = tmp*tmp*weights*weights
    endelse 
    ;stop
    ;help, tmp
    d[k,0] = transpose(sqrt(total(tmp,1)))  
    ;use 0 for faster execution (will assign full vector)
endfor
out =  d
endif 

if keyword_set(blas) then begin
d = dblarr(dim_y[1], dim_x[1])
   for k=0d, dim_y[1]-1d do begin   ;for each cluster (randomly chosen profile)
       data_in_temp = x 
       blas_axpy, data_in_temp, -1,  rebin(reform(y[*,k]),dim_x[0],dim_x[1])
       help, data_in_temp
       if keyword_set(weights) eq 0 then begin
          d[k,0] = transpose(sqrt(total((data_in_temp*data_in_temp),1)))
       endif else begin
          weights2x = rebin(weights*weights, dim_x[0], dim_x[1])
          d[k,0] = transpose(sqrt(total((data_in_temp*data_in_temp*weights2x),1)))
       endelse 
   endfor
out =  d
endif

return, out ;sqrt(total((x - y)^2., 1))

end
