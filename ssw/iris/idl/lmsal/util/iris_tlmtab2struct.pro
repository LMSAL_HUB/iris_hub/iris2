function iris_tlmtab2struct, table, version=version,outdir=outdir
;
;+
;   Name: iris_tlmtab2struct
;
;   Purpose: read/convert IRIS telemetry table -> structure
;
;   Input Parameters:
;      table - the table file to read - local path, table name, url or table number {10,2000}

retval=''
if n_elements(table) eq 0 then table=10 ; default is table10 
stab=strtrim(table,2) ; 

if not file_exist(table) then begin 
   parent='http://www.lmsal.com/iris_science/planningconf/'
   if n_elements(version) eq 0 then version=40
   sver='v'+strtrim(version,2)
   case 1 of 
      strmatch(stab,'http://*.csv',/fold): fullurl=stab
      is_member(stab,'10,2000'): tablename=sver+'-table'+stab+'.csv'
      strmatch(stab,'v*table*',/fold): tablename=stab+(['.csv',''])(strmatch(stab,'*.csv'))
      else: begin 
         box_message,'Invalid table name, returning...'
         return,-1
      endcase
   endcase
   if n_elements(fullurl) gt 0 then begin 
      tablename=ssw_strsplit(fullurl,'/',/tail)
   endif else fullurl=parent+tablename

   if n_elements(outdir) eq 0 then outdir=get_logenv('HOME')
   loctab=concat_dir(outdir,tablename) 
   ssw_file_delete,loctab ; start with clean slate
   sock_copy,fullurl,out_dir=outdir
   if not file_exist(loctab) then begin
      box_message,'problem with server transfer or no-such-table ('+tablename+') , bailing..'
      return, -1  ; early exit on problme
   endif
   table=loctab
endif
case 1 of 
   strpos(table,'table10') ne -1: retval=iris_tlmtab102struct(table)
   else: retval=iris_tlmtab2struct_1(table)
endcase

return,retval
end
