function ssw_hpkb_status_posts, t0, t1, lastn=lastn, delete_bogus=delete_bogus

temp={process_orig:'',process_targ:'',time_date:'',status:'',userid:'',contact:'',filetime:'', filename:''}

pfile=concat_dir('$ssw_path','hpkb/status/posts.txt')

qfiles=rd_tfile(pfile)

if keyword_set(lastn) then qfiles=last_nelem(qfiles,lastn) 
qsize=file_size(qfiles)
bogus=where(qsize lt 100,bcnt)
if bcnt gt 0 then begin 
   box_message,['Bogus Files:',qfiles(bogus)]
   if keyword_set(delete_bogus) then begin 
      stop,'pre delete'
      ssw_file_delete,qfiles(bogus)
   endif
   qfiles=qfiles(rem_elem(qfiles,qfiles(bogus))) ; ignore bogus
endif

nq=n_elements(qfiles)

retval=replicate(temp,nq)
ftimes=file2time(qfiles) 
for i=0,nq-1 do begin 
    if i mod 100 eq 0 then box_message,string(i)
   info=url_decode(qfile=qfiles(i))
   retval(i).process_orig=gt_tagval(info,/process_orig,missing='??')
   retval(i).process_targ=gt_tagval(info,/process_targ,missing='??')
   retval(i).time_date=gt_tagval(info,/time_date,missing=ftimes(i)) ; missing? use filetime
   retval(i).status=gt_tagval(info,/status,missing='??')
   retval(i).userid=gt_tagval(info,/userid,missing='??')
   retval(i).contact=gt_tagval(info,/contact,missing='??') 
endfor

retval.filetime=file2time(qfiles)
retval.filename=ssw_strsplit(qfiles,'/',/tail)

return,retval
end
  




