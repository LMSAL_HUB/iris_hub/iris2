pro mk_hk_web_plots, sttim, entim, hours=hours, days=days, mnem_names=mnem_names, $
  data_in=data_in, restore_file=restore_file, $
  use_internal_mnem_table=use_internal_mnem_table, screen=screen, $
  xsiz_lilwin=xsiz_lilwin, ysiz_lilwin=ysiz_lilwin, $
  xsiz_bigwin=xsiz_bigwin, ysiz_bigwin=ysiz_bigwin, psym=psym, color_num=color_num, $
  show_limits=show_limits, save_data=save_data

;+
; name: mk_hk_web_plots
;
; Parameters:
; sttim		start time
; entim		end time
; 
; INPUT (optional) Keywords
; hours
; days
; mnem_names		simple array/list of tlm mnemonics in 1d array
; data_in		data structure from "sag_get_mnem" call or the like
; restore_file		IDL restore file with variable called data which is copied to mnem_data
; use_internal_mnem	switch to use internal list of mnemonics
; screen		screen switch
; xsiz_lilwin
; ysiz_lilwin
; xsiz_bigwin
; ysiz_bigwin
; psym
; color_num
; show_limits
; save_data
;
; Other Keyword Checks NOT in the formal parameter list:
; infile_plot_params	list of TLM mnemonics for plots
; indir			dir path for mnemonics file
; mnem_list		??? becomes internal list of mnemonics, short names, units, LL,UL
; mnem_data 		copy of data_in, in the structure of a sag_get_mnem
;

if not exist(indir) then indir = '/net/solserv/home/slater/public_html/sot/hk'
;if not exist(indir) then indir = '$FPPSW/idl/fpp_test/'
if not exist(infil_plot_params) then infil_plot_params = 'sot_hk_plot_params.lis'
;if not exist(infil_plot_params) then infil_plot_params = 'webplots.tab'
if not exist(sttim) then $
  if keyword_set(hours) then win = hours else $
    if keyword_set(days) then win = days*24 else $
      stop,'No time window specified.  Stopping.'
if win lt 24 then win_string = strtrim(win,2) + ' hours' else $
  win_string = 'Last_' + string(fix(win/24),format='$(I2.2)') + '_Days'
if not exist(plotdir) then $
  plotdir = concat_dir('/net/solserv/home/slater/public_html/sot/hk',win_string)
;if not exist(web_link) then $
;  web_link = 'http://darts.isas.jaxa.jp/pub/solar/solarb/sot/hk/' + win_string
if not exist(web_link) then $
  web_link = 'http://www.lmsal.com/~slater/sot/hk/' + win_string
if keyword_set(test_data) then entim = '29-sep-2005' else $
  if not exist(entim) then entim = anytim2ints(ut_time())
sttim = anytim2ints(entim, off=-60*60*float(win))

if not keyword_set(use_internal_mnem_table) then begin
  infil_mnem_list = concat_dir(indir, infil_plot_params)
  mnem_list = rd_ulin_col(infil_mnem_list, /nohead, nocomment='#')
endif

if not exist(mnem_list) then begin
  mnem_list = $
  [['S_C_SSIMPWRV0',			'Null',		'Null',		 '27.5',	 '28.5'], $
   ['S_C_SSIMPWRIO',			'Null',		'Null',		  '1.4',	  '6.5'], $
   ['E_HMI_PWR_P3_3V',			'3V_Primary',	'Volts',	  '2.95',	  '3.4'], $
   ['E_HMI_PWR_P5V',			'5V_Primary',	'Volts',	  '4.95',	  '5.2'], $
   ['E_HMI_PWR_P15V',			'15V_Primary',	'Volts',	 '14.5',	 '15.2'], $
   ['E_HMI_PWR_M15V',			'15V_Main',	'Volts',	'-15.5',	'-14.5'], $
   ['E_HMI_PWR_MOTOR_CURRENT',		'Motor_Current','Amps',		  '0',		'500'  ], $
   ['HMI_KER_CM_ACCEPT',		'Null',		'Null',		  '1.0',	'999'  ], $
   ['HMI_KER_CM_REJECT',		'Null',		'Null',		  '0',		  '0  '], $
   ['E_HMI_ISS_DIODE1',			'Diode_1',	'Null',		  '9.0',	 '11.0'], $
   ['E_HMI_ISS_DIODE3',			'Diode_3',	'Null',		  '9.0',	 '11.0'], $
   ['E_HMI_ISS_YERROR',			'Y_Err',	'Null',		 '-2.0',	  '2.0'], $
   ['E_HMI_ISS_ZERROR',			'Z_Err',	'Null',		 '-2.0',	  '2.0'], $
   ['E_HMI_ISS_PZTA',			'Null',		'Null',		 '30.0',	 '40.0'], $
   ['E_HMI_ISS_PZTB',			'Null',		'Null',		 '30.0',	 '40.0'], $
   ['E_HMI_ISS_PZTC',			'Null',		'Null'	,	 '30.0',	 '40.0'], $
   ['E_HMI_TS29_CAM1_SIDE_CEB_EXT',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS28_CAM2_FRNT_CEB_EXT',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS23_OPT_AFTBENCH',		'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS39_OPT_FWDCVR',		'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS205_CEB1_PWB_TEMP',		'CEB1_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS206_CEB2_PWB_TEMP',		'CEB2_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS201_CEB1_CCD_TEMP1',		'CCD1_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS204_CEB2_CCD_TEMP2',		'CCD2_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS31_HEB_PWR_CNVR_PRI',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS33_HEB_RAD6000_CPU_A',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_OV1_LOOP',			'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS15_OVN_PREAMP',		'Null',		'Deg_C',	 '15.0',	 '35.0']]
endif

if not exist(mnem_names) then begin
  mnem_names = strupcase(reform(mnem_list(0,*)))
  mnem_labels = reform(mnem_list(1,*))
  ss_replace = where(mnem_labels eq 'Null',nreplace)
  if nreplace gt 0 then mnem_labels(ss_replace) = mnem_names(ss_replace)
  mnem_units = reform(mnem_list(2,*))
  ss_replace = where(mnem_units eq 'Null',nreplace)
  if nreplace gt 0 then mnem_units(ss_replace) = ''
  mnem_lolim = reform(mnem_list(3,*))
  ss_replace = where(mnem_lolim eq 'Null',nreplace)
  if nreplace gt 0 then mnem_lolim(ss_replace) = '-1000'
  mnem_hilim = reform(mnem_list(4,*))
  ss_replace = where(mnem_hilim eq 'Null',nreplace)
  if nreplace gt 0 then mnem_hilim(ss_replace) = '1000'
endif

n_plots = n_elements(mnem_names)

if not exist(nticks) then nticks = 1
if not exist(yticks) then yticks = 1
if not exist(ystyle) then ystyle = 1
if not exist(psym) then psym = 0
if not exist(ytnam_format) then begin
  format0 = '$(f6.2)' & format1 = '$(f6.2)'
endif else begin
  format0 = ytnam_format & format1 = ytnam_format
endelse
if not exist(color_num) then color_num = 12
if not exist(background) then background = 0
;if not exist(xmargin) then xmargin_big = [10,4]
if not exist(ymargin) then ymargin_big = [ 4,4]
if not exist(xsiz_lilwin) then xsiz_lilwin = 320
if not exist(ysiz_lilwin) then ysiz_lilwin = 224
if not exist(xsiz_bigwin) then xsiz_bigwin = 576
if not exist(ysiz_bigwin) then ysiz_bigwin = 448

if exist(data_in) then mnem_data = data_in
if exist(restore_file) then begin
  restore, restore_file
  mnem_data = data
endif

if not exist(mnem_data) then mnem_data = sag_get_mnem(sttim, entim, mnem_names, /nostring, /raw)
npack = n_elements(tag_names(mnem_data))
; TODO - Fix so linecolors works without producing black plots
;linecolors
if not keyword_set(screen) then set_plot,'z' else set_plot,'x'

for i=0,npack-1 do begin
  for j=0,n_elements(mnem_data.(i).(2))-1 do begin
    mnem0_time = anytim(mnem_data.(i).(0),/yohkoh)
    ntimes = n_elements(mnem0_time)
    mnem0_valu = mnem_data.(i).(1)(*,j)
    mnem0_name = strupcase(mnem_data.(i).(2)(j))
    ss_match = where(mnem_names eq mnem0_name, nmatch)
    if ( (size(mnem0_valu, /type) eq 2) or (size(mnem0_valu, /type) eq 3) ) then begin
      format0 = '$(i6)' & format1 = '$(i6)'
    endif
    ymin = min(mnem0_valu)
    ymax = max(mnem0_valu)
    if keyword_set(show_limits) then begin
      ywidth = float(mnem_hilim(ss_match)) - float(mnem_lolim(ss_match))
      yrange = [float(mnem_lolim(ss_match))-0.2*ywidth, $
        	float(mnem_hilim(ss_match))+0.2*ywidth]
    endif else begin
      yrange = 0
      ytickv = [ymin, ymax]
      if max(abs(ytickv)) gt 1 then $
        ytickname = string(ytickv,format=format0) else $
        ytickname = string(ytickv,format=format1)
    endelse

; Make the 'lil' plot:
    wdef,0,xsiz_lilwin,ysiz_lilwin
    if ymax ne ymin then $
      utplot,mnem0_time,mnem0_valu, $
        ytitle=mnem_units(ss_match), $
        title=mnem_labels(ss_match), nticks=nticks, yticks=yticks, $
        ytickv=ytickv, ytickname=ytickname, yrange=yrange, ystyle=ystyle, $
        psym=psym, color=color_num, background=background, $
        xmargin=xmargin, ymargin=ymargin else $
      utplot,mnem0_time,mnem0_valu, $
        ytitle=mnem_units(ss_match), $
        title=mnem_labels(ss_match), nticks=nticks, yrange=yrange, $
        ystyle=ystyle, psym=psym, color=color_num, $
        background=background, xmargin=xmargin, ymargin=ymargin
      outplot,[mnem0_time(0),mnem0_time(ntimes-1)], $
              [mnem_lolim(ss_match),mnem_lolim(ss_match)], color=5, thick=1, linestyle=2
      outplot,[mnem0_time(0),mnem0_time(ntimes-1)], $
              [mnem_hilim(ss_match),mnem_hilim(ss_match)], color=2, thick=1, linestyle=2

    outfil = mnem0_name + '_lil.gif'
    if keyword_set(qpause) then pause
    if (!d.name eq 'Z') then zbuff2file, concat_dir(plotdir, outfil), /gif

; Make the 'big' plot:
    wdef,0,xsiz_bigwin,ysiz_bigwin
    if ymax ne ymin then $
      utplot,mnem0_time,mnem0_valu, $
        ytitle=mnem_units(ss_match), $
        title=mnem_labels(ss_match), nticks=nticks, yticks=yticks, $
        ytickv=ytickv, ytickname=ytickname, yrange=yrange, ystyle=ystyle, $
        psym=psym, color=color_num, background=background, $
        xmargin=xmargin_big, ymargin=ymargin_big else $
      utplot,mnem0_time,mnem0_valu, $
        ytitle=mnem_units(ss_match), $
        title=mnem_labels(ss_match), nticks=nticks, yrange=yrange, $
        ystyle=ystyle, psym=psym, color=color_num, $
        background=background, xmargin=xmargin, ymargin=ymargin
      outplot,[mnem0_time(0),mnem0_time(ntimes-1)], $
              [mnem_lolim(ss_match),mnem_lolim(ss_match)], color=5, thick=1, linestyle=2
      outplot,[mnem0_time(0),mnem0_time(ntimes-1)], $
              [mnem_hilim(ss_match),mnem_hilim(ss_match)], color=2, thick=1, linestyle=2

    outfil = mnem0_name + '_big.gif'
    if keyword_set(qpause) then pause
    if (!d.name eq 'Z') then zbuff2file, concat_dir(plotdir, outfil), /gif

    if keyword_set(save_data) then begin
      outfil_txt = mnem0_name + '.txt'
      prstr, mnem0_time + '    ' + strtrim(mnem0_valu,2), file=concat_dir(plotdir, outfil_txt)
    endif
  endfor
endfor

set_plot,'x'

end
