function ssw_tilecomp_head2head,index, removez=removez
;
;   Name: ssw_tilecomp_head2head
;
;   Purpose: populate "standard" FITS tags with values from tile-compressed analogs
;
;   Input Parameters:
;      index - ssw index(header) structure vector or a single FTIS header (strarr)
;
;   Output:
;      function returns adjusted index/header; optionally with Z<params> removed
;
st_tags=['BITPIX','NAXIS','NAXIS1','NAXIS2']
tc_tags='Z'+st_tags

retval=index

for i=0,n_elements(tc_tags)-1 do begin 
   tct=gt_tagval(index,tc_tags(i),found=found)
   if found then fxaddpar,retval,st_tags(i),tct
endfor

return,retval
end


