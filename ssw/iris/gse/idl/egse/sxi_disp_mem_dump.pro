pro sxi_disp_mem_dump, recs, outfil=outfil, hc=hc, date=date, qstop=qstop, $
	lastn=lastn, ist=ist, ien=ien
;
;HISTORY:
;	Written Oct-00 by M.Morrison
;V1.01	25-Oct-00 (MDM) - Print EGSE receipt time 
;V1.02	 9-Aug-01 (MDM) - Added LASTN keyword option
;			- Added IST/IEN keyword options
;
if (n_elements(date) eq 0) then date = ut_time()
fid = time2file(date, /date)
infil = '$GSE_PKT_FILE_DIR/0x0031/' + fid + '.0x0031'
;
head = sag_rd_hk(infil, 0, 100000, /map)
;
if (n_elements(ist) eq 0) then ist = 0
if (n_elements(ien) eq 0) then ien = n_elements(head)-1
if (keyword_set(lastn)) then ist = n_elements(head)-lastn
;
if (n_elements(recs) eq 0) then begin
    recs = sag_rd_hk(infil, ist, ien)
    head = head(ist:ien)
end
;
out = ['SXI_DISP_MEM_DUMP  Ver 1.02  Program run: ' + !stime, ' ']
;
for i=0,n_elements(recs)-1 do begin
    rec0 = recs(i)
    dattim = sxi_time_conv('INSTRUMENT', struct=rec0.pkt_head, /string, /msec)
    edattim = sxi_time_conv('EGSE', head(i).egse_time, /string, /msec)
    out0 = ['LOBT: ' + dattim  + '  Addr:   0x'+zformat(rec0.addr,8) + string(rec0.addr, format='(i10)'), $
	    'EGSE: ' + edattim + '  Scnt:  ' + string( mask(rec0.pkt_head.scnt, 0, 14) ) + ' Rec: ' + strtrim(ist+i,2) ]
    for j=0,11 do begin
	ist2 = j*16
	ien2 = (ist2+15)<179
	fmt = '(4x, z8.8, 4(2x,4(1x,z2.2)))'
	out0 = [out0, string(rec0.addr+ist2, rec0.data(ist2:ien2), format=fmt)]
    end
    out = [out, out0, ' ']
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil)
if (keyword_set(qstop)) then stop
end
