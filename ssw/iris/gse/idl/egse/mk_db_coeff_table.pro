pro mk_db_coeff_table, str, outfil, invertit=invertit

arr = str2arr(str)
nc = fix(arr(2))
mnem = arr(1)
descr = arr(9)
coeff = float( arr(3:3+nc-1) )
;
x = indgen(256)
if (mnem eq 'SXI_CAMCCDTMP') then x = x*4
y = poly(x, coeff)
if (keyword_set(invertit)) then begin
    x = 255 - x
    x = reverse(x)
    y = reverse(y)
end
plot, x, y, tit=mnem
;
;;ymin = -60
;;ymax = 60
;;ss = where( (y ge ymin) and (y le ymax), nss)
ss = indgen(256)
nss = n_elements(ss)
;
fmt = '(f7.1)'
if (max(y) lt 1) and (min(y) gt -1) then fmt = '(f7.3)'
npair = 6.
nlin = ceil(nss/npair)
;mat = strarr(2*nss)
mat = strarr(2*nlin*npair)
ss2 = (indgen(nss)*npair*2 mod (nlin*npair*2))+ indgen(nss)/nlin*2
mat( ss2 ) = x(ss)
mat( ss2+1 ) = string(y(ss), format=fmt)

out = strarr(nlin)
;for i=0,nlin-1 do out(i) = arr2str( mat(i*npair*2:(i+1)*npair*2-1), delim=string(9b))
for i=0,nlin-1 do out(i) = arr2str( strtrim(mat(i*npair*2:(i+1)*npair*2-1),2), delim=',')

out = ['------------------------------------', $
	'Mnemonic = ' + mnem, $
	'Description ' + descr, $
	'Coefficients: ' + arr2str(coeff, delim=','), $
	out, $
	' ']
;
prstr, out, /nomore
file_append, outfil, out
;pause
end
;----------------------------------------------------------------------
;
outfil = '$HOME/mk_db_coeff_table.txt'
!p.multi = [0,4,5]
tv2, 600, 800, /init
loadct,0
!p.charsize = 1.4
!p.background = 255
!p.color = 0
;
if (n_elements(r) eq 0) then begin
    spawn, ['grepdb', 'ALG,'], r, /noshell
    dbfile = r(0)
    r = r(1:*)
    ss = where(strpos(r, 'GPIB') eq -1)
    r = r(ss)	;strip out GPIB
    ss = where(strpos(r, 'PCM') eq -1)
    r = r(ss)	;strip out PCM stuff
end
;
file_delete, outfil
file_append, outfil, 'MK_DB_COEFF_TABLE program run: ' + !stime
file_append, outfil, 'DB file: ' + dbfile
for i=0,n_elements(r)-1 do mk_db_coeff_table, r(i), outfil
;
plottime, 0, 0, 'MK_DB_COEFF_TABLE Ver 1.0'
plottime
if (!d.name eq 'Z') then zbuff2file, '$HOME/mk_db_coeff_table.gif'

end
