;+
; 
; Batch file called by cron job to run IRIS_PREP_RUN and fill in metadata on 
; IRIS images.
;
;-

; Close the journal that is opened on startup, and redirect journal entries to
; the iris_prep runlog area
tnow = RELTIME(/now)
ttag = TIME2FILE(tnow, /sec)
runlogdir = '/irisa/data/prep/runlog/'
journaldir = CONCAT_DIR(runlogdir, 'journal')
jfile = CONCAT_DIR(journaldir, ttag + '.pro')
BOX_MESSAGE, ['IRIS_PREP_RUN_BATCH: Switching off journalling, switching to ', $
	 jfile ]
JOURNAL
JOURNAL, jfile

CD, runlogdir, current = old_dir

; Read the work.txt file to see how many jobs are in progress
maxproc = 3
SPAWN, 'hostname', result, errcode
workfile = STRMID(result, 0, 4) + '_working.txt'
work_txt = RD_TFILE(workfile)
numlines = N_ELEMENTS(work_txt)
if numlines ge maxproc then begin
	PRINT, 'Too many processes in process...'
	RETURN
endif

; Check for a todo.txt file
if FILE_EXIST('todo.txt') then begin
	; Read the todo.txt file to determine which dates to work on
	todo_txt = RD_TFILE('todo.txt')
	; Re-write todo.txt without the first line (which you've picked up)
	if N_ELEMENTS(todo_txt) gt 1 then begin
		FILE_APPEND, 'todo.txt', todo_txt[1:*], /new
	endif else begin
		SPAWN, 'rm todo.txt', res, errcode
	endelse	
endif else begin
	; If there is no todo.txt file, then do a date (daylag) days ago
	daylag = 4.
	tstart = ANYTIM2TAI(tnow) - (3600. * 24. * daylag)
	tstop = ANYTIM2TAI(tnow) - (3600. * 24. * (daylag-1))
	day_start = STRMID(TAI2UTC(/ccsds, tstart), 0, 10)
	day_stop = STRMID(TAI2UTC(/ccsds, tstop), 0, 10)
	todo_txt = [day_start + ' ' + day_stop]
	PRINT, 'No todo.txt file; just using today-'+STRTRIM(daylag,2)+' ...'
endelse

; If the first line is valid, write the date to work on into the working.txt file
times = STRSPLIT(/extract, todo_txt[0])		;	STRSPLIT is a PITA
if N_ELEMENTS(times) lt 2 then begin
	PRINT, 'Hmm...bad line from todo.txt...'
	PRINT, times
	CD, old_dir
	RETURN
endif
workline = STRING(todo_txt[0], ttag, form = '(a60, a30)')
FILE_APPEND, workfile, workline

;+++++++++++++++
; Get to work!
;---------------
BOX_MESSAGE, ['IRIS_PREP_RUN_BATCH:', times[0], times[1]]
IRIS_PREP_RUN, times[0], times[1], outpath = '/irisa/data/prep/', /dowarp, $
	/shift_wave, /shift_fid, /strict, /measure_aia, /update_roll, /run_time, $
	/subdate
;+++++++++++++++
; Done!
;---------------

; Now write to done.txt and remove from working.txt
dttag = TIME2FILE(RELTIME(/now), /sec)
doneline = STRING(workline, dttag, form = '(a90, a30)')
FILE_APPEND, 'done.txt', doneline
working_txt = RD_TFILE(workfile)
numlines = N_ELEMENTS(working_txt)
if numlines lt 1 then begin
	PRINT, 'Hmm...where did my working file go?'
	CD, old_dir
	RETURN
endif
still_working = WHERE(working_txt ne workline, numstill)
case 1 of
	(numstill ge numlines)	:	begin	;	not sure how this could happen
		PRINT, 'Hmm...working on something, but not this one:', times
	end
	(numstill le 0)	:			begin	;	working on last line; delete file
		PRINT, 'Nothing left in working file except ', times
		FILE_DELETE, workfile
	end
	else			:			begin	;	leave the other stuff there
		PRINT, 'Removing from working.txt file ', times
		FILE_APPEND, workfile, working_txt[still_working], /new
	end
endcase
CD, old_dir

end