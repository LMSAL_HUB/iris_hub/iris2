;+
; NAME:
;       iris_bgcal_func()
;
; PURPOSE:
;       A function to calculate a limb-darkening function which is
;       arbitrarily offset from solar center, smoothed, scaled, and
;       off-set in intensity.  For use in iris_bgcal and iris_get_back.
;
; CALLING SEQUENCE:
;       inten = iris_get_back(x, y, params)
;
; INPUTS:
;       r - radius of observation in arcsec relative to the center of
;           the background distribution,
;           i.e. sqrt((x_obs-x0)^2+(y_obs-y0)^2) where 
;                x0   - X center of limb darkening function in arcsec
;                y0   - Y center of limb darkening function in arcsec
;
;       params - an array of the function parameters containing:
;                [dw, amp, off, rsun]
;                dw   - Gaussian width of smoothing funtion in arcsec
;                amp  - max intensity of limb darkening function from
;                       pedestal
;                off  - intensity pedestal for limb darkening function
;                rsun - solar radius for time of observation in arcsec
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;       Returns intensity array of dimensions input for r
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;       IDL > r_obs=sqrt((xcen-ld_pars[0])^2 + (ycen-ld_pars[1])^2)
;           > bg_level = iris_bgcal_func(r_obs, [ld_pars[2:*], rsun])
;
; MODIFICATION HISTORY:
;       Started 2013-Nov-14 by Sarah A. Jaeggli, Montana State University
;-

function ld_func, r, a
  t = asin(r/1.) ;mu angle

  ;limb darkening function at 0.5 micron
  u=0.97 & v=-0.22

  I_ld = 1.0 - u - v + u*cos(t) + v*(cos(t))^2

  ;set off-limb values to 0
  good=where(finite(I_ld) eq 1, complement=bad)
  I_ld[bad]=0.

  return, I_ld
end

function iris_bgcal_func, r, a

  dw=a[0]

  amp=a[1]
  off=a[2]

  rsun=a[3] ;solar radius is a fixed parameter

  ;calculate limb darkening function over a larger number of points so it
  ;can be accurately smoothed with a gaussian
  nr=1500
  n=nr*2+1

  rarray=findgen(n)-nr ;the radius in arcsec, 1 arcsec bins

  out=ld_func(rarray/rsun)

  psf=exp(-0.5*((rarray-nr)/dw)^2) + exp(-0.5*((rarray+nr)/dw)^2)

  out1=real_part(fft(fft(out)*fft(psf), /inverse))
  out1=out1/max(out1)

  ;use coordinate as index in function
  ridx=r+nr
  z=out1[ridx]

  z=z*amp + off

  return, z
end
