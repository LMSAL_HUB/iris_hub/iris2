function IRIS_PREP_FLAT, idata, iindex, $
	fmsg = fmsg, frecnum = frecnum, verbose = verbose, nodata = nodata, $
	nospat = nospat

;+
;
; Given a data array (single image) and header, applies the flat-field correction
; Uses Sarah Jaeggli's measured/computed flats
;
; INPUTS: 
;	idata	-	Image array (2D), already dark-subtracted
;	iindex	-	Image header structure
;	
; KEYWORDS:
;	fmsg	-	(OUTPUT) a string giving a description of the flatfield 
;				correction for the FITS history
;	frecnum -	(OUTPUT) a long integer specifying the record number of the 
;				flatfield that was applied
;	/nodata	-	(SWITCH) if set, then the idata input is returned as the output
;				Do this if you just want to get the version information of the
;				flat that would be applied given the iindex
;	/verbose -	(SWITCH) pass through to iris_prep_read_flats
;	/nospat	-	(SWITCH) currently does nothing
;
; RETURNS:
;	odata	-	Image array (2D) with the flat divided out
;
;-

common IRIS_PREP_FLAT_CB, flat_index, flat_loaded, flat_sji_2832, flat_sji_2796, $
	flat_sji_5000w, flat_sji_1330, flat_sji_1400, flat_sji_1600w, $
	flat_fuv_detector, flat_fuv_spatial, flat_nuv_detector, flat_nuv_spatial, $
	flat_sji_nuv

; Make sure that the proper flat is loaded in the common block
IRIS_PREP_READ_FLATS, iindex, verbose = verbose

case iindex.img_path of
	'FUV'		:	begin
		thisflat = flat_fuv_detector * flat_fuv_spatial
		frecnum = flat_loaded.fuv_detector * 1000l + flat_loaded.fuv_spatial
	end
	'NUV'		:	begin
		thisflat = flat_nuv_detector * flat_nuv_spatial
		frecnum = flat_loaded.nuv_detector * 1000l + flat_loaded.nuv_spatial
	end
	else		:	begin
		setflatstring = 'thisflat = flat_' + iindex.img_path
		setrecnumstring = 'frecnum = flat_loaded.' + iindex.img_path
		dummy1 = EXECUTE(setflatstring)
		dummy2 = EXECUTE(setrecnumstring)
	end
endcase

; If the /nodata keyword is set, just return the input data
if KEYWORD_SET(nodata) then RETURN, idata

fsize = SIZE(thisflat)
thisflat = REBIN(thisflat, fsize[1] / iindex.sumsptrl, fsize[2] / iindex.sumspat)

odata = idata / thisflat

fmsg = STRING('Flat fielded with recnum ', frecnum, form = '(a25,i7)')

RETURN, odata

end
