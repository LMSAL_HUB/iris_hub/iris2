function iris_l2_wave2extension,l2index,iwave, check_wave=check_wave, $
   quiet=quiet, nmatches=nmatches 
;
;   Name: iris_l2_wave2extension
;   
;   Purpose: given IRIS L2 file, return FITS extension# corresponding to WAVE 
;
;   Input Parameters:
;      l2index - IRIS primary header/index
;      wave - desired wave; like 'C II', 'mg' ,  2796. etc
;
;   Output:
;      Returns FITS extension number of the desired wave within the L2 file
;
;   Keyword Parameters:
;      quiet - (switch) - if set, don't print stuff (like multi-value warning)
;      nmatches - (output) - no wave match=0, single match=1, >1 = #match (multi-value)
;
;   History:
;      30-sep-2013 - l2 helper -> read_iris_l2
;       5-dec-2014 - if multi-valued solution for 'wave', take 1st & print message
;-
;

loud=1-keyword_set(quiet)

if not required_tags(l2index,'TDESC1,TWAVE1') then begin 
   box_message,'Expects an IRIS level2 header/index input'
   return,-1
endif

if n_elements(iwave) eq 0 then wave='C II' else wave=strtrim(iwave,2); default
l2index0=l2index[0]
tnames=tag_names(l2index0)
case 1 of 
   data_chk(wave,/string): begin
      iwave=strcompress(wave)
      sst=where(strmatch(tnames,'TDESC*',/fold),tcnt)
      varr=strcompress(str2arr(get_infox(l2index0,tnames[sst],delim='*'),'*'))
      sse=where(strmatch(varr,'*'+iwave+'*',/fold),nmatches)
      if keyword_set(check_wave) then stop,'varr,sse'
      if nmatches ge 1 then retval=str2number(tnames[sst[sse[0]]])  else begin
         box_message,'Wave not found...
         retval=-1
      endelse
      if nmatches gt 1 and loud then box_message, $
         'More than one extension match your WAVE pattern, returning 1st'
   endcase
   else: begin 
      nmatches=1
      sst=where(strmatch(tnames,'TWAVE*',/fold),tcnt)
      varr=float(str2arr(strtrim(strcompress(get_infox(l2index0,tnames[sst])),2),' '))
      test=abs(varr - wave)
      retval=(where(test eq min(test)))(0)
   endcase
endcase

return,retval
end
      
      


