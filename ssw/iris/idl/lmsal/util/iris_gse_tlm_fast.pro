pro iris_gse_tlm_fast, telem, save=save, refresh=refresh
;
;+
;   Name: iris_gse_tlm_fast
;
;   Purpose: load IRIS tlm dbase (common) from geny file
;
;
common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd, cond  ; GSE dbase commmon

file=get_logenv('IRI_EGSE_DB_FILE')
gfile=str_replace(file,'.xml','.geny')

saveit=keyword_set(save) and get_user() eq 'freeland'
refresh=keyword_set(refresh) or saveit

if not file_exist(gfile) or refresh then begin  
   box_message,'bypassing Fast start - loading tlm cache...'
   if refresh then delvarx,tlm ; force reload
   sag_iri_rd_itos_db
   if saveit then begin
      box_message,'Saving geny image of gse common   
      savegenx,tlm, alg, ylg, dsc, file=gfile,/over
   endif
endif else begin 
   restgenx,tlm, alg, ylg, dsc, file=gfile[0]
endelse

telem=tlm ; output common tlm

return
end
