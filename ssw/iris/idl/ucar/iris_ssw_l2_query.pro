function iris_ssw_l2_query, t0, t1, obsid = obsid, ds = ds, verbose = verbose,$
	open = open, fits = fits, get_files = get_files, data_home = data_home

; IDL Version 8.0, Mac OS X (darwin x86_64 m64)
; Journal File for mscott@tokyo.lmsal.com
; Working directory: /Users/mscott/Desktop
; Date: Tue Sep 24 17:13:12 2013

; Send requests/bugs/suggestions to mscott@ucar.edu

; Purpose: Find IRiS Level 2 data URL and QL information by querying the timeline 

; useage: 
; obsid = 4182010156
; t0 = '2013-08-20 00:00:000'
; t1 = '2013-08-21 00:00:000'
; fl = iris_ssw_l2_query(t0, t1, obsid = obsid)
; print, fl
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_185222_4182010156/
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_194022_4182010156/
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_201022_4182010156/
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_211522_4182010156/
; Adding the FITS keyword returns URLS for FITS files in the folder.
; t0 = '2013-08-20 15:00:000'
; t1 = '2013-08-20 16:00:000'
; fl = iris_ssw_l2_query(t0, t1, obsid = obsid, /fits)
; print, fl
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/iris_l2_20130820_150507_4182010156_SJI_1330_t000.fits
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/iris_l2_20130820_150507_4182010156_SJI_1400_t000.fits
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/iris_l2_20130820_150507_4182010156_SJI_2796_t000.fits
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/iris_l2_20130820_150507_4182010156_SJI_2832_t000.fits
; http://www.lmsal.com/solarsoft/irisa/data/level2_prelim03/2013/08/20/20130820_150507_4182010156/iris_l2_20130820_150507_4182010156_raster_t000_r00000.fits

; GET_FILES Keyword uses SOCKETS to download the FITS files from the Web
; DS Keyword allows the user to use different IRIS data series 
; OPEN Keyword is experimental - open the web browser at URL
; DATA_HOME Keyword points to where you keep IRIS Level 2 data, default is './' 

base_url = 'http://www.lmsal.com/solarsoft/irisa/data/'
data_series = 'level2_prelim03'

if keyword_set(ds) then data_series = ds[0]
if keyword_set(obsid) then obsid = obsid[0]

message, /cont, 'Using IRIS DataSeries '+data_series 

tl = iris_time2timeline(t0, t1)

if keyword_set(verbose) then info = get_infox(tl,tag_names(tl),/more)

ntl = n_elements(tl)
date = strarr(ntl)
start = strarr(ntl)
url = strarr(ntl)

for ii =0, ntl - 1 do begin
   tmp = str_sep(tl[ii].date_obs,'T')
   tmp2 = tmp[0]
   year = strmid(tmp2,0,4)
   month = strmid(tmp2,5,2)
   day = strmid(tmp2,8,2)
   date[ii] = year+'/'+month+'/'+day
   ;help, year, month, day
   tmp = str_sep(tmp[1],':')
   start[ii] = year+month+day+'_'+tmp[0]+tmp[1]+strmid(tmp[2],0,2)
   url[ii] = base_url + data_series +'/'+ date[ii] + '/'	+start[ii] +'_'+ strcompress(tl[ii].obsid,/rem) + '/'
endfor

if keyword_set(obsid) then begin
   best = where(tl.obsid eq obsid, count)
   if count gt 0 then url = url[best]
endif

if keyword_set(verbose) then more, url
if keyword_set(open) then spawn, 'open '+url+'www/'
if keyword_set(fits) then begin
   count = 0
   array = strarr(100000)
   for ii=0,n_elements(url)-1 do begin
       sock_dir,url[ii], xx
       if n_elements(xx) eq 0 then begin
	  message,'No FITS files in folder : '+url[ii], /cont
          goto, skip
       endif
       for jj=0,n_elements(xx)-1 do begin
	  true = strmatch(xx[jj],'*/iris_l2_*.fits')
	  if true then begin
		array[count] = xx[jj]
	  	count = count + 1
	  endif
       endfor
       skip:
   endfor
   array = array[0:count-1]
   url = array
endif

if count eq 0 then begin
   message, /cont, 'No L2 FITS files visible'
   url = -1
endif else begin
   if keyword_set(get_files) then message, /cont, 'Retrieving IRIS L2 FITS Files'
   if keyword_set(get_files) then iris_get_l2files, url, ds = ds, data_home = data_home
endelse

return, url
end

