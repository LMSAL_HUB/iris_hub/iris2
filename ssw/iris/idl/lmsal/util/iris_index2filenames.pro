function iris_index2filenames, index, parent_path=parent_path, names_only=names_only, level=level, urls=urls, prelim=prelim, nrt=nrt
;
;+
;   Name: construct determistic path names
;
;
;  History:
;     5-aug-2013 - S.L.Freeland - one other path (jsoc meta -> lmsal files/url)
;     7-aug-2013 - S.L.Freeland - /URLS keyword & function
;    20-aug-2013 - S.L.Freeland - prelim01->02 per Jean-Pierre
;     2-sep-2013 - S.L.Freeland - prelim03 support
;     9-sep-2013 - S.L.Freeland - add level 1.5 support (same jsoc DS as level1)
;    16-sep-2013 - S.L.Freeland - dark + (strange) jsoc:  hh:mm:60.00 - require img_type (added to iris_time2files min-query)
;    18-sep-2013 - S.L.Freeland - prelim keyword 
;    22-oct-2013 - S.L.Freeland - prelim->release/_nrt transition
;-

if not required_tags(index,'t_obs,img_path,instrume,img_type') then begin 
    box_message,'Need IRIS index, drms, or catalog vector'
    return,''
endif

urls=keyword_set(urls)

; t_obs not quite 'file2time' ready due to hh:mm:60.00 ... 
tobs=index.t_obs
dt0=ssw_deltat(tobs[0],ref='8:49 22-oct-2013',/hours) ; _prelim03 -> _nrt jsoc transition
dtt=ssw_deltat(tobs[0],ref='13:00 24-oct-2013',/hours) 
tobs=strcompress(str_replace(str_replace(str_replace(str_replace(str_replace(tobs,'T','_'),'Z',''),'-',' '),':',' '),'.',' '),/remove)
fnames='iris'+tobs+'_'+strlowcase(index.instrume) + (['','d'])(index.img_type eq 'DARK')+'.fits'


if n_elements(level)eq 0 then level=gt_tagval(index[0],/lvl_num,missing=1)

nrt=(keyword_set(nrt) or dt0 gt 0) and ssw_deltat(index[0].t_obs,reltime(/now),/hour) lt 150
sprelim=''
if keyword_set(prelim) then sprelim=(['_prelim' + string(prelim,format='(I2.2)'),'_nrt'])(nrt)
if nrt then sprelim='_nrt'
if keyword_set(names_only) then begin 
   retval=fnames
endif else begin 
   case 1 of
      data_chk(parent_path,/string): parent=parent_path[0]
      level eq 0: slev='level0'
      level eq 1 or level eq 1.5: begin ; these both use same jsoc DS
         slev='level1'+(['','p5'])(level eq 1.5) + sprelim
      endcase
      else: begin 
         box_message,'Unexpected jsoc data level'
         parent=curdir()
      endcase
   endcase
   case 1 of 
      n_elements(parent) gt 0: ; user defined
      urls: parent='/irisa/data/'
      nrt and level eq 1.5 and (anytim(index[0].t_obs) gt anytim('13:00 24-oct-2013')) : parent='/titan/iris/data/' ; need to break these transitions->function...
      else: parent=get_logenv('IRIS_DATA')
   endcase
   sparent=concat_dir(parent,slev)
   retval=concat_dir(ssw_time2paths(index.t_obs,parent=sparent,/hour), fnames)
endelse 

retval=(['','http://www.lmsal.com/solarsoft/'])(urls) + retval

return,retval
end

