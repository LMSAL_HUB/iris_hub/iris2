function ssw_iris_xml2struct, xmlfile , add_xml=add_xml, debug=dbug, xmldata=xmldata , $
   minimal_xml=minimal_xml
;
;+
;   Name: ssw_iris_xml2struct
;
;   Purpose: IRIS *.xml -> nested structure
;
;   Input Parameters:
;      xmlfile - one IRIS *.xml file name
;
;   Output:
;      function returns nested IDL structure which mirrors the xml contents
;      for today, like: {info:<blah>,header:<blah>,data:[<blah>] }
;
;   Keyword Parameters:
;      add_xml - if set, add full xml contents to structure in tag .XMLDATA
;      xmldata (output) - the xml contents as string array (useful for inverse struct->xml )
;      minimal_xml (switch) - if set, only save critical subset of xml in .XMLDATA
;
;   Calling Sequence:
;      xmlstr=ssw_iris_xml2struct(xmlfile [,/add_xml] [,/minimal_xml])
;
;   Calling Context: existing XML -> STRUCT -> update STRUCT -> STRUCT -> XML
;
;      xmlstr=ssw_iris_xml2struct(xmlfile,/minimal_xml)      ; XML-> STRUCT
;      [ make desired changes to xmlstr.{info,header,data}.<tags> ]
;      strxml=ssw_iris_struct2xml(xmlstr, outfile='new.xml') ; STRUCT -> XML
;
;   History:
;      15-Jan-2013 - S.L.Freeland
;      25-mar-2013 - S.L.Freeland - add /MINIMAL_XML keyword switch & function
;
;   Restrictions/Notes:
;      xml params which are restricted TAGNAMES per idl have an appended "_" ( .repeat -> .repeat_ )
;      currently all data types are string - tbd future option to type those
;-

debug=keyword_set(debug)

things=['Info','Header','Data']  ; << add to list if more Things are needed

if not file_exist(xmlfile) then begin 
   box_message,'Need valid xmlfile name input'

endif
xdat=rd_tfile(xmlfile)
minxml=''
for t=0,n_elements(things)-1 do begin
   tss=where(strpos(xdat,'<'+things(t)) ne -1,tcnt)
   tsse=where(strpos(xdat,'</'+things(t)) ne -1,tecnt)
   if t eq 0 and tss[0] gt 0 then minxml=xdat[0:tss[0]-1]
   if tcnt gt 0 and tecnt gt 0 then begin
      minxml=[minxml, xdat[tss[0]:tsse[0]]]
      ttags=strextract(xdat[tss(0)+1:tsse(0)-1],'<','>')
      ttags=str_replace(ttags,'/','')
      delvarx,one
      for tt=0,n_elements(ttags)-1 do begin 
         tagname=ttags(tt)
         if is_member(tagname,'repeat',/ignore_case) then tagname=tagname+'_' ; restricted tagnames!
         del0=ttags+'>'
	 del1='</'+ttags
	 one=add_tag(one,strextract(xdat[tss(0)+tt+1],del0[tt],del1[tt]),tagname)
       endfor

       if tcnt gt 1 then begin
          one=replicate(one,tcnt)
          tdat=xdat[tss[0]:last_nelem(tsse)]
          for nt=0,n_elements(del0)-1 do begin
             ssd=where(strpos(tdat,del0[nt]) ne -1, dcnt)
             if dcnt eq 0 then ssd=where(strpos(tdat,ttags[nt]+'/') ne -1,dcnt)
             one.(nt)=strextract(tdat(ssd),del0[nt],del1[nt])
          endfor
       endif 
       retval=add_tag(retval,one,things(t))
   endif
endfor
xmldata=temporary(xdat)
if last_nelem(tsse) lt n_elements(xmldata)-1 then minxml=[minxml,xmldata[last_nelem(tsse)+1:*]]
if keyword_set(add_xml) and data_chk(retval,/struct) then retval=add_tag(retval,xmldata,'xmldata')
if keyword_set(minimal_xml) and data_chk(retval,/struct) then retval=add_tag(retval,minxml,'xmldata')

if debug then stop,'retval'


return,retval
end
