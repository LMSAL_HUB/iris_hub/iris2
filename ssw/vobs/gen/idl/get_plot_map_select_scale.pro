function get_plot_map_select_scale,map
;
;+
;   Name: get_plot_map_select_scale
;
;   Purpose: get pix:phys scaling for JavaScript "plot_map selection gui" 
;
;   PROJECT: EGSO/CoSEC/VSO
;
;   Input Parameters:
;      NONE
;
;   Output Parameters:
;      funcioon returns structure with plot_map:js required info
;
;   History:
;      R.D.Bentley - Circa 10-mar-2005  
;      S.L.Freeland - vso helper procedure - 21-March-2005  
;
;
;   Restrictions:
;      assumes call follows a call to plot_map
;      probably only useful to the wrapper routine which generates
;      the JS GUI, but here it is...
;   
;
;-
common get_plot_map_select_scale, strtemp

if not data_chk(strtemp,/struct) then $    
   strtemp={imagewidth:0,imageheight:0, $
            imagephysicaltop:0.,imagephysicalbottom:0.,$
            imagephysicalleft:0., imagephysicalright:0.,$
            imagedate:'',origurl:''}


if not valid_map(map) then begin 
   box_message,'function requires input map...'
   return,-1
endif 

retval=strtemp
retval.imagewidth=!d.x_size
retval.imageheight=!d.y_size
retval.imagedate=anytim(map.time,/ecs,/trunc)

x0 = !x.crange(0) & x1 = !x.crange(1)
y0 = !y.crange(0) & y1 = !y.crange(1)
xsc = (x1-x0)/(!p.clip(2)-!p.clip(0))
ysc = (y1-y0)/(!p.clip(3)-!p.clip(1))

xp0 = x0-!p.clip(0)*xsc & xp1 = x1+(!d.x_size-!p.clip(2))*xsc
yp0 = y0-!p.clip(1)*ysc & yp1 = y1+(!d.y_size-!p.clip(3))*ysc
retval.imagephysicaltop=yp1
retval.imagephysicalbottom=yp0
retval.imagephysicalleft=xp0
retval.imagephysicalright=xp1
return,retval
end

