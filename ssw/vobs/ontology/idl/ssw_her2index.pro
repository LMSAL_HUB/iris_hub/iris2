pro ssw_her2index, her_records, index, datax, maps, maps_only=maps_onlin
;
;+
;   Name: ssw_her2index
;
;   Purpose: convert HER records ~ "index" ; optionally return data cubes and/or map arrays (fake data)
;
;   Input Paramters:
;      her_records - sswidl HER records (e.g. output from ssw_her_query.pro)
;
;   Output Parameters:
;      index - the ~SSW index-style corresponding to input HER
;      data - (optional) - faked data cubes corresponding to index - albeit LIST data type since nx/ny may vary
;      maps - (optional) - faked "DMZ" map - LIST data typ
;      NOTE - if keyword MAPS_ONLY switch is set, maps are returned in DATA param (for memory reductions)
;
if not required_tags(her_records,'hpc_x,hpc_y') then begin 
   box_message,'Requires one or more HER records in (output from ssw_her_query.pro), returning..
   return
endif

tindex={date_obs:'',date_end:'',peaktime:'',duration:'', $
   xcen:0.d,ycen:0.d,ref_helio:'', $
   cdelt1:0.6,cdelt2:.6, naxis1:0., naxis2:0., $
   telescop:'', instrume:'', $
   fovx:0., fovy:0.,description:'',kb_archivid:''}

all=her_records
nrec=n_elements(all)

index=replicate(tindex,nrec)
index.date_obs=all.event_starttime
index.date_end=all.event_endtime
index.peaktime=all.event_peaktime
index.duration=ssw_deltat(index.date_end,ref=index.date_obs,/sec)
index.description=all.event_description
index.kb_archivid=all.kb_archivid
index.telescop=all.obs_observatory
index.instrume=all.obs_instrument
index.xcen=all.hpc_x
index.ycen=all.hpc_y
index.fovx=abs(float(all.boundbox_c1ur) - float(all.boundbox_c1ll))
index.fovy=abs(float(all.boundbox_c2ur) - float(all.boundbox_c2ll))
index.naxis1=index.fovx/index.cdelt1
index.naxis2=index.fovy/index.cdelt2

if n_params() gt 2 then begin
   datax=list(bytarr(index[0].naxis1,index[0].naxis2))
   for d=1,nrec-1 do datax.add,bytarr(index[d].naxis1,index[d].naxis2)
endif
   
if n_params() gt 3 then begin
   index2map,index[0],datax[0],mapx
   mapx=add_tag(mapx,index[0],'index')
   maps=list(mapx)
   for d=1,nrec-1 do begin 
      index2map,index[d],datax[d],mapx
      mapx=add_tag(mapx,index[d],'index')
      maps.add,mapx
   endfor
endif

return
end
