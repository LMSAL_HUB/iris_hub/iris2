; Performs the projection of a dataset on the PCA eigenspace
; INPUT
; - A: NxM matrix that contains, as rows, the N M-dimensional observations
;    If the dataset are images of dimensions PxQ, we put each row as a M-dimensional vector
;    of size P*Q
; - eigenvalues: the set of M eigenvalues
; - eigenvectors: a matrix of size MxM containing the eigenvectors
;        eigenvectors[0,*] is the first eigenvector, associated with eigenvalues[0]
;        eigenvectors[i,*] is the i-th eigenvector, associated with eigenvalues[i]
; - n: the number of eigenvectors of the space in which we project the dataset
; OUTPUT
; - output: projected dataset
pro project_pca, A, eigenvalues, eigenvectors, n, output, coefs, rev=rev
	base = eigenvectors[0:n-1,*]
	if keyword_set(rev) then base = eigenvectors[n:*,*]
        ;coefs=A ## base
	;;coefs = A ## transpose(base)
	;output = coefs ## transpose(base)
	coefs = matrix_multiply(A, base, /btrans) 
	output = matrix_multiply(base, coefs, /atrans, /btrans)
end
