function iris_prep_version_check,index_file,loud=loud, quiet=quiet, fake_version=fake_version,debug=debug
;
;+
;   Name: iris_prep_version_check
;
;   Purpose: check IRIS Level2 index or file aginst iris_prep version history - warn if better version available
;
;   Input Parameters:
;      index_file - One IRIS level2 index record -OR- filename (full path)
;
;   Output:
;      boolean function - returns '1' if current data is ~OK, 0 implies better version availble
;
;   Keyword Parameters:
;      loud - if set, print extra info, even if OK
;      quiet - if set, no TTY message, even if Not-OK
;      fake_version - for testing - use This version instead of actual/.HISTORY iris_prep version#
;
;   Calling Examples:
;      IDL> calcheck=iris_prep_version_check(index)
;      IDL> calcheck=iris_prep_version_check(l2file)
;
;   History:
;      1-May-2017 - S.L.Freeland - read_iris_l2.pro and other plug-in
;     11-May-2017 - S.L.Freeland - ignore iris_prep version check for Hinode/AIA... etc
;     16-may-2017 - S.L.Freeland - more emphatic warning message to stand out from TTY-clutter
;     2018/07/11  - Wei Liu, added new date ranges for recent data reprocessing since iris_prep version 1.79
;     2019/02/13 - Ryan Timmons, more date updates.
;     2019/02/26 - Ryan Timmons, debugging stuff added.  Realized was
;                  giving spurious warnings when header empty (this
;                  happens with lots of missing data) so add an escape
;                  if datavals = 0.
;-

retval=0  ; assume fail

quiet=keyword_set(quiet)
loud=keyword_set(loud)
debug=keyword_set(debug)

case 1 of 
   required_tags(index_file,'sumsptrl,sumspat,history,date_obs') : index=index_file[0] ; index supplied
   required_tags(index_file,'telescop,naxis') : index=index_file[0] ; probably, non IRIS index - will verify soon
   (file_exist(index_file))(0): read_iris_l2,index_file[0],index ,/quiet,/silent; L2 file supplied
   else: begin 
      box_message,'Requires an IRIS Level2 index record or filename, bailing...'
      return,retval ; EARLY EXIT on bad input
   endcase
endcase

tele=strupcase(gt_tagval(index[0],/telescop,missing=''))
if tele[0] ne 'IRIS' then begin 
   if loud then box_message,'Non IRIS, skipping iris_prep logic...' ; currently, IRIS-only checks
   return,1 ; EARLY EXIT on Non-IRIS Level2 files
endif 

ip_version=(get_history(index[0],caller=iris_prep,/version))(0)
if keyword_set(fake_version) then begin 
   box_message,'Real Version: ' + strtrim(ip_version,2) + ' but using FAKE_VERSION: ' + strtrim(fake_version,2)
   ip_version=fake_version
endif


index=index[0]
dobs=anytim(index.date_obs)
summed=(index.sumsptrl gt 1 or index.sumsptrl gt 1)(0)
case 1 of 
   ~is_number(ip_version): imess='TEST Version!!'
   ;ip_version lt 1.42: ;all Not OK before this version
   ;dobs gt anytim('1-jan-2016') and ip_version lt 1.53:
   ;(dobs ge anytim('1-jan-2015') and dobs le anytim('1-jan-2016')) and ip_version lt 1.48:
   ;(dobs ge anytim('1-jul-2013') and dobs lt anytim('1-jan-2015')) and summed and ip_version lt 1.48:
                                ;--- 2018/07/11, Wei Liu's
                                ;addition for data reprocessing
                                ;b/c dark updates, etc. This also obsoletes all of the above. ------
   ip_version lt 1.79: ; mission-long reprocessing done 2017-May to Aug
   dobs gt anytim('2017/03/01') and ip_version lt 1.83:
   dobs gt anytim('2017/07/12') and ip_version lt 1.88:
   dobs gt anytim('2017/08/09') and ip_version lt 1.91:
   dobs gt anytim('2017/11/01') and ip_version lt 1.94:
   dobs gt anytim('2017/12/29') and ip_version lt 1.96:
   dobs gt anytim('2018/01/24') and ip_version lt 1.99:
   dobs gt anytim('2018/06/24') and ip_version lt 2.04:
   dobs gt anytim('2018/10/02') and ip_version lt 2.05:
   dobs gt anytim('2018/12/02') and ip_version lt 2.09:
 ;;dobs gt anytim('') and ip_version lt 1.:	; template for future additions
   else: retval=1 ; ok 
endcase

if index.datavals eq 0 then begin
   retval=1
   imess = 'This L2 file is all missing data, skipping calibration check'
endif

if n_elements(imess) eq 0 then begin
   if retval then imess='Current calibration OK' else $
      imess=['!!! WARNING: Better calibration exists !!!','Please re-download latest IRIS data', string(ip_version) + ' --- ' + string(dobs) + ' --- ' + index.date_obs]
endif
if loud or ~retval then box_message,imess,nbox=5,/center

if debug then stop

return,retval
end




      
