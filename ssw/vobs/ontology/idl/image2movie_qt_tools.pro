pro image2movie_qt_tools,movie_dir, movie_name, link=link, $
   parent_out=parent_out, loadsettings=loadsettings, rate=rate, $
   inextension=inextension, inmovie=inmovie
;+
;   Name: image2movie_qt_tools
;
;   Purpose: use qt_tools to generate qt/mpeg4/h264 movies
;
;   Input Paramters:
;      movie_dir - path containing graphics files
;      movie_name - user supplied movie_name (sans extension)
;
;   History:
;      1-mar-2010 - S.L.Freeland - ssw interface -> qt_export
;      6-may-2010 - S.L.Freeland - pattern from frame_????? -> frame_??? (max 999 frames)
;     16-jun-2010 - S.L.Freeland - default style file -> iPad.sty
;     15-nov-2010 - S.L.Freeland - issue with long movie names + qt_export
;                                  use temporary short name, then explicit mv
;     10-nov-2012 - S.L.Freeland - I3.3->I4.4 (allow more than 1000 frames)
;
; -

if file_exist(inmovie) then begin 
   break_file,inmovie,ll,movie_dir,movie_name,inext
   
endif
if n_elements(inextension) eq 0 then inext='*.png' else inext='*.'+ inextension


links=file_search(movie_dir,'frame_????.png')
if links(0) ne '' then begin 
    box_message,'Removing existing "frame_????.png" links'
    for i=0,n_elements(links)-1 do spawn,['rm','-f',links(i)],/noshell
endif

check=get_logenv('qtcheck') ne ''
if file_exist(inmovie) then files=inmovie else $
   files=file_search(movie_dir,inext)
if check then stop,'links,files'

break_file,files(0),ll,pp,ff,fext

if n_elements(rate) eq 0 then rate=15

break_file,movie_dir,ll,mpath,mfile,mext
if n_elements(parent_out) eq 0 then parent_out=mpath
if n_elements(movie_name) eq 0 then movie_name='i2m_qt'

srate=strtrim(rate,2)

defset='exportsettings.st'
defset='iPad.sty'
defset=concat_dir('$SSW_ONTOLOGY',concat_dir('data',defset))
qtenv=get_logenv('qt_style_file')
case 1 of 
   file_exist(loadsettings):  ; user supplied
   file_exist(qtenv): loadsettings=qtenv
   file_exist(defset): loadsettings=defset
   else: begin
      box_message,"Cant find loadsettings file - supply via LOADSETTINGS="
      return
   endcase
endcase
box_message,'Loadsettings/style>> ' + loadsettings

nf=n_elements(files)
lnames=concat_dir(movie_dir,'frame_'+string(indgen(nf),format='(I4.4)') + fext)
linkcmds='ln -s '+ files + ' ' + lnames 

ssw_file_delete,lnames
for i=0,nf-1 do spawn,str2arr(linkcmds(i),' '),/noshell

qt_paths=['/usr/local/bin/']
qt_export=concat_dir(qt_paths,'qt_export')
ss=where(file_exist(qt_export),sscnt)
if sscnt eq 0 then  qt_export=ssw_bin_path('qt_export',found=sscnt)
if sscnt eq 0 then begin
   box_message,'Cannot find qt_export binary; bailing...'
   return
endif
box_message,'Using qt_export>> ' + qt_export(ss(0))

; 19-nov-2010 - S.L.Freeland - another attempt at short enough + uniq tempname
stime=strmid(string(reverse(byte(strtrim(round(systime(1)),2)))),0,5)
tempname=strmid(string(reverse(byte(mfile))),0,15) + stime
if get_logenv('check') ne '' then stop,'tempname'
tempfile=concat_dir(parent_out,tempname)+'.mov'
ssw_file_delete,tempfile

cmd=qt_export(ss(0))+' --replacefile -1 --sequencerate='+srate+ ' ' + lnames(0) + $
' --loadsettings='+loadsettings + ' ' +  $
     tempfile
;    concat_dir(parent_out,movie_name) + '.mov'
if check then stop,cmd
spawn,cmd
if qtenv ne '' then begin 
   box_message,'using $qt_style_file='+qtenv
   break_File,qtenv,ll,pp,ff
   movie_name=movie_name+'_'+ff+'_sty'
endif
mname=concat_dir(parent_out,movie_name)+'.mov'
box_message,'renaming temp->movie_name'
ssw_file_delete,mname
spawn,['mv','-f',tempfile,mname],/noshell
if check then stop,'cmd
return
end




