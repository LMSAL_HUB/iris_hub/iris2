pro sdo_sc_decontamplus, sttim, entim, time2wait, smoot=smoot, win=win, continous=continous, yrange=yrange,xsize=xsize,ysize=ysize
;+
;	$Id: sdo_sc_decontamplus.pro 4942 2012-01-13 03:52:02Z sabolish $
;	$Name:  $
;
;	Name: aia_sc_curvol
;
;	Purpose: Monitor SC Current
;
;	Input:
;		sttim			starting time for plot
;		entim			ending time of plot
;
;   Optional Parameters:
;       time2wait       The plot refresh rate in Seconds, default is 5s or 1 minute
;       smoot           Enable "box car" smoothing function, of size specified (not working)
;       win        		Plot window width in hours to plot, default is .333 hours (20 min)
;		continous		Loop through continuously.  If the letter "p" is hit
;                 		while running continuously, a softcopy is made.  Hit the letter 'x'
;                 		and program will exit the continuous wait.  Default wait is 60 sec.
;		yrange			Specify the yrange .e.g. yrange=[min,max], default is autoscale.
;
; 	Hisotry:
;		9-Jan-08	G.Linford created to allow one to see sum of currents
;		15-Jan-08	G.Linford, setup on AIAEM for dev/debug.
;		22-Jan-08	G.Linford, add some of the standard plot options: yrange
;		28-Jan-08 	G.Linford, add cvs tags, new defaults refresh 5s, win=1hr
;		10-Feb-08	G.Linford, see changes in sdo_rt_decontam_monitor
;
;	$Log: sdo_sc_decontamplus.pro,v $
;	Revision 1.4  2009/02/11 01:35:28  linford
;	Log changes to bug fix in subroutine sdo_rt_decontam_monitor
;	
;	Revision 1.3  2008/06/27 07:29:58  linford
;	changed the default plot time range from 1hr to 20min
;	
;	Revision 1.2  2008/01/28 21:35:57  linford
;	add cvs tags, new def refresh 5s, win=1hr
;	
;
;-
; special setup for AIAEM1a/b using I&T data: 14-Jan-2008, 23:00 to 23:59 
ahost = get_host(1)
if ( strcmp(ahost,'aiaem1b') EQ 1 ) then begin
	setenv,'GSE_PKT_FILE_DIR=/net/aiaem1a/disk0/aiafm1a/egsesw/log/packets'
endif

print,getenv('GSE_PKT_FILE_DIR')


setenv, 'SAG_HKPLOT_NO_MNEM_DESCR=1'		; turn-off long mnemonic descriptions

if not exist(time2wait) then time2wait = 5             ; refresh rate in seconds

;- NOTE smoot or smoot is not USED 17/jul/07
if (keyWord_set(smoot)) then begin
        if ( (smoot mod(2)) ne 0 ) then begin           ; check for odd input value
                if ( smoot gt 15 ) then begin
                        print,' '
                        print,'*******************************************************************'
                        print,'Warnning: Large box-car smoothing value will clip peaks in the data'
                        print,'*******************************************************************'
                        print, ' '
                endif
        endif else begin                ; even input number not allowed
                smoot = smoot -1
        endelse
endif else begin
        smoot = 0
endelse

if (keyWord_set(win)) then begin
	lookback = win * 60;			convert to minutes
endif else begin
	win = .333
	lookback = win * 60;				default window width
	
endelse

if (keyWord_set(continous)) then begin
	if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
	if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=60.*60.*win)
endif else begin
	if (n_elements(sttim) eq 0) then begin
		print, 'Calling Error: must specify either start and end times for plot or continous mode:'
		goto, exitjump
	endif
endelse
sttim = fmt_tim(sttim)
entim = fmt_tim(entim)

;window, /free, xsize=1500, ysize=1150, title='AIA Real-Time EMI/EMC Monitor Plots' ; screen 1600x1200
window, /free, xsize=xsize, ysize=ysize, title='SDO Real-Time Decontam-Plus Current Monitor Plot' ; screen 1600x1200

;!p.multi=[0,1,3]                ; [0,1,3]==,start@0,1 col, 3 rows
;!p.font = 0                     ; non-hershey(-1), 0=dev font, true type=1


if (keyWord_set(continous)) then begin
qdone = 0
print, 'Waiting', format='($,a)'
while (not qdone) do begin
        ;!p.multi=[0,1,3]
		sdo_rt_decontam_monitor, sttim, entim, smoot=smoot, lookback=lookback, yrange=yrange
		
        ans = get_kbrd2(0)
    	if ans ne '' then begin
        	print, 'Keyboard stack check, value: '+ ans
        	if (ans eq 'p' || ans eq 'P') then begin
                        print,'Saving Plots'
                        utnow = ut_time(/ex)            ; external format
                        idat = anytim(utnow,/ext)       ; fix the format
                        idate=string(format='(i4,i2.2,i2.2,a1,i2.2,i2.2,i2.2)',idat[6],idat[5],idat[4],'_',idat[0],idat[1],idat[2]);yyyymmdd_hhmmss
                        gif_file = idate + '_HK.gif'
                        ;zbuff2file,gif_file,/gif,/xwin,outdir='/home/sdotc/aia_html/EMIEMC'
                        zbuff2file,gif_file,/gif,/xwin,outdir='/home/sdotc/aia_html/InT'
                        set_plot,'x'
            endif
        	if (ans eq 'x' || ans eq 'X') then goto, exitjump
    	endif

        wait, time2wait
		; recompute sttim and entim
		sttim = anytim2ints(ut_time(), off=-60.*60.*win)
		entim = anytim2ints(sttim, off=60.*60.*win)
		
endwhile
endif else begin
	sdo_rt_decontam_monitor, sttim, entim, smoot=smoot, lookback=lookback, yrange=yrange
endelse


exitjump:                                       ; exit loop
;clearplot										; reset
end
