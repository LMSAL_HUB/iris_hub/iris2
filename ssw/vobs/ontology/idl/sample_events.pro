feat_name_arr = ['ActiveRegion','BrightPoint','CME','CoronalDimming','CoronalHole', $
                 'CoronalWave','Filament','FilamentEruption','Flare','Loop','Oscillation',$
		'SunSpot','EmergingFlux']
feat_code_arr = ['AR','BP','CE','CD','CH','CW','FI','FE','FL','LP','OS','SS', 'EF']

FOR i=0,N_elements(feat_name_arr)-1 DO BEGIN
   event = struct4event(feat_code_arr[i])
   export_event, event, /write_file, outfil=feat_name_arr[i]+"_example.xml", /relax
ENDFOR

end
