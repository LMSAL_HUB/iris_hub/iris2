;+
; NAME:
;       iris_geocal_prep()
;
; PURPOSE:
;       Processes Level 1 data from the FUV geometry calibration
;       into a pseudo-Level 1.5 product.
;       How this is different from normal IRIS processing:
;             Preps geometry data (FUV), just does despiking, the dark
;             correction adds unnecesaary noise.  NUV geometry can be
;             derived from the flat data, which has it's own
;             prepping routine
;
; CALLING SEQUENCE:
;       result = iris_geocal_prep(index, data, [filename=, datadir=, /pickfile])
;
; INPUTS:
;       filename - an IDL save file with the Level 1 data in the form
;                  of index, data
;
;      -or-
;
;       index, data - the structure and array of the Level 1 data
;                     supplied directly from the get_geocal_data
;                     function
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filename of the IDL save file containing the index
;                structure and data array of the processed data
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       IRIS_PREP
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result=iris_geocal_getdata(time_start, time_end, index, data, $
;     >                            datadir=datadir, /nosave, /jsoc2)
;     > result=iris_geocal_prep(index, data, datadir=datadir)
;
; MODIFICATION HISTORY:
;       Started 2014-June-30 by Sarah A. Jaeggli, Montana State University
;-

function iris_geocal_prep, index, data, filename=filename, datadir=datadir, $
                           pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1.sav', $
                              title='Please select a L1 file to process')

  if keyword_set(filename) then restore, filename

  light=where(index.img_type eq 'LIGHT', lightcount)

  iris_prep, index, data, oindex, odata, $
             /nowarp, /noflat, /nodark, /noback, /despike_here

  index=oindex
  data=odata

  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $ ;year
             strmid(dt, 5,2) + $ ;month
             strmid(dt, 8,2) + $ ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_'+index[0].img_path+'_l1p5.sav'

  save, index, data, filename=savename
  print, 'Saving level 1.5 '+index[0].img_path+' geo data in '+savename

  return, savename

end
