function ssw_jsoc_struct2index, jsin
;+
;   Name: ssw_jsoc_struct2index
;
;   Purpose: jsoc json 'rs_list' keyword -> ssw index ("flatten")
;
;   Input Paramters:
;      jss - vector of JSCO structures; e.g. ssw_jsoc(/RS_LIST) output]

if not required_tags(jsin,'count,status') then begin 
   box_message,'Function requires JSOC RS_LIST structure vector'
   return,-1
endif

js=rem_tag(jsin,'SEGMENTS') 

ntags=n_tags(js)
varr='js.('+strtrim(indgen(ntags),2)+')(0)'

tnames=tag_names(js)
estring='tnames,' + arr2str(varr)
es='retval=create_struct('+estring + ')'
estat=execute(es)

if n_elements(js.(0)) gt 1 then retval=replicate(retval(0),retval.count)

for i=0,ntags-1 do retval.(i)=js.(i) 
retval=ssw_jsoc_timefix(retval)

return,retval
end
