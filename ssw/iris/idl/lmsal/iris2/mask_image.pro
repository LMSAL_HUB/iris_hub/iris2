function mask_image, ima

aux = ima
dim = size(aux)


ans = 0
list_mask_arrays = LIST()
list_mask_data = LIST()

while ans eq 0 do begin
    aux_mask = ima*0.
    print, 'Select the 2 opposite corners of the selection box'
    sel = tvg(aux, max_sel=2)
    if sel[0].selected_flag ne 0 then begin
        xb_1 = sel[0].coordinates[0]
        yb_1 = sel[0].coordinates[1]
        xb_2 = sel[1].coordinates[0]
        yb_2 = sel[1].coordinates[1]
        xb = xb_1 < xb_2
        yb = yb_1 < yb_2
        nxb = abs(xb_2 - xb_1)
        nyb = abs(yb_2 - yb_1)
        nxb = abs(xb_2 - xb_1)
        nyb = abs(yb_2 - yb_1)
        nxb = ceil(nxb)
        nyb = ceil(nyb)
        if nxb gt 0 and nyb gt 0 then begin
           aux_mask[xb:xb+nxb,yb:yb+nyb] = 1.
           if list_mask_arrays.Length eq 0 then begin
             list_mask_arrays= LIST(aux_mask)
             list_mask_data = LIST({xb:xb, nxb:nxb, yb:yb, nyb:nyb})
           endif else begin
             list_mask_arrays.Add, aux_mask
             list_mask_data.Add, {xb:xb, nxb:nxb, yb:yb, nyb:nyb}
           endelse  
           aux = ima+total(list_mask_arrays.ToArray(), 1)*ima
        endif else begin    
           print, 'WARNING: Two of the coordinates are the same. Nothing has been done.' 
           ;print, 'xb, nxb, yb, nyb', xb, nxb, yb, nyb
        endelse
    endif else begin 
        ans=1
    endelse    
endwhile

return, {list_mask_arrays:list_mask_arrays, list_mask_data:list_mask_data} 

end




    
