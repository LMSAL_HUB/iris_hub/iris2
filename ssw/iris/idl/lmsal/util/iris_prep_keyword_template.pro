function iris_prep_keyword_template,index, template_file=template_file, status=status
;
;+
;   Name: iris_prep_keyword_template
;
;   Purpose: append or return structure template for keywords added by iris_prep 
;
;   Input Parameters:
;      index - optional input structure for append
;
;   Output:
;      function returns structure template -or- appended if input INDEX is supplied
;
;   Keyword Parameters:
;      status - 0 if fail, 1 if success
;      template_file - optional .geny file containing desired template, probably only used for testing...
;
;   History:
;      7-May-2015 - S.L.Freeland
;
;-
;      

if n_elements(template_file) eq 0  then $ 
   template_file=concat_dir(concat_dir('$SSW_IRIS','data'),'iris_prep_keywords_template.geny')

template_file=template_file[0]
if file_exist(template_file) and strpos(template_file,'.geny') ne -1 then begin 
   restgenx,file=template_file,retval ; default is template
   if data_chk(index,/struct) then retval=join_struct(index,retval) ; optional append to input INDEX
endif else begin 
   box_message,'Cannot find or use template file>> ' + template_file[0]
   retval,-1
endelse

status=data_chk(retval,/struct)

return,retval
end
