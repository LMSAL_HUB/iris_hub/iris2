; ---------- SSW Web Service Checks ---------
box_message,'Checking SSW Services'
status=ssw_service_watchdog(/post,stat)
help,status,stat,/str

; ---------- SSW HER Watchdog (auth->upload->readback) ---
box_message,'SSW/HER communications checks'
set_logenv,'ssw_hpkb','HERhcrSDO10'
status=ssw_hpkb_her_watchdog(/login,/post)
if status then begin
   status=ssw_hpkb_her_watchdog(/post,stat) ; upload/readback test 
endif else begin 
   box_message,'HPKB Login failed - skipping upload/readback test
endelse


; --------- SSW HCR Watchdog (auth->upload->readback->expire )
ssw_coverage_ingest,/test,/post

end
