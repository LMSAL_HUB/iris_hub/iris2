pro iris_focus_trend, calculated_foci, historical_focus_vals, $
                      outdir=outdir, quiet=quiet

;+
;   Name: iris_focus_trend

;   Purpose: plot 


;   Input Parameters:


;   Keyword Parameters:


;   Output:


;   Calling Examples:


;-

; Data = [date, 1330 median focus, 1400 median focus] -- 
; 1330 & 1400 are medians of mosaic over FOV
; eclipse season tests are late in orbit after eclipse

times = calculated_foci.focus_obs_start_date[0,*]
;stop, 'check and see if times has a problem with looping back.'
ntimes = n_elements(times)
f1330 = float(calculated_foci[0:*].median_mosaic_focus_1330_str)
f1400 = float(calculated_foci[0:*].median_mosaic_focus_1400_str)
pmm,f1330
pmm,f1400

oldDevice = !D.NAME
set_plot, 'Z'
wdef,0,1000,800
!p.charsize = 1.5
!p.multi = 0

xtit = 'Start Time 28-Jul-2013'
ytit = 'FUV Focus Position'
title = 'IRIS Slit Jaw Image Focus Trending'
yr = [-150,-110]
;utplot,times,f1330,psym=-4,xtit=xtit,ytit=ytit,title=title,yr=yr,/ystyle
;is it the addition of the timerange values that is changing the years-range in the plot?  experiment.
utplot,times,f1330,psym=-4,xtit=xtit,ytit=ytit,title=title,yr=yr,timerange=[times[0], times[n_elements(times)-1]],/ystyle
outplot,times,f1400,psym=-5,line=1
;stop, 'what is that bottom line?'
;instead, replace those values in a loop on the array of the data structure of historical commanding.
terminal_loop_val = n_elements(historical_focus_vals) - 1
for i=0,terminal_loop_val do begin
;original:  tset = [historical_focus_vals[i].commanded_focus_start_date, historical_focus_vals[i].commanded_focus_end_date]
  tset = [anytim2cal(strmid(historical_focus_vals[i].commanded_focus_start_date,0,11), form=10), anytim2cal(strmid(historical_focus_vals[i].commanded_focus_end_date,0,11), form=10)]
  fset = [fix(historical_focus_vals[i].median_mosaic_focus_1330_str),fix(historical_focus_vals[i].median_mosaic_focus_1400_str)]
  print, string(i) + ': tset = ', strtrim(string(tset),2) + ', fset = '+ strtrim(string(fset),2)
;stop,'stopped: looking for error in outplot'
  outplot, tset, fset, line=2
endfor 

print, 'number of elements in historical_focus_vals = ', n_elements(historical_focus_vals)

xyouts,160,270,'Solid  = 1330',   /device
xyouts,160,220,'Dotted = 1400',   /device
xyouts,160,170,'Dashed = Default',/device
xyouts,160,120,'NUV trend is identical (NUV focus = FUV+2)',/device

if not keyword_set(outdir) then outdir='.'
tvlct,rct,gct,bct, /get
big = 255b-tvrd()
;outfile = outdir+'/'+'iris_focus_trending_'+strmid(times(ntimes-1),0,11)+'.gif'
outfile = outdir+'/'+'iris_focus_trending_'+strtrim(strmid(times(ntimes-1),0,9),2)+'.gif' ;new.  I made this 11 from 10

write_gif,outfile,big,rct,gct,bct
if not keyword_set(quiet) then print,'Saved plots in ', outfile

set_plot, oldDevice
print, 'Completed iris_focus_trend'
end
