pro tbtv_egse_screen, time, scr_name, mnem, infil, to_local=to_local, outfil=outfil
;
;
;
if (n_elements(time) eq 0) then time = ut_time()
if (n_elements(scr_name) eq 0) then scr_name = concat_dir('$SXI_TBTV_DB_DIR', 'tbtv_screen1.scr')
if (n_elements(mnem) eq 0) and (not keyword_set(infil)) then infil = '$SXI_TBTV_DB_DIR/tbtv_screen_mnem1.tab
if (n_elements(mnem) eq 0) then begin
    list = rd_tfile(infil, nocomment='#')
    mnem_mat = str2cols(list)
    mnem = strlowcase(strtrim(mnem_mat(0,*),2))
end
;
sttim = anytim2ints(time, off=-10*60)
entim = anytim2ints(time, off=10*60)
;
info = sag_get_mnem(sttim, entim, mnem, /nostring)
;
tags = tag_names(info)
ntags = n_elements(tags)
label = ''
value = 0.
for itag=0,ntags-1 do begin
    mnem0 = strlowcase(info.(itag).mnem)
    timarr = info.(itag).daytime
    ii = tim2dset(timarr, time)
    ;
    label = [label, mnem0]
    value = [value, reform(info.(itag).value(ii,*))]
end
;
ss = where( (strpos(label, 'zonedb') ne -1), nss)
for i=0,nss-1 do begin
    label0 = label(ss(i))
    ss2 = where( (strpos(label0, 'sxi_forzonedb') ne -1) or (strpos(label0, 'sxi_aftzonedb') ne -1), nss2)
    if (nss2 ne 0) then hk = bytarr(600) + byte( 255 - value(ss(i)) ) $
		   else hk = bytarr(600) + byte( value(ss(i)) )
    v = sag_ext_mnem(hk, strmid(label0, 0, 11) + 'tmp')
    label = [label, label(ss(i))+'tmp']
    value = [value, v]
end
;
dattim = timarr(ii)
if (keyword_set(to_local)) then dattim = ut_time(dattim, /to_local)
envmon_screen, scr_name, dattim, value, label, outfil=outfil, /nocolumn_adjust
;
end