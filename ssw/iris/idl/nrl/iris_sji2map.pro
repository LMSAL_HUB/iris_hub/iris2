
FUNCTION iris_sji2map, sjifile, trange=trange, tmatch=tmatch, nonorm=nonorm, $
                       no_dustbuster=no_dustbuster

;+ 
; NAME:
;      IRIS_SJI2MAP()
;
; PURPOSE
;      Takes an IRIS SJI file and puts the data into a map-movie
;      structure (which can be played with, e.g., movie_map.pro). 
;
; CATEGORY:
;      IRIS; SJI; images; display; maps.
;
; CALLING SEQUENCE:
;       Result = IRIS_SJI2MAP( SJIFILE )

; INPUTS:
;      Sjifile:  The name of an IRIS SJI file.
;
; OPTIONAL INPUTS:
;      Trange:   A 2-element array specifying a time range in any of
;                the standard SSW time formats. Only images lying
;                within this range will be returned. If a single time
;                is specified, then a single frame (closest to this
;                time) will be returned. 
;
;      Tmatch:   This is an array of times, and the routine will only
;                load those images that are closest in time to these
;                times. The cadence of TMATCH should be lower than
;                that of the SJI sequence. A single time can be
;                specified. 
;
; KEYWORDS:
;      NONORM:   By default each image is divided by the exposure
;                time. Setting /nonorm switches this off.
;
; OUTPUTS:
;      An array of IDL map structures containing the SJI images. Note
;      that the TIME tag of the output corresponds to the mid-point of
;      the SJI exposure.
;
;      If there's a problem then -1 will be returned.
;
; MODIFICATION HISTORY:
;      Ver.1, 3-Jan-2016, Peter Young
;      Ver.2, 6-Jan-2016, Peter Young
;          Expanded header; added check on inputs.
;      Ver.3, 6-May-2016, Peter Young
;          I now allow TRANGE and TMATCH to be scalars, so that a
;          single time can be specified; I've replaced the
;          exposure time with that returned from the object methods;
;          for the solar-Y check, this is only done if there are at
;          least 3 frames. 
;-

IF n_params() LT 1 THEN BEGIN
  print,'Use:  IDL> map = iris_sji2map( sjifile [, trange=, tmatch= ] )'
  return,-1
ENDIF 

IF n_elements(trange) NE 0 AND n_elements(tmatch) NE 0 THEN BEGIN
  print,'%IRIS_SJI2MAP:  please specify only one of TRANGE or TMATCH. Returning...'
  return,-1
ENDIF 

;
; Load the SJI data object.
;
d=iris_sji(sjifile)

;
; Get time information from the SJI object. Note that sji_tai is the
; time of the mid-point of the exposure. 
;
sji_ti=d->gettime()
sji_nexp=d->getnexp(0)
sji_exp=d->getexp(indgen(sji_nexp))
sji_ti=sji_ti+sji_exp/2.        ; get mid-time of exposure
sji_utc=d->ti2utc(sji_ti)
sji_tai=anytim2tai(sji_utc)
exp_time=d->getexp()   ; get exposure time

obj_destroy,d

CASE n_elements(trange) OF
  0: imagen=indgen(sji_nexp)
  1: tmatch=trange
  2: BEGIN
    tr_tai=anytim2tai(trange)
    imagen=where(sji_tai GE tr_tai[0] AND sji_tai LE tr_tai[1])
  END 
ENDCASE

;; IF n_elements(trange) NE 0 THEN BEGIN
;;   tr_tai=anytim2tai(trange)
;;   imagen=where(sji_tai GE tr_tai[0] AND sji_tai LE tr_tai[1])
;; ENDIF ELSE BEGIN
;;   imagen=indgen(sji_nexp)
;; ENDELSE 

;
; If TMATCH has been specified, then reduce index and list to those
; elements that are closest to the times in TMATCH. Note that index
; and list will have the same size as TMATCH and it's possible
; that the same image may end up appearing twice in the output (if the
; cadence of TMATCH is higher than that of the SDO sequence).
;
; Note that TRANGE is over-ridden if TMATCH is specifed.
;
IF n_elements(tmatch) NE 0 THEN BEGIN
  tmatch_tai=anytim2tai(tmatch)
  n=n_elements(tmatch)
  imagen=-1
  FOR i=0,n-1 DO BEGIN
    getmin=min(abs(sji_tai-tmatch_tai[i]),imin)
    imagen=[imagen,imin]
  ENDFOR
  imagen=imagen[1:*]
  junk=temporary(trange)  ; get rid of trange it's been defined.
ENDIF 

;
; The following rigmarole is necessary because imagen has to be at
; least a 2-element array, so I artificially create one when imagen is
; only a scalar.
;
swtch=0
IF n_elements(imagen) EQ 1 THEN BEGIN
  IF imagen EQ sji_nexp-1 THEN BEGIN
    swtch=2
    imagen=[imagen-1,imagen]
  ENDIF ELSE BEGIN
    swtch=1
    imagen=[imagen,imagen+1]
  ENDELSE 
ENDIF 

read_iris_l2,sjifile,index,data, imagen=imagen, trange=trange, $
             /keep_null,/silent

;
; This reformats the output for the case where imagen is a scalar.
;
IF swtch EQ 1 THEN BEGIN
  index=index[0]
  imagen=imagen[0]
  data=reform(data[*,*,0])
ENDIF
IF swtch EQ 2 THEN BEGIN
  index=index[1]
  imagen=imagen[1]
  data=reform(data[*,*,1])
ENDIF


IF NOT keyword_set(no_dustbuster) THEN data=iris_dustbuster(index,temporary(data))

;
; Convert to map and set time to the mid-time of the exposure.
;
index2map,index,data,map


;
; Apparently there's some problems with exposure times when
; read with read_iris_l2, so I make sure to use the exposure time
; obtained from the object methods.
;
map.dur=exp_time[imagen]

;
; The following normalizes each image by the exposure time. Note that
; the DUR tag is set to 1.
;
n=n_elements(map)
IF NOT keyword_set(nonorm) THEN BEGIN
  FOR i=0,n-1 DO BEGIN
    arr=map[i].data
    k=where(arr NE -200.)
    arr[k]=arr[k]/index[i].exptime
    map[i].data=arr
    map[i].dur=1.0
  ENDFOR 
ENDIF 
map.time=anytim2utc(/ccsds,sji_utc[imagen])

;
; I found one data-set (22-Oct-2013 23:00) where some frames had
; strange ycen values (the xcen values seemed OK) so the following
; checks for this and replaces them with the median ycen value.
;
IF n GT 2 THEN BEGIN 
  yi=findgen(n)
  yc_med=median(map.yc)
  k=where(abs(map.yc-yc_med) GT 2.0,nk)
  IF nk GT 0 THEN BEGIN
    print,'** There seem to be some anomalous YCEN values. See plot. **'
    plot,yi,map.yc,psym=1,xsty=3,ysty=3,/ynozero
    oplot,[0,n],[1,1]*yc_med,thick=2
    print,'** The anomalous values are replaced with a median of the image sequence. **'
    map[k].yc=yc_med
  ENDIF
ENDIF 

return,map

END
