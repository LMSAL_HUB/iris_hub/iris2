function IRIS_PREP_DEPEDESTAL, idata, iindex, ds_in = ds_in

;+
;
;
; Given a data array (single image) and header, removes the pedestal
;
;-

odata = idata
if N_ELEMENTS(ds_in) eq 0 then ds_in = 'iris.level1_prelim02'

dspec = 4144 / iindex.sumsptrl / 2
dspat = 1096 / iindex.sumspat / 2
case STRMID(iindex.img_path, 0, 3) of
	'FUV'	:	begin
					if ANYTIM2TAI(iindex.t_obs) gt ANYTIM2TAI('2013-07-26T23:26:00.00Z') then begin
						fped = [100.397, 97.1273, 94.3764, 100.886]		;	New lower pedestal for hi-gain FUV
					endif else begin
						fped = [123.38, 118.43, 116.30, 122.91]
					endelse
					case ds_in of
						'iris.level1_prelim02'	:	fped = fped - 110
						'iris.level1_prelim01'	:	fped = fped - 100
						else	:	fped = fped
					endcase
					odata[0:dspec - 1,0:dspat - 1] = odata[0:dspec - 1,0:dspat - 1] - fped[0]	;	Port E
					odata[dspec:*,0:dspat - 1] = odata[dspec:*,0:dspat - 1] - fped[1]	;	Port H
					odata[0:dspec - 1,dspat:*] = odata[0:dspec - 1,dspat:*] - fped[2]	;	Port F
					odata[dspec:*,dspat:*] = odata[dspec:*,dspat:*] - fped[3]	;	Port G
				end
	'NUV'	:	begin
					nped = [101.16, 102.07]
					case ds_in of
						'iris.level1_prelim02'	:	nped = nped - 90
						'iris.level1_prelim01'	:	nped = nped - 100
						else	:	nped = nped
					endcase
					odata[*,0:dspat - 1] = odata[*,0:dspat - 1] - nped[0]	;	Port E
					odata[*,dspat:*] = odata[*,dspat:*] - nped[1]	;	Port F
				end
	'SJI'	:	begin
					sped = [100.95, 98.45]
					case ds_in of
						'iris.level1_prelim02'	:	sped = sped - 90
						'iris.level1_prelim01'	:	sped = sped - 100
						else	:	sped = sped
					endcase
					odata[*,0:dspat - 1] = odata[*,0:dspat - 1] - sped[0]	;	Port H
					odata[*,dspat:*] = odata[*,dspat:*] - sped[1]	;	Port G
				end
	else	:	PRINT, 'wtf?'
endcase

RETURN, odata

end