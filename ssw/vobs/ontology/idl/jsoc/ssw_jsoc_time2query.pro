function ssw_jsoc_time2query, time0, time1, $
    day_window=day_window, hour_window=hour_window, $
    minute_window=minute_window, $
    ut=ut, tai=tai, ds=ds, bracket=bracket, ccsds=ccsds, $
    cadence=cadence, units_cadence=units_cadence, debug=debug, $
    fsn_only=fsn_only
;+
;   Name: ssw_jsoc_time2query
;
;   Purpose: SSW time or time range -> JSOC style query string 
;
;   Input Paramters:
;      time0 - time or start time of range
;      time1 - optional stop time of range
;
;   Output Paramters:
;      function returns jsoc time/query string 
;      "[<starttime>/<nn><{d,h,m,s}>]"
;
;   Keyword_Parameters:
;      minute_window, hour_window - optionally expand 'time0' window 
;      (per time_window, scalar implies -N/+N ;  2 elem array = [-M,+N] )
;      UT/TAI - optional -> ssw_time2jsoc
;      ds - optional root data series name (default='')
;
;   Method:
;      call ssw-gen routines, ssw_deltat, time_window, ssw_time2jsoc
;
;   History:
;      circa 1-jan-2009 - S.L.Freeland
;      31-aug-2010 - fix array vs function issue... - old habits...
;       9-jul-2012 - change cadence LIMITS multiplier 5->100
;                    (changes cadence units lookup)
;     23-jul-2013 - add /FSN_ONLY (iris.lev0 for example) which 
;                   uses drms macro for time based search (thanks Phil)
;-

debug=keyword_set(debug)
case n_params() of
   2:
   1: time1=time0
   else: begin 
      box_message,"need something to work with..."
      retval,""
   endcase
endcase
 
t0=anytim(time0,/ecs) & t1=anytim(time1,/ecs)

twindow=keyword_set(hour_window) or keyword_set(minute_window)

if twindow then time_window,[t0,t1], t0,t1, $ ; expand +/- time window
       hour_window=hour_window, minute_window=minute_window

if keyword_set(ccsds) then startt=anytim(t0,/ccsds,/trunc)+'Z' else $
startt=ssw_time2jsoc(t0,ut=ut, tai=tai) ; jsoc ranges are stop time + interval

cadencex=''
if keyword_set(fsn_only) then begin 
   if debug then box_message,'No time prime key option - faking it via macro'
   startt=anytim(t0,/ccsds,/trunc)
   stopt=anytim(t1,/ccsds,/trunc) 
   retval='? DATE__OBS >= $(' + startt +') AND DATE__OBS <= $(' + stopt + ') ?'
endif else begin 
   dth=ssw_deltat(t0,t1,/days)
   dts=ssw_deltat(t0,t1,/seconds)
   limits=[1., 60.,3600,24.*3600] 

   units=str2arr('s,m,h,d')

   flimits=100 ; changed limits factor 5->100 9-jul-2012 - slf
   ssl=last_nelem(where(dts gt (limits*flimits)))>0

   retval=startt + '/' + strtrim(round(dts*(1./limits[ssl])),2) + units(ssl)
   if keyword_set(cadence) then begin 
      ucad='m' ; default minutes
      if data_chk(units_cadence,/string) then ucad=strlowcase(strmid(units_cadence,0,1))
      case 1 of
         data_chk(cadence,/string) and strlen(cadence) gt 1: cadencex='@'+strtrim(cadence,2)
         else:cadencex='@'+ strtrim(cadence,2)+ucad
      endcase
   endif
endelse 

if n_elements(ds) eq 0 then ds=''
xbrack=(['','[]'])(strpos(strlowcase(ds),'harp') ne -1)
brack=keyword_set(bracket) or keyword_set(ds)
b1=(['','['])(brack)  & b2=(['',']'])(brack)
retval=ds+xbrack+b1+retval+cadencex+b2
if debug then stop,'retval time2query';
return,retval
end



