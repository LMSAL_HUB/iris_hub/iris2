function strrep, cadena_in, patron, nuevo

cadena0=reform(cadena_in)
dimcadena=size(cadena0)
if dimcadena[0] eq 0 then dimcadena[1]=1
out=cadena0

for t=0, dimcadena[1]-1 do begin

cadena=out[t]
donde=where_s(cadena, patron)
lon=strlen(cadena)
lon_patron=strlen(patron)
lon_nuevo=strlen(nuevo)

j=0
k=0
palabra=""

while j le lon do begin
    caracter=strmid(cadena,j,lon_patron)
    ;print, caracter," ", patron," ", palabra
    if caracter ne patron then begin
       palabra=palabra+strmid(caracter,0,1)
    endif else begin 
       palabra=palabra+nuevo
       j=j+lon_patron-1
    endelse
    ;if lon_patron ne lon_nuevo then j=j+lon_patron-1
    j=j+1
end

out[t]=palabra

endfor

return, out

end

