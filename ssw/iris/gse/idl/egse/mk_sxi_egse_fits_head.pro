pro mk_sxi_egse_fits_head, h, bhead, infil=infil, outfil=outfil
;+
;NAME:
;	mk_sxi_egse_fits_head
;PURPOSE:
;	Extract information from the binary header and build a 
;	FITS header
;HISTORY:
;V1.0	Written 20-Apr-00 by M.Morrison
;V1.01	21-Apr-00 (MDM) - Added logic to set instrument and CCD
;			  values based on env var
;V1.02	30-Apr-00 (MDM) - Added filter keywords
;			- Added LED keyword and logic to say source is LED
;V1.03	12-May-00 (MDM) - Modified to mask and only use 10 bits for CCD temperature
;V1.04	14-Jun-00 (MDM) - Added pitch/yaw recording option
;V1.05	15-Jun-00 (MDM) - Changed pitch/yaw recording method
;V1.06	 7-Jul-00 (MDM) - Added gain setting (hi/lo)
;			- Added temperatures (MDL)
;			- Corrected SHUTMDUR
;			- Added EGSE_DBV (database version)
;			- Changed SHUTTYPE to be "Telescope" rather than "EM Telescope"
;			- Added OUTFIL input, and reading it if it exists to grab the source value for regeneration
;V1.07	 8-Aug-00 (MDM) - Allow easy bypass of SXI_PY_INFO_FILE code (to speed up things)
;V1.08	13-Oct-00 (MDM) - Added IMG_SN (serial number) and AC_TYPE 
;			- Added FDB
;V1.09	16-Oct-00 (MDM) - Added FRMPRIOR (frame priority - prime/secondary)
;			- Corrected AC_TYPE (wasn't flagging drift properly)
;V1.10	25-Oct-00 (MDM) - Added SEXPLOBT (start exposure LOBT)
;			- Added SEC2ISP (seconds from ISP)
;			- Added SEQID & 
;V1.11	26-Oct-00 (MDM) - Added MIN_CNT, FRMLSTID & MIN_MODE
;V1.12	30-Oct-00 (MDM) - Added CIS_FILE and CIS_IMG
;V1.13	31-Oct-00 (MDM) - Added EXT_COL, EXT_LIN (extract coordinates)
;			- Added BINNING 
;V1.14	18-Dec-00 (MDM) - Added SEQ_EV, SEQ_EVST, SEC2MIN
;			- Added PERCENTD in mk_sxi_egse_fits
;V1.15	10-Jan-01 (MDM) - Added FM1_A and FM1_TEMP1
;V1.16	19-Jan-01 (MDM) - Made SEC2MIN positive (add 60 sec if negative)
;V1.17	29-Mar-01 (MDM) - Added SHUTSTAT & SHUTENC
;			- Changed FM_A CCD serial number to 03-23 (from 03-13)
;V1.18	 7-Aug-01 (MDM) - Added SXI_EMI_EMC_FITS option to bypass much of header
;			  generation
;			- Also added CAMDAC & F1ENC and F2ENC
;V1.19	17-Aug-01 (MDM) - Made byte variabled integers
;V1.20	30-Jan-02 (MDM) - Added FM2_A
;			- Added keyword HASSLSTZ and HASSFSTZ
;V1.21	29-Apr-02 (MDM) - Changed LED_PWR to look at FDB and power status
;V1.22	22-Aug-02 (MDM) - Added TPITCH and TYAW (pointing values from HASS data)
;-
;
progverno = 1.22
;
if (n_elements(bhead) eq 0) and (n_elements(infil) eq 0) then begin
    if (n_elements(ff) eq 0) then ff = file_list('.', '*.fit')
    ii = xmenu_sel(ff, /one)
    infil = ff(ii)
end
;
if (keyword_set(infil)) then begin
    img = mrdfits(infil, 0, h)
    bhead = mrdfits(infil, 1, hb)
end

;----------------------------
hk = bhead.hk
;;hk(188-6) = byte(bhead.fdb,0,4)		;insert FDB into HK  <<<<<<<<<<< TEMPORARY >>>>>>>>>>>>>>>>>
;
fxaddpar, h, 'EFITS_V', 	progverno
;
fxaddpar, h, 'IMG_SN', 		sag_ext_mnem(hk, 'SXI_FRAMECOUNTER'), ' Image serial number/counter'
fxaddpar, h, 'FDB', 		'0x' + string(sag_ext_mnem(hk, 'SXI_FDB'), '(z8.8)'), ' Frame Definition Block'
;
fxaddpar, h, 'F1ENC', 		fix(sag_ext_mnem(hk, 'SXI_F1ENC'))
fxaddpar, h, 'F2ENC', 		fix(sag_ext_mnem(hk, 'SXI_F2ENC'))
;
if (getenv('SXI_EMI_EMC_FITS') ne '') then return	;don't do stuff below
;
fxaddpar, h, 'F1POS', 		sag_ext_mnem(hk, 'SXI_F1POS')
fxaddpar, h, 'F2POS', 		sag_ext_mnem(hk, 'SXI_F2POS')
fxaddpar, h, 'F1FILTER', 	sag_ext_mnem(hk, 'SXI_F1FILTER')
fxaddpar, h, 'F2FILTER', 	sag_ext_mnem(hk, 'SXI_F2FILTER')

fxaddpar, h, 'SHUTTYPE', 	'Telescope'
fxaddpar, h, 'SHUTMODE', 	sag_ext_mnem(hk, 'SXI_FDB_EXP_TYPE')
fxaddpar, h, 'EXPOSURE', 	sag_ext_mnem(hk, 'SXI_SHUT_EXP_DUR') / 1000., '  Commanded exposure (Secs)'
fxaddpar, h, 'SHUT_OPC',	sag_ext_mnem(hk, 'SXI_SHUT_OPEN'), ' Shutter open counter'
fxaddpar, h, 'SHUT_CLC',	sag_ext_mnem(hk, 'SXI_SHUT_CLOSE'), ' Shutter close counter'
v = (sag_ext_mnem(hk, 'SXI_SHUT_CLOSE') - sag_ext_mnem(hk, 'SXI_SHUT_OPEN')) *4e-6
fxaddpar, h, 'SHUTMDUR',	v, ' Shutter measured duration (Secs)'
fxaddpar, h, 'SHUTSTAT',	'0x' + string(sag_ext_mnem(hk, 'SXI_SHSTAT'), '(z2.2)'), ' Shutter mechanism status'
fxaddpar, h, 'SHUTENC',		'0x' + string(sag_ext_mnem(hk, 'SXI_SHENC'), '(z2.2)'), ' Shutter mechanism encoder'
;
v1 = sag_ext_mnem(hk, 'SXI_AC_FRM_ENABLE', /raw)
v2 = sag_ext_mnem(hk, 'SXI_AC_ENABLE', /raw)
v3 = sag_ext_mnem(hk, 'SXI_ACMODE', /raw)
bits, v3, barr_v3
v = 'None'
if (v1) and (v2) then begin
    if (barr_v3(2)) then v = 'Drift' $
		    else v = 'HASS'
end
fxaddpar, h, 'AC_TYPE', 	v, ' Attitude Control Type'
fxaddpar, h, 'HASSY',		sag_ext_mnem(hk, 'SXI_HASSERRORY'), ' HASS error Y reading'
fxaddpar, h, 'HASSZ',		sag_ext_mnem(hk, 'SXI_HASSERRORZ'), ' HASS error Z reading'
fxaddpar, h, 'HASSLSTZ',	sag_ext_mnem(hk, 'SXI_HASSLASTZ'), ' HASS Z Last reading'
fxaddpar, h, 'HASSFSTZ',	sag_ext_mnem(hk, 'SXI_HASSFIRSTZ'), ' HASS Z first reading'

fxaddpar, h, 'FRMPRIOR', 	sag_ext_mnem(hk, 'SXI_FRAME_PRIORITY'), ' Frame Priority (Prime/Sec)'
fxaddpar, h, 'MIN_CNT',		sag_ext_mnem(hk, 'SXI_MINUTE_COUNT')
fxaddpar, h, 'SEQID',		sag_ext_mnem(hk, 'SXI_SEQID')
fxaddpar, h, 'SEQ_EV',		sag_ext_mnem(hk, 'SXI_SEQ_EVENT'), ' Active sequence event'
fxaddpar, h, 'SEQ_EVST',	'0x' + string(sag_ext_mnem(hk, 'SXI_SEQ_EVENT_STATES'), '(z4.4)'), ' Active events'
fxaddpar, h, 'FRMLSTID',	sag_ext_mnem(hk, 'SXI_FRAMELIST')
fxaddpar, h, 'MIN_MODE',	sag_ext_mnem(hk, 'SXI_MIN_UPDATE_MODE')
;
fxaddpar, h, 'CAMAMP', 		sag_ext_mnem(hk, 'SXI_CAMAMP')
fxaddpar, h, 'CAMDAC', 		fix(sag_ext_mnem(hk, 'SXI_CAMDAC'))
fxaddpar, h, 'SERCLK',	 	sag_ext_mnem(hk, 'SXI_CAMSERCLK')
fxaddpar, h, 'PARCLK', 		sag_ext_mnem(hk, 'SXI_CAMPARCLK') 
;
v = 'B'
if (fxpar(h, 'CAMAMP') eq 'North   ') then v = 'A'
fxaddpar, h, 'OP_AMP', 		v
fxaddpar, h, 'AMPAGAIN', sag_ext_mnem(hk, 'SXI_CAMGAIN')
fxaddpar, h, 'AMPBGAIN', sag_ext_mnem(hk, 'SXI_CAMGAIN')
v = 'Reverse'
if (fxpar(h, 'SERCLK') eq fxpar(h, 'CAMAMP')) then v = 'Forward'
fxaddpar, h, 'SER_READ', 	v
v = 'Reverse'
if (fxpar(h, 'PARCLK') eq 'West    ') then v = 'Forward'
fxaddpar, h, 'PAR_READ', 	v 

a = sag_ext_mnem(hk, 'SXI_CHSWTYP') 
b = sag_ext_mnem(hk, 'SXI_CHSWVER')
c = sag_ext_mnem(hk, 'SXI_CHSWPN')
fswver = string( a,b,c, format='(z3.3,".",z1,".",z4.4)')
fxaddpar, h, 'FSW_VER', fswver
fxaddpar, h, 'EGSE_DBV', sxi_get_egse_db_file(getenv('SXI_EGSE_DB_PREFIX'))

temps = (bhead.cam_head and '3FF'X) *28.44*3.5337*5/1024. - 273.15

fxaddpar, h, 'T_CCDS1', 	temps(3)
;fxaddpar, h, 'T_CASE1',	temps(8)
;fxaddpar, h, 'T_CASE2',	temps(9)

fxaddpar, h, 'DATEOBS', 	gt_day(fxpar(h, 'EGSE_TIM'), /str), '  GMT EGSE first pkt down'
fxaddpar, h, 'TIMEOBS', 	gt_time(fxpar(h, 'EGSE_TIM'), /str), '  GMT EGSE first pkt down'
v1 = sag_ext_mnem(hk, 'SXI_LOBT_SEC_SHUT')
v2 = sag_ext_mnem(hk, 'SXI_LOBT_FSEC_SHUT')
offset = v1 + v2/65536D
v = fmt_tim( anytim2ints( getenv('SAG_INSTRUMENT_EPOCH'), off=offset), /msec)
fxaddpar, h, 'SEXPLOBT',	v, ' Start of exposure LOBT'
v1 = sag_ext_mnem(hk, 'SXI_LOBT_SEC_ISP')
v2 = sag_ext_mnem(hk, 'SXI_LOBT_FSEC_ISP')
offset2 = v1 + v2/65536D
fxaddpar, h, 'SEC2ISP',		offset-offset2, ' Seconds from last ISP'
v3 = sag_ext_mnem(hk, 'SXI_MIN_START_CPHASE')/128.	;seconds from ISP to minute start
v4 = offset-offset2-v3
if (v4 lt 0) and (v4 gt -60) then v4 = v4 + 60
fxaddpar, h, 'SEC2MIN',		v4, ' seconds from minute start'
fxaddpar, h, 'MIN_STRT',	v3, ' seconds from ISP to minute start'
fxaddpar, h, 'SECSETUP',	sag_ext_mnem(hk, 'SXI_SETUP_DELAY')/128., ' seconds for setup delay'


fxaddpar, h, 'TIMEZONE', 	'GMT'
fxaddpar, h, 'PROJECT', 	'SXI'
fxaddpar, h, 'ACTIVITY', 	'Instrument Testing'
fxaddpar, h, 'MACHINE', 	get_host()

fxaddpar, h, 'SOURCE', 		'Unknown'
v = getenv('SXI_SOURCE_FITS')
if (keyword_set(outfil)) then if (file_exist(outfil)) then begin
    mreadfits, outfil, h_exist
    if (tag_exist(h_exist, 'SOURCE')) then begin
	v = h_exist.source
	print, 'Using SOURCE from old file: ' + v
    end
end
if (keyword_set(v)) then fxaddpar, h, 'SOURCE', v

fxaddpar, h, 'LED_PWR',		sag_ext_mnem(hk, 'SXI_FDB_LED')
if (strlowcase(strtrim(fxpar(h, 'LED_PWR'),2)) eq 'off') then fxaddpar, h, 'LED_PWR',		sag_ext_mnem(hk, 'SXI_LED')	;see if hard on
if (strlowcase(strtrim(fxpar(h, 'LED_PWR'),2)) eq 'on') then fxaddpar, h, 'SOURCE', 'LED'

case getenv('SXI_INSTRUMENT') of
	'EM_A': begin
			fxaddpar, h, 'TELESCOP', 	'SXI EM Instrument'
			fxaddpar, h, 'CCD_TYPE', 	'CCD64-BI-IE-20'
			fxaddpar, h, 'OBS_ID',		'8382-12-11'
			fxaddpar, h, 'CAM_TYPE',	'SXI EM'
		end
	'EM_B': begin
			fxaddpar, h, 'TELESCOP', 	'SXI EM Instrument'
			fxaddpar, h, 'CCD_TYPE', 	'CCD64-BI-IE-20'
			fxaddpar, h, 'OBS_ID',		'8382-7-23'
			fxaddpar, h, 'CAM_TYPE',	'SXI EM'
		end
	'FM1_TEMP1': begin
			fxaddpar, h, 'TELESCOP', 	'SXI FM1 Instrument'
			fxaddpar, h, 'CCD_TYPE', 	'CCD64-BI-IE-20'
			fxaddpar, h, 'OBS_ID',		'8382-06-63', ' LMSAL Dev #3'
			fxaddpar, h, 'CAM_TYPE',	'SXI BB'
		end
	'FM1_A': begin
			fxaddpar, h, 'TELESCOP', 	'SXI FM1 Instrument'
			fxaddpar, h, 'CCD_TYPE', 	'CCD64-BI-IE-20'
			fxaddpar, h, 'OBS_ID',		'8382-03-23', ' LMSAL Dev #2'	;29-Mar-01
			fxaddpar, h, 'CAM_TYPE',	'SXI FM1'
		end
	'FM2_A': begin
			fxaddpar, h, 'TELESCOP', 	'SXI FM2 Instrument'
			fxaddpar, h, 'CCD_TYPE', 	'CCD64-BI-IE-20'
			fxaddpar, h, 'OBS_ID',		'8382-03-24'
			fxaddpar, h, 'CAM_TYPE',	'SXI FM2'
		end
	else: print, 'MK_SXI_EGSE_FITS_HEAD: Warning, $SXI_INSTRUMENT not recognized'
endcase

fxaddpar, h, 'T_MIR', 	sag_ext_mnem(hk, 'SXI_MIRTMP'), 	' MDL Mirror Temp (C)'
fxaddpar, h, 'T_FORE', 	sag_ext_mnem(hk, 'SXI_FORZONETMP'), 	' MDL Fore-zone Temp (C)'
fxaddpar, h, 'T_AFT', 	sag_ext_mnem(hk, 'SXI_AFTZONETMP'), 	' MDL Aft-zone Temp (C)'
fxaddpar, h, 'T_CCD', 	sag_ext_mnem(hk, 'SXI_CCDZONETMP'), 	' MDL CCD-zone Temp (C)'
fxaddpar, h, 'T_PEB', 	sag_ext_mnem(hk, 'SXI_PEBTMP'), 	' MDL Power Electronics Box (PEB) Temp (C)'
fxaddpar, h, 'T_DEB', 	sag_ext_mnem(hk, 'SXI_DEBTMP'), 	' MDL Data Electronics Box (DEB) Temp (C)'

;py_file = '/sxisw/bin/fam_move_current.log'
py_file = getenv('SXI_PY_INFO_FILE')
if (keyword_set(py_file)) then if (file_exist(py_file)) then begin
    pymat = rd_tfile(py_file,3)
    pydattim = file2time( reform(pymat(0,*)), out='int' )
    pyimgtim = ut_time(fxpar(h,'EGSE_TIM'), /to_local)
    ;
    ;;print, '***** TEMP 1 HR FUDGE FACTOR ****
    ;;pyimgtim = fmt_tim(anytim2ints(pyimgtim, off=1*60.*60.))
    ;
    pyss = sel_timrange(pydattim, '1-Jan-80', pyimgtim, /between)
    if (pyss(0) ne -1) then begin
	pyii = max(pyss)
	dt = int2secarr(pydattim(pyii), pyimgtim)/60./60.	;hours
	if (abs(dt) lt 24) then begin
	    fxaddpar, h, 'YAW', float(pymat(1,pyii))/60.
	    fxaddpar, h, 'PITCH', float(pymat(2,pyii))/60.	;arcmin
	end
    end
end
;
fxaddpar, h, 'TPITCH',	 	sag_ext_mnem(hk, 'SXI_TEL_PNT_PITCH')
fxaddpar, h, 'TYAW', 		sag_ext_mnem(hk, 'SXI_TEL_PNT_YAW') 
;
fxaddpar, h, 'CIS_IMG', (fix( bhead.extra, 0, 1))(0)
fxaddpar, h, 'CIS_FILE', string(bhead.extra(2:11) )
fxaddpar, h, 'BINNING', sag_ext_mnem(hk, 'SXI_FDB_BIN_MODE')
fxaddpar, h, 'EXT_COL', sag_ext_mnem(hk, 'SXI_CCD_COL')
fxaddpar, h, 'EXT_LIN', sag_ext_mnem(hk, 'SXI_CCD_LINE')
;prstr, h, /nomore
end
