function ssw_java_version, string=string, full_string=full_string, $
   short=short, nodot=nodot
;
;+  Name: ssw_java_version
;
;   Purpose: return java/jre version #
;
;   Input Parameters:
;      none
;
;   Output:
;      function returns current java version
;
;   Keyword Parameters:
;      string - if set, return version number as string
;      full_string - if set, return verbatim output from spawn command
;      short - if set, only 3 character V.R
;      nodot - if set, only 2 character VR  ; later for auto file naming...
;
;   History:
;      25-feb-2009 - S.L.Freeland - originally to select between jars built w/different versions
;
;-
nodot=keyword_set(nodot)
short=keyword_set(short) or nodot
vcmd=str2arr('java -version',' ')

spawn,vcmd,/noshell, verbatim, /stderr

bstring='(build'
if keyword_set(full_string) then begin 
   retval=verbatim
endif else begin 
   ss=where(strpos(verbatim,bstring) ne -1, bcnt)
   if bcnt eq 0 then begin
      box_message,'Unexpected version string...
      retval=''
   endif else begin 
      retval=strtrim(strextract(verbatim(ss(0)),bstring,')') ,2)
      short=keyword_set(short) or keyword_set(nodot)
      if short then retval=strmid(retval,0,3)
      if nodot then retval=str_replace(retval,'.','')

   endelse

endelse
return,retval
end
