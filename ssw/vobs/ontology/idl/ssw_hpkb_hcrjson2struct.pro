function ssw_hpkb_hcrjson2struct, hcrjson, debug=debug
;+
;   Name: ssw_hpkb_hcrjson2struct
;
;   Purpose: HCR API json output -> SSW IDL structure vector
;
;   Input Paramters:
;      jsonin - json output from HCR API
;
;   Output:
;      function returns IDL structure vector jsonin "equivilent"
;
;   History:
;      8-feb-2010 - S.L.Freeland - rewrite for new (and unannounced!) HCR-API
;     15-feb-2010 - S.L.Freeland - faster algorithm
;     27-oct-2010 - S.L.Freeland - add web_dechunk.pro wrapper (for large/mission requests)
;     25-mar-2014 - S.L.Freeland - create_struct -> create_struct_temp for parallel-execution safe 
;
;-
;

debug=keyword_set(debug)

if not data_chk(hcrjson,/string) then begin 
   box_message,'expect HCR json output
   return,''
endif

jsonin=web_dechunk(hcrjson)
ljsonin=strcompress(strlowcase(jsonin),/remove)

res0=where(strpos(ljsonin,'"results":') ne -1, r0cnt)
rese=where(strpos(ljsonin,']') ne -1 and strpos(ljsonin,'"') eq -1, r1cnt)
valid=jsonin(res0+2:rese-2)

ssp=where(strpos(ljsonin,'":"') ne -1)
valid=temporary(jsonin(ssp))

param=strtrim(ssw_strsplit(valid,':',/head,tail=value),2)
keysearch=strtrim(all_vals(param),2)
keywords=strtrim(ssw_patt_replace(keysearch,'"',''),2)
keywords=keywords(uniq(keywords,sort(keywords)))
ntags=n_elements(keywords)

create_struct_temp,template,'',keywords,replicate('A',ntags)
ss0=where(strpos(param,'"'+keywords(0)+'"') ne -1,nret)

retval=replicate(template,nret)
value=ssw_patt_replace(value,'",','  ')
value=strtrim(ssw_patt_replace(value,'"',' '),2)
for i=0,ntags-1 do begin
   ss=where(strpos(param,keysearch(i)) ne -1, sscnt)
   retval.(i)=value(ss)
endfor

if debug then stop,'retval'
return,retval
end




