function iri_gfits, stfsn, enfsn, qdebug=qdebug
;;
nam='iri_gfits'

if (n_elements(stfsn) eq 0) then stop, nam+': must provide FSN or FSN range.  Cannot continue.'
if (n_elements(enfsn) eq 0) then enfsn=stfsn

nfiles=enfsn-stfsn+1
print, 'Looking for '+strcompress(nfiles,/remove)+' fits files starting at FSN '+strcompress(stfsn,/remove)

qq=lindgen(nfiles)+stfsn
if (keyword_set(qdebug)) then prstr, qq
dd=find_dirs(getenv('IRI_FITS_BASE_DIR'),/follow)
ff=file_list(dd, '*.fits')
break_file, ff, disk, dir, fil, exten
sorted=sort(fil)
ff=ff(sorted)
;if (keyword_set(qdebug)) then prstr, ff
fil=ulong(fil(sorted))

match2, fil, qq, suba, subb

if (keyword_set(qdebug)) then prstr, fil
if (keyword_set(qdebug)) then prstr, suba
tt=where(subb ne -1, ntt)
ff=ff(subb(tt))
if (ntt eq 0) then stop, nam+': No files found.  Cannot continue.'
if (ntt ne nfiles) then print, 'Found '+strcompress(ntt,/remove)+' of '+strcompress(nfiles,/remove)+' files'
print, 'Found '+strcompress(n_elements(ff),/remove)+' unique fits files'

if (keyword_set(qdebug)) then print, fil(subb)
if (keyword_set(qdebug)) then help, ff
if (keyword_set(qdebug)) then help, fil
if (keyword_set(qdebug)) then help, qq
if (keyword_set(qdebug)) then help, subb
if (keyword_set(qdebug)) then help, suba

return, ff
end
