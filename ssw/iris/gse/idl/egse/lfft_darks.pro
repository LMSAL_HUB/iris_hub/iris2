;
;
;
if (n_elements(date) eq 0) then date = ut_time()
if (n_elements(fmax) eq 0) then fmax = 4000
ff = sxi_stol2files(date, 'lfft_darks', 16, out=cmdout)
;
if (ff(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
img1 = rfits(ff(0))
sel_struct = {nx_block: 100, ny_block: 100, $
		x0: 10, x1: data_chk(img1, /xsize)-10, $
		y0: 10, y1: data_chk(img1, /ysize)-10, $
		b0: 3, b1: 6, $
		offset: 'NONE'}
logfil = '/sxisw/dbase/lfft/darks.tab'
outdir = '$SXI_LFFT_DIR/tmp'
fid = str_replace(time2file(cmdout), '.', '_')
;
info = {tit: 'SXI run of LFFT_DARKS ' + fmt_tim(cmdout)}
expos = [.04, 3, 6, 9]
;
!p.multi = 0
npe = 2
nexp = 4
sag_ltc, ff(0:7), nexp, npe, fmax, sel_struct, out=out, vmask=0, info=info
tit = 'LFFT_DARKS for run: ' + fmt_tim(cmdout) + '  South Amp'
sag_plot_avg, tit, expos, out, $
        logfil=logfil, $
        hc=concat_dir(outdir, 'darks_a_'+fid+'.ps'), $
        gif=concat_dir(outdir, 'darks_a_'+fid+'.gif')
disp12, ff(0:7), tit, option = 'STDEV', $
        hc=concat_dir(outdir, 'darks_a_'+fid+'disp12.ps'), $
        gif=concat_dir(outdir, 'darks_a_'+fid+'disp12.gif')


pause
sag_ltc, ff(8:15), nexp, npe, fmax, sel_struct, out=out, vmask=0, info=info
tit = 'LFFT_DARKS for run: ' + fmt_tim(cmdout) + '  North Amp'
sag_plot_avg, tit, expos, out, $
        logfil=logfil, $
        hc=concat_dir(outdir, 'darks_b_'+fid+'.ps'), $
        gif=concat_dir(outdir, 'darks_b_'+fid+'.gif')
disp12, ff(8:15), tit, option = 'STDEV', $
        hc=concat_dir(outdir, 'darks_b_'+fid+'disp12.ps'), $
        gif=concat_dir(outdir, 'darks_b_'+fid+'disp12.gif')


;------------------


end