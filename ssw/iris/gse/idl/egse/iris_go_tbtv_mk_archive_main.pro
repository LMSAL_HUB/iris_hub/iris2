;+
; $Id: iris_go_tbtv_mk_archive_main.pro 7846 2012-06-12 19:27:09Z linford $
; $Name:  $
; main: iris_go_tbtv_mk_archive_main.pro
;
; Purpose: Run tbtv_mk_archive to generate near Real-Time tables of temperatures
;	and thermal rates.  Note: can use getenv('env_var') to check the assigned 
;	values below.  For example: idl> print, getenv('SDO_TBTV_DB_DIR')
;
; History: 
;	Update for IRIS, 3/28/2012 G.Linford
;	Modified 5-Nov-2011, G.Linford for EDU TVAC
;	27-Oct_2007, G.Linford, created
;
; $Log: aia_go_tbtv_mk_archive_main.pro,v $
; Revision 1.3  2008/07/23 21:38:44  linford
; path change for sc tb/tv
;
; Revision 1.2  2008/04/12 17:56:04  linford
; update for SC EMI/EMC with new dir no13
;
; Revision 1.1  2007/10/27 19:08:02  linford
; mod from hmi version of the same name for TV1,3
;
;
;
;-

; -- Setup directories for RT run:
set_logenv,'SDO_TBTV_DB_DIR','/irisgse/env_test/tb_tv'					; input tables
set_logenv,'SDO_TBTV_HTML_DIR','/irisgse/env_test/tb_tv/html'		; output dirs
set_logenv,'SDO_REMOVE_HTML_DIRS','/irisgse/env_test/tb_tv/html'     ; all links relative to /..
; want: '/iris/irisdata/tvac/caseRT'
set_logenv,'GSE_PKT_FILE_DIR','/sswpkts/'
;set_logenv,'GSE_BAD_PKT_FILE_DIR','/net/irisiedu2a/disk1/egsesw/log/packets'
;set_logenv,'GSE_aia_SSIM_TM_PKT_FILE_DIR','/net/aiafm1a/disk2/log/packets'

tbtv_mk_archive,/testRT,/instr_only

end
