pro iri_disp_isp, tlmfile, ispout=ispout, raw=raw, outfil=outfil, hc=hc, quiet=quiet, tlmin=tlmin

common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd, cond

nam='iri_disp_isp'

if (n_elements(tlm) eq 0) then stat = execute(getenv('SAG_EGSE_DB_RD_PRO'))
if (n_elements(tlm) eq 0) then begin
	print, nam+': read problem with HK database.  Cannot continue.'
	stop
end
dat=iri_rd_tlm(tlmfile, files=tlmfile)
tlmin=tlmfile

isp=where(tlm.apid eq 56, nisp)
if (nisp eq -1 ) then begin
	print, nam+': could not find any mnemonics in packet with apid=56'
	return
endif
isptlm=tlm(isp)
fisp=where(dat.isp.pkt_head.apid eq 56, nfisp)
if (nfisp eq -1 ) then begin
	print, nam+': no ISP packets found'
	return
endif
ncadu=n_elements(dat)
ispout=strarr(n_elements(isptlm))
dat=dat(fisp)
nkeys=n_elements(isptlm)
for i=0,nkeys-1 do begin
	val=sag_ext_mnem(dat.isp.data,isptlm(i).name, raw=raw)
	case isptlm(i).conv_code of
		'': begin
			fc='(A-30,'+strcompress(nfisp,/rem)+'I-14)'
			if (isptlm(i).type eq 'unsigned') then val=ulong(val)
		end
		'A': fc='(A-30,'+strcompress(nfisp,/rem)+'F-14.3)'
		'Y': fc='(A-30,'+strcompress(nfisp,/rem)+'F-14.3)'
		'D': fc='(A-30,'+strcompress(nfisp,/rem)+'A-14)'
	endcase
	if (keyword_set(raw)) then fc='(A-30,'+strcompress(nfisp,/rem)+'I-14)'
	ispout(i)=string(isptlm(i).name, val, format=fc)
endfor
ispout=ispout(sort(ispout))
if (not keyword_set(quiet)) then begin
	prstr, ispout, /nomore, file=outfil, hc=hc
	print, string(nam, ': found ', nfisp, ' ISPs out of ', ncadu, ' CADUs', format="(2A,I-3,A, I-7, A)")
endif
end
