pro IRIS_PREP_PRINT_GEOWAVE_SUMMARY, geowave_dat, old_geowave_dat, $
	pstop = pstop

;+
;
; Given a geometrical correction structure, prints out a summary table describing
; the X and Y shift of a few key points
;
;-

xs = [520., 659.3, 2072./2., 1843.43]
ys = [-269.5, 0, 269.5] + 508.

nx = N_ELEMENTS(xs)
ny = N_ELEMENTS(ys)
xx = REBIN(xs, nx, ny)
yy = REBIN(TRANSPOSE(ys), nx, ny)

xo = INTERPOLATE(geowave_dat.dmapx, xx, yy)
yo = INTERPOLATE(geowave_dat.dmapy, xx, yy)

ymean = MEAN(yo, dim = 1)
ydev = STDDEV(yo, dim = 1)
ytilt = LIMITS(yo[3,*] - yo[0,*])
yscale = LIMITS(yo[*,2] - yo[*,0])

xmean = MEAN(xo, dim = 2)
xdev = STDDEV(xo, dim = 2)
xtilt = LIMITS(xo[*,2] - xo[*,0])
xscale = LIMITS(xo[3,*] - xo[0,*])

tag1 = geowave_dat.img_path
taglen = STRTRIM(STRLEN(tag1)+2, 2)
if N_TAGS(old_geowave_dat) gt 0 then begin
	; Print out a comparison table
	oxo = INTERPOLATE(old_geowave_dat.dmapx, xx, yy)
	oyo = INTERPOLATE(old_geowave_dat.dmapy, xx, yy)

	oymean = MEAN(oyo, dim = 1)
	oydev = STDDEV(oyo, dim = 1)
	oytilt = LIMITS(oyo[3,*] - oyo[0,*])
	oyscale = LIMITS(oyo[*,2] - oyo[*,0])

	oxmean = MEAN(oxo, dim = 2)
	oxdev = STDDEV(oxo, dim = 2)
	oxtilt = LIMITS(oxo[*,2] - oxo[*,0])
	oxscale = LIMITS(oxo[3,*] - oxo[0,*])
	tag2 = old_geowave_dat.img_path
	otaglen = STRTRIM(STRLEN(tag2)+2, 2)
	PRINT, '', '=======', tag1, '=======', '', 'NEW', 'OLD', $
		form = '(a7, a7, a' + taglen + ', a9, a2, a15, a20)'
	PRINT, 'MIN', 'MAX', 'MIN', 'MAX', form = '(a'+STRTRIM(taglen+35,2)+',3a10)'
	PRINT, 'X Axis:', form = '(a15)'
	PRINT, 'X axis scaling : ', xscale, oxscale, form = '(a30,4f10.2)'
	PRINT, 'X axis shift : ', LIMITS(xmean - xs), LIMITS(oxmean - xs), form = '(a30, 4f10.2)'
	PRINT, 'X axis tilt : ', xtilt, oxtilt, form = '(a30,4f10.2)'
	PRINT, 'Y Axis:', form = '(a15)'
	PRINT, 'Y axis scaling : ', yscale, oyscale, form = '(a30,4f10.2)'
	PRINT, 'Y axis shift : ', LIMITS(ymean - ys), LIMITS(oymean - ys), form = '(a30, 4f10.2)'
	PRINT, 'Y axis tilt : ', ytilt, oytilt, form = '(a30,4f10.2)'
endif else begin
	; Just print out a summary
	PRINT, '', '=======', tag1, '=======', '', 'MIN', 'MAX', $
		form = '(a7, a7, a' + taglen + ', a9, a2, 2a10)'
	PRINT, 'X Axis:', form = '(a15)'
	PRINT, 'X axis scaling : ', xscale, form = '(a30,2f10.2)'
	PRINT, 'X axis shift : ', LIMITS(xmean - xs), form = '(a30, 2f10.2)'
	PRINT, 'X axis tilt : ', xtilt, form = '(a30,2f10.2)'
	PRINT, 'Y Axis:', form = '(a15)'
	PRINT, 'Y axis scaling : ', yscale, form = '(a30,2f10.2)'
	PRINT, 'Y axis shift : ', LIMITS(ymean - ys), form = '(a30, 2f10.2)'
	PRINT, 'Y axis tilt : ', ytilt, form = '(a30,2f10.2)'
endelse

if KEYWORD_SET(pstop) then STOP
; Also check whether the kx/ky match the dmapx/dmapy

end