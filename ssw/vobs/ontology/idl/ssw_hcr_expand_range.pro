function ssw_hcr_expand_range, thing, valid_params=valid_params , query=query
;
;+ 
;   Name: ssw_hcr_expand_range
;
;   Purpose: convert <param=low~high> to HCR-ready query 
;
;   Input Parameters:
;      thing - one string of format (ala struct where) PARAM=LOW~HIGH
;
;   Output: 
;      function returns derived range bits [<min_param>=<MAX>, <max_param=<MAX> ] -or- verbatim query string (scalar) if /QUERY set
;
;   Keyword Parameters:
;      valid_params - vector of actual parameter names for matching/table lookup (defult is verbatim MIN_<thing> & MAX_<thing>
;
;   History:
;      12-oct-2014 - S.L.Freeland - HCR query helper (may have future query/client apps since not HCR specific)

if not data_chk(thing,/string) then begin 
   box_message,'Expect range string like PARAM=LOW~HIGH
   return,''
endif

stringx=strcompress(thing[0],/remove)

rdelim=['~',',']
tcnt=0
d=-1
while (tcnt eq 0) and d lt n_elements(rdelim)-1 do begin
   d=d+1
   delim=rdelim[d]
   tss=where(strmatch(stringx,'*=*' + rdelim[d] + '*'),tcnt)
endwhile

if tcnt eq 0 then begin 
   box_message,'Expect range string like PARAM=LOW~HIGH or PARAM=LOW,HIGH
   return,''
endif

paramx=ssw_strsplit(stringx,'=',tail=valuex)
minx=ssw_strsplit(valuex,rdelim[d],/head,tail=maxx)

smin='min_'+paramx
smax='max_'+paramx

if data_chk(valid_params,/string) then begin ; do param name lookup
   plow=strlowcase(paramx)
   vplow=strtrim(strlowcase(valid_params),2)
   ssmin=where(strpos(vplow,'min') ne -1 and strpos(vplow,plow) ne -1,mincnt)
   ssmax=where(strpos(vplow,'max') ne -1 and strpos(vplow,plow) ne -1,maxcnt)
   case 1 of 
      mincnt eq 1 and maxcnt eq 1: begin
         smin=vplow[ssmin]
         smax=vplow[ssmax]
      endcase
      mincnt eq 0 or maxcnt eq 0 : box_message,'No VALID_PARMS match for ' + plow
      minct gt 1 or maxcnt gt 1: box_message,'Multiple matches for param= ' + plow
      else: box_message,'param/valid_params mismatch of some kind or other...
   endcase
endif
   
retval=[smin + '=' + minx, smax + '=' + maxx]
if keyword_set(query) then retval='&' + arr2str(retval,'&')
return,retval   

stop,'paramx,minx,maxx
return,retval
end
