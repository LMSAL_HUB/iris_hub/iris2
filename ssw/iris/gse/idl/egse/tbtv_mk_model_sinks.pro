;bdir = ['/sxi01/html/TandC/DB/589', '/sxi01/html/TandC/DB/590']
;qcheck = 1
bdir = '/sxi01/html/TandC/DB/910
bdir = '/sxi01/html/TandC/DB/911
qcheck = 0
;
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_200'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_201'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_202'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_203'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_204'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_205'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_206'
;outdir = '/sxi01/html/Inst/Thermal/TB_FM1/rev_207'
outdir = '/sxi01/data/thermal/FM2_TB/rev_300'
;
compdir = '/sxi01/html/Inst/Thermal/TB_FM1
;
log = ['tbtv_mk_model_sinks  Ver 2.1  Program run: ' + !stime, $
	'Input Directory: ' + outdir, $
	'Comparison Directory: ' + compdir, $
	' ']
;
;
for ibdir=0,n_elements(bdir)-1 do begin
  case ibdir of
;	0: infil3 = concat_dir(outdir, 'sinks_form1')
;	1: infil3 = concat_dir(outdir, 'sinks_form2)
;;	0: infil3 = concat_dir(outdir, 'sinks_form3')
	0: begin
		xxx = file_list(outdir, '*form*', file=xxfile)
		ss = xmenu_sel(xxx, /one)
		if (ss eq -1) then stop
		infil3 = xxx(ss)
		infilnam3 = xxfile(ss)
	   end
  endcase
  mat = rd_ulin_col(infil3, nocomment='#', /nohead)

  dirs = file_list(bdir(ibdir), 'case*', /cd, file=file)
  for idir=0,n_elements(dirs)-1 do begin
    print, 'Running case: ' + file(idir)
    log = [log, ' ']
    ;
    filnam = file(idir) + '.inc'
    filnam = str_replace(filnam, '_', '')		;remove underscore
    filnam = str_replace(filnam, 'ctb', 'Ctb')		;name fix
    filnam = str_replace(filnam, 'c6', 'C6')		;name fix
    filnam = str_replace(filnam, 'ctb2', 'Ctb2')	;name fix
    filnam = str_replace(filnam, 'h6', 'H6')		;name fix
    filnam = str_replace(filnam, 'htb', 'Htb')		;name fix
    filnam = str_replace(filnam, 'htb2', 'Htb2')	;name fix
    filnam = str_replace(filnam, 'h3tb', 'Htb3')	;name fix
    filnam = str_replace(filnam, 'h7tb', 'Htb7')	;name fix
    for i=1,13 do filnam = str_replace(filnam, strtrim(i,2)+'T', string(i,format='(i2.2)'))	;name fix

    ;;filnam = 'sinks_' + filnam
    filnam = strmid(infilnam3, 0, strpos(infilnam3, 'form')) + filnam
    outfil = concat_dir(outdir, filnam)
    compfil = concat_dir(compdir, filnam)	;compare filename
    ;
    dir = dirs(idir)
    ff = file_list(dir, 'real_time*.txt')
    if (ff(0) eq '') then nfil = 0 else nfil = n_elements(ff)
    out0 = reform(mat(0,*))
    tt = strmid(out0, 0, 1)
    pp = where( (tt ne 'C') and (tt ne 'H') )
    out0(pp) = '        ' + out0(pp)
    qcare = 0
    for ifil=0,nfil-1 do begin
	print, 'Reading: ' + ff(ifil)
	mat0 = rd_tfile(ff(ifil), 5)
	v0 = strlowcase(reform(mat0(0,*)))
	v1 = reform(mat0(1,*))
	for ilin=0,n_elements(mat(0,*))-1 do begin
	    snode = mat(2,ilin)
	    if (snode ne '') then begin
	        inode = fix(snode)
		snode2 = strtrim(abs(inode),2)
		mnems = strlowcase(strtrim(str2arr(mat(1,ilin)),2))
		;print, mnems
		v = -9999
		nn = 0
		for i=0,n_elements(mnems)-1 do begin
		    ss = where_arr( v0, mnems(i), nss)
		    if (nss ge 1) then if (abs(float(v1(ss(nss-1)))) gt 1000) then nss = 0	;open TC
		    if (nss eq 0) then begin
			;stop, 'Cant find: ' + mnems(i)
		    end else begin
			;v = v + v1(ss(0))
			;v00 = float(v1(ss(0)))
			v00 = float(v1(ss(nss-1)))
			if (nn eq 0) then v = v00 $
				else v = [v, v00]
			nn = nn + 1
		    end
		end
		if (nn ge 1) then begin
		    if (max(v)-min(v) gt 5) then begin
			print, filnam, '  ', mnems
			print, v
			print, 'Average = ', total(v)/nn
			print, 'Big delta ', max(v) - min(v)
			;if (max(v)-min(v) gt 24) then stop
			;pause
		    end
		    v = total(v)/nn
		    vs = string(v, format='(f8.2)')
		    out0(ilin) = str_replace(out0(ilin), 'xxxxx', vs)
		end
		;
		infil2 = compfil
		if (keyword_set(qcheck)) and (nn ge 1) and (file_exist(infil2)) then begin
		    spawn, ['grep', snode2, infil2], r, /noshell
		    ;;if (n_elements(r) ne 1) then stop, 'spawn resulted in more than 1 hit
		    ;;arr = str2arr(r(0))
		    arr = str2arr(r(n_elements(r)-1))
		    if (strpos(arr(0), 'GEN') eq -1) then v_sink = float(arr(1)) $
							else v_sink = float(arr(3))
		    extra = '       '
		    if abs(v-v_sink) gt 5 then extra = ' <<<<  '
		    ;print, filnam, inode, string(v, v_sink, v-v_sink, format='(3f12.2)'), extra, mat(1,ilin)
		    log = [log, string(filnam, inode, v, v_sink, v-v_sink, extra, mat(1,ilin), format='(a, i8, 3f12.2, 2a)')]
		    ;if abs(v-v_sink) gt 5 then stop, 'Large delta
		    qcare = 1
		end
	    end
	end
    end


    out = ['C       tbtv_mk_model_sinks.pro  Ver 1.0  Program run ' + !stime, ' ', out0, $
	'C       -----------------------------------------------------', $
	'C         ' + string(reform(mat(0,*)) + '                                ', format='(a40)') + $
		 	reform(mat(1,*))]
    prstr, out, file=outfil
    if (max(strpos(out0, 'xxxxx')) ne -1) and (qcare) then stop, 'Couldnt find a mnemonic
  end
end
;
prstr, log, file=concat_dir(outdir, 'runlog')
end
