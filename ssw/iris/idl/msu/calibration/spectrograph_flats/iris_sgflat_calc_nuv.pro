;+
; NAME:
;       iris_sgflat_calc_nuv()
;
; PURPOSE:
;       For making the NUV spectral flat field.
;       Takes the temporal average of the data produced by
;       iris_sgflat_prep and does the forward geometry tranformation.
;       The average spatial and spectral patterns are found and
;       transformed with the reverse transformation and applied to the
;       original image to produce the flat field.
;
; CALLING SEQUENCE:
;       result=iris_sgflat_calc_nuv(filename=filename, [/pickfile,
;                                   datadir=, /geofile])
;
; INPUTS:
;       filename - an IDL save file with the Level 1.5 data in the form
;                  of index, data
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;       geofile - user can supply the string path and filename of an
;                 IDL save file produced by iris_spec_cal, or if set
;                 as a keyword the user can interactively select the file
;
; OUTPUTS:
;       result - filename of the FITS file containing the results
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result1=iris_sgflat_getdata(time_start, time_end, index, data, $
;     >                             datadir=datadir, /nosave, /nuv)
;     > result2=iris_sgflat_prep(index, data, datadir=datadir)
;     > result3=iris_sgflat_calc_nuv(filename=result2, datadir=datadir, $
;                                    /geofile)
;
; MODIFICATION HISTORY:
;       Started 2014-June-30 by Sarah A. Jaeggli, Montana State University
;-

function iris_sgflat_calc_nuv, index, data, filename=filename, $
                               datadir=datadir, geofile=geofile, $
                               pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir='' 

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*NUV_l1p5.sav', $
                              title='Please select a L1.5 file to process')

  if keyword_set(filename) then restore, filename

  if typename(geofile) eq 'INT' then $
     geofile=dialog_pickfile(path=datadir, filter='*_spec_cal.sav', $
                              title='Please select NUV geometry file')

  if index[0].img_path ne 'NUV' then begin
     print, 'Data provided is not NUV, exiting...'
     return, 0
  endif

  ;set some things...
  rotidx=5

  x0=0  & x1=2071
  dx0=2072 & dx1=4143

  xmask0=34 & xmask1=2045
  ymask0=20 & ymask1=1075


  ;produce a mean image from datacube
  flat=median(data, dimension=3)

  nx=(size(data))[1]
  ny=(size(data))[2]

  ;set up buffer for fiducial coordinates
  fx_coord=dblarr(2)
  fy_coord=dblarr(2)

  ;restore ccd flat
  ccd_flat_file = concat_dir(concat_dir('$SSW_IRIS','data'), $
                             'NUV_flat.fits')
  fxread, ccd_flat_file, ccd_flat
  ccd_flat=rotate(ccd_flat, rotidx)


  ;restore geometry data
  restore, geofile ;geo_solution

  ;calculate forward destortion map
  xo=rebin(dindgen(nx, 1), nx, ny)
  yo=rebin(dindgen(1, ny), nx, ny)

  xi_forward=replicate(0.D,nx,ny)
  yi_forward=replicate(0.D,nx,ny)

  for i=0,2 do begin
     for j=0,2 do begin
        xi_forward+=geo_solution.kx[i,j] * xo^j * yo^i
        yi_forward+=geo_solution.ky[i,j] * xo^j * yo^i
     endfor
  endfor

  ;calculate reverse destortion map
  xi_reverse=replicate(0.D,nx,ny)
  yi_reverse=replicate(0.D,nx,ny)

  for i=0,2 do begin
     for j=0,2 do begin
        xi_reverse+=geo_solution.kxi[i,j] * xo^j * yo^i
        yi_reverse+=geo_solution.kyi[i,j] * xo^j * yo^i
     endfor
  endfor


  ;mask overscan/bad pixels in data
  nanmask=replicate(!values.f_nan, nx, ny)
  nanmask[xmask0:xmask1,ymask0:ymask1]=1.

  spat_mask=replicate(!values.f_nan, nx, ny)
  spat_mask[xmask0:xmask1,*]=1.

  flat1=flat/ccd_flat*nanmask
  fill_missing, flat1, !values.f_nan, 1, /extrapolate
  fill_missing, flat1, !values.f_nan, 2, /extrapolate


  ;upsample the image
  scl=2D
  flat2=iris_sgflat_fftrebin(flat1, scl)

  ;upsample the nanmask
  bigmask=rebin(nanmask, nx*scl, ny*scl)
  big_spat_mask=rebin(spat_mask, nx*scl, ny*scl)

  ;scale the geometry results
  xi_forward=rebin(xi_forward, nx*scl, ny*scl)*scl
  yi_forward=rebin(yi_forward, nx*scl, ny*scl)*scl
  xi_reverse=rebin(xi_reverse, nx*scl, ny*scl)*scl
  yi_reverse=rebin(yi_reverse, nx*scl, ny*scl)*scl


  ;transform image to straight coordinates
  flat2=interpolate(flat2, xi_forward, yi_forward, cubic=-0.5)

  bigmask=interpolate(bigmask, xi_forward, yi_forward, cubic=-0.5)
  big_spat_mask=interpolate(big_spat_mask, xi_forward, yi_forward, cubic=-0.5)


  ;Build spatial flat
  ;only consider positions outside of Mg II line core
  big_spat_mask[1072-100:1072+100,*]=!values.f_nan
  big_spat_mask[1636-100:1636+100,*]=!values.f_nan

  spat_flat = mean(flat2*big_spat_mask, dimension=1, /nan)
  spat_profile = rebin(spat_flat, ny)  
  spat_flat /= median(spat_flat)
  
  ;fit and remove fiducials
  spat_flat_rm=spat_flat

  ;estimate the fiducial position
  yarray=dindgen(ny*scl)
  df=60     ;allowed fit range

  for f=0,1 do begin
     a=geo_solution.fiducials.polyfit[f,0:1]
     fpos=scl*(a[0] + a[1] * (nx/4.))

     fit=gaussfit(yarray[fpos-df:fpos+df], $
                  spat_flat[fpos-df:fpos+df], a, nterms=6)

     spat_flat_rm-=a[0]*exp(-0.5*((yarray-a[1])/a[2])^2)

     fx_coord[f]=nx/4.+x0
     fy_coord[f]=a[1]/scl
  endfor

  ;sort coordinates for later
  fx_coord=fx_coord[sort(fy_coord)]
  fy_coord=fy_coord[sort(fy_coord)]

  spat_flat     = rebin(reform(spat_flat,     1, ny*scl), nx*scl, ny*scl)
  spat_flat_rm  = rebin(reform(spat_flat_rm,  1, ny*scl), nx*scl, ny*scl)


  ;Build spectral flat
  spec_profile=rebin(median(flat2/spat_flat*bigmask, dimension=2), nx)

  spec_flat=smooth(flat2/spat_flat*bigmask, $
                   [0,ny*scl/12.], /edge_truncate, /nan)


  ;transform images back to curved coordinates
  spat_flat=interpolate(spat_flat, xi_reverse, yi_reverse, cubic=-0.5)
  spat_flat_rm=interpolate(spat_flat_rm, xi_reverse, yi_reverse, cubic=-0.5)
  
  spec_flat=interpolate(spec_flat, xi_reverse, yi_reverse, cubic=-0.5)


  ;bin images back down to size
  spat_flat=congrid(spat_flat, nx, ny, cubic=-0.5)
  spat_flat_rm=congrid(spat_flat_rm, nx, ny, cubic=-0.5)

  spec_flat=congrid(spec_flat, nx, ny, cubic=-0.5)


  ;divide and multiply to make master flat
  master_flat=flat / spec_flat / spat_flat * nanmask
  master_flat[where(finite(master_flat) ne 1)]=1.0

  master_spat_flat=spat_flat * nanmask
  master_spat_flat[where(finite(master_spat_flat) ne 1)]=1.0

  master_spat_flat_rm=spat_flat_rm * nanmask
  master_spat_flat_rm[where(finite(master_spat_flat_rm) ne 1)]=1.0


  ;display results
  imexam, master_flat*master_spat_flat, 0, 1.2


  ;write files
  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $ ;year
             strmid(dt, 5,2) + $ ;month
             strmid(dt, 8,2) + $ ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_NUV_flat.sav'
  fitsname=datadir+timestring+'_NUV_flat.fits'

  print, 'Saving NUV flat data in '+savename
  save, flat, ccd_flat, master_flat, master_spat_flat, master_spat_flat_rm, $
        fx_coord, fy_coord, spec_profile, spat_profile, filename=savename


  ;build a fits file
  mkhdr, hdr, [[[master_flat]], [[master_spat_flat]], [[master_spat_flat_rm]]]

  spawn, 'echo $USER', creator
  spawn, 'echo $HOSTNAME', host
  sxaddpar, hdr, 'CREATOR', creator[0]+'@'+host[0]

  sxaddpar, hdr, 'FID1_X0', fx_coord[0]
  sxaddpar, hdr, 'FID1_X1', fx_coord[1]
  sxaddpar, hdr, 'FID1_Y0', fy_coord[0]
  sxaddpar, hdr, 'FID1_Y1', fy_coord[1]

  print, 'Saving NUV flats to fits file '+fitsname
  writefits, fitsname, $
             [[[master_flat]], [[master_spat_flat]], [[master_spat_flat_rm]]], $
             hdr


  return, fitsname

end
