pro iris_set_gse, telem, quiet=quiet, slow=slow, save=save, $
   moc=moc, brassboard=brassboard, flight=flight, vc=vc, _extra=_extra, refresh=refresh

;
;+
;   Name: iris_set_gse
;
;   Purpose: setup iris gse within ssw; optionally select vc# (brassboard/flight/moc)
;
;   Input Parameters:
;      NONE:
;
;   Output Parameters:
;      telem - optionally, output entire GSE .TLM (from common)
;
;   Keyword Parameters:
;      /moc, /brassboard, /flight - default is MOC
;      /vcN - (inheritance) desired VC# ; implies /MOC - default=vc1
;      vc=N ; synonym for /VCN
;      /refresh - force redo of paths/tlm load
;      /slow - if set, force re-parse of ITOS(xml) -> gse tlm common  - default does fast load from geny image
;      /quiet - if set, inhibit path & environmental updates (-> ssw_paths & set_logenv
;
;   Calling Context:
;      (assume SSW session w/IRIS already active)
;     IDL> iris_set_gse,/vc1   ; add gse environment, set to MOC vc1 stream
;     IDL> seq=iris_time2seq('11-apr-2013','12-apr-2013',/fsn) ; evenvts -> seq starts -> hk FSN
;
;   History:
;      10-may-2013 - S.L.Freeland - SSW front end utility to setup/toggle IRIS/GSE options 
;

common iris_set_gse_blk, called

refresh=n_elements(called) eq 0 or keyword_set(refresh) or keyword_set(slow)


if refresh then begin ; force first time or on request - this piece is ~site independent, only $SSW_IRIS assumed
   ssw_path,'$SSW_IRIS/gse/idl', quiet=quiet
   hkf=concat_dir('$SSW_IRIS','gse/setup/setup_iris_hk')
   set_logenvx,file=hkf[0],quiet=quiet  ; eventually elevate -> gen/setup_logenv
   pkt=get_logenv('GSE_PKT_FILE_DIR')
   iris_gse_tlm_fast, telem, refresh=keyword_set(slow), save=save
   called=1
endif 

; 
settop=keyword_set(moc) or keyword_set(brassboard) or keyword_set(flight) or keyword_set(vc) or keyword_set(_extra) 

if ~file_exist(pkt) or settop then begin 
   hktop='/archive/iris/hk' ; lmsal specific for today - others, set $GSE_PKT_FILE_DIR prior to running this
   case 1 of 
      keyword_set(flight): pkttop=concat_dir(hktop,'flight/sswpkts')
      keyword_set(brassboard): pkttop=concat_dir(hktop,'brassboard/sswpkts')
      else: begin  ; /MOC or VCN 
         hktop='/archive/iris/moc/hk_archive-in'
         hktop='/irisa/moc/hk_archive-in'
         pkttop=concat_dir(hktop,'packets')
         case 1 of 
            keyword_set(vc): vcn=str2number(vc)
            data_chk(_extra,/struct): begin ; allow /vcN via inherit
               etn=tag_names(_extra)
               vcss=(where(strpos(tag_names(_extra),'VC') eq 0,vcnt))(0)
               if vcnt eq 0 then vcn=1 else vcn=str2number(etn[vcss[0]])
            endcase
            else: vcn=1 ; default is MOC vc1 (may change..)
         endcase
         pkttop=concat_dir(pkttop,'vc'+strtrim(vcn,2))
      endcase   
   endcase
   set_logenv,'GSE_PKT_FILE_DIR',pkttop
endif
return
end

