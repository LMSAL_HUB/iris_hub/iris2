pro ssw_coverage_ingest, paths, month=month, refresh=refresh, $
   debug=debug, $
   expire=expire , test_event=test_event, no_expire=no_expire, post=post, add_link=add_link
;+
;   Name: ssw_coverage_ingest
;
;   Purpose: submit one or more coverage VOEvents -> knowledge base
;
;   History:
;      Circa August 1 2008 - S.L.Freeland
;      25-feb-2009 - S.L.Freeland - add java version check/jar lookup
;      28-jan-2010 - S.L.Freeland - add /EXPIRE & /TEST & /NO_EXPIRE & POST
;-
add_link=1
debug=keyword_set(debug)

postit=keyword_set(post)
test=keyword_set(test_event) or postit

jarroot=concat_dir(concat_dir('$SSW_ONTOLOGY','java'),'coverage_ingest') 
jversion=jarroot + '_'+ ssw_java_version(/nodot) +'.jar'
jarfile=([jarroot+'.jar',jversion])(file_exist(jversion))
expjar=str_replace(jarroot,'coverage_ingest','deleteexpiredevent.jar')

jar=jarfile

if test then begin 
   template=jarroot+'_test_template.xml'   ; special TEST event template
   tdat=rd_tfile(template)
   sstest=where(strpos(tdat,'TEST_TIME') ne -1 ,tcnt)
   test_time=reltime(minutes=-1440,out='ccsds')
   tdat(sstest)=str_replace(tdat(sstest),'TEST_TIME',test_time)
   voename='VOEvent#VOEvent_ObsT'+test_time+'.xml'
   voepath=concat_dir(get_temp_dir(),voename)
   file_append,voepath,tdat,/new 
   if get_logenv('hpkb_check') ne '' then stop,'voepath'
   paths=voepath
   expire=1-keyword_set(no_expire) ; default for test is expire
   help,expire
endif

if not file_exist(paths(0)) then begin 
   box_message,'Need paths to existing VOE stuff
   return
endif

if not file_exist(jar) then begin 
   box_message,'Cannot find ingest jar file...'
   return
endif else box_message,jar

if keyword_set(month) then begin 


endif else ipaths=paths
chk=get_logenv('ssw_hcr') 
auth=([' ',' -username hcr_autosubmit -password '+chk+' '])(chk ne '')

cmds='java -jar ' + jar + auth + ipaths
nc=n_elements(cmds)
allout=''
add_link=keyword_set(add_link)
for i=0,nc-1 do begin 
   if add_link then begin
      box_message,'Adding get_data/catalog link to VOE...'
      dat=ssw_hpkb_hcr_voe2link(ipaths(i),/add_link)
if debug then stop,'dat'
   endif
   box_message,cmds(i)
   spawn,cmds(i),out,err
   box_message,['stdout',out,'stderr',err]
   allout=[allout,out]
endfor
authss=where(strpos(allout,'failed') ne -1,  acnt)
authstatus=acnt eq 0 

if keyword_set(expire) then begin 
   break_file,ipaths(0),ll,pp,eventid
   evt=(['','ivo://sot.lmsal.com/'])(strpos(eventid(0),'ivo') eq -1) + eventid(0) +'.000.xml'
   xml=ssw_hcr_query(event=evt(0),/xml_only)
   ss=where(strpos(xml,evt) ne -1,cnt)
   hcrstatus=cnt gt 0
   expstatus=1
   if get_logenv('check_hpkb') ne '' then stop,'expstatus'
   if hcrstatus then begin 
      expcmd = 'java -jar ' + expjar + ' ' + evt(0) 
      spawn,expcmd ; expire the event 
      xml=ssw_hpkb_hcr_query(event=evt(0),/xml_only)
      ss=where(strpos(xml,evt) ne -1,cnt)
      expstatus=cnt eq 0
      if get_logenv('hpkb_check') ne '' then stop,'expstatus'
   endif
   astatus=authstatus and hcrstatus and expstatus
   status=(['Failed','Running'])(authstatus and hcrstatus)
   status=([status,'Warning'])(expstatus eq 0)
   smess=(['Problem: ','All OK'])(astatus) + (['Login Failed',''])(authstatus) + $
      (['Upload/Readback Error',''])(hcrstatus) + (['Event Expiration Failed',''])(expstatus)
   box_message,[status,smess]
   if postit then begin 
      query=ssw_make_query( $
                        PROCESS_ORIG="Cov Event Create SSW - SF", $
                        CADENCE='10m', $
                        MESSAGE=smess, $
                        SUBJECT="SSW HCR Watchdog", $
                        STATUS=status, $
                        CONTACT="freeland@lmsal.com", $
                        BACKUP="somani", $
                        TIME_DATE=reltime(/now,out='ccsds'))
      cgiserv='http://www.lmsal.com/cgi-ssw/ssw_service_hpkb_status.sh?'
      sock_list,cgiserv + query ; POST status message
   endif

endif

return
end
