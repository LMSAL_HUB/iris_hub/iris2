function sxi_disp_events_s1, apid, date=date, recs=recs, lastnrec=lastnrec, nohass=nohass
;
;
;
if (n_elements(apid) eq 0) then apid = '0x0021'
;
eepoch = getenv('SAG_EGSE_EPOCH')

    if (n_elements(date) eq 0) then date = ut_time()
    out0 = 'No events for APID: ' + apid + ' for date: ' + fmt_tim(date)
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/' + apid + '/' + fid + '.' + apid
    head = sag_rd_hk(infil, 0, 10000, /map)

    if (data_type(head) ne 8) then begin
        print, 'No data in input file: ' + infil
        return, out0
    end
    n = n_elements(head)
    ist = 0
    if (keyword_set(lastnrec)) then ist = (n-lastnrec)>0
    recs = sag_rd_hk(infil, ist, n-1)
    head = sag_rd_hk(infil, ist, n-1, /map)
    head = head(ist:n-1)
    ;
    if (keyword_set(nohass)) then begin
	ss = where( recs.data(1) ne '4a'x, nss)
	if (nss ne 0) then begin
	    print, 'Removing ', n_elements(recs)-nss, ' HASS records
	    recs = recs(ss)
	    head = head(ss)
	end
    end
;
if (data_type(recs) ne 8) then begin
    out0 = 'No events for APID: ' + apid + ' for date: ' + fmt_tim(date)
    return, out0
end
;
edattim = sxi_time_conv('EGSE', head.egse_time)
dattim = sxi_time_conv('INSTRUMENT', struct=recs.pkt_head, /string, /msec)
;out0 = dattim + fstring(recs.data, format='(12(1x,z2.2))')
;^^ doesn't work properly for some reason.
out0 = dattim
for i=0,n_elements(out0)-1 do begin
    v1 = sag_ext_mnem(recs(i).data, 'SXI_EVENT20_TASK')
    v2 = sag_ext_mnem(recs(i).data, 'SXI_EVENT20_TYPE')
    v3 = sag_ext_mnem(recs(i).data, 'SXI_EVENT21_ERROR')
    out0(i) = fmt_tim(edattim(i)) + string(recs(i).data, format='(12(1x,z2.2))') + $
			string(v1, v2, v3, format='(2a8, a12)') + ' ' + dattim(i)
end

return, out0
end
;-------------------------------------------------------------
pro sxi_disp_events, outfil=outfil, hc=hc, date=date,  $
	computer=computer, sequence=sequence, lastnrec=lastnrec, $
	nohass=nohass
;
;HISTORY:
;V1.0	Written Oct-00 by M.Morrison
;V1.01	18-Oct-00 (MDM) - Added event task/type mnemonics
;V1.02	 1-Nov-00 (MDM) - Added "lastnrec" keyword
;V1.03	20-Aug-02 (MDM) - Added nohass
;V1.04	13-Mar-03 (BLG) - Added SXI_EVENT21_ERROR mnemonics
;
;
apids = ['0x0021', '0x0020']
if (keyword_set(computer)) then apids = '0x0021'	;only computer events
if (keyword_set(sequence)) then apids = '0x0020'	;only sequence
;
out = ['SXI_DISP_EVENTS  Ver 1.04  Program run: ' + !stime, ' ']
;
for i=0,n_elements(apids)-1 do begin
    out0 = sxi_disp_events_s1(apids(i), date=date, lastnrec=lastnrec, nohass=nohass)
    out = [out, ' ', out0]
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil)
end
