;
;+
;NAME:
;	sag_xhkplot
;PURPOSE:
;	To plot the SAG/LM HK data using a widget interface to
;	specify which mnemonics and how to plot it
;SAMPLE CALLING SEQUENCE:
;	sag_xhkplot
;	sag_xhkplot, '5-jan-95', '5-jan-95 3:00', mnem
;HISTORY:
;	Created 23-Sep-99 by M.Morrison
;	 8-Dec-99 (MDM) - Modified to call SAG_HKPLOT_INFO (not HKPLOT_INFO)
;	10-May-00 (MDM) - Put fix in for timerange and /local option
;	22-May-00 (MDM) - Added /RAW option
;-
;--------------------------------------------------------------------
pro sag_xhkplot_event, event
;
;
;
;TODO - need to read the time text field even if <CR> is not hit
;
common sag_xhkplot_blk1, hk, all, last_all, raw
common utcommon, utbase, utstart, utend, xst
;
qdebug = 1
qdebug = keyword_set(qdebug)
if (qdebug) then print, event_name(event)
;if (qdebug) then help, /st, event
widget_control,event.top,get_uvalue=all
widget_control, all.id_info, set_value='  '
;
qhk = data_type(hk) eq 8	;hk data has been read in
qmnem = all.mnem ne ''
qtime = (strtrim(arr2str(all.text_vals(0:1),delim=''),2) ne '') and $
	(strtrim(arr2str(all.text_vals(2:3),delim=''),2) ne '')
qplot = 0
save_dev = !d.name

case (strtrim(event_name(event),2)) of
   "BUTTON":begin                               ; option selection
      case strupcase(get_wvalue(event.id)) of
	 ;------ Main buttons
         "QUIT": begin
		widget_control, event.top,/destroy
	    endcase
	 "CLEARPLOT": begin
		clearplot
		clear_utplot
	    endcase
	 "SET PRINTER": set_printer
         "XLOADCT": xloadct, group=event.top
	 "DELETE LAST": if !d.window ne -1 then wdelete
	 "HARDCOPY": begin
		set_plot, 'ps'
		qplot = 1
            endcase
	 "READ DBASE": begin
		if (qtime and qmnem) then begin
			sttim = arr2str(all.text_vals(0:1), delim=' ')
			entim = arr2str(all.text_vals(2:3), delim=' ')
			mnem = str2arr(all.mnem)
			every = fix(all.text_vals(8)) > 1

			widget_control, all.id_info, set_value='*** Reading Database *** '
			mapx, event.top,/map,/show,sensitive=0
			hk = sag_get_mnem(sttim, entim, mnem, qdebug=qdebug, every=every, /nostring, raw=raw)
			mapx, event.top,/map,/show,sensitive=1
			widget_control, all.id_info, set_value=' '

			all.text_vals(4:7) = all.text_vals(0:3)		;copy RD time/date range into PLOT range
			for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)

			all.yrange_mnem = arr2str(fltarr(2,n_elements(mnem)))
		end else begin
			tbeep, 3
			out = ' '
			if (not qtime) then out = [out, 'You must enter the start/end time first']
			if (not qmnem) then out = [out, 'You must enter the mnemonics first']
			widget_control, all.id_info, set_value=out
		end
	     endcase
	  "PLOT ALL": begin
		qplot = 1
		all.plot_option = 0
	     endcase
	  "PLOT SEPARATELY": begin
		qplot = 1
		all.plot_option = 1
	     endcase
	  "PRINT VALUES": begin
		if (!d.window ne -1) then begin
			instruct = ['Mark on the plot the time(s) desired', $
					'Click with the middle or right button to specify a single time', $
					'Click with the left button if you wish to specify a time range', $
					'Click around starting point']
			widget_control, all.id_info, set_value=instruct
			wshow & cursor, x0, y0, /data, /down
			button = !err
			times = fmt_tim(  anytim2ints(xst, off=x0) )
			if (button eq 1) then begin
			    instruct = 'Click around the ending point'
			    widget_control, all.id_info, set_value=instruct
			    wshow & cursor, x0, y0, /data, /down
			    times = [times, fmt_tim(  anytim2ints(xst, off=x0) )]
			    qlist = 0
			end else begin
			    qlist = 1
			end

			;;mapx, event.top,/map,/show,sensitive=0
			widget_control, all.id_info, set_value='Extracting info - Stand BY'
			;;out = hkplot_info(hk, times = times)
			out = sag_hkplot_info(hk, times = times, list=qlist)
			;;mapx, event.top,/map,/show,sensitive=1
			widget_control, all.id_info, set_value='Done...'
			print, out
			;
			outfil = concat_dir(getenv('HOME'), 'sag_xhkplot_print.' + ex2fid(anytim2ex(!stime)))
			if (all.print_file ne '') then outfil = all.print_file
			all.print_file = outfil
			file_append, outfil, [out, ' ', ' ', ' ']
			widget_control, all.id_info, set_value='Output written to ' + outfil
		end
	      endcase
	  "SAVE INPUTS": begin
			out = ['# XHKPLOT inputs saved ' + !stime, $
				'STTIM_RD:   ' + arr2str(all.text_vals(0:1), delim=' '), $
				'ENTIM_RD:   ' + arr2str(all.text_vals(2:3), delim=' '), $
				'STTIM_PL:   ' + arr2str(all.text_vals(4:5), delim=' '), $
				'ENTIM_PL:   ' + arr2str(all.text_vals(6:7), delim=' '), $
				'EVERY:      ' + all.text_vals(8), $
				'MNEM:       ' + all.mnem, $
				'YRANGE_ALL: ' + arr2str(all.yrange_all), $
				'YRANGE_MNEM: ' + all.yrange_mnem]
			outfil = concat_dir(getenv('HOME'), 'sag_xhkplot_inputs.' + ex2fid(anytim2ex(!stime)))
			prstr, out, file=outfil
			widget_control, all.id_info, set_value='Current inputs written to ' + outfil
	     endcase

	  ;------------------ Selecting mnemonic list
	  "SELECT MNEMONIC FROM GROUP LIST": if (event.select) then begin
		lst_file = concat_dir('$SAG_TELEM_INFO', 'sag_xhkplot_mnem.lst')
		list = rd_tfile(lst_file)
		ss = where(strmid(list, 0, 3) eq '***')
		tits = strmid(list(ss), 4, 999)
		ntits = n_elements(tits)
		tits = tits(0:ntits-2)
		;
		mapx, event.top,/map,/show,sensitive=0
		itit = xmenu_sel(tits, /one, group=event.top)
		itit = itit(0)
		mapx, event.top,/map,/show,sensitive=1
		if (qdebug) then print, itit
		;
		if (itit ne -1) then begin
		    i1 = ss(itit) + 1
		    i2 = ss(itit+1) - 1
		    ;;mnem = list(i1:i2)
		    mnem_mat = str2cols(list(i1:i2))
		    mnem = strlowcase(strtrim(mnem_mat(0,*),2))

		    if (qdebug) then print, mnem
		    all.mnem = arr2str(mnem)
		    widget_control, all.id_mnem_rd, set_value=mnem

		    all.mnem_plot = all.mnem		;MDM added next three lines 8-Jun-95
		    mnem = str2arr(all.mnem_plot)
		    widget_control, all.id_mnem_pl, set_value=mnem
		end
	     end
	  "SELECT FROM FULL MNEMONIC LIST": if (event.select) then begin
		lst_file = concat_dir('$SAG_TELEM_INFO', 'sag_xhkplot_mnem.full_lst')
		list = rd_tfile(lst_file)

		mapx, event.top,/map,/show,sensitive=0
		ss = xmenu_sel(list, group=event.top)
		mapx, event.top,/map,/show,sensitive=1

		if (qdebug) then print, ss
		if (ss(0) ne -1) then begin
		    ;;mnem = strlowcase( strmid(list(ss), 1, 9) )
		    mnem_mat = str2cols(list(ss))
		    mnem = strlowcase(strtrim(mnem_mat(0,*),2))

		    if (qdebug) then print, mnem
		    all.mnem = arr2str(mnem)
		    widget_control, all.id_mnem_rd, set_value=mnem

		    all.mnem_plot = all.mnem		;MDM added next three lines 8-Jun-95
		    mnem = str2arr(all.mnem_plot)
		    widget_control, all.id_mnem_pl, set_value=mnem
		end

	     end
	  ;------------------ Timerange Plotting control
	  "PLOT FULL ORIGINAL INPUT TIME RANGE": begin
		all.text_vals(4:7) = all.text_vals(0:3)
		for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)
	     endcase
	  "PLOT TO FULL RANGE OF AVAILABLE DATA": begin
		if (qhk) then begin
		    n = n_elements(hk.(0).daytime)
		    all.text_vals(4) = gt_day(  hk.(0).daytime(0), /str)
		    all.text_vals(5) = gt_time( hk.(0).daytime(0), /str)
		    all.text_vals(6) = gt_day(  hk.(0).daytime(n-1), /str)
		    all.text_vals(7) = gt_time( hk.(0).daytime(n-1), /str)

		    for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)
		end else begin
		    tbeep, 3
		    widget_control, all.id_info, set_value='You must read the database first'
		end
	     endcase
	  "TO SET PLOT RANGE BY CLICKING ON PLOT": begin
		if (!d.window ne -1) then begin
			widget_control, all.id_info, set_value='Click around starting point'
			wshow & cursor, x0, y0, /data, /down
			time_tmp = anytim2ints(xst, off=x0)
			if (keyword_set(getenv('SAG_HKPLOT_TO_LOCAL'))) then time_tmp = ut_time(time_tmp)
			all.text_vals(4) = gt_day(  time_tmp, /str)
			all.text_vals(5) = gt_time( time_tmp, /str)
			for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)
			;
			widget_control, all.id_info, set_value='Click around ending point'
			wshow & cursor, x0, y0, /data, /down
			time_tmp = anytim2ints(xst, off=x0)
			if (keyword_set(getenv('SAG_HKPLOT_TO_LOCAL'))) then time_tmp = ut_time(time_tmp)
			all.text_vals(6) = gt_day(  time_tmp, /str)
			all.text_vals(7) = gt_time( time_tmp, /str)
			for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)
			widget_control, all.id_info, set_value='Click on PLOT button to re-plot'
			;qplot = 1
		end else begin
			tbeep, 3
			widget_control, all.id_info, set_value='You must plot the data first'
		end
	     endcase

	  ;------------------ Y Range Plotting control
	  "SET Y-RANGE FOR ALL MNEMONICS": begin
		;input, 'Enter Y minimum', ymin, !y.crange(0)
		;input, 'Enter Y Maximum', ymax, !y.crange(1)
		ymin = string(all.yrange_all(0))
		ymax = string(all.yrange_all(1))
		xmenu_gen_input, 'All Mnemonic Range ', ymin, ymax, labels=['YMIN: ','YMAX: '], group=event.top
		all.yrange_all = [float(ymin), float(ymax)]
	     endcase

	  "SET Y-RANGE FOR INDIVIDUAL MNEMONICS": begin
		mnem = str2arr(all.mnem) 
		n = n_elements(mnem)
		if (all.yrange_mnem eq '') then all.yrange_mnem = arr2str(fltarr(2,n))
		y = str2arr(all.yrange_mnem)
		y = reform(y, 2, n, /overwrite)
		ymin = string(y(0,*))
		ymax = string(y(1,*))
		xmenu_gen_input, mnem, ymin, ymax, labels=['YMIN: ','YMAX: '], group=event.top
		y(0,*) = float(ymin)
		y(1,*) = float(ymax)
		all.yrange_mnem = arr2str(y)
	     endcase
	  "SET Y-RANGE TO AVERAGE +/- 1 SIGMA": begin
		all.yrange_qsig = abs(1-all.yrange_qsig)
	     endcase

	  "SET Y-RANGE FOR ALL MNEMONICS BY CLICKING ON PLOT": begin
		if (!d.window ne -1) then begin
			widget_control, all.id_info, set_value='Click around Y-min'
			wshow & cursor, x0, y0, /data, /down
			all.yrange_all(0) = y0
			widget_control, all.id_info, set_value=['Y-Min set to ' + string(y0), 'Click around Y-Max']
			wshow & cursor, x0, y0, /data, /down
			widget_control, all.id_info, set_value=['Y-Max set to ' + string(y0), 'Click on PLOT button']
			all.yrange_all(1) = y0
			;;qplot = 1
		end else begin
			tbeep, 3
			widget_control, all.id_info, set_value='You must plot the data first'
		end
	     endcase
	  "RESET TO SELECT ALL MNEMONICS READ": begin
		all.mnem_plot = all.mnem

		mnem = str2arr(all.mnem_plot)
		widget_control, all.id_mnem_pl, set_value=mnem
	     endcase

	  ;------------------ Y Range Plotting control

	  "SET PSYM": begin
		if (event.select) then begin
		    psym = xmenu_psym(group=event.top)
		    if (psym ne -99) then !p.psym = psym
		end else begin
		    !p.psym = 0
		end
	     endcase
	  "ENABLE SEPARATE LINESTYLE FOR OVERPLOTTED LINES": begin
		all.qenab_linestyle = event.select	
	     endcase
	  "SET TO PLOT 4 PLOTS PER PAGE (2X2)": begin
		all.qenab_4plot = event.select
	     endcase

          else: message,/info,"UNKNOWN BUTTON:   " + strupcase(get_wvalue(event.id))
       endcase
   endcase 
   "TEXT_CH": begin
		newv = get_wvalue(event.id)
		ss = where(all.text_id eq event.id)
		ss = ss(0)
		case ss of
		    8: begin		;"every" text window
			    barr = byte(strcompress(newv, /remove))
			    if (min(barr) lt 48) or (max(barr) gt 57) then begin	;not a number
				tbeep, 3
				widget_control, all.id_info, set_value='The "EVERY" value must be an integer'
			    end else begin
				all.text_vals(ss) = newv
			    end
			    widget_control, all.text_id(8), set_value=all.text_vals(8)
			end
		    9: begin		;"last N hours" text window
			    barr = byte(strcompress(newv, /remove))
			    if (min(barr) lt 48) or (max(barr) gt 57) then begin	;not a number
				tbeep, 3
				widget_control, all.id_info, set_value='The "LAST N HOURS" value must be an integer'
			    end else begin
				all.text_vals(ss) = newv
				
                        	entim = ut_time()
                        	dur = float(all.text_vals(ss)) * 60.*60  ;in seconds
                        	sttim = anytim2ints(entim, off=-dur)

                        	all.text_vals(0)        = gt_day(sttim, /str)
                        	all.text_vals(1)        = gt_time(sttim, /str)
                        	all.text_vals(2)        = gt_day(entim, /str)
                        	all.text_vals(3)        = gt_time(entim, /str)

                        	for i=0,3 do widget_control, all.text_id(i), set_value=all.text_vals(i)
			    end
			    widget_control, all.text_id(9), set_value=all.text_vals(9)
			end
		    else: begin
			    if ((ss mod 2) eq 0) then newv2 = gt_day(newv,/str) $
					else newv2 = gt_time(newv,/str)
			    all.text_vals(ss) = newv2
			    if (ss eq 0) and (all.text_vals(2) eq '') then all.text_vals(2) = all.text_vals(0)

			    for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)
			end 
		end
	end

   "LIST": begin
	case event.id of
	    all.id_mnem_rd: begin
		mnem = str2arr(all.mnem)
		n = n_elements(mnem)
		ss = rem_elem(indgen(n), event.index)		;remove that item
		if (ss(0) eq -1) then mnem = '' else mnem = mnem(ss)
		all.mnem = arr2str(mnem)
		;
		widget_control, all.id_mnem_rd, set_value=mnem
	       end
	    all.id_mnem_pl: begin
		mnem = str2arr(all.mnem_plot)
		n = n_elements(mnem)
		ss = rem_elem(indgen(n), event.index)	;remove that item
		if (ss(0) eq -1) then mnem = '' else mnem = mnem(ss)
		all.mnem_plot = arr2str(mnem)
		;
		widget_control, all.id_mnem_pl, set_value=mnem
	       end
	endcase
   endcase
endcase

;--------------

if (qplot) then begin

    if (strtrim(all.mnem_plot,2) eq "") then begin
	qhk = 0
    end else begin
	mnem = str2arr(all.mnem_plot)
	if (qhk) then begin		;MDM added 19-Sep-95
	    sublist = sag_hkplot_info(hk, mnemlist=mnem)
	    tits = hkplot_info(hk, /tit)
	end
    end

    if (qhk) then begin
	if (all.text_vals(4) eq '') then all.text_vals(4:7) = all.text_vals(0:3)	;copy RD time/date range into PLOT range
	for i=0,7 do widget_control, all.text_id(i), set_value=all.text_vals(i)

	sttim = arr2str(all.text_vals(4:5), delim=' ')
	entim = arr2str(all.text_vals(6:7), delim=' ')
	timerange = [sttim, entim]
	every=all.text_vals(8)

	widget_control, all.id_info, set_value='*** Plotting Data *** '
	mapx, event.top,/map,/show,sensitive=0
	if (!d.name eq 'X') then begin
	    if (!d.window eq -1) then window
	    wshow
	end

	if (all.qenab_4plot and (all.plot_option ne 0)) then begin
	    !p.multi = [0,2,2]
	    ;!p.charsize = 1.3
	end else begin
	    !p.multi = 0
	    ;!p.charsize = 0
	end
	case all.plot_option of
	    0: begin
		yrange = all.yrange_all
		if (yrange(0) eq yrange(1)) then yrange = 0b
		;	
		sag_hkplot, hk, timerange=timerange, yrange=yrange, codelist=sublist, qdebug=qdebug, $
					qlinestyle=all.qenab_linestyle, $
				sigma=all.yrange_qsig*all.yrange_sig,every=every ;(DBG)
				
	       end
	    1: begin
		n = n_elements(str2arr(all.mnem))
		if (all.yrange_mnem eq '') then all.yrange_mnem = arr2str(fltarr(2,n))
		y = str2arr(all.yrange_mnem)
		y = reform(y, 2, n, /overwrite)

		n = n_elements(sublist)
		i = 0
		while (i le n-1) do begin
		    yrange = y(*, sublist(i))		;first try individual mnemonic ranges
		    if (yrange(0) eq yrange(1)) then yrange = all.yrange_all	;second try overall range
		    if (yrange(0) eq yrange(1)) then yrange = 0b		;third - reset

		    npp = (!p.multi(1) * !p.multi(2) )>1
		    qnover = ((i mod npp) ne npp-1) and (i ne n-1)
		    sag_hkplot, hk, timerange=timerange, yrange=yrange, codelist=sublist(i), $
				nover=qnover, tit=tits(sublist(i)), qdebug=qdebug, $
				sigma=all.yrange_qsig*all.yrange_sig,every=every
		    if (not qnover) and (i ne n-1) and (!d.name ne 'PS') then begin
			;pause
			result = xmenu_gen_but(['Continue', 'Abort'], group=event.top, $
						instruct=['Pausing to inspect the plot'], $
						location=[0,200] )
			if (result eq 'ABORT') then i = n
		    end
		    i = i+1
		end
	       end
	endcase

	mapx, event.top,/map,/show,sensitive=1
	widget_control, all.id_info, set_value='*** Finished Plotting ***'

	set_plot, save_dev
    end else begin
	tbeep, 3
	widget_control, all.id_info, set_value='You must read the database first and/or select mnemonics'
    end
end

widget_control,event.top, set_uvalue=all, bad_id=destroyed	;update structure holding all of the info

return
end
;---------------------------------------------------------------------
pro sag_xhkplot, sttim, entim, mnem, infil=infil, every=every, nsig=nsig, raw=raw0

common sag_xhkplot_blk1, hk, all, last_all, raw
if (n_elements(raw0) ne 0) then raw = raw0

;setenv, 'MDI_DEF_FONT=courier_bold10'
;font = 'newcenturyschlbk_bold10'
;font = def_font()
font = get_xfont(closest=13,/only_one)
if (getenv('MDI_DEF_FONT') ne "") then font = getenv('MDI_DEF_FONT')

widget_control, default_font=font
device, font=font
if (!d.window eq 32) then begin & wdelete & wdef, 16, 640, 512 & end		;MDM added 15-Sep-95

base0=widget_base(/column,title='X HKPlot', xoff=0, yoff=0)
xmenu, ['QUIT', 'CLEARPLOT', 'Set Printer', 'Read Dbase', 'Plot All', 'Plot Separately', $
		'Hardcopy', 'Print Values', 'Save Inputs'], $
	base0, $
	buttons=main_buts, /row

base2=widget_base(base0, /row, /frame)

;--------------

base_col1 = widget_base(base2, /column, /frame)
lab_base_col1 = widget_label(base_col1, value = 'Reading HK Database')

timebase = widget_base(base_col1, /row)
xx       = widget_label(timebase, value = 'Start Date:')
stdat_rd = widget_text(timebase, /editable, xsize=12, ysize=1)
xx       = widget_label(timebase, value = ' Time:')
sttim_rd = widget_text(timebase, /editable, xsize=12, ysize=1)

timebase = widget_base(base_col1, /row)
xx       = widget_label(timebase, value = 'End Date:  ')
endat_rd = widget_text(timebase, /editable, xsize=12, ysize=1)
xx       = widget_label(timebase, value = ' Time:')
entim_rd = widget_text(timebase, /editable, xsize=12, ysize=1)

basexx   = widget_base(base_col1, /row)
xx       = widget_label(basexx, value = 'Sample Every: ')
id_every = widget_text(basexx, /editable, xsize=4, ysize=1, value='1')

basexx   = widget_base(base_col1, /row)
xx       = widget_label(basexx, value = 'Last N Hours: ')
id_lastn = widget_text(basexx, /editable, xsize=6, ysize=1, value='1')

xmenu, ['Select Mnemonic from Group List', $
	'Select from Full Mnemonic List'], $
	base_col1, /column, /frame

base1a = widget_base(base_col1, /column, /frame)
lab1a  = widget_label(base1a, value='Mnemonics Read/To Read')
list1a = widget_list(base1a, xsize=10, ysize=5, value='')

;-------------------

base_col2 = widget_base(base2, /column, /frame)
lab_base_col2 = widget_label(base_col2, value = 'Plotting Selection')

timebase = widget_base(base_col2, /row)
xx       = widget_label(timebase, value = 'Start Date:')
stdat_pl = widget_text(timebase, /editable, xsize=10, ysize=1)
xx       = widget_label(timebase, value = ' Time:')
sttim_pl = widget_text(timebase, /editable, xsize=10, ysize=1)

timebase = widget_base(base_col2, /row)
xx       = widget_label(timebase, value = 'End Date:  ')
endat_pl = widget_text(timebase, /editable, xsize=10, ysize=1)
xx       = widget_label(timebase, value = ' Time:')
entim_pl = widget_text(timebase, /editable, xsize=10, ysize=1)

base_col2a = widget_base(base_col2, /column, /frame)
lab2a      = widget_label(base_col2a, value='Setting Plotting Time Ranges')
xmenu, ['Plot full original input time range', $
	'Plot to full range of available data', $
	'To set plot range by clicking on plot'], $
	base_col2a, /column

base_col2b = widget_base(base_col2, /column, /frame)
lab2b      = widget_label(base_col2b, value='Setting Y Range(s)')
xmenu, ['Set Y-range for all mnemonics', $
	'Set Y-range for all mnemonics by clicking on plot', $
	'Set Y-range for individual mnemonics', $
	'Set Y-range to average +/- 1 Sigma'], $
	base_col2b, /column

base_col2d = widget_base(base_col2, /column, /frame)
lab2d      = widget_label(base_col2d, value='Misc Plotting Control')
xmenu, ['Set PSYM', $
	'Enable Separate Linestyle for overplotted lines', $
	'Set to plot 4 plots per page (2x2)'], $
	base_col2d, /column, /nonexclusive
;;xmenu, ['Plot all Mnemonics on one plot', $
;;	'Plot each mnemonic on a separate plot'], $
;;	base_col2d, /column, /frame, /exclusive

base_col2c = widget_base(base_col2, /column, /frame)
lab2c      = widget_label(base_col2c, value='Set Which Mnemonics to plot (click below)')

base2a = widget_base(base_col2, /column, /frame)
lab2a  = widget_label(base2a, value='Mnemonics Selected (click on mnem to remove)')
list2a = widget_list(base2a, xsize=10, ysize=5, value='')

xmenu, ['Reset to select all mnemonics read'], $
	base_col2c, /column

;-------------------------

base3=widget_base(base0, /row, /frame)

id_info = widget_text(base3, xsize=60, ysize=4)
widget_control, id_info, /append

if (n_elements(sig) eq 0) then sig=1.0

all = {base:base0, $
	id_mnem_rd: list1a, $
	id_mnem_pl: list2a, $
	text_vals: strarr(10), $		;see which goes where below
	text_id: [stdat_rd, sttim_rd, endat_rd, entim_rd, $
		  stdat_pl, sttim_pl, endat_pl, entim_pl, $
		  id_every, id_lastn], $
	id_info: id_info, $
	mnem: '', $
	plot_option: 0, $
	qenab_linestyle: 0, $
	qenab_4plot: 0, $
	yrange_all: fltarr(2), $
	yrange_mnem: '', $
	yrange_sig: sig, $
	yrange_qsig: 0, $
	mnem_plot: '', $
	print_file: '', $
	junk:0}
all.text_vals(8) = '1'

;#1: if last session is still there - copy parts over
if (data_type(last_all) eq 8) then begin	;
    all.text_vals 	= last_all.text_vals
    all.mnem		= last_all.mnem
    all.mnem_plot	= last_all.mnem_plot
end

;#2: if an input file is specified, use it
if (keyword_set(infil)) then begin
    if (file_exist(infil)) then begin
	info = rd_tfile(infil, nocomment='#')
	for i=0,n_elements(info)-1 do begin
	    lin = info(i)
	    p = strpos(lin, ' ')
	    tag = strmid(lin, 0, p-1)
	    val = strtrim( strmid(lin, p, 999), 2)
	    if (strmid(tag, 2, 3) eq 'TIM') then begin
		day = gt_day(val, /str)
		tim = gt_time(val, /str)
	    end
	    case tag of
		'STTIM_RD': all.text_vals(0:1) = [day, tim]
		'ENTIM_RD': all.text_vals(2:3) = [day, tim]
		'STTIM_PL': all.text_vals(4:5) = [day, tim]
		'ENTIM_PL': all.text_vals(6:7) = [day, tim]
		'EVERY':    all.text_vals(8) = val
		'MNEM':     all.mnem = val
		'MNEM_PLOT':all.mnem_plot = val
		'YRANGE_ALL': all.yrange_all = val
		'YRANGE_MNEM': all.yrange_mnem = val
	    endcase
	end
    end else begin
	tbeep,3
	widget_control, all.id_info, set_value='No such file: ' + infil
    end
end

;#3: If keywords are passed in, it overrides all
if (keyword_set(sttim)) then begin
    all.text_vals(0) = gt_day(sttim, /str)
    all.text_vals(1) = gt_time(sttim, /str)
end
if (keyword_set(entim)) then begin
    all.text_vals(2) = gt_day(entim, /str)
    all.text_vals(3) = gt_time(entim, /str)
end
if (n_elements(mnem) ne 0) then all.mnem = arr2str(mnem)
if (n_elements(every) ne 0) then all.text_vals(8) = strtrim(every,2)
if (all.mnem_plot eq '') then all.mnem_plot = all.mnem		;copy whatever is in the to read list

;---- Update widgets which may have been updated/written to
for i=0,8 do widget_control, all.text_id(i), set_value=all.text_vals(i)
mnem = str2arr(all.mnem)
widget_control, all.id_mnem_rd, set_value=mnem
mnem = str2arr(all.mnem_plot)
widget_control, all.id_mnem_pl, set_value=mnem

widget_control,set_uvalue=all, base0
widget_control,base0,/realize
xmanager, 'sag_xhkplot', base0

mnem = str2arr(all.mnem)

last_all = all
return
end
