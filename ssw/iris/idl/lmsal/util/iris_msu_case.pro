pro iris_msu_case
;
;+
;   Name: iris_msu_case
;
;   Purpose: pre compile IRIS_*pro
;
;-
;
msutop=concat_dir('$SSW_IRIS','idl/msu')
caps=file_search(msutop,'IRIS_*.pro')
cdir=curdir()
if caps[0] ne '' then begin 
   break_file,caps,ll,paths,files,ext,vv
   links=concat_dir(paths,strlowcase(files)+'.pro')
   lss=where(file_exist(links),lecnt)
   for l=0,lecnt-1 do begin
      cd,paths[lss[l]]
      recompile,links[lss[l]]
   endfor
endif
cd,cdir ; restore original

return
end

