
FUNCTION iris_find_obs_dir, start_date, stop_date, top_dir=top_dir, count=count, $
                            files=files

;+
; NAME:
;      IRIS_FIND_OBS_DIR()
;
; PURPOSE:
;      This routine finds a list of all IRIS data directories between
;      start_date and stop_date.
;
; CATEGORY:
;      IRIS; file-handling.
;
; CALLING SEQUENCE:
;      Result = IRIS_FIND_OBS_DIR( Start_Date, Stop_Date )
;
; INPUTS:
;      Start_Date:  A start date in a standard SSW format. For
;                   example, '1-Jan-2015'.
;      Stop_Date:   A stop date in a standard SSW format. For
;                   example, '1-Jan-2015'.
;
; OPTIONAL INPUTS:
;      Top_Dir:  A string (or string array) containing the name(s) of
;                a top-level directory containing the data. For
;                example, '$HOME/iris/data'. If not set, then the routine
;                uses the top-level directories defined by the
;                $IRIS_DATA environment variable.
;	
; OPTIONAL INPUTS:
;      Count:  An integer containing the number of directories that
;              have been found.
;
; KEYWORD PARAMETERS:
;      FILES:  If set, then the routine finds all of the FITS files in the
;              directories, and returns the filenames. Note that the
;              filenames must end in '.fits' (so they can't be
;              compressed, for example).
;
; OUTPUTS:
;      The full names of the directories that have been found. If no
;      directories are found, then an empty string is returned.
;
; EXAMPLE:
;      IDL> dir=iris_find_obs_dir('1-jan-2015','31-dec-2015')
;      IDL> files=iris_find_obs_dir('1-jan-2015','31-dec-2015',/files)
;
; MODIFICATION HISTORY:
;      Ver.1, 18-Oct-2016, Peter Young
;      Ver.2, 16-Mar-2017, Peter Young
;        Made the call to file_search a standard one rather than a
;        recursive one in the hope of fixing a problem with speed on
;        network drives.
;-

IF n_params() LT 2 THEN BEGIN
  print,'Use:  IDL> dir = iris_find_obs_dir( start_date, stop_date [, top_dir=, count=, /files ]'
  return,''
ENDIF


IF n_elements(top_dir) EQ 0 THEN BEGIN 
  iris_data=getenv('IRIS_DATA')
  IF iris_data EQ '' THEN BEGIN
    print,'%IRIS_FIND_OBS_DIR: $IRIS_DATA is not defined. Please specify a top-level directory using the input TOP_DIR='
    print,'                    Returning...'
    return,''
  ENDIF
  iris_paths=BREAK_path(iris_data,/nocurrent)
  top_dir=concat_dir(iris_paths,'level2')
ENDIF 

ndir=n_elements(top_dir)

;
; The following creates an array of directory names of the form
; 'YYYY/MM/DD' for all days between start_date and stop_date. I do it
; by going to modified julian day format.
;
t0_mjd=anytim2utc(/mjd,start_date)
t1_mjd=anytim2utc(/mjd,stop_date)
;
n0=t0_mjd.mjd
n1=t1_mjd.mjd
;
ndays=n1-n0+1
;
str={mjd: 0l, time: 0l}
mstr=replicate(str,ndays)
;
mstr.mjd=n0+lindgen(ndays)
;
date_dir=time2fid(mstr,/full_year,delim='/')


;
; Now go through the date directories and check if they exist
; on the user's computer. The array 'outdir' contains the
; directory names that do exist.
;
outdir=''
count=0
FOR i=0,ndir-1 DO BEGIN 
  finddir=concat_dir(top_dir[i],date_dir)
  chck=file_info(finddir)
  k=where(chck.directory EQ 1,nk)
  count=count+nk
  IF nk GT 0 THEN outdir=[outdir,finddir[k]]
ENDFOR

IF count GT 0 THEN BEGIN
  outdir=outdir[1:*]
  date_dir=outdir
ENDIF ELSE BEGIN
  return,''
ENDELSE 

;
; Now go through the date directories and find the sub-directories
; within them.
;
output=file_search(date_dir,'*',/test_directory,count=count)

;
; If /files has been set, then search the directories for '.fits'
; files. 
;
IF keyword_set(files) THEN BEGIN
  search_str=concat_dir(output,'*.fits')
  output=file_search(search_str,count=count)
ENDIF 

return,output

END
