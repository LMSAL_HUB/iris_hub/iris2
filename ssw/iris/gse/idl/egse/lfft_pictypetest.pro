;
;
;
outdir = '$SXI_LFFT_DIR/tmp'
if (n_elements(date) eq 0) then date = gt_day( ut_time(), /str)
out = sxi_hist_sea(date, 'perform pictypetest')
out2 = sxi_hist_sea(date, 'Perform completed for procedure: pictypetest')
;
if (data_type(out) ne 8) then begin
    print, 'Cannot find any runs of PICTYPETEST on ' + gt_day(date, /str)
    return
end
if (data_type(out2) ne 8) then begin
    print, 'Cannot find any complete runs of PICTYPETEST on ' + gt_day(date, /str)
    return
end
if (n_elements(out) ne n_elements(out2)) then begin
    print, 'Mismatch on starts/ends for PICTYPETEST'
    one = 1
    ;;return
end
;
if (n_elements(out) gt 1) and (keyword_set(one)) then begin
    imenu = xmenu_sel( out.str, /one)
    if (imenu eq -1) then return
    out = out(imenu)
end
;
for i=0,n_elements(out)-1 do begin
    sttim = out(i)
    entim = out2(i)
    print, 'Checking run from: ' + fmt_tim(sttim) + ' to ' + fmt_tim(entim)
    ;
    sxi_tap_fdb_timing, sttim, entim, struct=struct, out=outstr
    ;
    fid = str_replace(time2file(out(i)), '.', '_')
    out = ['LFFT_PICTYPETEST Ver 1.02  Program run: ' + !stime, $
	' ', $
	outstr]
    outfil = concat_dir(outdir, 'lfft_pictypetest_' + fid + '.txt')
    prstr, out, fil=outfil
    sprint, outfil, /land
end
;

end