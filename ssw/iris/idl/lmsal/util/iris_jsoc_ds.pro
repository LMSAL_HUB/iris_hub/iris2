function iris_jsoc_ds,patt,time=time, level=level,_extra=_extra, pattern=pattern, only_one=only_one, refresh=refresh, $
   search_notes=search_notes, notes=notes, jsoc2=jsoc2
;
;+
;   Name: iris_jsoc_time2ds
;
;   Purpose: return time/level dependent IRIS JSOC Data Series names
;
;   Input Parameters:
;      patt - optional patttern to match
;
;   Keyword Parameters:
;      time - optional time - for epoch dependent series names
;      level - data level
;      _extra - optional pattern via /PATTERN
;      only_one - (switch) return single value if multiple matches 
;     search_notes - (switch) if set, search the series NOTES (default is series NAME)
;     notes - (output) - NOTES associated with function return NAMES
;
;   Method: setup/call ssw_jsoc.pro
;
;
;-
common iris_jsoc_time2ds_blk,all_iris

refresh=keyword_set(refresh) or n_elements(all_iris) eq 0

if refresh then all_iris=ssw_jsoc(/show_series,filt='iris',/jsoc2) ; 
patternx=''
case 1 of
   data_chk(patt,/string): patternx=patt
   data_chk(pattern,/string): patternx=pattern
   data_chk(_extra,/struct): patternx=(tag_names(_extra))(0)
   else:
endcase

ss=indgen(n_elements(all_iris.names))
count=n_elements(ss)
retval=''
if patternx ne '' then begin
   patternx=str_replace('*'+patternx+'*','**','*')
   sstrings=gt_tagval(all_iris.names,(['name','note'])(keyword_set(search_notes)))
   ss=where(strmatch(sstrings,patternx,/fold),count)
   if count gt 0 and n_elements(level) gt 0 then begin  ; optional sub-selection on data level
      slev='.lev'+strtrim(level,2)
      sss=where(strpos(sstrings[ss],slev) ne -1,count)
      if count gt 0 then ss=ss[sss]
   endif
endif 

if count ge 1 then begin
   if keyword_set(only_one) then ss=ss[0]
   retval=all_iris.names[ss].name
   notes=all_iris.names[ss].note
endif

return,retval
end
