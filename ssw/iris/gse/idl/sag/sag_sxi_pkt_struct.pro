function sag_sxi_pkt_struct, code, filnam
;
;
;HISTORY:
;	Written 23-Sep-99 by M.Morrison 
;	28-Sep-99 (MDM) - Added GPIB_INDEX_MAP
;			- Added 0x0400
;	29-Sep-99 (MDM) - Added APIDs 12 and 18 (hex)
;	 5-Nov-99 (MDM) - Added 0x0031
;	31-Mar-00 (MDM) - Added optional 2nd parameter "filnam" so that
;			  the output can be dependent on the date
;			- Added 401
;	 5-Oct-00 (MDM) - Added 0x20, 0x20, 0x13
;	21-Dec-01 (MDM) - Added 0x300
;
pkt_heads = {code: 0b, apid: 0b, scnt: 0, dlen: 0, time: 0L, ftime: 0}
img_drec = {serno: 0L, time: 0L, ftime: 0, pcnt: 0, xsiz: 0, ysiz: 0, $
                fdb: 0L, spare: bytarr(4)}
hass_drec = {serno: 0L, time: 0L, ftime: 0, pcnt: 0, npair_pkt: 0, npair_ser: 0, $
			spare: bytarr(12)}
isi_heads = {code: 0b, apid: 0b, scnt: 0, dlen: 0, sec_head: bytarr(18)}

;
case strupcase(code) of
    'INDEX_MAP': 	out = {config: 0b, mc_fc: 0b, pkt_err_flags: 0, $
				egse_time: 0L, vc_fc: 0b, pad1: 0b, $
				time: 0L, ftime: 0, serno: 0L, pad2: bytarr(8)}
    'GPIB_INDEX_MAP': 	out = {egse_time: 0L}
    'PKT_HEADS':	out = pkt_heads
    'IMG_DREC':		out = img_drec
    '.0X0010': 		out = {pkt_head: pkt_heads, data: bytarr(600)}
    '.0X0011': 		out = {pkt_head: pkt_heads, drec: img_drec, data: bytarr(804)}
    '.0X0012': 		out = {pkt_head: pkt_heads, drec: img_drec, data: bytarr(804)}
    '.0X0013': 		out = {pkt_head: pkt_heads, data: bytarr(828)}
    '.0X0018': 		out = {pkt_head: pkt_heads, drec: hass_drec, data: intarr(2,200)}
    '.0X0020': 		out = {pkt_head: pkt_heads, data: bytarr(12)}
    '.0X0021': 		out = {pkt_head: pkt_heads, data: bytarr(12)}
    '.0X0400': 		begin
			    out = {pkt_head: pkt_heads, data: bytarr(60)}
			    if (keyword_set(filnam)) then if (filnam le '20000324') then $
					out = {pkt_head: pkt_heads, data: bytarr(56)}
			end
    '.0X0401': 		out = {pkt_head: pkt_heads, data: bytarr(106)}
    '.0X0031': 		out = {pkt_head: pkt_heads, addr: 0L, data: bytarr(180)}
    '.0X0300': 		out = {pkt_head: pkt_heads, data: bytarr(24+512)}
    else: begin
		print, 'SAG_SXI_PKT_STRUCT:  Unrecognized code= ' + code
		out = 0b
	  end
endcase
;
return, out
end
