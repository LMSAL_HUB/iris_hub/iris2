;+
;
; SSW Batch file for generating IRIS timeline database
; 3/21/19 RPT: make it run both this day/previous day
;-

timenow = TIME2FILE(RELTIME(/now), /sec)

set_logenv,'GSE_PKT_FILE_DIR', '/irisa/moc/hk_archive-in/packets/vc2/'
case 1 of
	FILE_EXIST('/irisa/data/prep/aux/')	:	finalpath = '/irisa/data/prep/aux/'
	FILE_EXIST('/net/brooklyn/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/brooklyn/Volumes/earth/iris/data/prep/aux/'
	FILE_EXIST('/net/bay/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/bay/Volumes/earth/iris/data/prep/aux/'
	else	:	begin
		PRINT, 'Cannot reach irisa...'
		finalpath = '/irisa/data/prep/aux/'
	end
endcase
logpath = CONCAT_DIR(CONCAT_DIR('log', STRMID(timenow, 0, 4)), STRMID(timenow, 4, 2))
logpath = CONCAT_DIR(finalpath, logpath)
finalpath = CONCAT_DIR(finalpath, 'tim')

case 1 of
	FILE_EXIST('/sanhome/boerner/public_html/iris/tim')	:	pbpath = '/sanhome/boerner/public_html/iris/tim'
	else	:	begin
		PRINT, 'Cannot reach sanhome public_html...'
		pbpath = '~/iris/cronlog/'
	end
endcase

OPENW, lun, /get, CONCAT_DIR(logpath, timenow + '_timeline.txt')
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TIM_DATABASE_BATCH running... ', timenow
PRINTF, lun

; Generate data for today
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for hours=+1'
PRINTF, lun, '*************************************
daynow = RELTIME(hours = +1)
IRIS_MAKE_TIM_DATABASE, daynow, outpath = '/tmp', fname = fname
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
ofsize = FILE_SIZE(oldfile)
nfsize = FILE_SIZE(newfile)
PRINTF, lun, 'Old file: ', ofsize/1e6, 'New file: ', nfsize/1e6, '(MB)', $
	form = '(a15,f15.3,a15,f15.3,a5)'
if ofsize gt nfsize then begin
	PRINTF, lun, 'Keeping old file...'
	SPAWN, 'rm ' + newfile, res, err
	PRINTF, lun, 'rm result: ', res
	PRINTF, lun, 'rm errcode: ', err
endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
     endelse

; Generate data for previous day
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for hours=-7'
PRINTF, lun, '*************************************
daynow = RELTIME(hours = -7)
IRIS_MAKE_TIM_DATABASE, daynow, outpath = '/tmp', fname = fname
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
ofsize = FILE_SIZE(oldfile)
nfsize = FILE_SIZE(newfile)
PRINTF, lun, 'Old file: ', ofsize/1e6, 'New file: ', nfsize/1e6, '(MB)', $
	form = '(a15,f15.3,a15,f15.3,a5)'
if ofsize gt nfsize then begin
	PRINTF, lun, 'Keeping old file...'
	SPAWN, 'rm ' + newfile, res, err
	PRINTF, lun, 'rm result: ', res
	PRINTF, lun, 'rm errcode: ', err
endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
endelse



; Once per day at 10 pm, go back and generate 2-day old and 30-day old files
if STRMID(SYSTIME(), 11, 2) eq '22' then begin

	; Generate data for 2 days ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for hours=-48'
	PRINTF, lun, '*************************************
	daynow = RELTIME(hours = -48)
	IRIS_MAKE_TIM_DATABASE, daynow, outpath = '/tmp', fname = fname
	newfile = CONCAT_DIR('/tmp', fname)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	ofsize = FILE_SIZE(oldfile)
	nfsize = FILE_SIZE(newfile)
	PRINTF, lun, 'Old file: ', ofsize/1e6, 'New file: ', nfsize/1e6, '(MB)', $
		form = '(a15,f15.3,a15,f15.3,a5)'
	if ofsize gt nfsize then begin
		PRINTF, lun, 'Keeping old file...'
		SPAWN, 'rm ' + newfile, res, err
		PRINTF, lun, 'rm result: ', res
		PRINTF, lun, 'rm errcode: ', err
	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
	endelse

	; Generate data for one month ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for days=-30'
	PRINTF, lun, '*************************************
	daynow = RELTIME(days = -30)
	IRIS_MAKE_TIM_DATABASE, daynow, outpath = '/tmp', fname = fname
	newfile = CONCAT_DIR('/tmp', fname)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	ofsize = FILE_SIZE(oldfile)
	nfsize = FILE_SIZE(newfile)
	PRINTF, lun, 'Old file: ', ofsize/1e6, 'New file: ', nfsize/1e6, '(MB)', $
		form = '(a15,f15.3,a15,f15.3,a5)'
	if ofsize gt nfsize then begin
		PRINTF, lun, 'Keeping old file...'
		SPAWN, 'rm ' + newfile, res, err
		PRINTF, lun, 'rm result: ', res
		PRINTF, lun, 'rm errcode: ', err
	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
	endelse

endif

timeend = TIME2FILE(RELTIME(/now), /sec)
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TIM_DATABASE_BATCH done... ', timeend
PRINTF, lun

end
