;+
;$Id: hmi_db_pkt_indicators_main.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
;
; main:  hmi_db_pkt_indicators_main.pro
;
; call plot_pkt_indicators2 with formal parameters:
; batch enabled with 'yes' 
; plot type or APID, used in output filename
; list of telemetry points to plot
; path for output png file.
;
; History: 
;	28-Mar-07, G.Linford, modified the output font, to be slightly larger
;
;	28-Mar-07, G.Linford, add the "!p. changes below for font sizes.
!p.charsize=2.0                 ; use a large font size then the default
!p.font=1.0                     ; use true-type fonts
;	11-Jul-07, G.Linford, SSIM Thermal point name changes CCD temps,
;		'E_S_C_H_TS104_CCD1_HDR_PRI' -> 'E_S_C_H_TS105_CCD1_SIDE_PRI'
;		'E_S_C_H_TS105_CCD2_HDR_PRI' -> 'E_S_C_H_TS104_CCD2_FRNT_PRI'
;
;$Log: hmi_db_pkt_indicators_main.pro,v $
;Revision 1.5  2008/04/17 21:34:20  linford
;move the SC items to the top of the list
;
;Revision 1.4  2008/04/15 21:12:28  linford
;modified tlm points from E_AIA and E_HMI to use Y_ mnemonics
;
;Revision 1.3  2008/04/14 23:55:46  sdotc
;G.Linford, update for GSFC runs
;
;Revision 1.2  2007/09/30 00:00:57  linford
;fix idl ending mark for header, cvs tags
;
;
;-

; setup indicator plot for SC packet BUS voltage
indicators = ['E_S_C_PA_VBUS','E_S_C_PB_VBUS']
day_plot_pkt_indicators2, 'yes', 'SC_VOLT_', indicators,'/home/sdotc/hmi_html/tmp'

; set up indicator plot for SC packet for current
;indicators = ['S_C_SSIMPWRIO','S_C_SSIMPWRV0','S_C_SSIMPWRPV','S_C_SSIMPWRPI']
indicators = ['E_S_C_PA_HMIA_DC','E_S_C_PA_HMIB_DC','E_S_C_PB_HMIA_DC','E_S_C_PB_HMIB_DC']
day_plot_pkt_indicators2, 'yes', 'SC_CURR_', indicators,'/home/sdotc/hmi_html/tmp'

; set up indicator plot for packet 0x05 (kernel)
indicators = ['HMI_KER_VER_NUM_HK','HMI_KER_CM_ACCEPT','HMI_KER_CM_ACCEPT_SS']
day_plot_pkt_indicators2, 'yes', '0x05_', indicators, '/home/sdotc/hmi_html/tmp'

; Set up CEB temp plots:
indicators=['Y_HMI_TS28_CAM2_FRNT_CEB_EXT','Y_HMI_TS29_CAM1_SIDE_CEB_EXT']
day_plot_pkt_indicators2, 'yes', 'CEB_EXT_', indicators,'/home/sdotc/hmi_html/tmp'

; Set up CCD temp plots:
indicators=['HMI_TS201_CEB1_CCD_TEMP1','HMI_TS203_CEB1_CCD_TEMP2','HMI_TS202_CEB2_CCD_TEMP1','HMI_TS204_CEB2_CCD_TEMP2']
day_plot_pkt_indicators2, 'yes', 'CCD_', indicators,'/home/sdotc/hmi_html/tmp'

; set up indicator plot for HEB for temps:
indicators = ['Y_HMI_TS30_HEB_BPLATE_INT','Y_HMI_TS31_HEB_PWR_CNVR_PRI','Y_HMI_TS33_HEB_RAD6000_CPU_A','Y_HMI_TS34_HEB_RAD6000_CPU_B']
day_plot_pkt_indicators2, 'yes', 'HEB_', indicators, '/home/sdotc/hmi_html/tmp'

; set up indicator plot for SEQ status packet 0x015
indicators =['H15_SEQ_STATE','H15_SEQ_FRAME_COUNT','H15_SEQ_FILTERGRAM_SN']
day_plot_pkt_indicators2, 'yes', 'SEQ_FRAME_', indicators, '/home/sdotc/hmi_html/tmp'

; setup indicator plot for SEQ FSN status (do not plot values of FSN=0) 
indicators =['H15_SEQ_FILTERGRAM_SN']
day_plot_pkt_indicators2, 'yes', 'SEQ_FSN_', indicators, '/home/sdotc/hmi_html/tmp',/yrange

; Heater zone plots:
indicators=['Y_HMI_TS01_WINDOW_MTG_RING1','Y_HMI_TS02_WINDOW_MTG_RING2','Y_HMI_TS05_TELE_FWD1','Y_HMI_TS39_OPT_FWDCVR','Y_HMI_TS40_OPT_AFTCVR','Y_HMI_TS23_OPT_AFTBENCH','Y_HMI_TS37_OPT_FWD_BENCH_EXT']
day_plot_pkt_indicators2, 'yes', 'HTR_ZN_', indicators,'/home/sdotc/hmi_html/tmp'

;set up indicator plot for OVEN for temps:
indicators = ['Y_HMI_TS12_OVN_LYOT','Y_HMI_TS13_OVN_WBM','Y_HMI_TS14_OVN_NBM','Y_HMI_TS15_OVN_PREAMP']
day_plot_pkt_indicators2, 'yes', 'OVEN_', indicators, '/home/sdotc/hmi_html/tmp'

;set up indicator plot for WT-ENCODER:
indicators = ['H11_WT1_ENCODER','H11_WT2_ENCODER','H11_WT3_ENCODER','H11_WT4_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'HCM-WT_', indicators, '/home/sdotc/hmi_html/tmp'

;set up indicator plot for PS-ENCODER:
indicators = ['H11_PS1_ENCODER','H11_PS2_ENCODER','H11_PS3_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'HCM-PS_', indicators, '/home/sdotc/hmi_html/tmp'

;set up indicator plot for CF-ENCODER:
indicators = ['H11_CF1_ENCODER','H11_CF2_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'CAL-FOCUS_', indicators, '/home/sdotc/hmi_html/tmp'

;set up indicator plot for SH-ENCODER:
indicators =['HMI_SH1_ENCODER','HMI_SH2_ENCODER','HMI_SH1_ENCODER_ERROR','HMI_SH2_ENCODER_ERROR','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'SHUTTER_', indicators, '/home/sdotc/hmi_html/tmp'

; set up indicator plot #2 for Mech/ISS/Motor Curr packet 0x011
;indicators =['HMI_FSW_MC_LAST_RESULT','HMI_FSW_AL1_MOTION_COUNTER','HMI_FSW_AL2_MOTION_COUNTER']
;day_plot_pkt_indicators2, 'yes', '0x11-2_', indicators, '/home/sdotc/hmi_html/tmp'

end
