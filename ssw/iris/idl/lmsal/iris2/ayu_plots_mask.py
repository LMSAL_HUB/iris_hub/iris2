from matplotlib import rc, rcParams
from matplotlib.font_manager import  FontProperties


rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('font',**{'family':'Arial','sans-serif':['Arial']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rc('text.latex', preamble=r'\usepackage{cmbright}')
rc('text', usetex=True)
rc('axes', linewidth=1.5)
rc('lines', linewidth=3)
rc('font', weight='bold')
rc('font', size=30)
rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']




aux = read_sav.read('iris2model_20160114_230409_3630008076_raster_t000_r00000_mymask_01_01.sav')

iris2model = aux['iris2model']
mask_ondata = iris2model['mask_ondata'][0]
mask_extra = iris2model['mask_extra'][0]
mask_extra_data = iris2model['mask_extra_data'][0]
ima_ref = iris2model['ima_ref'][0]
ima_ref_mask = iris2model['ima_ref_mask'][0]
dim = mask_extra.shape
x_scl =  iris2model['header'][0].cdelt3
y_scl =  iris2model['header'][0].cdelt2
extent = [0, dim[1]*x_scl, 0, dim[0]*y_scl]

tvg(mask_extra, extent=extent, labelx_ima='X [arcsec]', labely_ima='Y [arcsec]')

plt.imshow((1.+mask_extra*2)*ima_ref, extent=extent, origin='lower', cmap='afmhot') 
plt.xlabel('X [arcsec]')
plt.ylabel('Y [arcsec]')
plt.show()

aux1 = read_sav.read('./iris2model_20160114_230409_3630008076_raster_t000_r00000_roi_01_02.sav')
iris2model = aux1['iris2model']
mask_ondata_1 = iris2model['mask_ondata'][0]
mask_extra_1 = iris2model['mask_extra'][0]
mask_extra_data_1 = iris2model['mask_extra_data'][0]
ima_ref_1 = iris2model['ima_ref'][0]
ima_ref_mask_1 = iris2model['ima_ref_mask'][0]
dim_1 = mask_extra_1.shape
x_scl =  iris2model['header'][0].cdelt3
y_scl =  iris2model['header'][0].cdelt2
extent_1 = [0, dim_1[1]*x_scl, 0, dim_1[0]*y_scl]




aux2 = read_sav.read('./iris2model_20160114_230409_3630008076_raster_t000_r00000_roi_02_02.sav')
iris2model = aux2['iris2model']
mask_ondata_2 = iris2model['mask_ondata'][0]
mask_extra_2 = iris2model['mask_extra'][0]
mask_extra_data_2 = iris2model['mask_extra_data'][0]
ima_ref_2 = iris2model['ima_ref'][0]
ima_ref_mask_2 = iris2model['ima_ref_mask'][0]
dim_2 = mask_extra_2.shape
x_scl =  iris2model['header'][0].cdelt3
y_scl =  iris2model['header'][0].cdelt2
extent_2 = [0, dim_2[1]*x_scl, 0, dim_2[0]*y_scl]

minima = 0.2
maxima = 1.7
plt.figure(figsize=(11.15, 10.))
plt.imshow(((1.+mask_extra_1*1+mask_extra_2*1)*ima_ref/1e-6).clip(min=minima, max=maxima), extent=extent, origin='lower', cmap='afmhot') 
cbar = plt.colorbar()
cbar.set_label('$Intensity\ [\mu erg\ cm^{-2}\ s^{-1}\ sr^{-1}\ Hz^{-1}]$')
plt.xlabel('X [arcsec]')
plt.ylabel('Y [arcsec]')
plt.tight_layout()
plt.savefig('mask_rois.png')
plt.show()

minima = 0.2
maxima = 1.7
plt.figure(figsize=(11.15, 10.))
plt.imshow(((1.+mask_extra)*ima_ref/1e-6).clip(min=minima, max=maxima), extent=extent, origin='lower', cmap='afmhot') 
cbar = plt.colorbar()
cbar.set_label('$Intensity\ [\mu erg\ cm^{-2}\ s^{-1}\ sr^{-1}\ Hz^{-1}]$')
plt.xlabel('X [arcsec]')
plt.ylabel('Y [arcsec]')
plt.tight_layout()
plt.savefig('mask_plage.png')
plt.show()

mask_part_1 = ima_ref*0.
mask_part_1[:,0:105] =1.
mask_part_1[:,109:212] = 1.
mask_part_1[:,216:] = 1.
plt.figure(figsize=(11.15, 10.))
plt.imshow(((1.+mask_part_1)*ima_ref/1e-6).clip(min=minima, max=maxima), extent=extent, origin='lower', cmap='afmhot', 
            interpolation='none')
cbar = plt.colorbar()
cbar.set_label('$Intensity\ [\mu erg\ cm^{-2}\ s^{-1}\ sr^{-1}\ Hz^{-1}]$')
plt.xlabel('X [arcsec]')
plt.ylabel('Y [arcsec]')
plt.tight_layout()
plt.savefig('mask_parts.png')
plt.show()



