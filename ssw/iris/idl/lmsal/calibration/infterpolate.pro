function INFTERPOLATE, p, x, y, z, $
	nospread = nospread, infval = infval, $
	_extra = extra
	
;+
;
; Wrapper for IDL's INTERPOLATE function that tries to handle Infs 
; 
; Should be a drop-in replacement for INTERPOLATE; only difference is that it
; preserves and propagates Infs (INTERPOLATE turns them into Nans). A little bit
; slower since it checks for Infs at the beginning, but if it doesn't find
; any there should be no additional overhead.
;
; KEYWORDS:
;	nospread- Note that, in the default mode, cubic interpolation turns an
;		isolated Inf pixel into a 4x4 block of Infs. This is strictly correct 
;		(none of the pixels whose interpolated value depends on an Inf has a known
;		finite value), but in some cases you may want to minimize the spread of 
;		the Infs. Set this keyword to use bilinear interpolation on the Infs so
;		that an isolated Inf becomes a 2x2 block instead of 1x1.
;
;	infval	-	If /nospread is set, then a finite value must be used in place of
;		Infs to populate the pixels that are only in the wings of the Infs. By 
;		default, the largest finite pixel value in the array is used; but you
;		can set infval to specify the value to use instead.
;
; MODIFIED:
;	2015-06-01	: Do not turn NaNs into Infs! (PBoerner)
;
;-

infs = WHERE(FINITE(p, /infinity), numinf)

if numinf gt 0 then begin
	; Some Infs present, so handle them
	mask = p
	mask[*] = 0.
	mask[infs] = !values.f_nan
	ptemp = p

	tight = KEYWORD_SET(nospread)
	if tight then begin
		if not KEYWORD_SET(infval) then infval = MAX(p[WHERE(FINITE(p), numfin)])
		ptemp[infs] = infval
		fname = 'BILINEAR'
	endif else begin
		fname = 'INTERPOLATE'
	endelse

	case N_PARAMS() of
		2	:	begin
			infmask = CALL_FUNCTION(fname, mask, x, _extra = extra)
			result = INTERPOLATE(ptemp, x, _extra = extra)			
		end
		3	:	begin
			infmask = CALL_FUNCTION(fname, mask, x, y, _extra = extra)
			result = INTERPOLATE(ptemp, x, y, _extra = extra)			
		end
		4	:	begin
			infmask = CALL_FUNCTION(fname, mask, x, y, z, _extra = extra)
			result = INTERPOLATE(ptemp, x, y, z, _extra = extra)			
		end
		else:	PRINT, 'INFTERPOLATE : Please specify 2-4 parameters'
	endcase
;	infpix = WHERE(infmask ne infmask, numnewinf)
	infpix = WHERE(FINITE(infmask, /infinity), numnewinf)
	if numnewinf gt 0 then result[infpix] = !values.f_infinity	
endif else begin
	; No Infs, so just call interpolate
	case N_PARAMS() of
		2	:	result = INTERPOLATE(p, x, _extra = extra)
		3	:	result = INTERPOLATE(p, x, y, _extra = extra)
		4	:	result = INTERPOLATE(p, x, y, z, _extra = extra)
		else:	PRINT, 'INFTERPOLATE : Please specify 2-4 parameters'
	endcase
endelse

RETURN, result

end
