function sxi_stol2files, date, sea_str, nimg, out=out
;
;+
;NAME:
;	sxi_stol2files
;PURPOSE:
;	Given a stol perf command, find all files associated with that execution
;HISTORY:
;	Written 18-Nov-99 by M.Morrison
;	30-Nov-00 (MDM) - Added DAYS_OUT keyword to sel_images call
;-
;
if (n_elements(date) eq 0) then date = gt_day( ut_time(), /str)
out = sxi_hist_sea(date, 'perform ' + sea_str)
;
if (data_type(out) ne 8) then begin
    print, 'Cannot find any runs of ' + strupcase(sea_str) + ' on ' + gt_day(date, /str)
    return, ''
end
print, 'Found the following times for runs:'
prstr, fmt_tim(out) + '  ' + out.str
;
if (n_elements(out) gt 1) then begin
    imenu = xmenu_sel( out.str, /one)
    if (imenu eq -1) then return, ''
    out = out(imenu)
end
;
ff = sxi_sel_images( out, nimg=nimg, days_out=2 )
return, ff
;
end
