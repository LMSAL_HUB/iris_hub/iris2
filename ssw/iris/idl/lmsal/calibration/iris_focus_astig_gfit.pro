function iris_focus_astig_gfit, imagefiles, darkfiles=darkfiles, xc=xc, yc=yc, npts=npts, lbl=lbl, outfile=outfile,   $
         n2fit=n2fit, magnify=magnify, nwindow=nwindow, vers=vers, histlo=histlo, $ 
         histhi=histhi, cube=cube, apodize=apodize, movie=movie, zbuff=zbuff, madfac=madfac, verbose=verbose

;
;+
;
;   Name: iris_focus_astig_gfit
;
;   Purpose:
;
;   Input Parameters: 
;      imagefiles
;
;   Keyword Parameters: 
;      (Phil Shirts guessed the purpose of many of these.  Most uncertain values were tagged with "(?)" in description.  
;        please update these, as appropriate.
;      darkfiles -
;      xc        - xcenter(?) -- default value: 250
;      yc        - ycenter(?) -- default value: 250
;      npts      - 
;      lbl       - label for plot    -- dafault value: 'IRIS'
;      outfile   - outfile base name -- default value: 'IRIS_focus'
;      n2fit     - number to fit(?)  -- default value: 5
;      magnify   - (?) default value is 0, when keyword set default value is 1
;      nwindow   - (?) default value: 1
;      vers      - version -- initial default value is 'iris_focus_astig_gfit.pro  Vers. 1.1'
;      histlo    - histogram low value(?) -- default value = 0.1
;      histhi    - histogram high value(?) -- default value: 0.9995
;      cube      - write a generated image cube to "$outfile".cube
;      apodize   - (?)
;      movie     - flag to make a movie
;      zbuff     - flag to use zbuffer ('Z' device) for plotting.  Default (when flag not set) is to use 'X' device.
;      madfac    - (?) default value: 20
;      verbose   - turns on some print statements
;
;   Output:
;
;   Calling Examples:
;
;   History:
;      derived from quick_focus_astig.pro, FPP version 1.0
;      26-Mar-2010 - TDT        -
;      08-feb-2016 - P.G.Shirts - 
;
;-
;

;I don't see where nx and ny are used - deprecated?
nx = 1000
ny = 1056

if keyword_set(zbuff) then set_plot,'z' else set_plot,'x'
if keyword_set(magnify) then magnify=1 else magnify=0
if keyword_set(verbose) then verbose=1 else verbose=0
;
if not keyword_set(vers)     then vers = 'iris_focus_astig_gfit.pro  Vers. 1.1'
if not keyword_set(xc)       then xc       = 250
if not keyword_set(yc)       then yc       = 250
if not keyword_set(npts)     then npts     = 400
if not keyword_set(lbl)      then lbl      = 'IRIS'
if not keyword_set(outfile)  then outfile  = 'IRIS_focus'
if not keyword_set(n2fit)    then n2fit    = 5
if not keyword_set(nwindow)  then nwindow  = 1
if not keyword_set(histlo)   then histlo   = 0.1
if not keyword_set(histhi)   then histhi   = 0.9995
if not keyword_set(madfac)   then madfac   = 20.

np2 = npts/2
np4=np2/2

if not keyword_set(apodize)  then apodize=0 else begin
  if (apodize lt 5 or apodize gt np4) then apodize = min([50,np4])
end

nwindow = (nwindow > 1) < 18
wdef,0,1300,900

ff = imagefiles
nf = n_elements(ff)

nfoc = fltarr(nf)
xfoc = fltarr(nf)
yfoc = fltarr(nf)
pfoc = fltarr(nf)
mfoc = fltarr(nf)

if (magnify) then imgcube=fltarr(np2+1,np2+1,nf) else imgcube = fltarr(2*np2+1,2*np2+1,nf)
nimgcube2 = n_elements(imgcube(*,0,0))/2

if (apodize gt 0) then begin
   apod = fltarr(2*np2+1,2*np2+1)
   apod(*,0) = 1.
   cosbell = 0.5*(1.-cos(!pi*(1.+findgen(apodize))/(apodize+1)))
   napod = n_elements(apod(*,0))
   apod(0:apodize-1,0) = cosbell
   apod(napod-apodize,0) = reverse(cosbell)
   for i=1,napod-1 do apod(*,i) = apod(i,0)*apod(*,0)
   apod(*,0) = apod(0,0)*apod(*,0)
end

read_iris,ff,allinfo,/nodata

nsort = sort(allinfo.ifmpos)
nfoc = allinfo(nsort).ifmpos
if (nf gt 4) then nterms=4 else nterms=3

for i=0,nf-1 do begin
  read_iris,ff(nsort(i)),info1,x,/silent
  xx = float(x(xc-np2:xc+np2,yc-np2:yc+np2))
  mxx = mean(xx)
  madxx = mean(abs(xx-mxx))
  ww = where(abs(xx-mxx) gt madfac*madxx,count)
  if (count gt 0) then xx(ww) = xx(ww-1)
  if (apodize gt 0) then xx = xx*apod
  if (magnify) then imgcube(*,*,i)=xx(np4:np4+np2,np4:np4+np2) else $
    imgcube(*,*,i)=xx
  if not keyword_set(zbuff) then tv,bytscl(congrid(imgcube(*,*,i),nimgcube2,nimgcube2)),i
  xfoc(i) = 100.*mean((xx(2:npts,*)-xx(0:npts-2,*))^2)/mxx^2
  yfoc(i) = 100.*mean((xx(*,2:npts)-xx(*,0:npts-2))^2)/mxx^2
  pfoc(i) = 100.*mean((xx(2:npts,2:npts)-xx(0:npts-2,0:npts-2))^2)/mxx^2
  mfoc(i) = 100.*mean((xx(2:npts,0:npts-2)-xx(0:npts-2,2:npts))^2)/mxx^2
end

xsort = reverse(sort(xfoc))
fmax = mean(xfoc(xsort(0:2)))
maxx = median(xsort(0:2))
maxx = (maxx > 1) < (nf-2)
lo = (maxx-n2fit) > 0
hi = (maxx+n2fit) < (nf-1)
xffit = nfoc(lo)+findgen(101)/100.*(nfoc(hi)-nfoc(lo))
guess=fltarr(4)
guess(3)=min(xfoc)
if (nterms eq 4) then guess(0)=fmax-guess(3) else guess(0)=fmax
guess(1)=nfoc(maxx)

if (maxx gt 1 and maxx lt nf-2) then d2=(xfoc(maxx-2)+xfoc(maxx+2)-2.*xfoc(maxx))/(0.5*nfoc(maxx+2)-nfoc(maxx-2))^2 $
    else begin 
      if (maxx eq 1) then d2=(xfoc(maxx-1)+xfoc(maxx+2)-2.*xfoc(maxx))/(0.5*nfoc(maxx+2)-nfoc(maxx-1))^2 $ else
        else  d2=(xfoc(maxx-2)+xfoc(maxx+1)-2.*xfoc(maxx))/(0.5*nfoc(maxx+1)-nfoc(maxx-2))^2
    end

guess(2)=sqrt(abs(guess(0)/d2))
guess(2)=15.
res = poly_fit(nfoc(lo:hi),xfoc(lo:hi),4,yfit=polfit,measure=0.2*xfoc(lo:hi))

wdef,1,600,600
plot,nfoc(lo:hi),xfoc(lo:hi),psym=4,yr=[0.5*min(xfoc(lo:hi)),1.5*max(xfoc(lo:hi))]
oplot,nfoc(lo:hi),polfit
measure = 0.2*abs(xfoc(lo:hi)-polfit)+0.1*xfoc(lo:hi)
res = gaussfit(nfoc(lo:hi),xfoc(lo:hi),ares,estimates=guess(0:nterms-1),nterms=nterms,sigma=sigmaa,measure=measure)
xmax = ares(0)
xfmax = ares(1)
xsig = sigmaa(1)

if (nterms eq 4) then xfit = ares(3)+ares(0)*exp(-0.5*((xffit-ares(1))/ares(2))^2) else $
   xfit = ares(0)*exp(-0.5*((xffit-ares(1))/ares(2))^2)

ysort = reverse(sort(yfoc))
fmax = mean(yfoc(ysort(0:2)))
maxy = median(ysort(0:2))
maxy = (maxy > 1) < (nf-2)
lo = (maxy-n2fit) > 0
hi = (maxy+n2fit) < (nf-1)
yffit = nfoc(lo)+findgen(101)/100.*(nfoc(hi)-nfoc(lo))
guess=fltarr(4)
guess(3)=min(yfoc)
if (nterms eq 4) then guess(0)=fmax-guess(3) else guess(0)=fmax
guess(1)=nfoc(maxy)

;note: pgs doesn't understand the $ else [cr] else construction in the following compound if-statement, or below
if (maxy gt 1 and maxy lt nf-2) then d2=(yfoc(maxy-2)+yfoc(maxy+2)-2.*yfoc(maxy))/(0.5*nfoc(maxy+2)-nfoc(maxy-2))^2 $
    else begin 
         if (maxy eq 1) then d2=(yfoc(maxy-1)+yfoc(maxy+2)-2.*yfoc(maxy))/(0.5*nfoc(maxy+2)-nfoc(maxy-1))^2 $ else
             else  d2=(yfoc(maxy-2)+yfoc(maxy+1)-2.*yfoc(maxy))/(0.5*nfoc(maxy+1)-nfoc(maxy-2))^2
    end

guess(2)=sqrt(abs(guess(0)/d2))
guess(2)=15.
res = poly_fit(nfoc(lo:hi),yfoc(lo:hi),4,yfit=polfit,measure=0.2*(yfoc(lo:hi)))
oplot,nfoc(lo:hi),yfoc(lo:hi),psym=5
oplot,nfoc(lo:hi),polfit,line=1
measure = 0.2*abs(yfoc(lo:hi)-polfit)+0.1*(yfoc(lo:hi))
res = gaussfit(nfoc(lo:hi),yfoc(lo:hi),ares,estimates=guess(0:nterms-1),nterms=nterms,sigma=sigmaa,measure=measure)
ymax = ares(0)
yfmax = ares(1)
ysig = sigmaa(1)

if (nterms eq 4) then yfit = ares(3)+ares(0)*exp(-0.5*((yffit-ares(1))/ares(2))^2) else $
   yfit = ares(0)*exp(-0.5*((yffit-ares(1))/ares(2))^2)

psort = reverse(sort(pfoc))
fmax = mean(yfoc(psort(0:2)))
maxp = median(psort(0:2))
maxp = (maxp > 1) < (nf-2)
lo = (maxp-n2fit) > 0
hi = (maxp+n2fit) < (nf-1)
pffit = nfoc(lo)+findgen(101)/100.*(nfoc(hi)-nfoc(lo))
guess=fltarr(4)
guess(3)=min(pfoc)
if (nterms eq 4) then guess(0)=fmax-guess(3) else guess(0)=fmax
guess(1)=nfoc(maxp)

if (maxp gt 1 and maxp lt nf-2) then d2=(pfoc(maxp-2)+pfoc(maxp+2)-2.*pfoc(maxp))/(0.5*nfoc(maxp+2)-nfoc(maxp-2))^2 $
    else begin 
         if (maxp eq 1) then d2=(pfoc(maxp-1)+pfoc(maxp+2)-2.*pfoc(maxp))/(0.5*nfoc(maxp+2)-nfoc(maxp-1))^2 $ else
             else  d2=(pfoc(maxp-2)+pfoc(maxp+1)-2.*pfoc(maxp))/(0.5*nfoc(maxp+1)-nfoc(maxp-2))^2
    end

guess(2)=sqrt(abs(guess(0)/d2))
guess(2)=15.
res = poly_fit(nfoc(lo:hi),pfoc(lo:hi),4,yfit=polfit,measure=0.2*(pfoc(lo:hi)))
oplot,nfoc(lo:hi),pfoc(lo:hi),psym=2
oplot,nfoc(lo:hi),polfit,line=2
measure = 0.2*abs(pfoc(lo:hi)-polfit)+0.1*(pfoc(lo:hi))
res = gaussfit(nfoc(lo:hi),pfoc(lo:hi),ares,estimates=guess(0:nterms-1),nterms=nterms,sigma=sigmaa,measure=measure)
pmax = ares(0)
pfmax = ares(1)
psig = sigmaa(1)

if (nterms eq 4) then pfit = ares(3)+ares(0)*exp(-0.5*((pffit-ares(1))/ares(2))^2) else $
   pfit = ares(0)*exp(-0.5*((pffit-ares(1))/ares(2))^2)

msort = reverse(sort(mfoc))
fmax = mean(mfoc(msort(0:2)))
maxm = median(msort(0:2))
maxm = (maxm > 1) < (nf-2)
lo = (maxm-n2fit) > 0
hi = (maxm+n2fit) < (nf-1)
mffit = nfoc(lo)+findgen(101)/100.*(nfoc(hi)-nfoc(lo))
guess=fltarr(4)
guess(3)=min(mfoc)

if (nterms eq 4) then guess(0)=fmax-guess(3) else guess(0)=fmax

guess(1)=nfoc(maxm)

if (maxm gt 1 and maxm lt nf-2) then d2=(mfoc(maxm-2)+mfoc(maxm+2)-2.*mfoc(maxm))/(0.5*nfoc(maxm+2)-nfoc(maxm-2))^2 $
    else begin 
         if (maxm eq 1) then d2=(mfoc(maxm-1)+mfoc(maxm+2)-2.*mfoc(maxm))/(0.5*nfoc(maxm+2)-nfoc(maxm-1))^2 $ else
             else  d2=(mfoc(maxm-2)+mfoc(maxm+1)-2.*mfoc(maxm))/(0.5*nfoc(maxm+1)-nfoc(maxm-2))^2
    end

guess(2)=sqrt(abs(guess(0)/d2))
guess(2)=15.
res = poly_fit(nfoc(lo:hi),mfoc(lo:hi),4,yfit=polfit,measure=0.2*(mfoc(lo:hi)))
oplot,nfoc(lo:hi),mfoc(lo:hi),psym=1
oplot,nfoc(lo:hi),polfit,line=3
measure = 0.2*abs(mfoc(lo:hi)-polfit)+0.1*(mfoc(lo:hi))
res = gaussfit(nfoc(lo:hi),mfoc(lo:hi),ares,estimates=guess(0:nterms-1),nterms=nterms,sigma=sigmaa,measure=measure)
mmax = ares(0)
mfmax = ares(1)
msig = sigmaa(1)

if (nterms eq 4) then mfit = ares(3)+ares(0)*exp(-0.5*((mffit-ares(1))/ares(2))^2) else $
mfit = ares(0)*exp(-0.5*((mffit-ares(1))/ares(2))^2)

if (verbose eq 1) then begin
print,'  X,   Y sharpness peak, position, sigma ',xmax,xfmax,xsig,ymax,yfmax,ysig
print,'+45, -45 sharpness peak, position, sigma ',pmax,pfmax,psig,mmax,mfmax,msig
endif

wdef,nwindow+1,1024,1280

now=systime()

!p.multi = [0,3,3]
if (magnify) then  origin = [xc-np2+np4,yc-np2+np4] else origin = [xc-np2,yc-np2]
xtit = 'CCD coordinates (px)'   
fmaxav = (pfmax+mfmax+xfmax+yfmax)/4.
temp = min(abs(nfoc-fmaxav),maxav)
temp = min(abs(nfoc-fmaxav+20),mlo)
temp = min(abs(nfoc-fmaxav-20),mhi)

ytit=allinfo(nsort(mlo)).t_obs
tt = imgcube(*,*,maxav)
htt = histogram(tt,min=1.0)
ntt = total(htt)
tot=htt(0) & i=0 & while (tot lt histlo*ntt) do begin i=i+1 & tot=tot+htt(i) & end  & ttlo=i
tot=htt(0) & i=0 & while (tot lt histhi*ntt) do begin i=i+1 & tot=tot+htt(i) & end  & tthi=i

title='Position '+string(nfoc(mlo),form='(i5)')
plot_image,bytscl(imgcube(*,*,mlo),ttlo,tthi),title=title,xtit=xtit,ytit=ytit,origin=origin,charsize=2.

ytit=allinfo(nsort(maxav)).t_obs
title='Position '+string(nfoc(maxav),form='(i5)')
plot_image,bytscl(imgcube(*,*,maxav),ttlo,tthi),  title=title,xtit=xtit,ytit=ytit,origin=origin,scale=scale,charsize=2.

ytit=allinfo(nsort(mhi)).t_obs
title='Position '+string(nfoc(mhi),form='(i5)')
plot_image,bytscl(imgcube(*,*,mhi),ttlo,tthi),title=title,xtit=xtit,ytit=ytit,origin=origin,scale=scale,charsize=2.

!p.multi=[2,0,3]
!p.charsize=2.
!x.charsize=1.25
!y.charsize=1.25
!z.charsize=1.25

scmax = 1.05*max([xfoc,yfoc,pfoc,mfoc])
scmin = 0.98*min([xfoc,yfoc,pfoc,mfoc])
yrng = [scmin,scmax]
xrng = [min(nfoc)-7,max(nfoc)+7]
xstart = 0.67*xrng(0)+0.33*xrng(1)
ystart = scmin+0.25*(scmax-scmin)
dely = 0.09*(scmax-scmin)
title = lbl+'    '+allinfo(0).img_path+'   Focus Test   '+allinfo(0).t_obs+' to '+allinfo(nf-1).t_obs

plot,nfoc,xfoc,psym=4,/ynoz,ytit='X or +45 Sharpness',tit=title, $ 
     xr=xrng,yr=yrng,xstyle=1,ystyle=1
oplot,xffit,xfit
oplot,nfoc,pfoc,psym=5

oplot,pffit,pfit,line=1
xyouts,xstart,ystart,'Peak Location (sigma)'
xyouts,xstart,ystart-dely,'   X: '+string(xfmax,form='(F5.0)')+'   ('+string(xsig,form='(F4.1)')+')'
xyouts,xstart,ystart-2*dely,'+45: '+string(pfmax,form='(F5.0)')+'   ('+string(psig,form='(F4.1)')+')'
xyouts,0.95*xrng(0)+0.05*xrng(1),0.15*yrng(0)+0.85*yrng(1),allinfo(0).img_path

xtit = vers+'                   '+'Focus Position (steps)'+'                                              '+strmid(now,4,25)
rrr = sqrt((xc-2048.)^2+(yc-2048.)^2)
title = 'XCEN, YCEN, NPTS, R (all px): '+string([xc,yc,npts,rrr],form='(4I5)')+'     '

plot,nfoc,yfoc,psym=4,/ynoz,xtit=xtit,ytit='Y or -45 Sharpness',title=title,subtit=st, $
     xr=xrng,yr=yrng,xstyle=1,ystyl=1
oplot,yffit,yfit
oplot,nfoc,mfoc,psym=5
oplot,mffit,mfit,line=1
xyouts,xstart,ystart,'Peak Location (sigma)'
xyouts,xstart,ystart-dely,'   Y: '+string(yfmax,form='(F5.0)')+'   ('+string(ysig,form='(F4.1)')+')'
xyouts,xstart,ystart-2*dely,'-45: '+string(mfmax,form='(F5.0)')+'   ('+string(msig,form='(F4.1)')+')'
xyouts,0.95*xrng(0)+0.05*xrng(1),0.15*yrng(0)+0.85*yrng(1),allinfo(0).img_path

!p.charsize=1.0
!p.multi=0

tvlct,rct,gct,bct, /get
big = 255b-tvrd()
outfile = outfile+'_'+strmid(allinfo(0).img_path,4) + '_' + $ 
          string(xc,form='(i3.3)')+'_'+string(yc,form='(i3.3)')
write_gif,outfile+'.gif',big,rct,gct,bct

if (verbose eq 1) then begin
  print,'Saved plots in ', outfile+'.gif'
endif

if keyword_set(cube) then lp_write,bytscl(imgcube,ttlo,tthi),outfile+'.cube'

if keyword_set(movie) then begin
  wdef,0,800,800
  if (magnify) then  origin = [xc-np2+np4,yc-np2+np4] else origin = [xc-np2,yc-np2]
  xtit = 'CCD coordinates (px)'   
  for i=0,nf-1 do begin
    ytit=allinfo(nsort(i)).t_obs
    title=allinfo(nsort(i)).img_path+'   Focus'+string(nfoc(i),form='(i5)')
    plot_image,bytscl(imgcube(*,*,i),ttlo,tthi),title=title,xtit=xtit,ytit=ytit,origin=origin,charsize=2.
    big = 255b-tvrd()
    write_gif,outfile+'_mov_'+string(i,form='(i2.2)')+'.gif',big,rct,gct,bct
    if ((i eq 0) AND (verbose eq 1)) then print,'Saved movie images in ',outfile+'_??.gif'
    ; Quicktime at 6 fr/sec works well
  end
end

set_plot,'x'
allfocs=[[xfmax,yfmax,pfmax,mfmax],[xsig,ysig,psig,msig]]
return,allfocs
end
