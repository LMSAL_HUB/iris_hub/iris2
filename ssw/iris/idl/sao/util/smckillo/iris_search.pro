function iris_search, t1, t2, localdir=localdir

; NAME:   iris_search.pro
;       
; PURPOSE:  Works with fid_finder to find the wanted files
;
; CATEGORY: Image Calibration
;
; CALLING SEQUENCE: files = iris_search(t1, t2)
;
; INPUT ARGUMENTS:  
;       t1 = The start time of the files to pull
;		t2 = The end time of the files to pull
;		
;
; INPUT KEYWORD PARAMETER:
;		localdir = Set this to where you want to look for files
;
; OUTPUTS: An array containing:
;		list = An array containing the files found
;
; MODIFICATION HISTORY:
;	2013/08/26 Written by Sean McKillop (smckillop@cfa.harvard.edu)
; Generate the slice fits. 

if not keyword_set(localdir) then localdir = '/data/IRIS/level0/'

ts = [anytim(t1), anytim(t2)] ;times in seconds
if ts[1] le ts[0] then begin
	box_message, 'Oops! t1 input is later than t2.'
	return, -1
endif
n_hrs = ceil((ts[1] - ts[0]) / 60 / 60) ;number of hours in range
hrs = anytim(ts[0] + indgen(n_hrs+1)*3600D, /ecs) ;array of full dates for each hour
hr_paths = strmid(hrs,0,10)+'/'+'H'+strmid(hrs,11,2)+'00/' ;filepaths to each hour

list = ''
for j=0., n_elements(hr_paths)-1 do list = [list,file_search(localdir+hr_paths[j], '*.fits')]
found = where(list ne '', count)

if count eq 0 then begin
	box_message, 'No files found'
	return, -1
endif

list = list[found] ;remove blank strings

;use filenames to sort out times within the hours:
bases = file_basename(list)
times = anytim(strmid(bases,4,4)+'/'+strmid(bases,8,2)+'/'+strmid(bases,10,2)+' '+strmid(bases,13,2)+':'+strmid(bases,15,2)+':'+strmid(bases,17,2))
want = where(times ge ts[0] AND times le ts[1], count2)

if count2 eq 0 then begin
	box_message, 'No files found'
	return, -1
endif

list = list[want]

return, list
end
